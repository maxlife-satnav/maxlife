Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class Controls_HRAddLease
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim maintper As Decimal = 0
    Dim SERVPER As Decimal = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        txtsdate.Attributes.Add("readonly", "readonly")
        txtedate.Attributes.Add("readonly", "readonly")
        txtescfromdate2.Attributes.Add("readonly", "readonly")
        txtesctodate2.Attributes.Add("readonly", "readonly")
        txtEscalationDate.Attributes.Add("readonly", "readonly")
        txtesctodate1.Attributes.Add("readonly", "readonly")
        txttenure.Attributes.Add("readonly", "readonly")
        txtAgreedate.Attributes.Add("readonly", "readonly")
        txtagreeregdate.Attributes.Add("readonly", "readonly")
        

        If Not IsPostBack Then
            step1heading.Visible = True
            step2heading.Visible = False
            step3heading.Visible = False
            ClearTextBox(Me)
            txtldemail.Text = ""
            txtescfromdate2.Text = ""
            txtesctodate2.Text = ""
            'txtEmail1.Text = ""
            txtInvestedArea.Text = 0
            txtpay.Text = 0
            txtOccupiedArea.Text = 0

            txtagreeamt.Text = 0
            txtregamt.Text = 0
            txtnotice.Text = 0
            txtlock.Text = 0
            txtmob.Text = 0
            txtpmonthrent.Text = 0
            txtpsecdep.Text = 0
            txtEmpRcryAmt.Text = ""
            ddlStatus.SelectedItem.Value = "1"
            ddlStatus.Enabled = False
            ddlCity.Enabled = False

            txtld2email.Text = ""
            txtld2mob.Text = 0
            txtld2rent.Text = 0
            txtld2sd.Text = 0

            txtld3email.Text = ""
            txtld3mob.Text = 0
            txtld3rent.Text = 0
            txtld3sd.Text = 0
            txtbrkamount.Text = 0

            BindPropertyType()
            BindProperty()
            BindCity()
            BindEmpCity()

            BindLandlordstate()
            BindGrid()
            BindLeaseType()
            BindLesse()

            ddlLesse.Enabled = False
            lblMsg.Text = ""

            ddlMode.SelectedValue = "2"
            ddlMode.Enabled = False
            ddlproptype.SelectedValue = "1"
            ddlproptype.Enabled = False
            ddlLeaseType.SelectedValue = "2"
            ddlLeaseType.Enabled = False

            panrecwiz.Visible = False
            panPOA.Visible = False
            panbrk.Visible = False
            panwiz3.Visible = False
            panld1.Visible = False
            pnlld2.Visible = False
            pnlld3.Visible = False

            btn1Next.Visible = True
            btn2prev.Visible = False
            btn2Next.Visible = False
            btn3Prev.Visible = False
            btn3Finish.Visible = False
            txtrcryfromdate.Text = ""
            txtrcrytodate.Text = ""
            txtEmpRcryAmt.ReadOnly = True
            txtentitle.ReadOnly = True
            ddlesctype.SelectedValue = "FLT"
            ddlesctype.Enabled = False
            panbrk.Visible = False
            txtbrkremail.Text = ""
            txtbrkamount.Text = 0
            txtbrkmob.Text = 0
            txtproptax.Text = 0
            GetEntitle()
            trregagree.Visible = False
            pnlesc1.Visible = False
            pnlesc2.Visible = False
            panel1.Visible = False
            panel2.Visible = False
            panel3.Visible = False
            panel4.Visible = False
            panel5.Visible = False
            panel6.Visible = False
            txtEmpAccNo.ReadOnly = True
            txtEmpBankName.ReadOnly = True
            txtEmpBranch.ReadOnly = True
            txtrcryfromdate.ReadOnly = True
            txtrcrytodate.ReadOnly = True
            panafteresc1.Visible = False
            panafteresc2.Visible = False
            txtrcramt1.ReadOnly = True
            txtrcramt2.ReadOnly = True
            txtrcrfrmdate1.ReadOnly = True
            txtrcrfrmdate2.ReadOnly = True
            txtrcrtodate1.ReadOnly = True
            txtrcrtodate2.ReadOnly = True
            panelL12.Visible = False
            pnll22.Visible = False
            pnll32.Visible = False
            ddlCity.Enabled = False

            GetLeaseName()
            BindRecoveryDetails()

            txtsdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
            txtedate.Text = DateTime.Now.AddYears(1).ToString("MM/dd/yyyy")
            Dim dt As DateTime = Convert.ToDateTime(txtsdate.Text)
            txtEscalationDate.Text = dt.AddYears(1).ToString("MM/dd/yyyy")
            Dim dt1 As DateTime = Convert.ToDateTime(txtEscalationDate.Text)
            txtesctodate1.Text = dt1.AddYears(1).ToString("MM/dd/yyyy")
            txtagreeregdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
            txtAgreedate.Text = DateTime.Now.ToString("MM/dd/yyyy")
        End If
        txtsdate.Attributes.Add("readonly", "readonly")
        txtedate.Attributes.Add("readonly", "readonly")
        txtEscalationDate.Attributes.Add("readonly", "readonly")
        txtesctodate1.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub bindpropertytax()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROEPRTY_TAX")
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            maintper = ds.Tables(0).Rows(0).Item("MAINTENANCE_CHARGE")
            SERVPER = ds.Tables(0).Rows(0).Item("SERVICE_TAX")
        End If
    End Sub

    Private Sub BindProperty()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_PROPERTY_LEASE_ADD")
        sp.Command.AddParameter("@dummy", 0, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        ddlproperty.DataSource = sp.GetDataSet()
        ddlproperty.DataTextField = "PN_NAME"
        ddlproperty.DataValueField = "BDG_ID"
        ddlproperty.DataBind()
        ddlproperty.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    Private Sub BindEmpCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMP_CITY")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_CITY")).Selected = True
        End If
    End Sub
    Private Sub BindRecoveryDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"LEASE_EMPLOYEE_ACCOUNT_DETAILS")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim DS As New DataSet()
            DS = sp.GetDataSet()
            If DS.Tables(0).Rows.Count > 0 Then
                txtEmpAccNo.Text = DS.Tables(0).Rows(0).Item("AUR_ACCOUNT_NUMBER")
                txtEmpBankName.Text = DS.Tables(0).Rows(0).Item("AUR_BANK_NAME")
                txtEmpBranch.Text = DS.Tables(0).Rows(0).Item("AUR_BRANCH_NAME")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GET_EMPDETAILS_RMAPPROVAL")
        sp.Command.AddParameter("@LESSE_ID", Session("UID"), DbType.String)
        gvEmpDetails.DataSource = sp.GetDataSet()
        gvEmpDetails.DataBind()
    End Sub
    Public Sub ClearTextBox(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearTextBox(ctrl)
            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Text = ""
            End If
        Next ctrl
    End Sub
    Private Function ValidBroker1()
        Dim validBroker As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALID_BROKER_LABEL")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        validBroker = sp.ExecuteScalar()
        Return validBroker
    End Function
    Private Sub GetLeaseName()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETLEASENAME")
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        Dim sno As String = ds.Tables(0).Rows(0).Item("SNO")
        txtstore.Text = ddlLesse.SelectedItem.Text + "/" + sno
    End Sub
    Private Sub GetEntitle()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETENTITLE_USER")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtentitle.Text = ds.Tables(0).Rows(0).Item("ENTITLEMENT")
                lblmaxrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
                lblmaxsd.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
                'txtstore1.Text = ds.Tables(0).Rows(0).Item("RECOVERY")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITY_ALL")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@USR_ID", Session("UID"), DbType.Int32)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select City--", "--Select City--"))
    End Sub
    Private Sub BindPropertyType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("UID"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLesse()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_LESSE_DOC")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlLesse.DataSource = sp.GetDataSet()
        ddlLesse.DataTextField = "AUR_FIRST_NAME"
        ddlLesse.DataValueField = "AUR_ID"
        ddlLesse.DataBind()
        ddlLesse.Items.FindByValue(Session("UID")).Selected = True
        ddlLesse.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindLeaseType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVE_LEASETYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlLeaseType.DataSource = sp3.GetDataSet()
        ddlLeaseType.DataTextField = "PN_LEASE_TYPE"
        ddlLeaseType.DataValueField = "PN_LEASE_ID"
        ddlLeaseType.DataBind()
        ddlLeaseType.Items.Insert(0, New ListItem("--Select LeaseType--", "0"))
    End Sub
    Protected Sub ddlagreeres_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlagreeres.SelectedIndexChanged
        If ddlagreeres.SelectedValue = "Yes" Then
            trregagree.Visible = True
        Else
            trregagree.Visible = False
        End If
    End Sub
    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpaymentmode.SelectedIndexChanged
        If ddlpaymentmode.SelectedIndex > 0 Then
            If ddlpaymentmode.SelectedItem.Value = "1" Then
                panel1.Visible = True
                panel2.Visible = False
                panelL12.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "2" Then
                panel1.Visible = False
                panel2.Visible = False
                panelL12.Visible = True
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
                panelL12.Visible = False
            End If
        End If
    End Sub
    Private Sub Landlord1Add()
        Try
            lblMsg.Text = ""
            Dim dec_ld2rent As Decimal = 0
            Dim dec_ld3rent As Decimal = 0
            Dim dec_ld2sd As Decimal = 0
            Dim dec_ld3sd As Decimal = 0
            If txtld2rent.Text <> "" Then
                dec_ld2rent = CDbl(txtld2rent.Text)
            End If
            If txtld3rent.Text <> "" Then
                dec_ld3rent = CDbl(txtld3rent.Text)
            End If
            If txtld2sd.Text <> "" Then
                dec_ld2sd = CDbl(txtld2sd.Text)
            End If
            If txtld3sd.Text <> "" Then
                dec_ld3sd = CDbl(txtld3sd.Text)
            End If
            'Dim validatelease As Integer
            'validatelease = ValidateleaseL()
            'If validatelease = 0 Then
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord1 Details already there for this Lease!');", True)
            'ElseIf CDbl(txtpmonthrent.Text) > CDbl(CDbl(txtInvestedArea.Text) - (CDbl(dec_ld2rent) + CDbl(dec_ld3rent))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Month Rent exceeded!');", True)
            'ElseIf CDbl(txtpsecdep.Text) > CDbl(CDbl(txtpay.Text) - (CDbl(dec_ld2sd) + CDbl(dec_ld3sd))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Security Deposit exceeded!');", True)
            ' Else

            AddLandLord1()
            Landlord1History()
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('LandLord1 Details Added Succesfully!');", True)
            ' End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlld2mode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlld2mode.SelectedIndexChanged
        If ddlld2mode.SelectedIndex > 0 Then
            If ddlld2mode.SelectedItem.Value = "1" Then
                panel3.Visible = False
                panel4.Visible = False
                pnll22.Visible = False
            ElseIf ddlld2mode.SelectedItem.Value = "2" Then
                pnll22.Visible = True
                panel3.Visible = False
                panel4.Visible = False
            ElseIf ddlld2mode.SelectedItem.Value = "3" Then
                panel3.Visible = False
                panel4.Visible = True
                pnll22.Visible = False
            End If
        End If
    End Sub
    Private Sub Landlord2Add()
        Try
            lblMsg.Text = ""
            Dim dec_ld1rent As Decimal = 0
            Dim dec_ld3rent As Decimal = 0
            Dim dec_ld1sd As Decimal = 0
            Dim dec_ld3sd As Decimal = 0

            Dim dec_ld2sd As Decimal = 0
            Dim dec_ld2rent As Decimal = 0

            If txtld2sd.Text <> "" Then
                dec_ld2sd = CDbl(txtld2sd.Text)
            End If

            If txtld2rent.Text <> "" Then
                dec_ld2rent = CDbl(txtld2rent.Text)
            End If

            If txtpmonthrent.Text <> "" Then
                dec_ld1rent = CDbl(txtpmonthrent.Text)
            End If
            If txtld3rent.Text <> "" Then
                dec_ld3rent = CDbl(txtld3rent.Text)
            End If
            If txtpsecdep.Text <> "" Then
                dec_ld1sd = CDbl(txtpsecdep.Text)
            End If
            If txtld3sd.Text <> "" Then
                dec_ld3sd = CDbl(txtld3sd.Text)
            End If
            'Dim validateLandLord2 As Integer
            'validateLandLord2 = ValidateLeaseL2()
            'If validateLandLord2 = 0 Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord2 Details already there for this Lease');", True)
            'ElseIf dec_ld2rent > CDbl(CDbl(txtInvestedArea.Text) - (CDbl(dec_ld1rent) + CDbl(dec_ld3rent))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Month Rent exceeded!');", True)
            'ElseIf dec_ld2sd > CDbl(CDbl(txtpay.Text) - (CDbl(dec_ld1sd) + CDbl(dec_ld3sd))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Security Deposit exceeded!');", True)
            'ElseIf txtld1name.Text <> "" And txtld2sd.Text <> "" And txtld2rent.Text <> "" And txtld2frmdate.Text <> "" And txtld2todate.Text <> "" Then
            '    lblMsg.Text = ""
            AddLandLord2()
            Landlord2History()
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('LandLord2 Details Added Succesfully!');", True)
            'End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlld3mode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlld3mode.SelectedIndexChanged
        If ddlld3mode.SelectedIndex > 0 Then
            If ddlld3mode.SelectedItem.Value = "1" Then
                panel5.Visible = False
                panel6.Visible = False
                pnll32.Visible = False
            ElseIf ddlld3mode.SelectedItem.Value = "2" Then
                pnll32.Visible = True
                panel3.Visible = False
                panel4.Visible = False
            ElseIf ddlld3mode.SelectedItem.Value = "3" Then
                panel5.Visible = False
                panel6.Visible = True
                pnll32.Visible = False
            End If
        End If
    End Sub
    Private Sub Landlord3Add()
        Try
            lblMsg.Text = ""
            Dim dec_ld1rent As Decimal = 0
            Dim dec_ld2rent As Decimal = 0
            Dim dec_ld3rent As Decimal = 0
            Dim dec_ld1sd As Decimal = 0
            Dim dec_ld2sd As Decimal = 0
            Dim dec_ld3sd As Decimal = 0

            If txtld3rent.Text <> "" Then
                dec_ld3rent = CDbl(txtld3rent.Text)
            End If

            If txtld3sd.Text <> "" Then
                dec_ld3sd = CDbl(txtld3sd.Text)
            End If


            If txtpmonthrent.Text <> "" Then
                dec_ld1rent = CDbl(txtpmonthrent.Text)
            End If

            If txtld2rent.Text <> "" Then
                dec_ld2rent = CDbl(txtld2rent.Text)
            End If
            If txtpsecdep.Text <> "" Then
                dec_ld1sd = CDbl(txtpsecdep.Text)
            End If
            If txtld2sd.Text <> "" Then
                dec_ld2sd = CDbl(txtld2sd.Text)
            End If
            'Dim validateLandlord3 As Integer
            'validateLandlord3 = ValidateLeaseL3()
            'If validateLandlord3 = 0 Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord3 Details already there for this Lease');", True)
            'ElseIf dec_ld3rent > CDbl(CDbl(txtInvestedArea.Text) - (CDbl(dec_ld1rent) + CDbl(dec_ld2rent))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Month Rent Exceeded');", True)
            'ElseIf dec_ld3sd > CDbl(CDbl(txtpay.Text) - (CDbl(dec_ld1sd) + CDbl(dec_ld2sd))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Security Deposit Exceeded');", True)
            'ElseIf txtld1name.Text <> "" And txtld3sd.Text <> "" And txtld3rent.Text <> "" And txtld3fromdate.Text <> "" And txtld3todate.Text <> "" Then
            '    lblMsg.Text = ""
            AddLandLord3()
            Landlord3History()
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord3 Details Added Succesfully');", True)
            '    End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Landlord1History()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_LANDLORD_HISTORY")
        sp.Command.AddParameter("LEASE_NAME", txtstore.Text, DbType.String)
        sp.Command.AddParameter("LANDLORD_NAME", txtldname.Text, DbType.String)
        sp.Command.AddParameter("FROM_DATE", txtpfromdate.Text, DbType.Date)
        sp.Command.AddParameter("TO_DATE", txtptodate.Text, DbType.Date)
        sp.Command.AddParameter("LEASE_RENT", txtpmonthrent.Text, DbType.Decimal)
        sp.Command.AddParameter("LANDLORD_NUMBER", "LANDLORD1", DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub Landlord2History()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_LANDLORD_HISTORY")
        sp.Command.AddParameter("LEASE_NAME", txtstore.Text, DbType.String)
        sp.Command.AddParameter("LANDLORD_NAME", txtld1name.Text, DbType.String)
        sp.Command.AddParameter("FROM_DATE", txtld2frmdate.Text, DbType.Date)
        sp.Command.AddParameter("TO_DATE", txtld2todate.Text, DbType.Date)
        sp.Command.AddParameter("LEASE_RENT", txtld2rent.Text, DbType.Decimal)
        sp.Command.AddParameter("LANDLORD_NUMBER", "LANDLORD2", DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub Landlord3History()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_LANDLORD_HISTORY")
        sp.Command.AddParameter("LEASE_NAME", txtstore.Text, DbType.String)
        sp.Command.AddParameter("LANDLORD_NAME", txtld3name.Text, DbType.String)
        sp.Command.AddParameter("FROM_DATE", txtld3fromdate.Text, DbType.Date)
        sp.Command.AddParameter("TO_DATE", txtld3todate.Text, DbType.Date)
        sp.Command.AddParameter("LEASE_RENT", txtld3rent.Text, DbType.Decimal)
        sp.Command.AddParameter("LANDLORD_NUMBER", "LANDLORD3", DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub AddBrokerageDetails()
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@LEASE_NAME", SqlDbType.NVarChar, 200)
        param(0).Value = txtstore.Text
        param(1) = New SqlParameter("@AMOUNT_OF_BROKERAGE_PAID", SqlDbType.Float)
        If txtbrkamount.Text = "" Then
            txtbrkamount.Text = 0
            param(1).Value = 0
        Else
            param(1).Value = txtbrkamount.Text
        End If

        param(2) = New SqlParameter("@BROKER_NAME", SqlDbType.NVarChar, 200)
        If txtbrkname.Text = "" Then
            param(2).Value = ""
        Else
            param(2).Value = txtbrkname.Text
        End If

        param(3) = New SqlParameter("@BROKER_ADDRESS", SqlDbType.NVarChar, 1000)
        If txtbrkaddr.Text = "" Then
            param(3).Value = ""
        Else
            param(3).Value = txtbrkaddr.Text
        End If

        param(4) = New SqlParameter("@PAN_NO", SqlDbType.NVarChar, 50)
        If txtbrkpan.Text = "" Then
            param(4).Value = ""
        Else
            param(4).Value = txtbrkpan.Text
        End If

        param(5) = New SqlParameter("@EMAIL", SqlDbType.NVarChar, 50)
        If txtbrkremail.Text = "" Then
            param(5).Value = ""
        Else
            param(5).Value = txtbrkremail.Text
        End If

        param(6) = New SqlParameter("@MOBILE_NO", SqlDbType.NVarChar, 15)
        If txtbrkmob.Text = "" Then
            txtbrkmob.Text = 0
            param(6).Value = 0
        Else
            param(6).Value = txtbrkmob.Text
        End If

        ObjSubSonic.GetSubSonicExecute("INSERT_BROKERAGE_DETAILS", param)
    End Sub
    Private Sub AddAgreementDetails()
        Dim param(14) As SqlParameter
        param(0) = New SqlParameter("@LEASE_NAME", SqlDbType.NVarChar, 200)
        param(0).Value = txtstore.Text
        param(1) = New SqlParameter("@AGREEMENT_EXECUTION_DATE", SqlDbType.DateTime)
        If txtAgreedate.Text = "" Then
            param(1).Value = DBNull.Value
        Else
            param(1).Value = txtAgreedate.Text
        End If

        param(2) = New SqlParameter("@AMOUNT_OF_STAMP_DUTY_PAID", SqlDbType.Float)

        If txtagreeamt.Text = "" Then
            txtagreeamt.Text = 0
            param(2).Value = 0
        Else
            param(2).Value = txtagreeamt.Text
        End If

        param(3) = New SqlParameter("@REGISTRATION_AMOUNT", SqlDbType.Float)
        If txtregamt.Text = "" Then
            txtregamt.Text = 0
            param(3).Value = 0
        Else
            param(3).Value = txtregamt.Text
        End If

        param(4) = New SqlParameter("@EFFECTIVE_AGREEMENT_DATE", SqlDbType.DateTime)
        If txtsdate.Text = "" Then
            param(4).Value = DBNull.Value
        Else
            param(4).Value = txtsdate.Text
        End If

        param(5) = New SqlParameter("@EXPIRY_AGREEMENT_DATE", SqlDbType.DateTime)
        If txtedate.Text = "" Then
            param(5).Value = DBNull.Value
        Else
            param(5).Value = txtedate.Text
        End If

        param(6) = New SqlParameter("@AGREEMENT_REGISTERED", SqlDbType.NVarChar, 200)

        param(6).Value = ddlagreeres.SelectedItem.Value
        param(7) = New SqlParameter("@REGISTRATION_DATE", SqlDbType.DateTime)
        If txtagreeregdate.Text = "" Then
            param(7).Value = DBNull.Value
        Else
            param(7).Value = txtagreeregdate.Text
        End If

        param(8) = New SqlParameter("@SUB_REGISTARS_OFFICE_NAME", SqlDbType.NVarChar, 200)
        If txtagreesub.Text = "" Then
            param(8).Value = ""
        Else
            param(8).Value = txtagreesub.Text
        End If

        param(9) = New SqlParameter("@TERMINATION_NOTICE", SqlDbType.NVarChar, 200)
        If txtnotice.Text = "" Then
            param(9).Value = ""
        Else
            param(9).Value = txtnotice.Text
        End If

        param(10) = New SqlParameter("@LOCKIN_PERIOD_OF_AGREEMENT", SqlDbType.NVarChar, 200)
        If txtlock.Text = "" Then
            param(10).Value = ""
        Else
            param(10).Value = txtlock.Text
        End If

        param(11) = New SqlParameter("@POA_NAME", SqlDbType.NVarChar, 200)
        param(11).Value = txtPOAName.Text
        param(12) = New SqlParameter("@POA_MOBILE", SqlDbType.NVarChar, 200)
        param(12).Value = txtPOAMobile.Text
        param(13) = New SqlParameter("@POA_EMAIL", SqlDbType.NVarChar, 200)
        param(13).Value = txtPOAEmail.Text
        param(14) = New SqlParameter("@POA_ADDRESS", SqlDbType.NVarChar, 1000)
        param(14).Value = txtPOAAddress.Text
        ObjSubSonic.GetSubSonicExecute("INSERT_LEASE_AGREEMENT_DETAILS", param)
    End Sub
    Private Sub AddEscalation()

        Dim decrcryAmt As Decimal = 0
        If txtEmpRcryAmt.Text <> "" Then
            decrcryAmt = txtEmpRcryAmt.Text
        End If
        Dim param(12) As SqlParameter
        param(0) = New SqlParameter("@LEASE_NAME", SqlDbType.NVarChar, 200)
        param(0).Value = txtstore.Text
        If ddlesc.SelectedValue = "Yes" Then
            param(1) = New SqlParameter("@LEASE_ESCALATED_FROM_DATE1", SqlDbType.DateTime)
            param(1).Value = txtEscalationDate.Text
            param(2) = New SqlParameter("@LEASE_ESCALATED_TO_DATE1", SqlDbType.DateTime)
            param(2).Value = txtesctodate1.Text
            param(3) = New SqlParameter("@LEASE_ESCALATED_FROM_DATE2", SqlDbType.DateTime)
            If txtescfromdate2.Text = "" Then
                param(3).Value = DBNull.Value
            Else
                param(3).Value = txtescfromdate2.Text
            End If
            param(4) = New SqlParameter("@LEASE_ESCALATED_TO_DATE2", SqlDbType.DateTime)
            If txtesctodate2.Text = "" Then
                param(4).Value = DBNull.Value
            Else
                param(4).Value = txtesctodate2.Text
            End If
            param(5) = New SqlParameter("@LEASE_ESCALATED_AMOUNT1", SqlDbType.Float)
            param(5).Value = txtfirstesc.Text
            param(6) = New SqlParameter("@LEASE_ESCALATED_AMOUNT2", SqlDbType.Float)
            param(6).Value = txtsecondesc.Text
        Else
            param(1) = New SqlParameter("@LEASE_ESCALATED_FROM_DATE1", SqlDbType.DateTime)
            param(1).Value = DBNull.Value
            param(2) = New SqlParameter("@LEASE_ESCALATED_TO_DATE1", SqlDbType.DateTime)
            param(2).Value = DBNull.Value
            param(3) = New SqlParameter("@LEASE_ESCALATED_FROM_DATE2", SqlDbType.DateTime)
            param(3).Value = DBNull.Value
            param(4) = New SqlParameter("@LEASE_ESCALATED_TO_DATE2", SqlDbType.DateTime)
            param(4).Value = DBNull.Value
            param(5) = New SqlParameter("@LEASE_ESCALATED_AMOUNT1", SqlDbType.Float)
            param(5).Value = 0
            param(6) = New SqlParameter("@LEASE_ESCALATED_AMOUNT2", SqlDbType.Float)
            param(6).Value = 0
        End If
        If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) Then
            param(7) = New SqlParameter("@RECOVERY_FROMDATE", SqlDbType.DateTime)
            param(7).Value = txtrcryfromdate.Text
            param(8) = New SqlParameter("@RECOVERY_TODATE", SqlDbType.DateTime)
            param(8).Value = txtrcrytodate.Text
            param(9) = New SqlParameter("@RECOVERY_AMOUNT", SqlDbType.Float)
            param(9).Value = decrcryAmt
            param(10) = New SqlParameter("@EMPLOYEE_ACCOUNT_NUMBER", SqlDbType.NVarChar)
            param(10).Value = txtEmpAccNo.Text
            param(11) = New SqlParameter("@EMPLOYEE_BANK_NAME", SqlDbType.NVarChar)
            param(11).Value = txtEmpBankName.Text
            param(12) = New SqlParameter("@EMPLOYEE_BRANCH_NAME", SqlDbType.NVarChar)
            param(12).Value = txtEmpBranch.Text
        Else
            param(7) = New SqlParameter("@RECOVERY_FROMDATE", SqlDbType.DateTime)
            param(7).Value = DBNull.Value
            param(8) = New SqlParameter("@RECOVERY_TODATE", SqlDbType.DateTime)
            param(8).Value = DBNull.Value
            param(9) = New SqlParameter("@RECOVERY_AMOUNT", SqlDbType.Float)
            param(9).Value = decrcryAmt
            param(10) = New SqlParameter("@EMPLOYEE_ACCOUNT_NUMBER", SqlDbType.NVarChar)
            param(10).Value = ""
            param(11) = New SqlParameter("@EMPLOYEE_BANK_NAME", SqlDbType.NVarChar)
            param(11).Value = ""
            param(12) = New SqlParameter("@EMPLOYEE_BRANCH_NAME", SqlDbType.NVarChar)
            param(12).Value = ""

        End If
        ObjSubSonic.GetSubSonicExecute("ADD_ESCALATION_DETAILS", param)
    End Sub
    Private Sub AddLease()
        Try

            Dim dec_Flat As Decimal = 0
            Dim dec_Percent As Decimal = 0
            Dim Builtup As Decimal = 0
            If txtOccupiedArea.Text <> "" Then
                Builtup = CDbl(txtOccupiedArea.Text)
            End If

            Dim param(41) As SqlParameter
            param(0) = New SqlParameter("@LEASE_NAME", SqlDbType.NVarChar, 200)
            param(1) = New SqlParameter("@PROPERTY_TYPE", SqlDbType.NVarChar, 200)
            param(2) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
            param(3) = New SqlParameter("@PROPERTY_ADDRESS1", SqlDbType.NVarChar, 200)
            param(4) = New SqlParameter("@PROPERTY_ADDRESS2", SqlDbType.NVarChar, 200)
            param(5) = New SqlParameter("@PROPERTY_ADDRESS3", SqlDbType.NVarChar, 200)
            param(6) = New SqlParameter("@CTS_NUMBER", SqlDbType.NVarChar, 4000)
            param(7) = New SqlParameter("@LEASE_TYPE", SqlDbType.NVarChar, 4000)
            param(8) = New SqlParameter("@LESSE", SqlDbType.NVarChar, 200)
            param(9) = New SqlParameter("@lease_escalation", SqlDbType.NVarChar, 200)
            param(10) = New SqlParameter("@ESCALATED_EMAIL", SqlDbType.NVarChar, 200)
            param(11) = New SqlParameter("@LEASE_RENT", SqlDbType.Decimal)
            param(12) = New SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal)
            param(13) = New SqlParameter("@LEASE_STATUS", SqlDbType.Int)
            param(14) = New SqlParameter("@SECURITY_DEPOSIT", SqlDbType.Decimal)
            param(15) = New SqlParameter("@LEASE_ESC_TYPE", SqlDbType.NVarChar, 200)
            param(16) = New SqlParameter("@PERCENTAGE", SqlDbType.Decimal)
            param(17) = New SqlParameter("@FLAT_AMOUNT", SqlDbType.Decimal)
            param(18) = New SqlParameter("@ENTITLE_LEASE_AMOUNT", SqlDbType.Decimal)
            param(19) = New SqlParameter("@LEASE_COST_PER", SqlDbType.NVarChar, 200)
            param(20) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
            param(21) = New SqlParameter("@LEASE_COMMENTS", SqlDbType.NVarChar, 200)
            param(22) = New SqlParameter("@PIN_CODE", SqlDbType.NVarChar, 200)
            param(23) = New SqlParameter("@LANDLORDS", SqlDbType.NVarChar, 200)
            param(24) = New SqlParameter("@POA", SqlDbType.NVarChar, 200)

            param(25) = New SqlParameter("@COMPLETE_ADDRESS", SqlDbType.NVarChar, 200)
            param(26) = New SqlParameter("@STATE", SqlDbType.NVarChar, 200)
            param(27) = New SqlParameter("@REGION", SqlDbType.NVarChar, 200)
            param(28) = New SqlParameter("@STAMP_DUTY", SqlDbType.Decimal)
            param(29) = New SqlParameter("@REGISTRATION_CHARGES", SqlDbType.Decimal)
            param(30) = New SqlParameter("@PROFESSIONAL_FEES", SqlDbType.Decimal)
            param(31) = New SqlParameter("@BROKERAGE_AMOUNT", SqlDbType.Decimal)
            param(32) = New SqlParameter("@BASIC_RENT", SqlDbType.Decimal)
            param(33) = New SqlParameter("@MAINTENANCE_CHARGES", SqlDbType.Decimal)
            param(34) = New SqlParameter("@DG_BACKUP_CHARGES", SqlDbType.Decimal)
            param(35) = New SqlParameter("@SERVICE_TAX", SqlDbType.Decimal)
            param(36) = New SqlParameter("@PROPERTY_TAX", SqlDbType.Decimal)
            param(37) = New SqlParameter("@LEASEHOLDIMPROVEMENTS", SqlDbType.NVarChar, 200)
            param(38) = New SqlParameter("@RENT_REVISION", SqlDbType.Decimal)
            param(39) = New SqlParameter("@TENURE_AGREEMENT", SqlDbType.Date)
            param(40) = New SqlParameter("@FURNITURE", SqlDbType.NVarChar, 200)
            param(41) = New SqlParameter("@OFFCIE_EQUIPMENTS", SqlDbType.NVarChar, 200)


            param(0).Value = txtstore.Text
            param(1).Value = ddlproptype.SelectedItem.Value
            param(2).Value = ddlCity.SelectedItem.Value
            param(3).Value = ddlproperty.SelectedItem.Value
            param(4).Value = txtBuilding.Text
            param(5).Value = txtprop3.Text
            param(6).Value = txtLnumber.Text
            param(7).Value = ddlLeaseType.SelectedItem.Value
            param(8).Value = Session("UID")
            param(9).Value = ddlesc.SelectedItem.Value
            param(10).Value = ""
            param(11).Value = txtInvestedArea.Text
            param(12).Value = Builtup
            param(13).Value = ddlStatus.SelectedItem.Value
            param(14).Value = txtpay.Text
            param(15).Value = ddlesctype.SelectedItem.Value
            param(16).Value = dec_Percent
            param(17).Value = dec_Flat
            param(18).Value = txtentitle.Text
            param(19).Value = ddlMode.SelectedItem.Value
            param(20).Value = Session("UID")
            param(21).Value = txtComments.Text
            param(22).Value = txtpincode.Text
            param(23).Value = ddlleaseld.SelectedItem.Value
            param(24).Value = ddlpoa.SelectedItem.Value




            param(25).Value = txtBuilding.Text
            param(26).Value = txtprop3.Text
            param(27).Value = txtregion.Text
            param(28).Value = txtsduty.Text
            param(29).Value = txtregcharges.Text
            param(30).Value = txtpfees.Text
            param(31).Value = txtbrokerage.Text
            param(32).Value = txtbasic.Text
            param(33).Value = txtmain.Text
            param(34).Value = txtdg.Text
            param(35).Value = txtservicetax.Text
            param(36).Value = txtproptax.Text
            param(37).Value = txtimp.Text
            param(38).Value = txtrentrev.Text
            param(39).Value = txttenure.Text
            param(40).Value = txtfurniture.Text
            param(41).Value = txtofcequip.Text

            Dim i As Integer = ObjSubSonic.GetSubSonicExecuteScalar("AXIS_SP_PN_LEASES_ADDHR", param)



        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateleaseL()
        Dim validatelease As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LEASE")
        sp.Command.AddParameter("@LEASE_NAME", txtstore.Text, DbType.String)
        validatelease = sp.ExecuteScalar()
        Return validatelease
    End Function
    Private Function ValidateLeaseL2()
        Dim validatelandlord2 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LANDLORD2")
        sp.Command.AddParameter("@LEASE_NAME", txtstore.Text, DbType.String)
        validatelandlord2 = sp.ExecuteScalar()
        Return validatelandlord2
    End Function
    Private Function ValidateLeaseL3()
        Dim validatelandlord3 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LANDLORD3")
        sp.Command.AddParameter("@LEASE_NAME", txtstore.Text, DbType.String)
        validatelandlord3 = sp.ExecuteScalar()
        Return validatelandlord3
    End Function
    Private Sub AddLandLord1()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LANDLORD1")
            sp.Command.AddParameter("@LEASE_NAME", txtstore.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NAME", txtldname.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS", txtldaddr.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS2", txtld1addr2.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS3", txtld1addr3.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_CITY", ddlld1city.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_STATE", ddlstate.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PIN", txtld1Pin.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PAN", txtPAN.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_EMAIL", txtldemail.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_MOB", txtmob.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NUMBER", "LANDLORD1", DbType.String)
            sp.Command.AddParameter("@FROM_DATE", txtpfromdate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtptodate.Text, DbType.Date)
            sp.Command.AddParameter("@RENT_AMOUNT", txtpmonthrent.Text, DbType.Decimal)
            sp.Command.AddParameter("@SECURITY_DEPOSIT", txtpsecdep.Text, DbType.Decimal)
            sp.Command.AddParameter("@PAYMENT_MODE", ddlpaymentmode.SelectedItem.Value, DbType.String)
            If ddlpaymentmode.SelectedItem.Value = "1" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtBankName.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtAccNo.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlpaymentmode.SelectedItem.Value = "2" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", "", DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtL12Accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "AXIS", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtDeposited.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtIBankName.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", txtIFSC.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH", txtbrnch.Text, DbType.String)

            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub AddLandLord2()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LANDLORD1")
            sp.Command.AddParameter("@LEASE_NAME", txtstore.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NAME", txtld1name.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS", txtld2addr.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS2", txtld2addr2.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS3", txtld2addr3.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_CITY", ddlld2city.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_STATE", ddlld2state.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PIN", txtld2Pin.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PAN", txtld2pan.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_EMAIL", txtld2email.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_MOB", txtld2mob.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NUMBER", "LANDLORD2", DbType.String)
            sp.Command.AddParameter("@FROM_DATE", txtld2frmdate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtld2todate.Text, DbType.Date)
            sp.Command.AddParameter("@RENT_AMOUNT", txtld2rent.Text, DbType.Decimal)
            sp.Command.AddParameter("@SECURITY_DEPOSIT", txtld2sd.Text, DbType.Decimal)
            sp.Command.AddParameter("@PAYMENT_MODE", ddlld2mode.SelectedItem.Value, DbType.String)
            If ddlld2mode.SelectedItem.Value = "1" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld2bankname.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtld2accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlld2mode.SelectedItem.Value = "2" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", "", DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtl22accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlld2mode.SelectedItem.Value = "3" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "AXIS", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld2Deposited.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtld2IBankName.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", txtld2IFSC.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH", txtl2brnchname.Text, DbType.String)

            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub AddLandLord3()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LANDLORD1")
            sp.Command.AddParameter("@LEASE_NAME", txtstore.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NAME", txtld3name.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS", txtld3addr.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS2", txtld3addr2.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS3", txtld3addr3.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_CITY", ddlld3city.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_STATE", ddlld3state.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PIN", txtld3Pin.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PAN", txtld3pan.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_EMAIL", txtld3email.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_MOB", txtld3mob.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NUMBER", "LANDLORD3", DbType.String)
            sp.Command.AddParameter("@FROM_DATE", txtld3fromdate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtld3todate.Text, DbType.Date)
            sp.Command.AddParameter("@RENT_AMOUNT", txtld3rent.Text, DbType.Decimal)
            sp.Command.AddParameter("@SECURITY_DEPOSIT", txtld3sd.Text, DbType.Decimal)
            sp.Command.AddParameter("@PAYMENT_MODE", ddlld3mode.SelectedItem.Value, DbType.String)
            If ddlld3mode.SelectedItem.Value = "1" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld3bankname.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtLd3acc.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlld3mode.SelectedItem.Value = "2" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", "", DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtl32accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)

            ElseIf ddlld3mode.SelectedItem.Value = "3" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "AXIS", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld3Deposited.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtld3IBankName.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", txtld3ifsc.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH", txtl3brnch.Text, DbType.String)

            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Protected Sub Wizard1_FinishButtonClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WizardNavigationEventArgs) Handles Wizard1.FinishButtonClick
    '    Try
    '        If Page.IsValid = True Then
    '            lblMsg.Text = ""
    '            AddLease()
    '            AddEscalation()
    '            AddAgreementDetails()
    '            Dim ValidBroker As Integer
    '            ValidBroker = ValidBroker1()
    '            If ValidBroker = 0 Then
    '                AddBrokerageDetails()
    '            End If

    '            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETNAMEEMAIL")
    '            sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
    '            Dim ds1 As New DataSet()
    '            ds1 = sp1.GetDataSet()
    '            Dim STREMAIL As String = ds1.Tables(0).Rows(0).Item("AUR_EMAIL")
    '            Dim strname As String = ds1.Tables(0).Rows(0).Item("AUR_HR_NAME")
    '            Dim strHRemail As String = ds1.Tables(0).Rows(0).Item("AUR_HR_EMAIL")
    '            Dim strcircleemail As String = ds1.Tables(0).Rows(0).Item("AUR_CIRCLE_EMAIL")
    '            Dim strccmail As String = strHRemail + "," + strcircleemail

    '            Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_GETHRLEASE_MESSAGE")
    '            Dim DS As New DataSet()
    '            DS = SP.GetDataSet()

    '            Dim STRMSG As String = DS.Tables(0).Rows(0).Item("mail_body")
    '            Dim STRSUBJ As String = DS.Tables(0).Rows(0).Item("mail_subject")
    '            STRMSG = STRMSG.Replace("@@EmpNamee", ddlLesse.SelectedItem.Text)
    '            STRMSG = STRMSG.Replace("@@LeaseName", txtstore.Text)
    '            Send_Mail(STREMAIL, STRSUBJ, STRMSG, strccmail)
    '            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=16")
    '        Else
    '            Response.Write("<script language=javascript> alert('Please Check the Data You Entered or Fill the Mandatory Fields')</SCRIPT>")
    '            ' lblMsg.Text = "Please Check the Data You Entered or Fill the Mandatory Fields"
    '        End If

    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal subject As String, ByVal msg As String, ByVal strRMEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LEASE_INSERT_AMTMAIL")
        sp.Command.AddParameter("@VC_ID", ID, DbType.String)
        sp.Command.AddParameter("@VC_MSG", msg, DbType.String)
        sp.Command.AddParameter("@VC_MAIL", MailTo, DbType.String)
        sp.Command.AddParameter("@VC_SUB", subject, DbType.String)
        sp.Command.AddParameter("@DT_MAILTIME", Date.Today(), DbType.String)
        sp.Command.AddParameter("@VC_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@VC_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@VC_MAIL_CC", strRMEmail, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Protected Sub ddlesc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlesc.SelectedIndexChanged
        If ddlesc.SelectedIndex > 0 Then
            If ddlesc.SelectedValue = "Yes" Then
                Dim diff As Integer = DateDiff("yyyy", CDate(txtsdate.Text), CDate(txtedate.Text))
                If diff = 1 Then
                    Dim diff1 As Integer = DateDiff("m", CDate(txtsdate.Text), CDate(txtedate.Text))
                    If diff1 <= 12 Then
                        lblMsg.Text = "Escalation will takes place only if the Difference of Effective and Expiry Agreement Date is more than a Year "
                        ddlesc.SelectedIndex = 0
                        Exit Sub
                    ElseIf diff1 > 12 And diff1 <= 24 Then
                        lblMsg.Text = ""
                        pnlesc1.Visible = True
                        pnlesc2.Visible = False
                    ElseIf diff1 > 24 And diff1 <= 36 Then
                        lblMsg.Text = ""
                        pnlesc1.Visible = True
                        pnlesc2.Visible = True
                    End If


                    '                    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Escalation will takes place only if the Difference of Effective and Expiry Agreement Date is more than a Year ');", True)


                ElseIf diff = 2 Then
                    lblMsg.Text = ""
                    pnlesc1.Visible = True
                    pnlesc2.Visible = False
                Else
                    lblMsg.Text = ""
                    pnlesc1.Visible = True
                    pnlesc2.Visible = True
                End If
                Dim dt As DateTime = Convert.ToDateTime(txtsdate.Text)
                txtEscalationDate.Text = dt.AddYears(1).ToString("MM/dd/yyyy")
                Dim dt1 As DateTime = Convert.ToDateTime(txtEscalationDate.Text)
                Dim todate1 As DateTime = dt1.AddYears(1).ToString("MM/dd/yyyy")
                txtesctodate1.Text = todate1.AddDays(-1).ToString("MM/dd/yyyy")
                txtesctodate2.Text = Convert.ToString(DBNull.Value)
                txtescfromdate2.Text = Convert.ToString(DBNull.Value)
                txtfirstesc.Text = 0
                txtsecondesc.Text = 0
            ElseIf ddlesc.SelectedValue = "No" Then
                lblMsg.Text = ""
                pnlesc1.Visible = False
                pnlesc2.Visible = False
                txtEscalationDate.Text = ""
                txtesctodate1.Text = ""
                txtesctodate2.Text = ""
                txtescfromdate2.Text = ""
                txtEscalationDate.Text = Convert.ToString(DBNull.Value)
                txtesctodate1.Text = Convert.ToString(DBNull.Value)
                txtesctodate2.Text = Convert.ToString(DBNull.Value)
                txtescfromdate2.Text = Convert.ToString(DBNull.Value)
                txtfirstesc.Text = ""
                txtsecondesc.Text = ""
            End If
        Else
            lblMsg.Text = ""
            pnlesc1.Visible = False
            pnlesc2.Visible = False
            txtEscalationDate.Text = Convert.ToString(DBNull.Value)
            txtesctodate1.Text = Convert.ToString(DBNull.Value)
            txtesctodate2.Text = Convert.ToString(DBNull.Value)
            txtescfromdate2.Text = Convert.ToString(DBNull.Value)
            txtfirstesc.Text = ""
            txtsecondesc.Text = ""
        End If
        txtimp.Focus()
    End Sub
    Private Sub Clear_Recovery_Details()
        txtEmpBankName.Text = ""
        txtEmpBranch.Text = ""
        txtEmpAccNo.Text = ""
        txtEmpRcryAmt.Text = ""
        txtrcrytodate.Text = Date.Today
        txtrcryfromdate.Text = Date.Today
    End Sub
    Protected Sub btn3Finish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn3Finish.Click
        Try
            If Page.IsValid = True Then
                lblMsg.Text = ""

                Dim ld3rent As Decimal = 0
                Dim ld2rent As Decimal = 0
                Dim ld1rent As Decimal = 0
                Dim ld3deposit As Decimal = 0
                Dim ld2deposit As Decimal = 0
                Dim ld1deposit As Decimal = 0


                If txtld3rent.Text <> "" Then
                    ld3rent = CDbl(txtld3rent.Text)
                End If

                If txtld2rent.Text <> "" Then
                    ld2rent = CDbl(txtld2rent.Text)
                End If

                If txtpmonthrent.Text <> "" Then
                    ld1rent = CDbl(txtpmonthrent.Text)
                End If

                If txtld3sd.Text <> "" Then
                    ld3deposit = CDbl(txtld3sd.Text)
                End If

                If txtld2sd.Text <> "" Then
                    ld2deposit = CDbl(txtld2sd.Text)
                End If

                If txtpsecdep.Text <> "" Then
                    ld1deposit = CDbl(txtpsecdep.Text)
                End If


                If CDbl(txtInvestedArea.Text) <> (ld3rent + ld2rent + ld1rent) Then
                    lblMsg.Text = "Landlord(s) Month Rent not matched the  Lease Rent Entered"

                    Exit Sub
                ElseIf CDbl(txtpay.Text) <> (ld3deposit + ld2deposit + ld1deposit) Then
                    lblMsg.Text = "Landlord(s) Security Deposit not Matched the Lease Security Deposit Entered"

                    Exit Sub
                Else
                    lblMsg.Text = ""
                    If ddlleaseld.SelectedItem.Value = "1" Then
                        Landlord1Add()
                    ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                        Landlord1Add()
                        Landlord2Add()
                    ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                        Landlord1Add()
                        Landlord2Add()
                        Landlord3Add()
                    End If


                    AddLease()
                    AddEscalation()
                    AddAgreementDetails()
                    AddBrokerageDetails()
                    AddAxis_Labelmaster1(txtstore.Text)
                    AddAxis_Labelmaster2(txtstore.Text)
                    AddAxis_Labelmaster3(txtstore.Text)

                    Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=54", False)
                End If
            End If

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub
    Private Sub AddAxis_Labelmaster1(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT")
        sp.Command.AddParameter("@AXIS_SNO", 1, DbType.Int32)
        sp.Command.AddParameter("@AXIS_LABEL", "Willingness of the staff member, stating that he has identified the flat and would like to take possession of the same mentioning clearly the date of occupation", DbType.String)
        sp.Command.AddParameter("@_STATUS", "Y", DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Command.AddParameter("@SETS", "SET1", DbType.String)
        sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
        sp.ExecuteScalar()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT")
        sp1.Command.AddParameter("@AXIS_SNO", 2, DbType.Int32)
        sp1.Command.AddParameter("@AXIS_LABEL", "Incase the proposal is exceeding the entitlement of the staff during any period of the leased arrangement, an undertaking needs to be submitted by the staff that the excess over the entitlement may be deducted from their respective savings bank account.", DbType.String)
        sp1.Command.AddParameter("@_STATUS", "Y", DbType.String)
        sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@SETS", "SET1", DbType.String)
        sp1.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
        sp1.ExecuteScalar()
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT")
        sp2.Command.AddParameter("@AXIS_SNO", 3, DbType.Int32)
        sp2.Command.AddParameter("@AXIS_LABEL", "Undertaking to be duly acknowledged by the staff member wherein he states that the premises being provided to him by the Bank, would be kept in proper condition and he would comply with all the necessary formalities. This undertaking is to be acknowledged by the staff at the time of collecting the security deposit to be handedover to the landlord, i.e. prior to taking the possession of the said premises.", DbType.String)
        sp2.Command.AddParameter("@_STATUS", "Y", DbType.String)
        sp2.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp2.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp2.Command.AddParameter("@SETS", "SET1", DbType.String)
        sp2.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
        sp2.ExecuteScalar()

    End Sub
    Private Sub AddAxis_Labelmaster2(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT1")
        sp.Command.AddParameter("@AXIS_SNO", 1, DbType.Int32)
        sp.Command.AddParameter("@AXIS_LABEL", "Offer letter from the landlord stating that he is willing to offer his flat on leave and licence basis to AXIS BANK for the concerned staff.( A draft copy of the offer is as per ANNEXURE I )", DbType.String)
        sp.Command.AddParameter("@_STATUS", "Y", DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Command.AddParameter("@SETS", "SET2", DbType.String)
        sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
        sp.ExecuteScalar()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT1")
        sp1.Command.AddParameter("@AXIS_SNO", 2, DbType.Int32)
        sp1.Command.AddParameter("@AXIS_LABEL", "Proof of ownership to be submitted by the landlord ( which may comprise either Share Certificate of the society / sale agreement / society maintenance bill and receipt ---- any one) ", DbType.String)
        sp1.Command.AddParameter("@_STATUS", "Y", DbType.String)
        sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@SETS", "SET2", DbType.String)
        sp1.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
        sp1.ExecuteScalar()
        If ddlpoa.SelectedItem.Value = "Yes" Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT1")
            sp2.Command.AddParameter("@AXIS_SNO", 3, DbType.Int32)
            sp2.Command.AddParameter("@AXIS_LABEL", "Incase landlord is not present in Mumbai or is not able to execute the agreement, for any reasons, then the Power of Attorney holder should submit a copy of Power of Attorney. The POA should either be notarized or registered. The POA should also bear photograph of the POA holder.", DbType.String)
            sp2.Command.AddParameter("@_STATUS", "Y", DbType.String)
            sp2.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp2.Command.AddParameter("@REQ_ID", reqid, DbType.String)
            sp2.Command.AddParameter("@SETS", "SET2", DbType.String)
            sp2.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
            sp2.ExecuteScalar()
        Else
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT1")
            sp2.Command.AddParameter("@AXIS_SNO", 3, DbType.Int32)
            sp2.Command.AddParameter("@AXIS_LABEL", "Incase landlord is not present in Mumbai or is not able to execute the agreement, for any reasons, then the Power of Attorney holder should submit a copy of Power of Attorney. The POA should either be notarized or registered. The POA should also bear photograph of the POA holder.", DbType.String)
            sp2.Command.AddParameter("@_STATUS", "N", DbType.String)
            sp2.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp2.Command.AddParameter("@REQ_ID", reqid, DbType.String)
            sp2.Command.AddParameter("@SETS", "SET2", DbType.String)
            sp2.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
            sp2.ExecuteScalar()
        End If
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT1")
        sp3.Command.AddParameter("@AXIS_SNO", 4, DbType.Int32)
        sp3.Command.AddParameter("@AXIS_LABEL", "Payment instructions to be given by landlord, incase there is more than one landlord of the said flat. It may be mentioned in the Offer letter or a separate letter to that effect may be given.( Please note that the TDS would be deducted on the entire amount of rent � irrespective of the number of landlords), unless specifically stated and an affidavit to that effect be submitted. ", DbType.String)
        sp3.Command.AddParameter("@_STATUS", "Y", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp3.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp3.Command.AddParameter("@SETS", "SET2", DbType.String)
        sp3.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
        sp3.ExecuteScalar()
    End Sub
    Private Sub AddAxis_Labelmaster3(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT2")
        sp.Command.AddParameter("@AXIS_SNO", 1, DbType.Int32)
        sp.Command.AddParameter("@AXIS_LABEL", "A letter from the broker stating that the premises being taken by our staff member is routed through him and he would submit the bill at a later date.", DbType.String)
        If txtbrkname.Text <> "" Then
            sp.Command.AddParameter("@_STATUS", "Y", DbType.String)
        Else
            sp.Command.AddParameter("@_STATUS", "N", DbType.String)
        End If

        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Command.AddParameter("@SETS", "SET3", DbType.String)
        sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
        sp.ExecuteScalar()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT2")
        sp1.Command.AddParameter("@AXIS_SNO", 2, DbType.Int32)
        sp1.Command.AddParameter("@AXIS_LABEL", "Brokerage bill for the concerned premises  (deal)", DbType.String)
        If txtbrkname.Text <> "" Then
            sp1.Command.AddParameter("@_STATUS", "Y", DbType.String)
        Else
            sp1.Command.AddParameter("@_STATUS", "N", DbType.String)
        End If

        sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@SETS", "SET3", DbType.String)
        sp1.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
        sp1.ExecuteScalar()
    End Sub
    Private Sub uptaxismaster()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_UPDATE_LABELMASTER")
            sp.Command.AddParameter("@EMPID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Broker()
        Dim ValidBroker As Integer
        ValidBroker = ValidBroker1()
        If ValidBroker = 0 Then
            'panbrk.Visible = True
            txtbrkamount.Text = 0
            txtbrkmob.Text = 0
        Else
            panbrk.Visible = False
        End If
    End Sub

    Protected Sub btn1Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn1Next.Click
        If Page.IsValid = True Then
            step1heading.Visible = False
            step2heading.Visible = True
            step3heading.Visible = False
            txtbrkamount.Text = txtbrokerage.Text
            txtbrkamount.ReadOnly = True
            txtpfromdate.Text = txtsdate.Text
            txtptodate.Text = txtedate.Text
            txtld2frmdate.Text = txtsdate.Text
            txtld2todate.Text = txtedate.Text
            txtld3fromdate.Text = txtsdate.Text
            txtld3todate.Text = txtedate.Text
            txtpfromdate.ReadOnly = True
            txtptodate.ReadOnly = True
            txtld2frmdate.ReadOnly = True
            txtld2todate.ReadOnly = True
            txtld3fromdate.ReadOnly = True
            txtld3todate.ReadOnly = True
            Dim dt As DateTime = Convert.ToDateTime(txtsdate.Text).ToString("MM/dd/yyyy")
            txtrcryfromdate.Text = txtsdate.Text
            If ddlesc.SelectedItem.Value = "Yes" Then
                txtrcrytodate.Text = dt.AddYears(1).ToString("MM/dd/yyyy")
            Else
                txtrcrytodate.Text = txtedate.Text
            End If

            txtrcrfrmdate1.Text = txtEscalationDate.Text
            txtrcrtodate1.Text = txtesctodate1.Text
            txtrcrfrmdate2.Text = txtescfromdate2.Text
            txtrcrtodate2.Text = txtesctodate2.Text
            Dim Interest As Double = ((CDbl(txtpay.Text) * 9) / 100) / 12
            txtstore1.Text = Interest

            If ddlesc.SelectedItem.Value = "Yes" Then


                If CDate(txtEscalationDate.Text) >= CDate(txtsdate.Text) And (txtesctodate1.Text) <= CDate(txtedate.Text) Then

                    If CDate(txtsdate.Text) > CDate(txtedate.Text) Then
                        lblMsg.Text = "Expiry Date of Agreement Should be more than Effective Date of Agreement"
                    ElseIf DateDiff(DateInterval.Year, CDate(txtsdate.Text), CDate(txtedate.Text)) > 10 Then
                        lblMsg.Text = "Maximum Lease Period(10 Years)exceeded "
                    ElseIf DateDiff(DateInterval.Month, CDate(txtsdate.Text), CDate(txtedate.Text)) < 6 Then
                        lblMsg.Text = "Minimum Lease Period(6 Months) "
                    Else

                        If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) Then
                            ' ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Please enter the amount within your eligibility ');", True)
                            lblMsg.Text = "Please enter the amount within your eligibility"
                            lblMsg.Visible = True
                            Exit Sub
                        End If

                        'If CDate(txtsdate.Text) < DateTime.Now.ToString("dd-MMM-yyyy") Then
                        '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Effective Date of Agreement Should not be less than Today Date ');", True)
                        'ElseIf CDate(txtedate.Text) < CDate(txtsdate.Text) Then
                        '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Expiry Date of Agreement Should not be less than Effective Date of Agreement');", True)
                        'Else
                        panwiz1.Visible = False
                        btn1Next.Visible = False
                        lblMsg.Text = ""

                        If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                            txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                            panrecwiz.Visible = True
                            panafteresc1.Visible = True
                            lblHead.Text = "Recovery & Brokerage Details"
                        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                            panrecwiz.Visible = True
                            txtrcramt1.Text = ""
                            panafteresc1.Visible = False

                            lblHead.Text = "Recovery & Brokerage Details"
                        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                            panrecwiz.Visible = False

                            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                                panafteresc1.Visible = True
                                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                            Else
                                panafteresc1.Visible = False
                                txtrcramt1.Text = ""
                            End If
                        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                            Clear_Recovery_Details()
                            panrecwiz.Visible = False
                            panafteresc1.Visible = False
                            lblHead.Text = "Brokerage Details"
                        End If
                        panPOA.Visible = False
                        panld1.Visible = False
                        pnlld2.Visible = False
                        pnlld3.Visible = False
                        btn2Next.Visible = True
                        btn2prev.Visible = True
                        btn3Prev.Visible = False
                        btn3Finish.Visible = False
                        panbrk.Visible = True

                        'End If
                    End If
                Else
                    ' ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Escalation Dates should be between Effective agreement date and Expiry agreement date!');", True)
                    lblMsg.Text = "Escalation Dates should be between Effective agreement date and Expiry agreement date!"
                    lblMsg.Visible = True
                    Exit Sub

                End If
            ElseIf ddlesc.SelectedItem.Value = "No" Then
                If CDate(txtsdate.Text) > CDate(txtedate.Text) Then
                    lblMsg.Text = "Expiry Date of Agreement Should be more than Effective Date of Agreement"
                ElseIf DateDiff(DateInterval.Year, CDate(txtsdate.Text), CDate(txtedate.Text)) > 5 Then
                    lblMsg.Text = "Maximum Lease Period(5 Years)exceeded "
                ElseIf DateDiff(DateInterval.Month, CDate(txtsdate.Text), CDate(txtedate.Text)) < 6 Then
                    lblMsg.Text = "Minimum Lease Period(6 Months) "
                Else



                    'If CDate(txtsdate.Text) < DateTime.Now.ToString("dd-MMM-yyyy") Then
                    '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Effective Date of Agreement Should not be less than Today Date ');", True)
                    'ElseIf CDate(txtedate.Text) < CDate(txtsdate.Text) Then
                    '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Expiry Date of Agreement Should not be less than Effective Date of Agreement');", True)
                    'Else
                    panwiz1.Visible = False
                    btn1Next.Visible = False
                    lblMsg.Text = ""

                    If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                        txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                        txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                        panrecwiz.Visible = True
                        panafteresc1.Visible = True
                        lblHead.Text = "Recovery & Brokerage Details"
                    ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                        txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                        panrecwiz.Visible = True
                        txtrcramt1.Text = ""
                        panafteresc1.Visible = False

                        lblHead.Text = "Recovery & Brokerage Details"
                    ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                        panrecwiz.Visible = False

                        If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                            panafteresc1.Visible = True
                            txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                        Else
                            panafteresc1.Visible = False
                            txtrcramt1.Text = ""
                        End If
                    ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                        Clear_Recovery_Details()
                        panrecwiz.Visible = False
                        panafteresc1.Visible = False
                        lblHead.Text = "Brokerage Details"
                    End If
                    panPOA.Visible = False
                    panld1.Visible = False
                    pnlld2.Visible = False
                    pnlld3.Visible = False
                    btn2Next.Visible = True
                    btn2prev.Visible = True
                    btn3Prev.Visible = False
                    btn3Finish.Visible = False
                    panbrk.Visible = True

                    'End If
                End If
            End If

            'Else
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Please Check the Data You Entered or Fill the Mandatory Fields!');", True)
        End If
    End Sub

    Protected Sub btn2Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn2Next.Click
        If Page.IsValid = True Then
            step1heading.Visible = False
            step2heading.Visible = False
            step3heading.Visible = True
            txtpfromdate.Text = txtsdate.Text
            txtptodate.Text = txtedate.Text
            txtld2frmdate.Text = txtsdate.Text
            txtld2todate.Text = txtedate.Text
            txtld3fromdate.Text = txtsdate.Text
            txtld3todate.Text = txtedate.Text

            If ddlpoa.SelectedItem.Value = "Yes" Then
                panPOA.Visible = True
                lblHead.Text = "Power of Attorney & Landlord Details"
            Else
                panPOA.Visible = False
                txtPOAName.Text = ""
                txtPOAAddress.Text = ""
                txtPOAMobile.Text = ""
                txtPOAEmail.Text = ""
                lblHead.Text = "Landlord Details"
            End If


            If ddlleaseld.SelectedItem.Value = "1" Then
                txtpmonthrent.Text = txtInvestedArea.Text
                txtpsecdep.Text = txtpay.Text
                txtpmonthrent.ReadOnly = True
                txtpsecdep.ReadOnly = True
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
            ElseIf ddlleaseld.SelectedItem.Value = "2" Or ddlleaseld.SelectedItem.Value = "3" Then
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
                txtpmonthrent.Text = 0
                txtpsecdep.Text = 0
                txtpmonthrent.ReadOnly = False
                txtpsecdep.ReadOnly = False
            End If

            If ddlleaseld.SelectedItem.Value = "1" Then
                panld1.Visible = True
                pnlld2.Visible = False
                pnlld3.Visible = False

            ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                panld1.Visible = True
                pnlld2.Visible = True
                pnlld3.Visible = False

            ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                panld1.Visible = True
                pnlld2.Visible = True
                pnlld3.Visible = True

            End If
            btn1Next.Visible = False
            btn2prev.Visible = False
            btn2Next.Visible = False
            btn3Prev.Visible = True
            btn3Finish.Visible = True
            panbrk.Visible = False
            panwiz1.Visible = False
            panafteresc1.Visible = False
            panrecwiz.Visible = False

        End If
    End Sub

    Protected Sub btn2prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn2prev.Click
        txtpfromdate.Text = txtsdate.Text
        txtptodate.Text = txtedate.Text
        txtld2frmdate.Text = txtsdate.Text
        txtld2todate.Text = txtedate.Text
        txtld3fromdate.Text = txtsdate.Text
        txtld3todate.Text = txtedate.Text
        step1heading.Visible = True
        step2heading.Visible = False
        step3heading.Visible = False
        panafteresc1.Visible = False
        panPOA.Visible = False
        panrecwiz.Visible = False
        panwiz1.Visible = True
        btn1Next.Visible = True
        btn2prev.Visible = False
        btn2Next.Visible = False
        btn3Prev.Visible = False
        btn3Finish.Visible = False
        panld1.Visible = False
        pnlld2.Visible = False
        pnlld3.Visible = False
        panbrk.Visible = False
        lblHead.Text = "Lease Details"
    End Sub

    Protected Sub btn3Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn3Prev.Click
        panbrk.Visible = True
        panPOA.Visible = False
        panld1.Visible = False
        pnlld2.Visible = False
        pnlld3.Visible = False
        panwiz1.Visible = False
        btn2Next.Visible = True
        btn2prev.Visible = True
        btn3Prev.Visible = False
        btn3Finish.Visible = False
        btn1Next.Visible = False
        step1heading.Visible = False
        step2heading.Visible = True
        step3heading.Visible = False


        If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
            txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
            panrecwiz.Visible = True
            panafteresc1.Visible = True


            lblHead.Text = "Brokerage  & Recovery Details"
        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
            panrecwiz.Visible = True
            txtrcramt1.Text = ""
            panafteresc1.Visible = False

            lblHead.Text = "Brokerage  & Recovery Details"
        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
            panrecwiz.Visible = False
            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                panafteresc1.Visible = True
                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)

            Else
                panafteresc1.Visible = False
                txtrcramt1.Text = ""

            End If
        Else
            Clear_Recovery_Details()
            panrecwiz.Visible = False
            panafteresc1.Visible = False

            lblHead.Text = "Brokerage Details"
        End If


    End Sub



    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            lblMsg.Text = ""
            GetEntitle()
        Else
            txtentitle.Text = 0
            lblmaxrent.Text = 0
            lblmaxsd.Text = 0
            lblMsg.Text = "Please Select City"
        End If
    End Sub
    Private Sub BindLandlordstate()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY_STATE")

        ddlstate.DataSource = sp.GetDataSet()
        ddlstate.DataTextField = "STE_NAME"
        ddlstate.DataValueField = "STE_CODE"
        ddlstate.DataBind()
        ddlstate.Items.Insert(0, New ListItem("--Select--", "--Select--"))

        ddlld2state.DataSource = sp.GetDataSet()
        ddlld2state.DataTextField = "STE_NAME"
        ddlld2state.DataValueField = "STE_CODE"
        ddlld2state.DataBind()
        ddlld2state.Items.Insert(0, New ListItem("--Select--", "--Select--"))

        ddlld3state.DataSource = sp.GetDataSet()
        ddlld3state.DataTextField = "STE_NAME"
        ddlld3state.DataValueField = "STE_CODE"
        ddlld3state.DataBind()
        ddlld3state.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub

    Protected Sub ddlstate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlstate.SelectedIndexChanged
        'If ddlstate.SelectedIndex > 0 Then
        '    BindCity(ddlstate.SelectedItem.Value)
        'Else
        '    ddlld1city.Items.Clear()
        'End If
    End Sub
    'Private Sub BindCity(ByVal state As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LANDLORD1_CITY")
    '    sp.Command.AddParameter("@STATE", state, DbType.String)
    '    ddlld1city.DataSource = sp.GetDataSet()
    '    ddlld1city.DataTextField = "CTY_NAME"
    '    ddlld1city.DataValueField = "CTY_CODE"
    '    ddlld1city.DataBind()
    '    ddlld1city.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    'End Sub
    Protected Sub ddlld2state_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlld2state.SelectedIndexChanged
        'If ddlld2state.SelectedIndex > 0 Then
        '    BindCity2(ddlld2state.SelectedItem.Value)
        'Else
        '    ddlld2city.Items.Clear()
        'End If
    End Sub
    'Private Sub BindCity2(ByVal state As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LANDLORD2_CITY")
    '    sp.Command.AddParameter("@STATE", state, DbType.String)
    '    ddlld2city.DataSource = sp.GetDataSet()
    '    ddlld2city.DataTextField = "CTY_NAME"
    '    ddlld2city.DataValueField = "CTY_CODE"
    '    ddlld2city.DataBind()
    '    ddlld2city.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    'End Sub

    Protected Sub ddlld3state_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlld3state.SelectedIndexChanged
        'If ddlld3state.SelectedIndex > 0 Then
        '    BindCity3(ddlld3state.SelectedItem.Value)
        'Else
        '    ddlld3city.Items.Clear()
        'End If

    End Sub
    'Private Sub BindCity3(ByVal state As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LANDLORD3_CITY")
    '    sp.Command.AddParameter("@STATE", state, DbType.String)
    '    ddlld3city.DataSource = sp.GetDataSet()
    '    ddlld3city.DataTextField = "CTY_NAME"
    '    ddlld3city.DataValueField = "CTY_CODE"
    '    ddlld3city.DataBind()
    '    ddlld3city.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    'End Sub

    Protected Sub ddlleaseld_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlleaseld.SelectedIndexChanged
        If ddlleaseld.SelectedIndex > 0 Then

            If ddlleaseld.SelectedItem.Value = "1" Then
                txtpmonthrent.Text = txtInvestedArea.Text
                txtpsecdep.Text = txtpay.Text
                txtpmonthrent.ReadOnly = True
                txtpsecdep.ReadOnly = True
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
            ElseIf ddlleaseld.SelectedItem.Value = "2" Or ddlleaseld.SelectedItem.Value = "3" Then
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
                txtpmonthrent.Text = 0
                txtpsecdep.Text = 0
                txtpmonthrent.ReadOnly = False
                txtpsecdep.ReadOnly = False
            End If

        End If
        ddlpoa.Focus()
    End Sub

    Protected Sub btntotal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btntotal.Click
        'added by praveen on14/02/2014
        If CType(txtbrokerage.Text, Integer) >= CType(txtInvestedArea.Text, Integer) Then
            'lblMsg.Text = "Brokerage should be less than the basic"
            'lblMsg.Visible = True
            ' ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Brokerage should be less than the Basic Rent amount');</SCRIPT>", False)
            lblMsg.Visible = True
            lblMsg.Text = "Brokerage should be less than the Basic Rent amount"
        Else
            'ends here
            onetimecost()
            maintenancecost()

            Dim rent As Decimal = 0
            Dim esc1 As Decimal = 0
            Dim esc2 As Decimal = 0
            If txtservicetax.Text <> "" Then
                rent = CDbl(txtservicetax.Text)
            End If
            If txtmain.Text <> "" Then
                esc1 = CDbl(txtmain.Text)
            End If
            If txtInvestedArea.Text <> "" Then
                esc2 = CDbl(txtInvestedArea.Text)
            End If

            txttotalrent.Text = rent + esc1 + esc2
            'added by praveen on 14/02/2014
            txtpmonthrent.Text = rent + esc1 + esc2
            txtpmonthrent.ReadOnly = True
            'ends here
            lblMsg.Text = String.Empty
        End If

    End Sub

    Protected Sub ddlproperty_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproperty.SelectedIndexChanged
        Dim var As Integer
        var = getpropertytype()
        If var = 1 Then

            txtdg.ReadOnly = False

            txtimp.ReadOnly = False
            backup1.Visible = True
            backup2.Visible = True
        Else
            txtdg.Text = 0
            txtdg.ReadOnly = True
            txtimp.Text = "NA"
            txtimp.ReadOnly = True
            backup1.Visible = False
            backup2.Visible = False
        End If
    End Sub
    Private Function getpropertytype() As Integer
        Dim var As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROPERTY_TYPE_LEASE")
        sp.Command.AddParameter("@PROPERTY", ddlproperty.SelectedItem.Value, DbType.String)
        var = sp.ExecuteScalar()
        Return var
    End Function

    Protected Sub txtOccupiedArea_TextChanged(sender As Object, e As EventArgs) Handles txtOccupiedArea.TextChanged

        If txtOccupiedArea.Text = "" Then
            ' ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Enter Area in SQFT!');", True)
            lblMsg.Text = "Please Enter Area in SQFT!"
            lblMsg.Visible = True
            Exit Sub
        ElseIf IsNumeric(txtOccupiedArea.Text) Then     'else is added by praveen
            bindpropertytax()
            txtmain1.Text = maintper * CDbl(txtOccupiedArea.Text)
            txtservicetax.Text = SERVPER * CDbl(txtOccupiedArea.Text)
            lblMsg.Visible = False
        Else
            'Page.ClientScript.RegisterStartupScript(Me.GetType(), "k", "<script type='Text/JavaScript'> alert('Have you Verified Driver License Number?');</script>", True)
            ScriptManager.RegisterStartupScript(Me, [GetType](), "function", "showalert('Please Enter Numerics only');", True)
            lblMsg.Text = "Please enter numerics only in Builtup area"
            lblMsg.Visible = True
        End If
    End Sub


    Private Sub onetimecost()

        Dim sduty As Decimal = 0
        Dim regcharges As Decimal = 0
        Dim professionalfees As Decimal = 0
        Dim brokeragefees As Decimal = 0

        If txtsduty.Text = "" Then
            sduty = 0
        Else
            sduty = CDbl(txtsduty.Text)
        End If

        If txtregcharges.Text = "" Then
            regcharges = 0
        Else
            regcharges = CDbl(txtregcharges.Text)
        End If

        If txtpfees.Text = "" Then
            professionalfees = 0
        Else
            professionalfees = CDbl(txtpfees.Text)
        End If

        If txtbrokerage.Text = "" Then
            brokeragefees = 0
        Else
            brokeragefees = CDbl(txtbrokerage.Text)
        End If

        txtbasic.Text = sduty + regcharges + professionalfees + brokeragefees




    End Sub
    Private Sub maintenancecost()
        Dim dgbackup As Decimal = 0
        Dim furniture As Decimal = 0
        Dim office As Decimal = 0
        Dim maintenance As Decimal = 0

        If txtdg.Text = "" Then
            dgbackup = 0
        Else
            dgbackup = CDbl(txtdg.Text)
        End If

        If txtfurniture.Text = "" Then
            furniture = 0
        Else
            furniture = CDbl(txtfurniture.Text)
        End If

        If txtofcequip.Text = "" Then
            office = 0
        Else
            office = CDbl(txtofcequip.Text)
        End If

        If txtmain1.Text = "" Then
            maintenance = 0
        Else
            maintenance = CDbl(txtmain1.Text)
        End If

        txtmain.Text = dgbackup + furniture + office + maintenance
    End Sub


End Class
