<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeasePaymentHistory.ascx.vb"
    Inherits="Controls_LeasePaymentHistory" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                    <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Search by Tenant Code/Tenant Name/Property Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvempid" runat="Server" ControlToValidate="txtempid"
                    ErrorMessage="Please Enter Tenant Code/Tenant Name/Property Name" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtempid" runat="Server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <%--<div style="float:left!important;">--%>
    <div class="col-md-6">
        <div class="row col-md-12">
            <asp:Button ID="btnsubmit" runat="server" ValidationGroup="Val1" CssClass="btn btn-primary custom-button-color" Text="submit" />
        </div>
    </div>
</div>
<div id="panelgrid1" runat="server" class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvitems" runat="server" AllowPaging="true" AllowSorting="true"
            AutoGenerateColumns="false" EmptyDataText="No Lease Payment Details Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:TemplateField HeaderText="Lease">
                    <ItemTemplate>
                        <asp:Label ID="lbllease" runat="Server" Text='<%#Eval("LEASE_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lessor">
                    <ItemTemplate>
                        <asp:Label ID="lbllesse" runat="Server" Text='<%#Eval("LESSE_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Start Date">
                    <ItemTemplate>
                        <asp:Label ID="lblleasesdate" runat="Server" Text='<%#Eval("LEASE_START_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Expiry Date">
                    <ItemTemplate>
                        <asp:Label ID="lblleaseedate" runat="Server" Text='<%#Eval("LEASE_EXPIRY_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkviewdetails" runat="server" Text="View Details" CommandArgument='<%# Container.DataItemIndex %>'
                            CommandName="View"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<div id="TR" runat="Server" cellpadding="2" cellspacing="0" class="row" border="1">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Date</label>
                <div class="col-md-7">
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To Date</label>
                <div class="col-md-7">
                    <div class='input-group date' id='todate'>
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <asp:Button ID="btnGo" runat="Server" Text="Go" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
        </div>
    </div>
</div>
<div id="pnlgrid2" runat="server" class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvitems1" runat="Server" AutoGenerateColumns="false" AllowPaging="true"
            AllowSorting="true" EmptyDataText="No Payment Details Found" CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:TemplateField HeaderText="Lease">
                    <ItemTemplate>
                        <asp:Label ID="lbllease" runat="Server" Text='<%#Eval("LEASE_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lesse">
                    <ItemTemplate>
                        <asp:Label ID="lbllesse" runat="Server" Text='<%#Eval("LESSE_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Landlord Name">
                    <ItemTemplate>
                        <asp:Label ID="lbllandlord" runat="Server" Text='<%#Eval("LANDLORD_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Payment Mode">
                    <ItemTemplate>
                        <asp:Label ID="lblpayment" runat="Server" Text='<%#Eval("PAYMENT_MODE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="From Date">
                    <ItemTemplate>
                        <asp:Label ID="lblfdate" runat="Server" Text='<%#Eval("FROM_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To Date">
                    <ItemTemplate>
                        <asp:Label ID="lbltdate" runat="Server" Text='<%#Eval("TO_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Paid Date">
                    <ItemTemplate>
                        <asp:Label ID="lblpdate" runat="Server" Text='<%#Eval("PAID_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Paid Amount">
                    <ItemTemplate>
                        <asp:Label ID="lblpAmount" runat="Server" Text='<%#Eval("PAID_AMOUNT" ,"{0:c2}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
