
Partial Class Controls_ItemViewReq
    Inherits System.Web.UI.UserControl
    Dim obj As New clsSubSonicCommonFunctions()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fillgrid()
        End If
    End Sub
    Private Sub fillgrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_FOR_ASSETVIEWREQUEST")
            sp.Command.AddParameter("@AUR_ID", Session("Uid"), Data.DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception

        End Try
       
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

  
End Class