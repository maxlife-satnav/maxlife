Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGVendorModify
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim AVR_CODE As String = Request.QueryString("code")
        Dim AVR_NAME As String = txtName.Text

        Try
            Dim cntVenCat As Integer = 0

            For i As Integer = 0 To chkVendorCat.Items.Count - 1
                If chkVendorCat.Items(i).Selected = True Then
                    cntVenCat += 1
                End If
            Next
            If cntVenCat = 0 Then
                lblMsg.Text = "Please select Vendor Category."
                Exit Sub
            End If

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDT_AMG_VENDOR")

            sp1.Command.AddParameter("@AVR_CODE", AVR_CODE, DbType.String)
            sp1.Command.AddParameter("@AVR_NAME", AVR_NAME, DbType.String)
            'sp1.Command.AddParameter("@AVR_GRADE", rdbtnGrade.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_ADDR", txtAddress.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_CITY", ddlCity.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@AVR_STATE", ddlState.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@AVR_COUNTRY", txtCountry.Text, DbType.String)

            sp1.Command.AddParameter("@AVR_PHNO", txtPhone.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_MOBILE_PHNO", txtMobile.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_EMAIL", txtEmail.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_STATUS", rdbtnStatus.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_STA_ID", rdbtnStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AVR_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AVR_REMARKS", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_ACCNO", txtBankAcNo.Text, DbType.String)

            sp1.Command.AddParameter("@AVR_BRN_NAME", txtBranchName.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_IFSC_CODE", txtIFSCCode.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_BANK_NAME", txtBankName.Text, DbType.String)

            sp1.Command.AddParameter("@AVR_PAN_NO", txtPANno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_GIR_NO", txtGIRno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_TAN_NO", txtTANno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_LSTNO", txtlstno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_WSTNO", txtwctno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_CSTNO", txtcstno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_STCIRCLE", txtstcircle.Text, DbType.String)

            sp1.ExecuteScalar()


            For j As Integer = 0 To chkVendorCat.Items.Count - 1
                If chkVendorCat.Items(j).Selected = True Then
                    InsertVendorCategory(AVR_CODE, chkVendorCat.Items(j).Value)
                End If
            Next

            lblMsg.Text = "Data Modified successfully"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub InsertVendorCategory(ByVal VendorCode As String, ByVal VendorCatName As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AMG_VEN_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = VendorCode
        param(1) = New SqlParameter("@CAT_NAME", SqlDbType.NVarChar, 200)
        param(1).Value = VendorCatName
        ObjSubsonic.GetSubSonicExecute("INSERT_VERDOR_CATEGORY", param)
    End Sub

    Public Sub Cleardata()
        txtName.Text = ""
        txtAddress.Text = ""
        ddlCity.SelectedValue = 0
        ' ddlState.SelectedValue = 0
        'txtCountry.Text = ""
        txtPhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        'ddlUser.SelectedItem.Value = 0
        txtRemarks.Text = ""
        txtBankAcNo.Text = ""
        txtBankName.Text = ""
        txtBranchName.Text = ""
        txtIFSCCode.Text = ""

        txtPANno.Text = ""
        txtGIRno.Text = ""
        txtTANno.Text = ""
        txtlstno.Text = ""
        txtwctno.Text = ""
        txtcstno.Text = ""
        txtstcircle.Text = ""

        chkVendorCat.ClearSelection()


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_STATE")
            'sp2.Command.AddParameter("@dummy", " ", DbType.String)
            'ddlState.DataSource = sp2.GetDataSet()

            'ddlState.DataTextField = "STE_NAME"
            'ddlState.DataValueField = "STE_CODE"
            'ddlState.DataBind()
            'ddlState.Items.Insert(0, New ListItem("--Select--", "0"))
            Get_City()
            Dim AVR_CODE As String = Request.QueryString("code")
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSETVENDOR_DETAILS")
            sp3.Command.AddParameter("@AVR_CODE", AVR_CODE, DbType.String)
            Dim DS3 As New DataSet
            DS3 = sp3.GetDataSet()

            If DS3.Tables(0).Rows.Count > 0 Then

                txtName.Text = DS3.Tables(0).Rows(0).Item("AVR_NAME")

                'Dim lblGrade As Integer = DS3.Tables(0).Rows(0).Item("AVR_GRADE")
                'If lblGrade = 0 Then
                '    rdbtnGrade.SelectedValue = 0
                'Else
                '    rdbtnGrade.SelectedValue = 1
                'End If
                txtAddress.Text = DS3.Tables(0).Rows(0).Item("AVR_ADDR")
                'txtCountry.Text = DS3.Tables(0).Rows(0).Item("AVR_COUNTRY")
                'Dim li As New ListItem
                'li = ddlState.Items.FindByText(DS3.Tables(0).Rows(0).Item("AVR_STATE"))
                'If Not li Is Nothing Then
                '    ddlState.ClearSelection()
                '    li.Selected = True
                'End If



                Dim li1 As New ListItem
                li1 = ddlCity.Items.FindByValue(DS3.Tables(0).Rows(0).Item("AVR_CITY"))
                If Not li1 Is Nothing Then
                    ddlCity.ClearSelection()
                    li1.Selected = True
                End If

                txtPhone.Text = DS3.Tables(0).Rows(0).Item("AVR_PHNO")
                txtMobile.Text = DS3.Tables(0).Rows(0).Item("AVR_MOBILE_PHNO")
                txtEmail.Text = DS3.Tables(0).Rows(0).Item("AVR_EMAIL")
                txtBankName.Text = DS3.Tables(0).Rows(0).Item("AVR_BANK_NAME")
                txtBankAcNo.Text = DS3.Tables(0).Rows(0).Item("AVR_ACCNO")
                txtBranchName.Text = DS3.Tables(0).Rows(0).Item("AVR_BRN_NAME")
                txtIFSCCode.Text = DS3.Tables(0).Rows(0).Item("AVR_IFSC_CODE")
                Dim lblStatus As Integer = DS3.Tables(0).Rows(0).Item("AVR_STATUS")
                If lblStatus = 0 Then
                    rdbtnStatus.SelectedValue = 0
                Else
                    rdbtnStatus.SelectedValue = 1
                End If

                txtRemarks.Text = DS3.Tables(0).Rows(0).Item("AVR_REMARKS")


                Dim VenCatName As String
                VenCatName = Convert.ToString(DS3.Tables(0).Rows(0).Item("CAT_NAME"))

                Dim parts As String() = VenCatName.Split(New Char() {","c})


                For i As Integer = 0 To parts.Length - 1
                    For j As Integer = 0 To chkVendorCat.Items.Count - 1
                        If chkVendorCat.Items(j).Value = parts(i) Then
                            chkVendorCat.Items(j).Selected = True
                        End If
                    Next
                Next






                txtPANno.Text = DS3.Tables(0).Rows(0).Item("AVR_PAN_NO")
                txtGIRno.Text = DS3.Tables(0).Rows(0).Item("AVR_GIR_NO")
                txtTANno.Text = DS3.Tables(0).Rows(0).Item("AVR_TAN_NO")
                txtlstno.Text = DS3.Tables(0).Rows(0).Item("AVR_LSTNO")
                txtwctno.Text = DS3.Tables(0).Rows(0).Item("AVR_WSTNO")
                txtcstno.Text = DS3.Tables(0).Rows(0).Item("AVR_CSTNO")
                txtstcircle.Text = DS3.Tables(0).Rows(0).Item("AVR_STCIRCLE")

            End If


        End If
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmAMGVendorGetDetails.aspx")
    End Sub

    'Protected Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlState.SelectedIndexChanged
    '    If ddlState.SelectedIndex > 0 Then
    '        Get_City()
    '    Else
    '        ddlCity.Items.Clear()
    '        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
    '        ddlCity.SelectedValue = 0
    '    End If


    'End Sub
    Private Sub Get_City()

        Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY")
        sp4.Command.AddParameter("dummy", "", DbType.String)
        Dim ds4 As New DataSet
        ds4 = sp4.GetDataSet()
        ddlCity.DataSource = ds4
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
End Class
