<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewTenantDetails.ascx.vb"
    Inherits="Controls_ViewTenantDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>

<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>

<%--<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">--%>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Enter Tenant Code to find Tenant<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfsearch" runat="server" ControlToValidate="txtSearch"
                    Display="None" ErrorMessage="Please Enter Tenant Code" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <%--<div class="col-md-3">--%>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="row">
                    <div class="col-md-10 control-label">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color" CausesValidation="true" />

                        <asp:LinkButton ID="LBTN1" runat="Server" Text="GetAllRecords"></asp:LinkButton>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Property Type <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                    Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlproptype" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlproptype_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select City<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvcity" runat="server" ControlToValidate="ddlCity"
                    Display="None" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="0"
                    Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="TRUE">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Location <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Property<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvproperty" runat="server" ControlToValidate="ddlProp"
                    Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1" InitialValue="0"
                    Enabled="True"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlProp" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />

        </div>
    </div>
</div>
<div id="t1" runat="Server">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <div class="row">
                <cc1:ExportPanel ID="exportpanel1" runat="server">
                    <asp:GridView ID="gvPropType" runat="server" EmptyDataText="No Tenant Details Found."
                        CssClass="table table-condensed table-bordered table-hover table-striped"
                        AllowPaging="True" AllowSorting="false" PageSize="8" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="Sno" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" runat="server" Text='<%# Eval("SNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tenant Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Eval("TEN_CODE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tenant Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("TEN_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tenant Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("TEN_EMAIL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Occupied Area">
                                <ItemTemplate>
                                    <asp:Label ID="lblarea" runat="server" Text='<%# Eval("TEN_OCCUPIEDAREA") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Maintenance Fee">
                                <ItemTemplate>
                                    <asp:Label ID="lblfee" runat="server" Text='<%# Eval("TEN_MAINT_FEE", "{0:c2}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tenant Rent">
                                <ItemTemplate>
                                    <asp:Label ID="lblRent" runat="server" Text='<%# Eval("TEN_RENT_PER_SFT", "{0:c2}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("TEN_REMARKS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <a href='frmEditTenantDetails.aspx?ID=<%# Eval("SNO") %>'>EDIT</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgDelete" ImageUrl="~/images/delete.gif" CommandArgument='<%# Eval("SNO") %>'
                                                CommandName="Delete" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="Button1" CommandName="Delete" CommandArgument="<%# Container.DataItemIndex %>" Text="Delete" runat="server" />
                                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="Button1" ConfirmText="Sure want to delete this entry?"></asp:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:ButtonField Text="DELETE" CommandName="Delete" />--%>
                        </Columns>
                        <RowStyle HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </cc1:ExportPanel>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">

                <asp:Button ID="btnexporttoexcel" runat="Server" Text="Export to Excel" Visible="false" CssClass="btn btn-primary custom-button-color" />

            </div>
        </div>
    </div>
</div>
