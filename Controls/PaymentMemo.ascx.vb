﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_PaymentMemo
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_REQDETAILS_PAYMENT")
            gvitems1.DataSource = sp.GetDataSet()
            gvitems1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
        gvitems1.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvitems1.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvitems1.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As String = lblID.Text
            txtStore.Text = id
            BindDetails(txtStore.Text)
            gvItems.Visible = True
            BindpayDetails()
            gvpaydetails.Visible = True
            pnlpaydetails.Visible = True
            pnlpayment.Visible = True
            pnluploaddoc.Visible = True
            BindCategory()
        End If
    End Sub
    Private Sub BindDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_DETAILS_BYREQ")
            sp.Command.AddParameter("@REQUESTID", id, DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
            pnljourneydetails.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCategory()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CATEGORY_GET")
            ddlCategory.DataSource = sp.GetDataSet()
            ddlCategory.DataTextField = "CATEGORY"
            ddlCategory.DataValueField = "SNO"
            ddlCategory.DataBind()
            ddlCategory.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindDetails(txtStore.Text)
    End Sub

    Protected Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID1"), Label)
            Dim id As Integer = Convert.ToInt32(lblID.Text)

        End If
    End Sub
    Private Sub BindpayDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_GET_PAYDETAILS_SJP")
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            gvpaydetails.DataSource = sp.GetDataSet()
            gvpaydetails.DataBind()
            pnlpaydetails.Visible = True

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnpay_Click(sender As Object, e As EventArgs) Handles btnpay.Click
        ' Try
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_ADD_PAYEMNT_SJP")
        sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
        sp.Command.AddParameter("@AMOUNT", Convert.ToDouble(txtamount.Text), DbType.Double)
        sp.Command.AddParameter("@COMMENT", txtcomment.Text, DbType.String)
        sp.Command.AddParameter("@PAYTYPE", "Cash", DbType.String)
        sp.Command.AddParameter("@BANKNAME", "Null", DbType.String)
        sp.Command.AddParameter("@aur_id", Session("UID"), DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks2.aspx?id=65")
        'Catch ex As Exception
        'Response.Write(ex.Message)
        ' End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.SelectedIndexChanged
        Try
            If rbActions.SelectedItem.Text = "Cash" Then
                pnlcash.Visible = True
                pnlcheck.Visible = False
                clear()
            ElseIf rbActions.SelectedItem.Text = "Check" Then
                pnlcheck.Visible = True
                pnlcash.Visible = False
                clear()
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub clear()
        Try
            txtamount.Text = ""
            txtcomment.Text = ""
            txtbankname.Text = ""
            txtcommentcheck.Text = ""

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnpaycheck_Click(sender As Object, e As EventArgs) Handles btnpaycheck.Click
        ' Try
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_ADD_PAYEMNT_SJP")
        sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
        sp.Command.AddParameter("@AMOUNT", Convert.ToDouble(0), DbType.Double)
        sp.Command.AddParameter("@COMMENT", txtcommentcheck.Text, DbType.String)
        sp.Command.AddParameter("@PAYTYPE", "Check", DbType.String)
        sp.Command.AddParameter("@BANKNAME", txtbankname.Text, DbType.String)
        sp.Command.AddParameter("@aur_id", Session("UID"), DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks2.aspx?id=65")
        'Catch ex As Exception
        'Response.Write(ex.Message)
        ' End Try
    End Sub

    Protected Sub gvpaydetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvpaydetails.PageIndexChanging
        gvpaydetails.PageIndex = e.NewPageIndex()
        BindpayDetails()
    End Sub

    Protected Sub gvpaydetails_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvpaydetails.RowCommand

    End Sub

    Protected Sub gvpaydetails_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvpaydetails.RowEditing
        gvpaydetails.EditIndex = e.NewEditIndex
        BindpayDetails()
    End Sub

    Protected Sub gvpaydetails_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles gvpaydetails.RowCancelingEdit
        gvpaydetails.EditIndex = -1
        BindpayDetails()
    End Sub

    Protected Sub gvpaydetails_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles gvpaydetails.RowUpdating
        Dim row As GridViewRow = gvpaydetails.Rows(e.RowIndex)
        Dim lblID1 As Label = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("lblID2"), Label)
        Dim txtstay As TextBox = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("txtstay"), TextBox)
        Dim txtfood As TextBox = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("txtfood"), TextBox)
        Dim txtentertainment As TextBox = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("txtentertainment"), TextBox)
        Dim txtexpfamily As TextBox = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("txtexpfamily"), TextBox)
        Dim txtconveyance As TextBox = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("txtconveyance"), TextBox)
        Dim txttelephone As TextBox = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("txttelephone"), TextBox)
        Dim txtlaundry As TextBox = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("txtlaundry"), TextBox)
        Dim txtreamount As TextBox = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("txtreamount"), TextBox)
        Dim txtkms As TextBox = DirectCast(gvpaydetails.Rows(e.RowIndex).FindControl("txtkms"), TextBox)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_INSERT_PAYDETAILS")
        sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
        sp.Command.AddParameter("@DETAILSID", Convert.ToInt32(lblID1.Text), DbType.Int32)
        sp.Command.AddParameter("@CREATED_BY", Session("UID"), DbType.String)
        If txtstay.Text <> "" Then
            sp.Command.AddParameter("@STAY", Convert.ToInt32(txtstay.Text), DbType.Int32)
        Else
            sp.Command.AddParameter("@STAY", Convert.ToInt32(0), DbType.Int32)
        End If

        If txtfood.Text <> "" Then
            sp.Command.AddParameter("@FOOD", Convert.ToInt32(txtfood.Text), DbType.Int32)
        Else
            sp.Command.AddParameter("@FOOD", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtentertainment.Text <> "" Then
            sp.Command.AddParameter("@ENTERTAINMENT", Convert.ToInt32(txtentertainment.Text), DbType.Int32)
        Else
            sp.Command.AddParameter("@ENTERTAINMENT", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtexpfamily.Text <> "" Then
            sp.Command.AddParameter("@EXPOFFAMILY", Convert.ToInt32(txtexpfamily.Text), DbType.Int32)
        Else
            sp.Command.AddParameter("@EXPOFFAMILY", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtreamount.Text <> "" Then
            sp.Command.AddParameter("@REMAMOUNT", Convert.ToInt32(txtreamount.Text), DbType.Int32)
        Else
            sp.Command.AddParameter("@REMAMOUNT", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtconveyance.Text <> "" Then
            sp.Command.AddParameter("@CONVEYANCE", Convert.ToInt32(txtconveyance.Text), DbType.Int32)
        Else
            sp.Command.AddParameter("@CONVEYANCE", Convert.ToInt32(0), DbType.Int32)
        End If
        If txttelephone.Text <> "" Then
            sp.Command.AddParameter("@TELEPHONE", Convert.ToInt32(txttelephone.Text), DbType.Int32)
        Else
            sp.Command.AddParameter("@TELEPHONE", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtlaundry.Text <> "" Then
            sp.Command.AddParameter("@LAUNDRYEXP", Convert.ToInt32(txtlaundry.Text), DbType.Int32)
        Else
            sp.Command.AddParameter("@LAUNDRYEXP", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtkms.Text <> "" Then
            sp.Command.AddParameter("@NOOFKMS", Convert.ToInt32(txtkms.Text), DbType.Int32)
        Else
            sp.Command.AddParameter("@NOOFKMS", Convert.ToInt32(0), DbType.Int32)
        End If
        sp.ExecuteScalar()
        gvpaydetails.EditIndex = -1
        BindpayDetails()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            BindGridDoc()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategory.SelectedIndexChanged
        If ddlCategory.SelectedIndex > 0 Then
            BindDocType()
        Else
            ddlDocType.Items.Clear()
            ddlDocType.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlDocType.SelectedIndex = 0
        End If

    End Sub
    Private Sub BindDocType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOC_TYPE_CATEGORY")
            sp.Command.AddParameter("@CATEGORY", ddlCategory.SelectedItem.Value, DbType.Int32)
            ddlDocType.DataSource = sp.GetDataSet()
            ddlDocType.DataTextField = "DOC_TYPE"
            ddlDocType.DataValueField = "SNO"
            ddlDocType.DataBind()
            ddlDocType.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGridDoc()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOCUMENTS_VIEW_SJP")
            sp.Command.AddParameter("@CATEGORY", ddlCategory.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@DOC_TYPE", ddlDocType.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvdoc.DataSource = ds
            gvdoc.DataBind()
            For i As Integer = 0 To gvdoc.Rows.Count - 1
                Dim hypbtnDocs As HyperLink = CType(gvdoc.Rows(i).FindControl("hypbtnDocs"), HyperLink)
                Dim lblViewFilename As Label = CType(gvdoc.Rows(i).FindControl("lblViewFilename"), Label)
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & lblViewFilename.Text
                hypbtnDocs.NavigateUrl = filePath
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvdoc_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvdoc.PageIndexChanging
        gvdoc.PageIndex = e.NewPageIndex
        BindGridDoc()
    End Sub
End Class
