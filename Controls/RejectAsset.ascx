<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RejectAsset.ascx.vb"
    Inherits="Controls_RejectAsset" %>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Asset Rejection
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Asset Rejection</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="center" valign="top" height="100%">
                <table width="100%" cellpadding="2px">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Asset Name:</td>
                        <td align="left">
                            <asp:Label ID="lblAstName" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            Asset Code:</td>
                        <td align="left">
                            <asp:Label ID="lblAstCode" runat="server"></asp:Label></td>
                    </tr>
                    <tr runat="Server">
                        <td align="left">
                            Allocated Date:</td>
                        <td align="left">
                            <asp:Label ID="lblAllocatedDate" runat="server"></asp:Label></td>
                    </tr>
                    <tr id="tr3" runat="Server">
                        <td align="left">
                            Remarks:</td>
                        <td align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Button ID="btnAssetReject" Text="Submit" runat="server" CssClass="button" />
                            <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="button" />
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
