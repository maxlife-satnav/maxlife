Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_UpdateWorkRequest
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        ' if sts=Completed, paid amt shoud be = estd amount 
        Dim totalPaid As Decimal = 0
        If (ddlwstatus.SelectedValue = 2) Then
            If gvwrequest.Rows.Count > 0 Then
                For i = 0 To gvwrequest.Rows.Count - 1
                    Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblpamount"), Label)
                    totalPaid = totalPaid + CDec(lblstatus.Text)
                Next
                If (CDec(totalPaid + txtpamount.Text)).Equals(CDec(txtamount.Text)) Then
                Else
                    lblmsg.Text = "Total paid amount Should be Equal to Estimated Amount"
                    lblmsg.Visible = True
                    Return
                End If
            Else
                If (CDec(txtpamount.Text)).Equals(CDec(txtamount.Text)) Then
                Else
                    lblmsg.Text = "Total paid amount Should be Equal to Estimated Amount"
                    lblmsg.Visible = True
                    Return
                End If
            End If
        Else
            If gvwrequest.Rows.Count > 0 Then
                For i = 0 To gvwrequest.Rows.Count - 1
                    Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblpamount"), Label)
                    totalPaid = totalPaid + CDec(lblstatus.Text)
                Next
                If (CDec(totalPaid + txtpamount.Text)) > (CDec(txtamount.Text)) Then

                    lblmsg.Text = "Total paid amount Should be Less than or Equal to Estimated Amount"
                    lblmsg.Visible = True
                    Return
                End If
            Else
                If (CDec(txtpamount.Text)) > (CDec(txtamount.Text)) Then
                    lblmsg.Text = "Total paid amount Should be Less than or Equal to Estimated Amount"
                    lblmsg.Visible = True
                    Return
                End If
            End If
        End If
        If txtStartDate.Text > txtExpiryDate.Text Then
            lblmsg.Text = "End Date Should be more than Start Date"
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "update_workreq")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            sp.Command.AddParameter("@START_DATE", txtStartDate.Text, DbType.Date)
            sp.Command.AddParameter("@END_DATE", txtExpiryDate.Text, DbType.Date)
            sp.Command.AddParameter("@PAID_AMOUNT", txtpamount.Text, DbType.Decimal)
            sp.Command.AddParameter("@OUTSTANDING_AMOUNT", txtoamount.Text, DbType.Decimal)
            sp.Command.AddParameter("@STATUS", ddlstatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
            sp.ExecuteScalar()
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_STATUS")
            sp1.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            sp1.Command.AddParameter("@REQUEST_STATUS", ddlwstatus.SelectedItem.Value, DbType.Int32)
            sp1.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=13")
            'lblMsg.Text = "WorkRequest Updated Succesfully"
            fillgrid()
            Cleardata()
        End If


    End Sub
    Public Sub fillgrid()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_WORKREQ1_DETAILS")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        gvwrequest.DataSource = sp.GetDataSet()
        gvwrequest.DataBind()
        For i As Integer = 0 To gvwrequest.Rows.Count - 1
            Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "0" Then
                lblstatus.Text = "Pending"
            ElseIf lblstatus.Text = "1" Then
                lblstatus.Text = "InProgress"
            Else
                lblstatus.Text = "2"
                lblstatus.Text = "Completed"

            End If
        Next

    End Sub
    Public Sub Cleardata()
        getrequest()
        ddlWorkRequest.SelectedValue = 0
        txtStartDate.Text = ""
        txtExpiryDate.Text = ""
        txtpamount.Text = ""
        txtoamount.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtRemarks.Text = ""
        ddlstatus.SelectedIndex = -1
        ddlwstatus.SelectedIndex = -1
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getrequest()

            'txtStartDate.Attributes.Add("onClick", "displayDatePicker('" + txtStartDate.ClientID + "')")
            'txtStartDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            'txtExpiryDate.Attributes.Add("onClick", "displayDatePicker('" + txtExpiryDate.ClientID + "')")
            'txtExpiryDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        End If
        txtStartDate.Attributes.Add("readonly", "readonly")
        txtExpiryDate.Attributes.Add("readonly", "readonly")
    End Sub
    Public Sub getrequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETWORKREQ")
        sp.Command.AddParameter("@User", Session("UID"), DbType.String)
        ddlWorkRequest.DataSource = sp.GetDataSet()
        ddlWorkRequest.DataTextField = "PN_WORKREQUEST_REQ"
        ddlWorkRequest.DataBind()
        ddlWorkRequest.Items.Insert(0, New ListItem("--Select WorkRequest--", "0"))
    End Sub

    Public Sub Clear()
        txtStartDate.Text = ""
        txtExpiryDate.Text = ""
        txtpamount.Text = ""
        txtoamount.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtRemarks.Text = ""
        ddlstatus.SelectedIndex = -1
        ddlwstatus.SelectedIndex = -1
        txtcity.Text = ""
        txtproptype.Text = ""
        txtProperty.Text = ""
    End Sub

    Protected Sub ddlWorkRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkRequest.SelectedIndexChanged
        If ddlWorkRequest.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETWORKDETAILS")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtWorkTitle.Text = ds.Tables(0).Rows(0).Item("WORK_TITLE")
                txtWorkSpec.Text = ds.Tables(0).Rows(0).Item("WORK_SPECIFICATIONS")
                txtamount.Text = ds.Tables(0).Rows(0).Item("ESTIMATED_AMOUNT")
                txtcity.Text = ds.Tables(0).Rows(0).Item("PN_BLDG_CODE")
                txtLocation.Text = ds.Tables(0).Rows(0).Item("pn_cty_code")
                txtProperty.Text = ds.Tables(0).Rows(0).Item("PN_LOC_ID")
                txtproptype.Text = ds.Tables(0).Rows(0).Item("PROP_CODE")
                Dim status As Integer = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
                If status = 0 Then
                    ddlwstatus.SelectedValue = 0
                ElseIf status = 1 Then
                    ddlwstatus.SelectedValue = 1
                Else
                    ddlwstatus.SelectedValue = 2
                End If
            Else
                Clear()
            End If
            fillgrid()
        Else
            Clear()
            fillgrid()
        End If
    End Sub

    Protected Sub txtpamount_TextChanged(sender As Object, e As EventArgs) Handles txtpamount.TextChanged
        If txtpamount.Text <> String.Empty Then
            lblmsg.Text = String.Empty
            Dim totalPaid As Decimal = 0
            If IsNumeric(txtpamount.Text) Then
                If gvwrequest.Rows.Count > 0 Then
                    For i = 0 To gvwrequest.Rows.Count - 1
                        Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblpamount"), Label)
                        totalPaid = totalPaid + CDec(lblstatus.Text)
                    Next
                    If (totalPaid + txtpamount.Text) <= txtamount.Text Then
                        txtoamount.Text = txtamount.Text - (totalPaid + txtpamount.Text)
                        txtoamount.ReadOnly = True
                        'If CInt(txtamount.Text) >= CInt(txtpamount.Text) Then
                        'txtoamount.Text = (CInt(txtamount.Text) - CInt(txtpamount.Text)).ToString()
                    Else
                        lblmsg.Text = "The Paid amount should be less than the Estimated amount"
                        lblmsg.Visible = True
                    End If
                Else
                    If CDec(txtamount.Text) >= CDec(txtpamount.Text) Then
                        txtoamount.Text = (CDec(txtamount.Text) - CDec(txtpamount.Text)).ToString()
                        txtoamount.ReadOnly = True
                    End If
                End If
            Else
                lblmsg.Text = "Please enter numerics in Paid amount"
                lblmsg.Visible = True
            End If
        End If
    End Sub
End Class
