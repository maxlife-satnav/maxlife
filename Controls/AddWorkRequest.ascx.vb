Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AddWorkRequest
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click


        Dim WorkReq As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMdd") + "/" + Session("uid")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_WORK_REQUEST")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", WorkReq, DbType.String)
        sp.Command.AddParameter("@PN_BLDG_CODE", ddlcity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@PN_LOC_ID", ddlProperty.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@PN_CTY_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@WORK_TYPE", ddlReq.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@WORK_TITLE", txtWorkTitle.Text, DbType.String)
        sp.Command.AddParameter("@WORK_SPECIFICATIONS", txtWorkSpec.Text, DbType.String)
        sp.Command.AddParameter("@ESTIMATED_AMOUNT", txtamount.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_NAME", txtVendor.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_PHONE", txtPhno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_DETAILS", txtAddress.Text, DbType.String)

        sp.Command.AddParameter("@REQUEST_RAISED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@Prop_Type", ddlproptype.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=10")
        'lblMsg.Text = "Work Request Added Succesfully"
        Cleardata()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            BindPropType()
            BindCity()

            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_DETAILS_FOR_PI")
            'sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            'Dim ds As New DataSet()
            'ds = sp.GetDataSet()
            'ddlcity.SelectedValue = ds.Tables(0).Rows(0).Item("CITY_ID")
            'ddlcity.Enabled = False

        End If
    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlcity.DataSource = sp.GetDataSet()
        ddlcity.DataTextField = "CTY_NAME"
        ddlcity.DataValueField = "CTY_CODE"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlcity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", ""))

        Catch ex As Exception

        End Try

    End Sub


    Public Sub Cleardata()
        ddlProperty.SelectedValue = 0
        'ddlBuilding.SelectedValue = 0
        'ddlReq.SelectedIndex = -1
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtVendor.Text = ""
        txtPhno.Text = ""
        txtRemarks.Text = ""
        txtAddress.Text = ""
        ' ddlstatus.SelectedIndex = -1

    End Sub

    Protected Sub ddlproptype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlproptype.SelectedIndexChanged
        If ddlproptype.SelectedIndex > 0 Then
            BindProp()
        Else
            ddlProperty.Items.Clear()
            ddlProperty.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlProperty.SelectedIndex = 0
        End If
    End Sub

    Private Sub BindProp()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROP")
            sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@dummy", ddlcity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@user", Session("UID"), DbType.String)
            ddlProperty.DataSource = sp.GetDataSet()
            ddlProperty.DataTextField = "PN_NAME"
            ddlProperty.DataValueField = "BDG_ID"
            ddlProperty.DataBind()

            ddlProperty.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlcity.SelectedIndexChanged
        BindCityLoc()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlcity.SelectedIndex > 0 Then
            BindProp()
        Else

            ddlproptype.SelectedIndex = 0
            ddlProperty.Items.Clear()
            ddlProperty.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlProperty.SelectedIndex = 0
        End If
    End Sub
End Class
