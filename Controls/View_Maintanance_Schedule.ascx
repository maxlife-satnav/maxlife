<%@ Control Language="VB" AutoEventWireup="false" CodeFile="View_Maintanance_Schedule.ascx.vb"
    Inherits="Controls_View_Maintanance_Schedule" %>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">View Maintenance Schedule Details
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>View Maintenance Schedule Details</strong>
            </td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <table width="100%" cellpadding="2px">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:GridView ID="gv_MainSch" runat="server" EmptyDataText="No Requests Found." AllowPaging="true"
                                PageSize="10" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Request Id" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPVM_BKG_TS" runat="server" Text='<%#Eval("PVM_BKG_TS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Name" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPVM_AVR_NAME" runat="server" Text='<%#Eval("PVM_AVR_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maintanance Plan" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPVM_PLAN_FREQ" runat="server" Text='<%#Eval("PVM_PLAN_FREQ") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Date" ItemStyle-Wrap="False" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPVM_PLAN_FDATE" runat="server" Text='<%#Eval("PVM_PLAN_FDATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date" ItemStyle-Wrap="False" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPVM_PLAN_TDATE" runat="server" Text='<%#Eval("PVM_PLAN_TDATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("PVM_BKG_TS") %>'
                                                CommandName="Edit">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <a href="#" onclick="showPopWin('ViewMaintPlanDtls.aspx?REQID=<%# Eval("PVM_BKG_TS") %>',750,480,null)">
                                                <asp:Label ID="lbntreqid" runat="server" Text='View'></asp:Label>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkUpdate" runat="server" CommandArgument='<%#Eval("PVM_BKG_TS") %>'
                                                CommandName="Update">Update</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
