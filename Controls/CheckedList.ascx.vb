Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_CheckedList
    Inherits System.Web.UI.UserControl

    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim ArrList1 As New ArrayList()
            ArrList1.Add(New Items(1, "Willingness of the staff member, stating that he has identified the flat and would like to take possession of the same mentioning clearly the date of occupation"))
            ArrList1.Add(New Items(2, "Incase the proposal is exceeding the entitlement of the staff during any period of the leased arrangement, an undertaking needs to be submitted by the staff that the excess over the entitlement may be deducted from their respective savings bank account."))
            ArrList1.Add(New Items(3, "Undertaking to be duly acknowledged by the staff member wherein he states that the premises being provided to him by the Bank, would be kept in proper condition and he would comply with all the necessary formalities. This undertaking is to be acknowledged by the staff at the time of collecting the security deposit to be handedover to the landlord, i.e. prior to taking the possession of the said premises."))
            gv.DataSource = ArrList1
            gv.DataBind()

            Dim ArrList2 As New ArrayList()
            ArrList2.Add(New Items(1, "Offer letter from the landlord stating that he is willing to offer his flat on leave and licence basis to WIPRO for the concerned staff.( A draft copy of the offer is as per ANNEXURE I )"))
            ArrList2.Add(New Items(2, "Proof of ownership to be submitted by the landlord ( which may comprise either Share Certificate of the society / sale agreement / society maintenance bill and receipt ---- any one) "))
            ArrList2.Add(New Items(3, "Incase landlord is not present in Mumbai or is not able to execute the agreement, for any reasons, then the Power of Attorney holder should submit a copy of Power of Attorney. The POA should either be notarized or registered. The POA should also bear photograph of the POA holder."))
            ArrList2.Add(New Items(4, "Payment instructions to be given by landlord, incase there is more than one landlord of the said flat. It may be mentioned in the Offer letter or a separate letter to that effect may be given.( Please note that the TDS would be deducted on the entire amount of rent � irrespective of the number of landlords), unless specifically stated and an affidavit to that effect be submitted. "))
            gv1.DataSource = ArrList2
            gv1.DataBind()


            Dim ArrList3 As New ArrayList()
            ArrList3.Add(New Items(1, "A letter from the broker stating that the premises being taken by our staff member is routed through him and he would submit the bill at a later date."))
            ArrList3.Add(New Items(2, "Brokerage bill for the concerned premises  (deal)"))
            gv2.DataSource = ArrList3
            gv2.DataBind()



        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim reqid As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + Session("UID")
            lblMsg.Text = ""
            Grid1(reqid)
            Grid2(reqid)
            Grid3(reqid)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_LABELMASTER_RMAPPROVAL1")
            sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
            sp.ExecuteScalar()
            Response.Redirect("frmAddLeaseUser.aspx")

            'Cleardata()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Cleardata()
        For i As Integer = 0 To gv.Rows.Count - 1
            Dim rdbtn As RadioButtonList = DirectCast(gv.Rows(i).FindControl("rdbtn"), RadioButtonList)
            rdbtn.SelectedIndex = -1
        Next
        For i As Integer = 0 To gv1.Rows.Count - 1
            Dim rdbtn1 As RadioButtonList = DirectCast(gv1.Rows(i).FindControl("rdbtn1"), RadioButtonList)
            rdbtn1.SelectedIndex = -1
        Next
        For i As Integer = 0 To gv2.Rows.Count - 1
            Dim rdbtn2 As RadioButtonList = DirectCast(gv2.Rows(i).FindControl("rdbtn2"), RadioButtonList)
            rdbtn2.SelectedIndex = -1
        Next
    End Sub
    Private Sub Grid1(ByVal reqid As String)



        For i As Integer = 0 To gv.Rows.Count - 1
            '    Dim rdbtn As RadioButtonList = DirectCast(gv.Rows(i).FindControl("rdbtn"), RadioButtonList)
            '    If rdbtn.SelectedIndex >= 0 Then
            '        lblMsg.Text = ""

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT")
            sp.Command.AddParameter("@AXIS_SNO", CType(gv.Rows(i).FindControl("lbl1"), Label).Text, DbType.Int32)
            sp.Command.AddParameter("@AXIS_LABEL", CType(gv.Rows(i).FindControl("lbl2"), Label).Text, DbType.String)
            sp.Command.AddParameter("@_STATUS", CType(gv.Rows(i).FindControl("rdbtn"), RadioButtonList).SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
            sp.Command.AddParameter("@SETS", "SET1", DbType.String)
            sp.ExecuteScalar()
            '    Else
            'lblMsg.Text = "Please select Yes or No"
            '    End If
        Next

    End Sub
    Private Sub Grid2(ByVal reqid As String)


        For i As Integer = 0 To gv1.Rows.Count - 1
            '    Dim rdbtn1 As RadioButtonList = DirectCast(gv1.Rows(i).FindControl("rdbtn1"), RadioButtonList)
            '    If rdbtn1.SelectedIndex >= 0 Then

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT1")
            sp.Command.AddParameter("@AXIS_SNO", CType(gv1.Rows(i).FindControl("lbl3"), Label).Text, DbType.Int32)
            sp.Command.AddParameter("@AXIS_LABEL", CType(gv1.Rows(i).FindControl("lbl4"), Label).Text, DbType.String)
            sp.Command.AddParameter("@_STATUS", CType(gv1.Rows(i).FindControl("rdbtn1"), RadioButtonList).SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
            sp.Command.AddParameter("@SETS", "SET2", DbType.String)
            sp.ExecuteScalar()
            '    Else
            ''Flag1 = False
            'lblMsg.Text = "Please select Yes or No"
            '    End If
        Next



    End Sub
    Private Sub Grid3(ByVal reqid As String)


        For i As Integer = 0 To gv2.Rows.Count - 1
            '    Dim rdbtn2 As RadioButtonList = DirectCast(gv2.Rows(i).FindControl("rdbtn2"), RadioButtonList)
            '    If rdbtn2.SelectedIndex >= 0 Then

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_REPORT_DOCUMENT2")
            sp.Command.AddParameter("@AXIS_SNO", CType(gv2.Rows(i).FindControl("lbl5"), Label).Text, DbType.Int32)
            sp.Command.AddParameter("@AXIS_LABEL", CType(gv2.Rows(i).FindControl("lbl6"), Label).Text, DbType.String)
            sp.Command.AddParameter("@_STATUS", CType(gv2.Rows(i).FindControl("rdbtn2"), RadioButtonList).SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
            sp.Command.AddParameter("@SETS", "SET3", DbType.String)
            sp.ExecuteScalar()
            ' Else

            'lblMsg.Text = "Please select Yes or No"
            '    End If
        Next


    End Sub
End Class

Public Class Items
    Dim _SNO As Integer
    Dim _ItemString As String

    Public Sub New()
        _SNO = 0
        _ItemString = String.Empty
    End Sub

    Public Sub New(ByVal SNO As Integer, ByVal ItemString As String)
        _SNO = SNO
        _ItemString = ItemString
    End Sub
    Public Property SNO() As Integer
        Get
            Return _SNO
        End Get
        Set(ByVal value As Integer)
            _SNO = value
        End Set
    End Property
    Public Property ItemString() As String
        Get
            Return _ItemString
        End Get
        Set(ByVal value As String)
            _ItemString = value
        End Set
    End Property
End Class
