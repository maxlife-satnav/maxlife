<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApproveRequest.ascx.vb"
    Inherits="Controls_ApproveRequest" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Work Request<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvProperty" runat="server" ControlToValidate="ddlWorkRequest"
                    Display="None" ErrorMessage="Please Select Work Request" ValidationGroup="Val1"
                    InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlWorkRequest" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    City <span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50"
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Location <span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control" MaxLength="50"
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Property Type <span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtproptype" runat="server" CssClass="form-control"
                        MaxLength="50" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Property<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtproperty" runat="server" CssClass="form-control"
                        MaxLength="50" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Title<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control"
                        MaxLength="50" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Specifications<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control"
                        Rows="3" TextMode="MultiLine" MaxLength="500" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Estimated Amount<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Vendor Name<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control"
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Status<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtstatus" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Remarks<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                        Rows="3" MaxLength="250"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnApprove" CssClass="btn btn-primary custom-button-color" runat="server" Text="Approve" ValidationGroup="Val1"
                CausesValidation="true" />
            <asp:Button ID="btnReject" CssClass="btn btn-primary custom-button-color" runat="server" Text="Reject" ValidationGroup="Val1"
                CausesValidation="true" />
        </div>
    </div>
</div>
