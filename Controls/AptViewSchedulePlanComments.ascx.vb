﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AptViewSchedulePlanComments
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            BindDep()
            BindRM()
            BindDetails()
        End If
    End Sub
    Private Sub BindDep()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDep.DataSource = sp.GetDataSet()
            ddlDep.DataTextField = "DEP_NAME"
            ddlDep.DataValueField = "DEP_CODE"
            ddlDep.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRM()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEES_RM")
            'sp.Command.AddParameter("@dep_code", ddlDep.SelectedItem.Value, DbType.String)
            ddlRM.DataSource = sp.GetDataSet()
            ddlRM.DataTextField = "AUR_FIRST_NAME"
            ddlRM.DataValueField = "AUR_ID"
            ddlRM.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_GETSCHEDULEJOURNEY_BYREQID")
            sp.Command.AddParameter("@REQUESTID", Request.QueryString("rid"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                lblreqid.Text = ds.Tables(0).Rows(0).Item("REQUESTID")
                lblreqdate.Text = ds.Tables(0).Rows(0).Item("REQUESTED_ON")
                lblstatus.Text = ds.Tables(0).Rows(0).Item("STA_TITLE")
                txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtAssociateID.Text = ds.Tables(0).Rows(0).Item("AUR_NO")
                ddlDep.ClearSelection()
                ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
                ddlRM.ClearSelection()
                ddlRM.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True
                txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                gvItems.DataSource = ds.Tables(0)
                gvItems.DataBind()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Protected Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As String = lblID.Text
            lblsno.Text = lblID.Text
            pnlview.Visible = True
            pnladdcomments.Visible = False
            BindComments()
            lblMsg.Text = ""
        ElseIf e.CommandName = "Add" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            pnlview.Visible = False
            pnladdcomments.Visible = True
            lblsno.Text = lblID.Text
        End If
    End Sub
    Private Sub BindComments()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_GET_COMMENTS_SCHEDULEJOURNEY")
            sp.Command.AddParameter("@REQUESTID", Request.QueryString("rid"), DbType.String)
            sp.Command.AddParameter("@DETAILSID", Convert.ToInt32(lblsno.Text), DbType.Int32)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                gvviewcomments.DataSource = ds.Tables(0)
                gvviewcomments.DataBind()
            Else
                gvviewcomments.EmptyDataText = "Sorry! No Available Records..."
                gvviewcomments.DataBind()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnaddcommnet_Click(sender As Object, e As EventArgs) Handles btnaddcommnet.Click
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_ADD_SCHEDULECOMMENT")
            sp.Command.AddParameter("@REQUESTID", Request.QueryString("rid"), DbType.String)
            sp.Command.AddParameter("@COMMENTED_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@DETAILS_ID", Convert.ToInt32(lblsno.Text), DbType.Int32)
            If (txtcomment.Text <> "") Then
                sp.Command.AddParameter("@COMMENT", txtcomment.Text, DbType.String)
            Else
                sp.Command.AddParameter("@COMMENT", "NA", DbType.String)
            End If
            sp.ExecuteScalar()
            lblMsg.Text = "Comments Added Successfully."
            txtcomment.Text = ""
            ' Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=46")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        Response.Redirect("~/EFM/EFM_Webfiles/frmViewSchedulePlan.aspx")
    End Sub

    Protected Sub gvviewcomments_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvviewcomments.PageIndexChanging
        gvviewcomments.PageIndex = e.NewPageIndex()
        BindComments()
    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindDetails()
    End Sub
End Class
