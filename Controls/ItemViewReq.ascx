<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ItemViewReq.ascx.vb"
    Inherits="Controls_ItemViewReq" %>
<div class="row" style="margin-top: 10px">
   <div class="col-md-12">       
            <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                EmptyDataText="No Requests Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>

                      <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                             <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmItemViewReqDetails.aspx?RID={0}") %>'
                                 Text='<%# Eval("AIR_REQ_ID")%> '></asp:HyperLink>
                            <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                   <%-- <asp:BoundField DataField="AIR_REQ_ID" HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left" />--%>
                    <asp:BoundField DataField="AUR_NAME" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                    <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                    <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                <%--    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                        <ItemTemplate>
                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmItemViewReqDetails.aspx?RID={0}") %>'
                                Text="View Details"></asp:HyperLink>
                            <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>





