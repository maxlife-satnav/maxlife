Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports System.Data.OleDb
Partial Class Controls_UpdateLeasePayments
    Inherits System.Web.UI.UserControl
    Dim strFileType As String = ""
    Dim strpath As String = ""
    Dim connString As String = ""
    Dim str As String = ""
   
    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"LEASE_PAYMENT_DOWNLOADABLE_FORMAT")
        Dim gv As New GridView
        gv.DataSource = sp.GetDataSet()
        gv.DataBind()
        Export("LEASE_PAYMENT_DETAILS_" & DateTime.Now.ToString("yyyyMMddHHmmss") & ".xls", gv)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub

    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Public Sub writetodb(ByVal strNewPath As String, ByVal strFileType As String)
        Try
            'If strFileType.Trim() = ".xls" Then
            '    connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strNewPath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            'ElseIf strFileType.Trim() = ".xlsx" Then
            '    connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strNewPath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            'End If
            If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strNewPath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Upload excel files only..."
            End If
            str = "Select * from [Payment_Template$]"
            'Create connection object
            Dim conn As New OleDbConnection(connString)
            'Open connection
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Dim cmd As New OleDbCommand(str, conn)
            Dim ds As New DataSet()
            Dim da As New OleDbDataAdapter(cmd)
            da.Fill(ds, "sheet1")
            da.Dispose()
            conn.Close()
            conn.Dispose()
            Dim objAdapter As SqlDataAdapter = Nothing
            Dim objDBRow As DataRow = Nothing
            Dim objDSDBTable As New DataSet()
            Dim ObjCmdBuilder As SqlCommandBuilder = Nothing
            objAdapter = New SqlDataAdapter("SELECT * FROM " & Session("TENANT") & "." & "PN_LEASE_RECEIPTS", ConfigurationManager.ConnectionStrings("CSAmantraFAM").ToString())
            objAdapter.Fill(objDSDBTable, "PN_LEASE_RECEIPTS")
            Dim dt As DataTable = objDSDBTable.Tables(0)
            For Each objDataRow As DataRow In ds.Tables(0).Rows
                objDBRow = dt.NewRow()
                objDBRow("LANDLORD_NUMBER") = objDataRow(0).ToString()
                objDBRow("LANDLORD_NAME") = objDataRow(1).ToString()
                objDBRow("LEASE_NAME") = objDataRow(2).ToString()
                objDBRow("PROPERTY_TYPE") = objDataRow(3).ToString()
                objDBRow("CITY") = objDataRow(4).ToString()
                objDBRow("PAYMENT_MODE") = objDataRow(5).ToString()
                objDBRow("CHEQUE_NUMBER") = objDataRow(6).ToString()
                objDBRow("ISSUING_BANK") = objDataRow(7).ToString()
                objDBRow("ACCOUNT_NUMBER") = objDataRow(8).ToString()
                objDBRow("DEPOSITED_BANK") = objDataRow(9).ToString()
                objDBRow("IFSC_CODE") = objDataRow(10).ToString()
                objDBRow("PAID_DATE") = Convert.ToDateTime(objDataRow(11).ToString())
                objDBRow("PAID_AMOUNT") = Convert.ToDouble(objDataRow(12).ToString())
                objDBRow("REMAKRS") = objDataRow(13).ToString()
                objDBRow("AUR_ID") = objDataRow(14).ToString()
                objDBRow("BRANCH_NAME") = objDataRow(15).ToString()
                objDBRow("FROM_DATE") = Convert.ToDateTime(objDataRow(16).ToString())
                objDBRow("TO_DATE") = Convert.ToDateTime(objDataRow(17).ToString())
                dt.Rows.Add(objDBRow)
            Next
            ObjCmdBuilder = New SqlCommandBuilder(objAdapter)
            objAdapter.Update(objDSDBTable, "PN_LEASE_RECEIPTS")
            lblMsg.Text = "Payment Details Added Successfully"
        Catch ex As Exception
            'Response.Write(ex.Message)
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            lblMsg.Text = ""
        End If
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        If fpBrowseDoc.HasFile Then
            strFileType = System.IO.Path.GetExtension(fpBrowseDoc.FileName).ToString().ToLower()
            'Check file type
            If strFileType = ".xlsx" Then

                strpath = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & DateTime.Now.ToString("ddMMyyyyHHmmss") & "_" + fpBrowseDoc.FileName
                fpBrowseDoc.PostedFile.SaveAs(strpath)
                writetodb(strpath, strFileType)

            Else
                lblMsg.Text = "Only Excel(XLSX) files allowed"
            End If
        Else
            lblMsg.Text = "Please select Excel(XLSX) file to Upload"
        End If
    End Sub

End Class