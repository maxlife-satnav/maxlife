<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LveLeaveRMApproval.ascx.vb" Inherits="Controls_LveLeaveRMApproval" %>



<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:TextBox ID="txtstore" runat="Server" Visible="false"></asp:TextBox>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate Name<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Department<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDep" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                    </asp:DropDownList>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate ID<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlRM" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Contact Number<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control"
                        MaxLength="10"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Date<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To Date<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="tr1">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">From Time(hh:mm)<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cboHr" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                            <asp:ListItem Value="0">-HH-</asp:ListItem>
                            <asp:ListItem Value="1">0</asp:ListItem>
                            <asp:ListItem Value="2">1</asp:ListItem>
                            <asp:ListItem Value="3">2</asp:ListItem>
                            <asp:ListItem Value="4">3</asp:ListItem>
                            <asp:ListItem Value="5">4</asp:ListItem>
                            <asp:ListItem Value="6">5</asp:ListItem>
                            <asp:ListItem Value="7">6</asp:ListItem>
                            <asp:ListItem Value="8">7</asp:ListItem>
                            <asp:ListItem Value="9">8</asp:ListItem>
                            <asp:ListItem Value="10">9</asp:ListItem>
                            <asp:ListItem Value="11">10</asp:ListItem>
                            <asp:ListItem Value="12">11</asp:ListItem>
                            <asp:ListItem Value="13">12</asp:ListItem>
                            <asp:ListItem Value="14">13</asp:ListItem>
                            <asp:ListItem Value="15">14</asp:ListItem>
                            <asp:ListItem Value="16">15</asp:ListItem>
                            <asp:ListItem Value="17">16</asp:ListItem>
                            <asp:ListItem Value="18">17</asp:ListItem>
                            <asp:ListItem Value="19">18</asp:ListItem>
                            <asp:ListItem Value="20">19</asp:ListItem>
                            <asp:ListItem Value="21">20</asp:ListItem>
                            <asp:ListItem Value="22">21</asp:ListItem>
                            <asp:ListItem Value="23">22</asp:ListItem>
                            <asp:ListItem Value="24">23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="cboMin" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                            <asp:ListItem Value="0">-MM-</asp:ListItem>
                            <asp:ListItem Value="00">00</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="45">45</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">To Time(hh:mm)<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlHH" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                            <asp:ListItem Value="0">-HH-</asp:ListItem>
                            <asp:ListItem Value="1">0</asp:ListItem>
                            <asp:ListItem Value="2">1</asp:ListItem>
                            <asp:ListItem Value="3">2</asp:ListItem>
                            <asp:ListItem Value="4">3</asp:ListItem>
                            <asp:ListItem Value="5">4</asp:ListItem>
                            <asp:ListItem Value="6">5</asp:ListItem>
                            <asp:ListItem Value="7">6</asp:ListItem>
                            <asp:ListItem Value="8">7</asp:ListItem>
                            <asp:ListItem Value="9">8</asp:ListItem>
                            <asp:ListItem Value="10">9</asp:ListItem>
                            <asp:ListItem Value="11">10</asp:ListItem>
                            <asp:ListItem Value="12">11</asp:ListItem>
                            <asp:ListItem Value="13">12</asp:ListItem>
                            <asp:ListItem Value="14">13</asp:ListItem>
                            <asp:ListItem Value="15">14</asp:ListItem>
                            <asp:ListItem Value="16">15</asp:ListItem>
                            <asp:ListItem Value="17">16</asp:ListItem>
                            <asp:ListItem Value="18">17</asp:ListItem>
                            <asp:ListItem Value="19">18</asp:ListItem>
                            <asp:ListItem Value="20">19</asp:ListItem>
                            <asp:ListItem Value="21">20</asp:ListItem>
                            <asp:ListItem Value="22">21</asp:ListItem>
                            <asp:ListItem Value="23">22</asp:ListItem>
                            <asp:ListItem Value="24">23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlMM" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                            <asp:ListItem Value="0">-MM--</asp:ListItem>
                            <asp:ListItem Value="00">00</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="45">45</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Contact Address<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TextMode="MultiLine"
                        Rows="3" MaxLength="500"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reason for Leave<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtReason" runat="server" CssClass="form-control" TextMode="MultiLine"
                        Rows="3" MaxLength="250"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Type of Leave<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLeaveType" runat="server" CssClass="selectpicker" data-live-search="true" s Enabled="false">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Total Available Leaves As On Today<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtTotalLeave" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="tr2" runat="Server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Number of Days<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtNoLeaves" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Remarks<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="row">

            <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />--%>

            <asp:Button ID="btnSubmit" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>



