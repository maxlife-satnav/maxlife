<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddPropertyDocuments.ascx.vb"
    Inherits="Controls_AddPropertyDocuments" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Add Property Documents
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
            <td colspan="3" align="left">
                <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Add Property Documents</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table id="table2" cellspacing="0" cellpadding="1" width="100%" border="1">
                    <tr>
                        <td align="left" style="width: 50%; height: 26px;">
                            Select City<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvcity" runat="server" Display="None" ErrorMessage="Please Select City"
                                ControlToValidate="ddlCity" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%; height: 26px;">
                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="clsComboBox" AutoPostBack="TRUE"
                                Width="99%">
                               
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 50%; height: 26px;">
                            Select Property<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvproperty" runat="server" Display="None" ErrorMessage="Please Select Property"
                                ControlToValidate="ddlPropIDName" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%; height: 26px">
                            <asp:DropDownList ID="ddlPropIDName" runat="server" CssClass="clsComboBox" AutoPostBack="TRUE"
                                Width="99%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 50%; height: 26px;">
                            Browse Document<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvdoc" runat="server" Display="None" ErrorMessage="Please Select File"
                                ControlToValidate="ddlPropIDName" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                                ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                                ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$"> 
                            </asp:RegularExpressionValidator></td>
                        <td align="left" style="width: 50%; height: 26px;">
                            <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="97%" /></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 50%; height: 26px;">
                            Document Title<font class="clsNote">*</font>
                             <asp:RequiredFieldValidator ID="rfvtitle" runat="server" Display="None" ErrorMessage="Please Enter Title"
                                ControlToValidate="txtDocTitle"  ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%; height: 26px;">
                            <asp:TextBox ID="txtDocTitle" runat="server" Width="97%" CssClass="clsTextField"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" ValidationGroup="Val1"
                                CausesValidation="true" />
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 17px">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../images/table_bot_mid_bg.gif" style="height: 17px">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
    </Triggers>
</asp:UpdatePanel>
