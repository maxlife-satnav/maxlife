Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_DisposableAssetsRequisition
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblMsg.Text = ""
            BindSurrenderRequisitions()
        End If
    End Sub

    Private Sub BindSurrenderRequisitions()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")
        ObjSubsonic.GetSubSonicDataSet("GET_ALLDESPOSED_REQ", Param)
        If gvSurrenderAstReq.Rows.Count = 0 Then
            gvSurrenderAstReq.DataSource = Nothing
            gvSurrenderAstReq.DataBind()

        End If
        Dim param1(0) As SqlParameter
        param1(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param1(0).Value = Session("UID")
        ObjSubsonic.GetSubSonicDataSet("GET_ADMINDISPOSE_REQ", param1)

        If gvAstDispoReq.Rows.Count = 0 Then
            gvAstDispoReq.DataSource = Nothing
            gvAstDispoReq.DataBind()

        End If
        If gvSurrenderAstReq.Rows.Count = 0 And gvAstDispoReq.Rows.Count = 0 Then
            divremarks.Visible = False
        End If
    End Sub

    Protected Sub gvSurrenderAstReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSurrenderAstReq.PageIndexChanging
        gvSurrenderAstReq.PageIndex = e.NewPageIndex
        BindSurrenderRequisitions()
    End Sub

    Protected Sub gvSurrenderAstReq_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSurrenderAstReq.RowCommand
        If e.CommandName = "Edit" Then
            Response.Redirect("frmDisposableAssetsRequisitionDTLS.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub

    Protected Sub gvAstDispoReq_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAstDispoReq.RowCommand
        If e.CommandName = "Edit" Then
            Response.Redirect("frmAdminAstDisposableDTLS.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub

    Protected Sub gvAstDispoReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAstDispoReq.PageIndexChanging
        gvAstDispoReq.PageIndex = e.NewPageIndex
        BindSurrenderRequisitions()
    End Sub

  
    Protected Sub btnApprovAll_Click(sender As Object, e As EventArgs) Handles btnApprovAll.Click
        ''Surrender Disposable
        Dim count1 As Integer = 0

        For Each gvRow As GridViewRow In gvSurrenderAstReq.Rows
            Dim SurrchkSelect As CheckBox = DirectCast(gvRow.FindControl("SurrchkSelect"), CheckBox)
            If SurrchkSelect.Checked = True Then
                count1 = count1 + 1
                Exit For
            End If
        Next
        If count1 > 0 Then
            For Each gvRow As GridViewRow In gvSurrenderAstReq.Rows
                Dim SurrchkSelect As CheckBox = DirectCast(gvRow.FindControl("SurrchkSelect"), CheckBox)
                If SurrchkSelect.Checked = True Then
                    Dim lblAstCode As Label = DirectCast(gvRow.FindControl("lblAAT_AST_CODE"), Label)
                    Dim lbl_salvge As Label = DirectCast(gvRow.FindControl("lblSalvage"), Label)
                    Dim lblSREQ_ID As Label = DirectCast(gvRow.FindControl("lblSREQ_ID"), Label)

                    Dim param(5) As SqlParameter
                    param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                    param(0).Value = lblSREQ_ID.Text
                    param(1) = New SqlParameter("@RM_AUR_ID", SqlDbType.NVarChar, 200)
                    param(1).Value = Session("UID")
                    param(2) = New SqlParameter("@RM_REMARKS", SqlDbType.NVarChar, 2000)
                    param(2).Value = txtRemarks.Text
                    param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
                    param(3).Value = 1041
                    param(4) = New SqlParameter("@AST_SALAVAGE_VALUE", SqlDbType.NVarChar, 2000)
                    param(4).Value = CDbl(lbl_salvge.Text)
                    param(5) = New SqlParameter("@AST_DISPOSE", SqlDbType.Bit)
                    param(5).Value = True
                    ObjSubsonic.GetSubSonicDataSet("UPDATESURRENDER_REQUISTION_byAdmin", param)
                    send_mail(lblSREQ_ID.Text)
                    'Response.Redirect("frmDisposableAssetsRequisition.aspx")
                    ' Response.Redirect("frmAssetThanks.aspx?RID=outwardapp&MReqId=" + Request.QueryString("REQ_ID"))
                End If
            Next
            lblMsg.Visible = True
            lblMsg.Text = "Dispose Request Approved successfully..."
        End If
        ''Admin Disposable
        Dim count2 As Integer = 0
        For Each gvRow As GridViewRow In gvAstDispoReq.Rows
            Dim DispchkSelect As CheckBox = DirectCast(gvRow.FindControl("DispchkSelect"), CheckBox)
            If DispchkSelect.Checked = True Then
                count2 = count2 + 1
                Exit For
            End If
        Next
        If count2 > 0 Then
            For Each gvRow2 As GridViewRow In gvAstDispoReq.Rows
                Dim DispchkSelect As CheckBox = DirectCast(gvRow2.FindControl("DispchkSelect"), CheckBox)
                If DispchkSelect.Checked = True Then
                    Dim lblAstCode As Label = DirectCast(gvRow2.FindControl("lblAAT_AST_CODE"), Label)
                    Dim lbl_salvge As Label = DirectCast(gvRow2.FindControl("lblSalvage"), Label)
                    Dim lblDREQ_ID As Label = DirectCast(gvRow2.FindControl("lblDREQ_ID"), Label)


                    Dim param(3) As SqlParameter
                    param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                    param(0).Value = lblDREQ_ID.Text
                    param(1) = New SqlParameter("@DREQ_ADMIN_BY", SqlDbType.NVarChar, 200)
                    param(1).Value = Session("UID")
                    param(2) = New SqlParameter("@DREQ_ADMIN_REMARKS", SqlDbType.NVarChar, 2000)
                    param(2).Value = txtRemarks.Text
                    param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
                    param(3).Value = 1041

                    ObjSubsonic.GetSubSonicDataSet("UPDATEDISPOSABLE_REQUISTION_byAdmin", param)
                    send_mail(lblDREQ_ID.Text)
                    'Response.Redirect("frmDisposableAssetsRequisition.aspx")
                    'Response.Redirect("frmAssetThanks.aspx?RID=outwardapp&MReqId=" + Request.QueryString("REQ_ID"))
                End If
            Next
            lblMsg.Visible = True
            lblMsg.Text = "Dispose Request Approved successfully..."
            BindSurrenderRequisitions()
        End If
        If count1 = 0 And count2 = 0 Then
            lblMsg.Visible = True
            lblMsg.Text = "Please select the assets to Approve"
        End If

    End Sub
    Public Sub send_mail(ByVal reqid As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_DISPOSE_REQUISITION_APPROVAL")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Execute()
    End Sub
   
End Class
