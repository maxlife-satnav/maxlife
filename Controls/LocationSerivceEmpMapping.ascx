﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LocationSerivceEmpMapping.ascx.vb" Inherits="Controls_LocationSerivceEmpMapping" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="Label1" CssClass="col-md-5 control-label" runat="server">Select Location<span style="color: red;">*</span></asp:Label>
                <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                    ErrorMessage="Please Select Location" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Location"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblAssetBrand" CssClass="col-md-5 control-label" runat="server">Select Service Category<span style="color: red;">*</span></asp:Label>
                <asp:Label ID="lblTemp" CssClass="col-md-5 control-label" runat="server" Visible="false"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlServiceCat"
                    Display="none" ErrorMessage="Please Select Service Category" InitialValue="" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlServiceCat"
                    ErrorMessage="Please Select Service Category!" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlServiceCat" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Service Category"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Primary Employee Id <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtEmpId"
                    Display="none" ErrorMessage="Please Enter Primary Employee Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtEmpId"
                    ErrorMessage="Please Enter Primary Employee Id" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtEmpId" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Secondary Employee Id <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSecEmpId"
                    Display="none" ErrorMessage="Please Enter Secondary Employee Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmpId"
                    ErrorMessage="Please Enter Secondary Employee Id" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtSecEmpId" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Tertiary Employee Id<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTeritaryEmpId"
                    Display="none" ErrorMessage="Please Enter Tertiary Employee Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmpId"
                    ErrorMessage="Please Enter Tertiary Employee Id" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtTeritaryEmpId" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Status<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                    Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Status">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/WorkSpace/SMS_Webfiles/helpmasters.aspx" CausesValidation="False" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvSer" runat="server" AllowPaging="True" AllowSorting="False"
            RowStyle-HorizontalAlign="LEFT" HeaderStyle-HorizontalAlign="LEFT"
            PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Location Service Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("SRD_MAP_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Location Name">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" CssClass="lblStatus" Text='<%#Bind("LCM_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Service Category">
                    <ItemTemplate>
                        <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("SER_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Primary Id">
                    <ItemTemplate>
                        <asp:Label ID="lblEmployee" runat="server" CssClass="lblStatus" Text='<%#Bind("SRQ_MAP_AUR_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Secondary Id">
                    <ItemTemplate>
                        <asp:Label ID="lblEmployee2" runat="server" CssClass="lblStatus" Text='<%#Bind("SRQ_MAP_AUR_ID2")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tertiary Id">
                    <ItemTemplate>
                        <asp:Label ID="lblEmployee3" runat="server" CssClass="lblStatus" Text='<%#Bind("SRQ_MAP_AUR_ID3")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#Bind("SRQ_MAPSTA_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField Text="EDIT" CommandName="EDIT" />
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>


