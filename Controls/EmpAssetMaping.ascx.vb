Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_EmpAssetMaping
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            lblMsg.Text = ""
            If Not IsPostBack Then
                BindCategories()
                'bindreqid()
                'BindLocation()
                panel1.Visible = False
            End If
        End If
    End Sub

    Private Sub BindCategories()
        'GetChildRows("0")
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
        ddlAstSubCat.Items.Clear()
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        ddlReqId.Items.Clear()
        ddlLocation.Items.Clear()
        gvItems.Visible = False
        If ddlAstCat.SelectedIndex > 0 Then
            getassetsubcategory()
        Else
            'pnlItems.Visible = False
            'txtRemarks.Text = ""
            'lblMsg.Visible = False
        End If
    End Sub

    Private Sub getassetsubcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        ddlLocation.Items.Clear()
        ddlReqId.Items.Clear()
        gvItems.Visible = False
        If ddlAstCat.SelectedIndex > 0 Then
            getbrandbycatsubcat()
        End If

    End Sub
    Private Sub getbrandbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        ddlModel.Items.Clear()
        ddlReqId.Items.Clear()
        ddlLocation.Items.Clear()
        gvItems.Visible = False
        If ddlAstBrand.SelectedIndex <> 0 Then
            getmakebycatsubcat()
        End If
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ddlModel.Items.Insert(0, "--Select--")

    End Sub


    Protected Sub ddlModel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModel.SelectedIndexChanged
        bindreqid()
    End Sub

    Private Sub bindreqid()
        Try
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GETALLREQID_ASTSPCMAP_EMPMAP")
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GETALLREQID_EMP_ON_CATEGORY_FILTERS")
            sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AST_MODEL", ddlModel.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlReqId.DataSource = sp.GetDataSet()
            ddlReqId.DataTextField = "AIR_REQ_TS"
            ddlReqId.DataValueField = "AIR_REQ_TS"
            ddlReqId.DataBind()
            If ddlReqId.Items.Count <> 0 Then
                ddlReqId.Items.Insert(0, New ListItem("--Select--", "0"))
            Else
                lblMsg.Text = "There are no Requisitions !"
            End If

            ' GridView1.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    ' Commented by Praveen on 31 Dec 2014
    'Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
    '    Try
    '        If ddlLocation.SelectedIndex > 0 Then
    '            BindTower()
    '            panel1.Visible = False
    '        Else
    '            ddlTower.Items.Clear()
    '            ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlTower.SelectedValue = 0
    '            ddlfloor.Items.Clear()
    '            ddlfloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlfloor.SelectedValue = 0
    '            panel1.Visible = False
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
    '    Try
    '        If ddlTower.SelectedIndex > 0 Then
    '            BindFloor()


    '        Else
    '            ddlfloor.Items.Clear()
    '            ddlfloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlfloor.SelectedValue = 0
    '            panel1.Visible = False

    '        End If

    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Protected Sub ddlfloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlfloor.SelectedIndexChanged
    '    Try
    '        If ddlfloor.SelectedIndex > 0 Then
    '            BindAssets()
    '            'EmpLoad()
    '            panel1.Visible = True

    '        Else
    '            panel1.Visible = False

    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Private Sub BindTower()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTWR")
    '        sp.Command.AddParameter("@dummy", ddlLocation.SelectedItem.Value, DbType.String)
    '        ddlTower.DataSource = sp.GetDataSet()
    '        ddlTower.DataTextField = "TWR_NAME"
    '        ddlTower.DataValueField = "TWR_CODE"
    '        ddlTower.DataBind()
    '        ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Private Sub BindFloor()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TWRFLR")
    '        sp.Command.AddParameter("@dummy", ddlLocation.SelectedItem.Value, DbType.String)
    '        sp.Command.AddParameter("@dummy1", ddlTower.SelectedItem.Value, DbType.String)
    '        ddlfloor.DataSource = sp.GetDataSet()
    '        ddlfloor.DataTextField = "FLR_NAME"
    '        ddlfloor.DataValueField = "FLR_CODE"
    '        ddlfloor.DataBind()
    '        ddlfloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    ' Ends here

    Private Sub BindLocation()
        Try
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION")
            'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_LOCTION_BYREQID")
            sp.Command.AddParameter("@REQ_ID", ddlReqId.SelectedItem.Value, DbType.String)
            ddlLocation.DataSource = sp.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            'ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

            If ddlLocation.Items.Count <> 0 Then
                ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
            Else
                lblMsg.Text = "There are no Requisitions !"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLocationByAsset()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION_BY_ASSETREQID")
        sp.Command.AddParameter("@REQ_ID", ddlReqId.SelectedValue, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        ddlLocation.SelectedValue = ds.Tables(0).Rows(0).Item("LOC_ID")
        ddlLocation.Enabled = False
    End Sub
    Protected Sub ddlReqId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReqId.SelectedIndexChanged
        gvItems.Visible = True
        BindLocation()
        BindLocationByAsset()
        BindAssets()
        panel1.Visible = True

    End Sub

    Private Sub BindAssets()
        Try
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASTS")
            'sp.Command.AddParameter("@loc", ddlLocation.SelectedItem.Value, DbType.String)
            'sp.Command.AddParameter("@tow", "", DbType.String)
            'sp.Command.AddParameter("@flr", "", DbType.String)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASTSREQID_EMPMAP")
            sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedValue, DbType.String)
            sp.Command.AddParameter("@REQ_ID", ddlReqId.SelectedValue, DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
            For Each drow As GridViewRow In gvItems.Rows


                ' Dim ddlAssets As DropDownList = DirectCast(drow.FindControl("ddlassets"), DropDownList)

                ' Assets_Loadddl(ddlAssets)
                'Dim lblAssets As Label = DirectCast(drow.FindControl("lblAssets"), Label)

                'Dim li As ListItem = ddlAssets.Items.FindByValue(lblAssets.Text)
                'If Not li Is Nothing Then
                '    li.Selected = True
                'End If

                Dim ddlEmp As DropDownList = DirectCast(drow.FindControl("ddlEmp"), DropDownList)

                Emp_Loadddl(ddlEmp)
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub Assets_Loadddl(ByVal ddlAssets As DropDownList)
        Try
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASTS")
            'sp.Command.AddParameter("@loc", ddlLocation.SelectedItem.Value, DbType.String)
            'sp.Command.AddParameter("@tow", "", DbType.String)
            'sp.Command.AddParameter("@flr", "", DbType.String)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASTSREQID_EMPMAP")
            sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@REQ_ID", ddlReqId.SelectedValue, DbType.String)
            ddlAssets.DataSource = sp.GetDataSet()
            ddlAssets.DataTextField = "AAT_NAME"
            ddlAssets.DataValueField = "AAT_CODE"
            ddlAssets.DataBind()
            ddlAssets.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub Emp_Loadddl(ByVal ddlEmp As DropDownList)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AEMP_LOC")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
            'sp.Command.AddParameter("@tow", ddlTower.SelectedItem.Value, DbType.String)
            'sp.Command.AddParameter("@flr", ddlfloor.SelectedItem.Value, DbType.String)
            ddlEmp.DataSource = sp.GetDataSet()
            ddlEmp.DataTextField = "AUR_FIRST_NAME"
            ddlEmp.DataValueField = "AUR_ID"
            ddlEmp.DataBind()
            ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            If e.CommandName = "Allocate" Then
                Dim Asset, Emp As String
                Dim lnkAllocate As LinkButton = DirectCast(e.CommandSource, LinkButton)
                Dim gvRow As GridViewRow = DirectCast(lnkAllocate.NamingContainer, GridViewRow)


                'Dim ddlAssets As DropDownList = CType(gvRow.FindControl("ddlAssets"), DropDownList)
                Dim lblAssets As Label = CType(gvRow.FindControl("lblAssets"), Label)
                Dim ddlEmp As DropDownList = CType(gvRow.FindControl("ddlEmp"), DropDownList)
                If ddlEmp.SelectedIndex > 0 Then

                    Asset = lblAssets.Text
                    Emp = ddlEmp.SelectedItem.Value

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_ALLOCATE_ASSET")
                    sp.Command.AddParameter("@AAT_AST_CODE", Asset, DbType.String)
                    sp.Command.AddParameter("@AAT_EMP_ID", Emp, DbType.String)
                    sp.Command.AddParameter("@AAT_REQ_ID", ddlReqId.SelectedValue, DbType.String)

                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_AMT_ASSET_SPACE_STAT")
                    sp1.Command.AddParameter("@AAT_AST_CODE", Asset, DbType.String)
                    sp1.Command.AddParameter("@LOC_ID", ddlLocation.SelectedValue, DbType.String)

                    sp1.ExecuteScalar()
                    sp.ExecuteScalar()
                    send_mail(ddlReqId.SelectedValue, Asset)

                    panel1.Visible = True
                    BindAssets()
                    If gvItems.Rows.Count = 0 Then
                        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_ASTSREQID_EMPMAP")
                        sp2.Command.AddParameter("@REQ_ID", ddlReqId.SelectedValue, DbType.String)
                        sp2.ExecuteScalar()
                        bindreqid()
                    End If
                    lblMsg.Text = "Asset mapped successfully..."
                Else
                    lblMsg.Text = "Please select Employee ID to map"
                End If
            End If


        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindAssets()
    End Sub


    Public Sub send_mail(ByVal reqid As String, ByVal AAT_CODE As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_EMP_MAP")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@AAT_CODE", AAT_CODE, DbType.String)
        sp1.Execute()
    End Sub

    'Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
    '    If ddlLocation.SelectedIndex <> 0 Then
    '        BindAssets()
    '        panel1.Visible = True
    '    Else
    '        panel1.Visible = False

    '    End If
    'End Sub



End Class
