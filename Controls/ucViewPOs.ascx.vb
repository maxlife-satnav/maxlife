Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class Controls_ucViewPOs
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BingGridView(Nothing)
        End If
        ' Dim strHelpFile As String = (Request.Url.Scheme + (Uri.SchemeDelimiter + (Request.Url.Authority + (Request.ApplicationPath + "/form1.aspx?ID=" & Fields!ID.Value))))
    End Sub
    Public Sub BingGridView(ByVal gv As GridView)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@StatusId", SqlDbType.Int)
        param(0).Value = 2
        'ObjSubsonic.BindGridView(gv, "USP_AMG_ITEM_PO_GetByStatusIdFinalPOS", param)

        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("USP_AMG_ITEM_PO_GetByStatusIdFinalPOS", param)

  

        Dim rds As New ReportDataSource()
        rds.Name = "ViewPORptDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/ViewPOReport.rdlc")
        ReportViewer1.LocalReport.EnableHyperlinks = True

        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p2 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())

        ReportViewer1.LocalReport.SetParameters(p2)
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

    End Sub

    'Protected Sub gvViewpo_Sorting(sender As Object, e As GridViewSortEventArgs) Handles gvViewpo.Sorting
    '    Dim ds As DataSet = TryCast(Session("dataset"), DataSet)
    '    Dim dt = ds.Tables(0)

    '    If dt IsNot Nothing Then

    '        'Sort the data.
    '        dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
    '        gvViewpo.DataSource = dt
    '        gvViewpo.DataBind()

    '    End If



    'End Sub

    'Private Function GetSortDirection(ByVal column As String) As String

    '    ' By default, set the sort direction to ascending.
    '    Dim sortDirection = "ASC"

    '    ' Retrieve the last column that was sorted.
    '    Dim sortExpression = TryCast(ViewState("SortExpression"), String)

    '    If sortExpression IsNot Nothing Then
    '        ' Check if the same column is being sorted.
    '        ' Otherwise, the default value can be returned.
    '        If sortExpression = column Then
    '            Dim lastDirection = TryCast(ViewState("SortDirection"), String)
    '            If lastDirection IsNot Nothing _
    '              AndAlso lastDirection = "ASC" Then

    '                sortDirection = "DESC"

    '            End If
    '        End If
    '    End If

    '    ' Save new values in ViewState.
    '    ViewState("SortDirection") = sortDirection
    '    ViewState("SortExpression") = column

    '    Return sortDirection

    'End Function

    'Protected Sub gvViewpo_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvViewpo.PageIndexChanging
    '    gvViewpo.PageIndex = e.NewPageIndex
    '    BingGridView(gvViewpo)
    'End Sub

    '    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
    '        Dim gv As New GridView
    '        BingGridView(gv)
    '        Export("PODetails_Report" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls", gv)
    '    End Sub
    '#Region "Export"
    '    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
    '        HttpContext.Current.Response.ContentType = "application/ms-excel"
    '        Dim sw As StringWriter = New StringWriter
    '        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
    '        '  Create a form to contain the grid
    '        Dim table As Table = New Table
    '        table.GridLines = gv.GridLines
    '        '  add the header row to the table
    '        If (Not (gv.HeaderRow) Is Nothing) Then
    '            PrepareControlForExport(gv.HeaderRow)
    '            table.Rows.Add(gv.HeaderRow)
    '        End If
    '        '  add each of the data rows to the table
    '        For Each row As GridViewRow In gv.Rows
    '            PrepareControlForExport(row)
    '            table.Rows.Add(row)
    '        Next
    '        '  add the footer row to the table
    '        If (Not (gv.FooterRow) Is Nothing) Then
    '            PrepareControlForExport(gv.FooterRow)
    '            table.Rows.Add(gv.FooterRow)
    '        End If
    '        '  render the table into the htmlwriter
    '        table.RenderControl(htw)
    '        '  render the htmlwriter into the response
    '        HttpContext.Current.Response.Write(sw.ToString)
    '        HttpContext.Current.Response.End()
    '    End Sub
    '    ' Replace any of the contained controls with literals
    '    Private Shared Sub PrepareControlForExport(ByVal control As Control)
    '        Dim i As Integer = 0
    '        Do While (i < control.Controls.Count)
    '            Dim current As Control = control.Controls(i)
    '            If (TypeOf current Is LinkButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
    '            ElseIf (TypeOf current Is ImageButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
    '            ElseIf (TypeOf current Is HyperLink) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
    '            ElseIf (TypeOf current Is DropDownList) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
    '            ElseIf (TypeOf current Is TextBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
    '            ElseIf (TypeOf current Is CheckBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
    '                'TODO: Warning!!!, inline IF is not supported ?
    '            End If
    '            If current.HasControls Then
    '                PrepareControlForExport(current)
    '            End If
    '            i = (i + 1)
    '        Loop
    '    End Sub
    '#End Region
End Class
