<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAssetBrand.ascx.vb" Inherits="Controls_AddAssetBrand" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-bottom: 20px">
    <div class="col-md-6">
        <label class="col-md-2 btn btn-default pull-right">
            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                ToolTip="Please Select Add to add new Asset Barand and Select Modify to modify the existing Asset Brand" />
            Add
        </label>
    </div>
    <div class="col-md-6">
        <label class="btn btn-default" style="margin-left: 25px">
            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                ToolTip="Please Select Add to add new Asset Barand and Select Modify to modify the existing Asset Brand" />
            Modify
        </label>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblAssetBrand" runat="server" CssClass="col-md-5 control-label" Visible="False">Select Asset Brand<span style="color: red;">*</span></asp:Label>
                <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlBrand"
                    ErrorMessage="Please Select the Asset Brand" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Asset Brand"
                        AutoPostBack="True" Visible="False">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Brand/Make Code<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtBrand"
                    Display="none" ErrorMessage="Please Enter Brand/Make Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtBrand"
                    ErrorMessage="Please Enter Brand/Make Code in alphabets,numbers" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Brand/Make Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBrandName"
                    Display="none" ErrorMessage="Please Enter Brand/Make Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtBrandName"
                    ErrorMessage="Please Enter Brand/Make Name in alphabets,numbers" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter name in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtBrandName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Asset Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                    Display="none" ErrorMessage="Please Select Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Asset Category" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Asset Sub Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                    Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Asset Sub Category" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Status</label>
                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                    Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Status">
                        <%--<asp:ListItem>--Select--</asp:ListItem>--%>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
          <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks</label>
            <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtBrand"
                    Display="none" ErrorMessage="Please Enter Asset Category Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtBrand"
                    ErrorMessage="Please Enter Asset Category Code" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>--%>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" CausesValidation="true" />

            <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" OnClick="btnBack_Click" CausesValidation="False" />
  
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">      

        <asp:GridView ID="gvBrand" runat="server" AllowPaging="True" AllowSorting="False"
            RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" 
            PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Asset Brand Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("manufacturerID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Brand Name">
                    <ItemTemplate>
                        <asp:Label ID="lblCode" runat="server" CssClass="lblCode" Text='<%#BIND("manufacturer")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Brand Code">
                    <ItemTemplate>
                        <asp:Label ID="lblASTCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("manufactuer_code")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Assets Sub Category">
                    <ItemTemplate>
                        <asp:Label ID="lblSubCat" runat="server" CssClass="lblCat" Text='<%#BIND("AST_SUBCAT_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Category">
                    <ItemTemplate>
                        <asp:Label ID="lblCat" runat="server" CssClass="lblCat" Text='<%#BIND("VT_TYPE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("MANUFACTURER_STATUS")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
              <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>       
    </div>
</div>