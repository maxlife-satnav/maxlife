Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_SurrenderRequisitions
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindSurrenderRequisitions()
        End If
    End Sub

    Private Sub BindSurrenderRequisitions()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@RM_AUR_ID", SqlDbType.Int)
        param(0).Value = Session("uid")
        ObjSubsonic.BindGridView(gvSurrenderAstReq, "GET_ALLSURRENDERREQ", param)
        If gvSurrenderAstReq.Rows.Count = 0 Then
            gvSurrenderAstReq.DataSource = Nothing
            gvSurrenderAstReq.DataBind()
        End If
    End Sub

    Protected Sub gvSurrenderAstReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSurrenderAstReq.PageIndexChanging
        gvSurrenderAstReq.PageIndex = e.NewPageIndex
        BindSurrenderRequisitions()
    End Sub

    Protected Sub gvSurrenderAstReq_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSurrenderAstReq.RowCommand
        If e.CommandName = "Edit" Then
            Response.Redirect("UpdateSurrenderRequisitions.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub

End Class
