Imports System.Data
Partial Class Controls_AssetInwardEntry
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindvendor()
            pnlItems.Visible = False
            lblMsg.Visible = False
        End If
    End Sub

    Private Sub bindpos()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FINILIZEDPOS")
        sp.Command.AddParameter("@AIP_AVR_CODE", ddlVendor.SelectedItem.Value, DbType.String)
        ddlPO.DataSource = sp.GetDataSet()
        ddlPO.DataTextField = "CODE1"
        ddlPO.DataValueField = "CODE2"
        ddlPO.DataBind()
        ddlPO.Items.Insert(0, "--Select--")
    End Sub

    Private Sub bindvendor()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FINILIZEDPOSVENDOR")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlVendor.DataSource = sp.GetDataSet()
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, "--Select--")

    End Sub

    Protected Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged
        lblMsg.Text = " "
        If ddlVendor.SelectedIndex > 0 Then
            bindpos()
            gvItems.Visible = False
            pnlItems.Visible = False
        Else
            pnlItems.Visible = False
            ddlPO.SelectedIndex = 0
            lblMsg.Visible = False
            gvItems.Visible = False
            pnlItems.Visible = False

        End If
    End Sub

    Protected Sub ddlPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPO.SelectedIndexChanged
        lblMsg.Text = " "
        If ddlPO.SelectedIndex > 0 Then
            gvItems.Visible = True
            pnlItems.Visible = True
            bindgrid()
        Else
            pnlItems.Visible = False
            lblMsg.Visible = False
            gvItems.Visible = False
            pnlItems.Visible = False
        End If

    End Sub

    Private Sub bindgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASSETSFORFINALIZEDPO")
        sp.Command.AddParameter("@AIP_AVR_CODE", ddlVendor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIPD_PO_ID", ddlPO.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet()
        gvItems.DataSource = ds
        gvItems.DataBind()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim count As Integer = 0
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPOQty As TextBox = DirectCast(row.FindControl("txtPOQty"), TextBox)
            Dim lblProductId1 As Label = DirectCast(row.FindControl("lblAst_code"), Label)
            Dim lblReqId As Label = DirectCast(row.FindControl("lblReqId"), Label)
            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtQty.Text) Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Please enter Quantity for selected checkbox"
                    Exit Sub
                ElseIf Integer.Parse(txtQty.Text) > Integer.Parse(txtPOQty.Text) Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Please enter Correct Received Qty for selected checkbox"
                    Exit Sub
                ElseIf Integer.Parse(txtQty.Text) = 0 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Quantity should be greater than zero"
                    Exit Sub
                Else
                    count = count + 1
                    UpdatePODetails(lblProductId1.Text, ddlPO.SelectedItem.Value, Integer.Parse(txtQty.Text), lblReqId.Text)
                    'UpdatePODetails(Integer.Parse(lblProductId1.Text), ddlPO.SelectedItem.Value, Integer.Parse(txtQty.Text), lblReqId.Text)
                End If

            End If

        Next

        If count = 0 Then
            lblMsg.Visible = True
            lblMsg.Text = "Please select the Asset(S)..."
            Exit Sub
        End If
    End Sub

    Private Sub UpdatePODetails(ByVal productid As String, ByVal po As String, ByVal rcvdqty As Integer, ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_UPDATE_PODETAILSRECEIVED_NP")
        sp.Command.AddParameter("@AIPD_AST_CODE", productid, DbType.String)
        sp.Command.AddParameter("@AIPD_PO_ID", po, DbType.String)
        sp.Command.AddParameter("@AIPD_RCD_QTY", rcvdqty, DbType.Int32)
        sp.Command.AddParameter("@AIPD_ITMREQ_ID", reqid, DbType.String)

        Dim flag As Integer = sp.ExecuteScalar()
        lblMsg.Visible = True
        lblMsg.Text = "Asset Inward process completed..."
        ddlPO.SelectedIndex = 0
        ddlVendor.SelectedIndex = 0
        pnlItems.Visible = False
        If flag = 0 Then
            send_mail_Inward_Entry(po, "Completed")
        Else
            send_mail_Inward_Entry(po, "Pending")
        End If
    End Sub

    Public Sub send_mail_Inward_Entry(ByVal POnum As String, ByVal sta As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_INWARD_ENTRY")
        sp.Command.AddParameter("@REQ_ID", POnum, DbType.String)
        sp.Command.AddParameter("@STATUS", sta, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Execute()
    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        bindgrid()
    End Sub

End Class
