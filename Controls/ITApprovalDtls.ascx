<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ITApprovalDtls.ascx.vb"
    Inherits="Controls_ITApprovalDtls" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Location</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlSLoc" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Tower</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlSTower" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Floor</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlSFloor" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Assets Found." AllowPaging="true"
            PageSize="10" CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblAAS_AAT_CODE" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblAAT_NAME" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To Location</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDLoc" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="False">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To Tower</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDTower" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True"
                        Enabled="False">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To Floor</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDFloor" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="False">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Person To Receive Assets</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPersonName" runat="server" MaxLength="50" CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="multiline" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
            <asp:Button ID="btnReject" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reject" />
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Approve" />
        </div>
    </div>
</div>
