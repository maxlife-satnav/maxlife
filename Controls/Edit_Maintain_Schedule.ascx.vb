Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions


Partial Class Controls_Edit_Maintain_Schedule
    Inherits System.Web.UI.UserControl

    Dim ObjSubsonic As New clsSubSonicCommonFunctions






    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If


        Dim rid, freq, fdate, tdate, pmdate
        rid = Request.QueryString("Req_id")

        If IsPostBack = False Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = rid
            ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_DTLS_BYPLAN", param)
            While ObjDR.Read
                freq = ObjDR("PVM_PLAN_FREQ").ToString()
                fdate = ObjDR("PVM_PLAN_FDATE").ToString()
                tdate = ObjDR("PVM_PLAN_TDATE").ToString()
            End While
            If ObjDR.IsClosed = False Then
                ObjDR.Close()
            End If
           


            txtfromdate.SelectedDate = FormatDateTime(fdate, DateFormat.ShortDate)
            txttodate.SelectedDate = FormatDateTime(tdate, DateFormat.ShortDate)

            radWeekly.Checked = False
            radBiMonthly.Checked = False
            radMonthly.Checked = False
            radQuarterly.Checked = False
            radYearly.Checked = False
            radDaily.Checked = False

            'Put user code to initialize the page here
            panWeekly.Visible = False
            panMonthly.Visible = False
            panBiMonthly.Visible = False
            panQuarterly.Visible = False
            panYearly.Visible = False
            panDaily.Visible = False

            If freq = "Weekly" Then
                panWeekly.Visible = True
                radWeekly.Checked = True

                Dim param1(0) As SqlParameter
                param1(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                param1(0).Value = rid
                ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_SCHDL_DT_BYPLAN", param1)
                While ObjDR.Read
                   pmdate = ObjDR("pmdate").ToString()
                End While
                If ObjDR.IsClosed = False Then
                    ObjDR.Close()
                End If

 

                Dim day, i
                day = Weekday(pmdate)
                For i = 0 To W_DrpDwnWeek.Items.Count - 1
                    If (W_DrpDwnWeek.Items(i).Value = day) Then
                        W_DrpDwnWeek.Items(i).Selected = True
                    End If
                Next


            ElseIf freq = "Monthly" Then
                panMonthly.Visible = True
                radMonthly.Checked = True


                Dim param1(0) As SqlParameter
                param1(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                param1(0).Value = rid
                ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_SCHDL_DT_BYPLAN", param1)
                While ObjDR.Read
                    pmdate = ObjDR("pmdate").ToString()
                End While
                If ObjDR.IsClosed = False Then
                    ObjDR.Close()
                End If


                Dim dd, i
                dd = day(pmdate)

                M_DrpDwnDate.Items.Clear()
                For i = 1 To 31
                    M_DrpDwnDate.Items.Add(i)
                Next


                For i = 0 To M_DrpDwnDate.Items.Count - 1
                    If (M_DrpDwnDate.Items(i).Value = dd) Then
                        M_DrpDwnDate.Items(i).Selected = True
                    End If
                Next

            ElseIf freq = "BiMonthly" Then
                panBiMonthly.Visible = True
                radBiMonthly.Checked = True



                Dim param1(0) As SqlParameter
                param1(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                param1(0).Value = rid
                ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_SCHDL_DT_BYPLAN", param1)
                While ObjDR.Read
                    pmdate = ObjDR("pmdate").ToString()
                End While
                If ObjDR.IsClosed = False Then
                    ObjDR.Close()
                End If


                Dim dd, mm, i
                dd = day(pmdate)
                mm = Month(pmdate)

                B_DrpDwnDate.Items.Clear()
                For i = 1 To 31
                    B_DrpDwnDate.Items.Add(i)
                Next


                For i = 0 To B_DrpDwnDate.Items.Count - 1
                    If (B_DrpDwnDate.Items(i).Value = dd) Then
                        B_DrpDwnDate.Items(i).Selected = True
                    End If
                Next

                For i = 0 To B_DrpDwnMonth().Items.Count - 1
                    If (B_DrpDwnMonth().Items(i).Value = mm) Then
                        B_DrpDwnMonth().Items(i).Selected = True
                    End If
                Next

            ElseIf freq = "Quarterly" Then
                panQuarterly.Visible = True
                radQuarterly.Checked = True

                Dim param1(1) As SqlParameter
                param1(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                param1(0).Value = rid
                param1(1) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                param1(1).Value = "4,5,6"
                ObjDR = ObjSubsonic.GetSubSonicDataReader("GET_FAM_SCHDL_DT_BYPLAN", param1)
                Do While ObjDR.Read()
                    pmdate = ObjDR("pmdate").ToString()
                Loop

                If ObjDR.IsClosed = False Then
                    ObjDR.Close()
                End If



                Dim dd, mm, i
                dd = day(pmdate)
                mm = Month(pmdate)

                Q1_DrpDwnDate.Items.Clear()
                For i = 1 To 31
                    Q1_DrpDwnDate.Items.Add(i)
                Next


                For i = 0 To Q1_DrpDwnDate.Items.Count - 1
                    If (Q1_DrpDwnDate.Items(i).Value = dd) Then
                        Q1_DrpDwnDate.Items(i).Selected = True
                    End If
                Next

                For i = 0 To Q1_DrpDwnMonth().Items.Count - 1
                    If (Q1_DrpDwnMonth().Items(i).Value = mm) Then
                        Q1_DrpDwnMonth().Items(i).Selected = True
                    End If
                Next


                Dim param2(1) As SqlParameter
                param2(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                param2(0).Value = rid
                param2(1) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                param2(1).Value = "7,8,9"
                ObjDR = ObjSubsonic.GetSubSonicDataReader("GET_FAM_SCHDL_DT_BYPLAN", param2)
                Do While ObjDR.Read()
                    pmdate = ObjDR("pmdate").ToString()
                Loop
                If ObjDR.IsClosed = False Then
                    ObjDR.Close()
                End If

 

                dd = day(pmdate)
                mm = Month(pmdate)

                Q2_DrpDwnDate.Items.Clear()
                For i = 1 To 31
                    Q2_DrpDwnDate.Items.Add(i)
                Next


                For i = 0 To Q2_DrpDwnDate.Items.Count - 1
                    If (Q2_DrpDwnDate.Items(i).Value = dd) Then
                        Q2_DrpDwnDate.Items(i).Selected = True
                    End If
                Next

                For i = 0 To Q2_DrpDwnMonth().Items.Count - 1
                    If (Q2_DrpDwnMonth().Items(i).Value = mm) Then
                        Q2_DrpDwnMonth().Items(i).Selected = True
                    End If
                Next


                Dim param3(1) As SqlParameter
                param3(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                param3(0).Value = rid
                param3(1) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                param3(1).Value = "10,11,12"
                ObjDR = ObjSubsonic.GetSubSonicDataReader("GET_FAM_SCHDL_DT_BYPLAN", param3)
                Do While ObjDR.Read()
                    pmdate = ObjDR("pmdate").ToString()
                Loop
                If ObjDR.IsClosed = False Then
                    ObjDR.Close()
                End If


 

                dd = day(pmdate)
                mm = Month(pmdate)

                Q3_DrpDwnDate.Items.Clear()
                For i = 1 To 31
                    Q3_DrpDwnDate.Items.Add(i)
                Next


                For i = 0 To Q3_DrpDwnDate.Items.Count - 1
                    If (Q3_DrpDwnDate.Items(i).Value = dd) Then
                        Q3_DrpDwnDate.Items(i).Selected = True
                    End If
                Next

                For i = 0 To Q3_DrpDwnMonth().Items.Count - 1
                    If (Q3_DrpDwnMonth().Items(i).Value = mm) Then
                        Q3_DrpDwnMonth().Items(i).Selected = True
                    End If
                Next

                Dim param4(1) As SqlParameter
                param4(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                param4(0).Value = rid
                param4(1) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                param4(1).Value = "1,2,3"
                ObjDR = ObjSubsonic.GetSubSonicDataReader("GET_FAM_SCHDL_DT_BYPLAN", param4)
                Do While ObjDR.Read()
                    pmdate = ObjDR("pmdate").ToString()
                Loop
                If ObjDR.IsClosed = False Then
                    ObjDR.Close()
                End If

 


                dd = day(pmdate)
                mm = Month(pmdate)

                Q4_DrpDwnDate.Items.Clear()
                For i = 1 To 31
                    Q4_DrpDwnDate.Items.Add(i)
                Next


                For i = 0 To Q4_DrpDwnDate.Items.Count - 1
                    If (Q4_DrpDwnDate.Items(i).Value = dd) Then
                        Q4_DrpDwnDate.Items(i).Selected = True
                    End If
                Next

                For i = 0 To Q4_DrpDwnMonth().Items.Count - 1
                    If (Q4_DrpDwnMonth().Items(i).Value = mm) Then
                        Q4_DrpDwnMonth().Items(i).Selected = True
                    End If
                Next

            ElseIf freq = "Yearly" Then
                panYearly.Visible = True
                radYearly.Checked = True

                


                Dim param1(0) As SqlParameter
                param1(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                param1(0).Value = rid
                ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_SCHDL_DT_BYPLAN", param1)
                While ObjDR.Read
                    pmdate = ObjDR("pmdate").ToString()
                End While
                If ObjDR.IsClosed = False Then
                    ObjDR.Close()
                End If

                Dim dd, mm, i
                dd = day(pmdate)
                mm = Month(pmdate)

                Y_DrpDwnDate.Items.Clear()
                For i = 1 To 31
                    Y_DrpDwnDate.Items.Add(i)
                Next


                For i = 0 To Y_DrpDwnDate.Items.Count - 1
                    If (Y_DrpDwnDate.Items(i).Value = dd) Then
                        Y_DrpDwnDate.Items(i).Selected = True
                    End If
                Next

                For i = 0 To Y_DrpDwnMonth.Items.Count - 1
                    If (Y_DrpDwnMonth.Items(i).Value = mm) Then
                        Y_DrpDwnMonth.Items(i).Selected = True
                    End If
                Next

            ElseIf freq = "Daily" Then
                panDaily.Visible = True
                radDaily.Checked = True
                Dim tt, i

                Dim param1(0) As SqlParameter
                param1(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                param1(0).Value = rid
                ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_SCHDL_DT_BYPLAN", param1)
                While ObjDR.Read
                    tt = ObjDR("pmdate").ToString()
                End While
                If ObjDR.IsClosed = False Then
                    ObjDR.Close()
                End If

 

                D_DrpDwnHour.Items.Clear()
                For i = 0 To 23
                    D_DrpDwnHour.Items.Add(i)
                Next

                For i = 0 To D_DrpDwnHour.Items.Count - 1
                    If (D_DrpDwnHour.Items(i).Value = tt) Then
                        D_DrpDwnHour.Items(i).Selected = True
                    End If
                Next

            End If


        End If
    End Sub



    Private Sub radWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radweekly.CheckedChanged
        Try
            radWeekly.Checked = True
            radBiMonthly.Checked = False
            radMonthly.Checked = False
            radQuarterly.Checked = False
            radYearly.Checked = False
            radDaily.Checked = False
            panWeekly.Visible = True
            panMonthly.Visible = False
            panBiMonthly.Visible = False
            panQuarterly.Visible = False
            panYearly.Visible = False
            panDaily.Visible = False
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub


    Private Sub radBiMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbimonthly.CheckedChanged
        Try
            radWeekly.Checked = False
            radBiMonthly.Checked = True
            radMonthly.Checked = False
            radQuarterly.Checked = False
            radYearly.Checked = False
            radDaily.Checked = False
            panWeekly.Visible = False
            panMonthly.Visible = False
            panBiMonthly.Visible = True
            panQuarterly.Visible = False
            panYearly.Visible = False
            panDaily.Visible = False

            Dim i
            B_DrpDwnDate.Items.Clear()
            For i = 1 To 31
                B_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub radMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radmonthly.CheckedChanged
        Try
            radWeekly.Checked = False
            radBiMonthly.Checked = False
            radMonthly.Checked = True
            radQuarterly.Checked = False
            radYearly.Checked = False
            radDaily.Checked = False
            panWeekly.Visible = False
            panMonthly.Visible = True
            panBiMonthly.Visible = False
            panQuarterly.Visible = False
            panYearly.Visible = False
            panDaily.Visible = False

            Dim i
            M_DrpDwnDate.Items.Clear()
            For i = 1 To 31
                M_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub radQuarterly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radquarterly.CheckedChanged
        Try
            radWeekly.Checked = False
            radBiMonthly.Checked = False
            radMonthly.Checked = False
            radQuarterly.Checked = True
            radYearly.Checked = False
            radDaily.Checked = False
            panWeekly.Visible = False
            panMonthly.Visible = False
            panBiMonthly.Visible = False
            panQuarterly.Visible = True
            panyearly.Visible = False
            panDaily.Visible = False

            Dim i
            Q1_DrpDwnDate.Items.Clear()
            For i = 1 To 30
                Q1_DrpDwnDate.Items.Add(i)
            Next

            Q2_DrpDwnDate.Items.Clear()
            For i = 1 To 31
                Q2_DrpDwnDate.Items.Add(i)
            Next

            Q3_DrpDwnDate.Items.Clear()
            For i = 1 To 31
                Q3_DrpDwnDate.Items.Add(i)
            Next

            Q4_DrpDwnDate.Items.Clear()
            For i = 1 To 31
                Q4_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub radYearly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radyearly.CheckedChanged
        Try
            radWeekly.Checked = False
            radBiMonthly.Checked = False
            radMonthly.Checked = False
            radQuarterly.Checked = False
            radYearly.Checked = True
            radDaily.Checked = False
            panWeekly.Visible = False
            panMonthly.Visible = False
            panBiMonthly.Visible = False
            panQuarterly.Visible = False
            panYearly.Visible = True
            panDaily.Visible = False

            Dim i
            Y_DrpDwnDate.Items.Clear()
            For i = 1 To 31
                Y_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub radDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles raddaily.CheckedChanged
        Try
            radWeekly.Checked = False
            radBiMonthly.Checked = False
            radMonthly.Checked = False
            radQuarterly.Checked = False
            radYearly.Checked = False
            radDaily.Checked = True
            panWeekly.Visible = False
            panMonthly.Visible = False
            panBiMonthly.Visible = False
            panQuarterly.Visible = False
            panYearly.Visible = False
            panDaily.Visible = True

            D_DrpDwnHour.Items.Clear()
            Dim i
            For i = 0 To 23
                D_DrpDwnHour.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub Y_DrpDwnMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim i, mmold, mmnew, yy, dd, date1, date2
            mmold = Y_DrpDwnMonth.SelectedItem.Value
            yy = Year(getoffsetdatetime(DateTime.Now))
            If mmold = 12 Then
                mmnew = 1
                yy = yy + 1
            Else
                mmnew = mmold + 1
            End If
            date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
            date2 = mmnew & "/01/" & yy
            dd = DateDiff(DateInterval.Day, date1, date2)

            Y_DrpDwnDate.Items.Clear()

            For i = 1 To dd
                Y_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub B_DrpDwnMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim i, mmold, mmnew, yy, dd, date1, date2
            mmold = B_DrpDwnMonth.SelectedItem.Value
            yy = Year(getoffsetdatetime(DateTime.Now))
            If mmold = 12 Then
                mmnew = 1
                yy = yy + 1
            Else
                mmnew = mmold + 1
            End If


            date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
            date2 = mmnew & "/01/" & yy
            dd = DateDiff(DateInterval.Day, date1, date2)

            B_DrpDwnDate.Items.Clear()
            For i = 1 To dd
                B_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub Q1_DrpDwnMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim i, mmold, mmnew, yy, dd, date1, date2
            mmold = Q1_DrpDwnMonth.SelectedItem.Value
            yy = Year(getoffsetdatetime(DateTime.Now))
            If mmold = 12 Then
                mmnew = 1
                yy = yy + 1
            Else
                mmnew = mmold + 1
            End If


            date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
            date2 = mmnew & "/01/" & yy
            dd = DateDiff(DateInterval.Day, date1, date2)
            Q1_DrpDwnDate.Items.Clear()
            For i = 1 To dd
                Q1_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub Q2_DrpDwnMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim i, mmold, mmnew, yy, dd, date1, date2
            mmold = Q2_DrpDwnMonth.SelectedItem.Value
            yy = Year(getoffsetdatetime(DateTime.Now))
            If mmold = 12 Then
                mmnew = 1
                yy = yy + 1
            Else
                mmnew = mmold + 1
            End If


            date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
            date2 = mmnew & "/01/" & yy
            dd = DateDiff(DateInterval.Day, date1, date2)
            Q2_DrpDwnDate.Items.Clear()
            For i = 1 To dd
                Q2_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub Q3_DrpDwnMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim i, mmold, mmnew, yy, dd, date1, date2
            mmold = Q3_DrpDwnMonth.SelectedItem.Value
            yy = Year(getoffsetdatetime(DateTime.Now))
            If mmold = 12 Then
                mmnew = 1
                yy = yy + 1
            Else
                mmnew = mmold + 1
            End If


            date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
            date2 = mmnew & "/01/" & yy
            dd = DateDiff(DateInterval.Day, date1, date2)
            Q3_DrpDwnDate.Items.Clear()
            For i = 1 To dd
                Q3_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub Q4_DrpDwnMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim i, mmold, mmnew, yy, dd, date1, date2
            mmold = Q4_DrpDwnMonth.SelectedItem.Value
            yy = Year(getoffsetdatetime(DateTime.Now))
            If mmold = 12 Then
                mmnew = 1
                yy = yy + 1
            Else
                mmnew = mmold + 1
            End If

            date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
            date2 = mmnew & "/01/" & yy
            dd = DateDiff(DateInterval.Day, date1, date2)
            Q4_DrpDwnDate.Items.Clear()
            For i = 1 To dd
                Q4_DrpDwnDate.Items.Add(i)
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    'Private Sub DrpDwnCatType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DrpDwnCatType.SelectedIndexChanged
    '    Try
    '        panPeriod.Visible = False
    '        If DrpDwnCatType.SelectedItem.Value <> "--Select--" Then
    '            panPeriod.Visible = True
    '            radWeekly.Checked = False
    '            radBiMonthly.Checked = False
    '            radMonthly.Checked = False
    '            radQuarterly.Checked = False
    '            radYearly.Checked = False
    '            radDaily.Checked = False
    '        End If
    '    Catch ex As Exception
    '        'Dim pageURL, trace, MSG As String
    '        Session("pageURL") = ex.GetBaseException.ToString
    '        Session("trace") = (ex.StackTrace.ToString)
    '        Session("MSG") = Session("pageURL") + Session("Trace")
    '        Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
    '    End Try

    'End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim uid As String = Session("uid")
        Dim rid = Request.QueryString("Req_id")
        If Page.IsValid = True Then
            Dim dateflag
            dateflag = Check_Dates()

            If dateflag = 1 Then

                Dim Planflag
                Planflag = Check_Plan_Id(rid)
                If Planflag = 1 Then


                    Dim insertflag As Boolean
                    Dim schdtime As String = "00"
                    Dim fd, td, maindate, tempdate As Date
                    Dim diffmm, i, j, diffyy, dd, mm, yy, dd1, mm1, yy1, dd2, mm2, dd3, mm3, dd4, mm4, flag, ast_Id, type_id
                    Dim selradfreq, selradfor, selradtype As String
                    Dim prmid, catid, ctr As Integer
                    Dim twmid

                    Dim PVD_PLANTYPE_ID As Array
                    Dim PVD_PLANAAT_ID As Array

                    If radYearly.Checked = True Then
                        selradfreq = "Yearly"
                    ElseIf radQuarterly.Checked = True Then
                        selradfreq = "Quarterly"
                    ElseIf radBiMonthly.Checked = True Then
                        selradfreq = "BiMonthly"
                    ElseIf radMonthly.Checked = True Then
                        selradfreq = "Monthly"
                    ElseIf radWeekly.Checked = True Then
                        selradfreq = "Weekly"
                    ElseIf radDaily.Checked = True Then
                        selradfreq = "Daily"
                    End If

                    fd = CDate(txtfromdate.SelectedDate)
                    td = CDate(txttodate.SelectedDate)
                    type_id = ""
                    ast_Id = ""

                    Dim param(0) As SqlParameter
                    param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                    param(0).Value = rid
                    ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_IDS", param)
                    While ObjDR.Read
                        type_id = type_id & ObjDR(0).ToString & ","
                        ast_Id = ast_Id & ObjDR(1).ToString & ","
                    End While
                    If ObjDR.IsClosed = False Then
                        ObjDR.Close()
                    End If

                    type_id = Left(type_id, Len(type_id) - 1)
                    ast_Id = Left(ast_Id, Len(ast_Id) - 1)

                    PVD_PLANTYPE_ID = Split(type_id, ",")
                    PVD_PLANAAT_ID = Split(ast_Id, ",")

                    '************* DELETING OLD RECORDS FOR PARTICULAR REQ ID ********************************
                    Dim param_DEL(0) As SqlParameter
                    param_DEL(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                    param_DEL(0).Value = rid
                    ObjSubsonic.GetSubSonicExecute("DELETE_OLDMAIN_DETAILS", param_DEL)



                    For j = LBound(PVD_PLANTYPE_ID) To UBound(PVD_PLANTYPE_ID)
                        type_id = PVD_PLANTYPE_ID(j)
                        ast_Id = PVD_PLANAAT_ID(j)
                         
                        If radWeekly.Checked = True Then
                            flag = 1
                            tempdate = fd
                            While flag = 1
                                If W_DrpDwnWeek.SelectedItem.Value = DatePart(DateInterval.Weekday, tempdate) Then
                                    flag = 2
                                Else
                                    tempdate = DateAdd(DateInterval.Day, 1, tempdate)
                                End If
                            End While
                            fd = tempdate
                            maindate = fd
                       
                            For i = 1 To DateDiff(DateInterval.Day, fd, td)
                                If maindate >= fd And maindate <= td Then


                                    Dim paramM_InsertDETAILS(7) As SqlParameter

                                    paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                    paramM_InsertDETAILS(0).Value = rid
                                    paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                    paramM_InsertDETAILS(1).Value = type_id

                                    paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                    paramM_InsertDETAILS(2).Value = ast_Id

                                    paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                    paramM_InsertDETAILS(3).Value = maindate

                                    paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                    paramM_InsertDETAILS(4).Value = schdtime

                                    paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                    paramM_InsertDETAILS(5).Value = 1

                                    paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                    paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

                                    paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                    paramM_InsertDETAILS(7).Value = uid

                                    ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

                                    Dim param_updateDetails(3) As SqlParameter
                                    param_updateDetails(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                    param_updateDetails(0).Value = selradfreq
                                    param_updateDetails(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                    param_updateDetails(1).Value = fd
                                    param_updateDetails(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                    param_updateDetails(2).Value = td
                                    param_updateDetails(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                    param_updateDetails(3).Value = rid

                                    ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PVM_DtS", param_updateDetails)


                                


                                End If
                                maindate = DateAdd(DateInterval.Day, 7, maindate)
                            Next

                        ElseIf radMonthly.Checked = True Then
                            If Year(td) <> Year(fd) Then
                                diffmm = 12 - Month(fd)
                                diffyy = Year(td) - Year(fd)
                                If diffyy = 1 Then
                                    diffmm = diffmm + Month(td)
                                Else
                                    diffmm = diffmm + (diffyy * 12) + Month(td)
                                End If
                            Else
                                diffmm = Month(td) - Month(fd)
                            End If

                            mm = Month(fd)
                            yy = Year(fd)
                            dd = M_DrpDwnDate.SelectedItem.Value

                            If mm = 12 Then
                                mm1 = 1
                                yy1 = yy + 1
                            Else
                                mm1 = mm + 1
                                yy1 = yy
                            End If

                            For i = 0 To diffmm

                                maindate = mm & "/1/" & yy
                                tempdate = mm1 & "/1/" & yy1
                                If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                                    maindate = mm & "/" & dd & "/" & yy
                                    If maindate >= fd And maindate <= td Then
                                         

                                        Dim paramM_InsertDETAILS(7) As SqlParameter

                                        paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(0).Value = rid
                                        paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(1).Value = type_id

                                        paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(2).Value = ast_Id

                                        paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(3).Value = maindate

                                        paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(4).Value = schdtime

                                        paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(5).Value = 1

                                        paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

                                        paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(7).Value = uid

                                        ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

                                        Dim param_updateDetails(3) As SqlParameter
                                        param_updateDetails(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                        param_updateDetails(0).Value = selradfreq
                                        param_updateDetails(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                        param_updateDetails(1).Value = fd
                                        param_updateDetails(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                        param_updateDetails(2).Value = td
                                        param_updateDetails(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        param_updateDetails(3).Value = rid

                                        ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PVM_DtS", param_updateDetails)


                                    End If
                                End If

                                If mm = 12 Then
                                    mm = 1
                                    yy = yy + 1
                                Else
                                    mm = mm + 1
                                End If

                                If mm1 = 12 Then
                                    mm1 = 1
                                    yy1 = yy1 + 1
                                Else
                                    mm1 = mm1 + 1
                                End If
                            Next

                        ElseIf radBiMonthly.Checked = True Then
                            If Year(td) <> Year(fd) Then
                                diffmm = 12 - Month(fd)
                                diffyy = Year(td) - Year(fd)
                                If diffyy = 1 Then
                                    diffmm = diffmm + Month(td)
                                Else
                                    diffmm = diffmm + (diffyy * 12) + Month(td)
                                End If
                            Else
                                diffmm = Month(td) - Month(fd)
                            End If

                            mm = B_DrpDwnMonth.SelectedItem.Value
                            yy = Year(fd)
                            dd = B_DrpDwnDate.SelectedItem.Value

                            If mm = 12 Then
                                mm1 = 1
                                yy1 = yy + 1
                            Else
                                mm1 = mm + 1
                                yy1 = yy
                            End If

                            For i = 0 To diffmm

                                maindate = mm & "/1/" & yy
                                tempdate = mm1 & "/1/" & yy1
                                If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                                    maindate = mm & "/" & dd & "/" & yy
                                    If maindate >= fd And maindate <= td Then
                                         
                                    


                                        Dim paramM_InsertDETAILS(7) As SqlParameter

                                        paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(0).Value = rid
                                        paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(1).Value = type_id

                                        paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(2).Value = ast_Id

                                        paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(3).Value = maindate

                                        paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(4).Value = schdtime

                                        paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(5).Value = 1

                                        paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

                                        paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(7).Value = uid

                                        ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

                                        Dim param_updateDetails(3) As SqlParameter
                                        param_updateDetails(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                        param_updateDetails(0).Value = selradfreq
                                        param_updateDetails(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                        param_updateDetails(1).Value = fd
                                        param_updateDetails(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                        param_updateDetails(2).Value = td
                                        param_updateDetails(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        param_updateDetails(3).Value = rid

                                        ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PVM_DtS", param_updateDetails)


                                    End If
                                End If

                                If mm = 12 Then
                                    mm = 2
                                    yy = yy + 1
                                ElseIf mm = 11 Then
                                    mm = 1
                                    yy = yy + 1
                                Else
                                    mm = mm + 2
                                End If

                                If mm = 12 Then
                                    mm1 = 1
                                    yy1 = yy + 1
                                Else
                                    mm1 = mm + 1
                                    yy1 = yy
                                End If
                            Next

                        ElseIf radQuarterly.Checked = True Then
                            If Year(td) <> Year(fd) Then
                                diffmm = 12 - Month(fd)
                                diffyy = Year(td) - Year(fd)
                                If diffyy = 1 Then
                                    diffmm = diffmm + Month(td)
                                Else
                                    diffmm = diffmm + (diffyy * 12) + Month(td)
                                End If
                            Else
                                diffmm = Month(td) - Month(fd)
                            End If

                            mm1 = Q1_DrpDwnMonth.SelectedItem.Value
                            dd1 = Q1_DrpDwnDate.SelectedItem.Value

                            mm2 = Q2_DrpDwnMonth.SelectedItem.Value
                            dd2 = Q2_DrpDwnDate.SelectedItem.Value

                            mm3 = Q3_DrpDwnMonth.SelectedItem.Value
                            dd3 = Q3_DrpDwnDate.SelectedItem.Value

                            mm4 = Q4_DrpDwnMonth.SelectedItem.Value
                            dd4 = Q4_DrpDwnDate.SelectedItem.Value

                            mm = Month(fd)
                            yy = Year(fd)
                            For i = 0 To diffmm

                                If mm = mm1 Then
                                    maindate = mm1 & "/" & dd1 & "/" & yy
                                    If maindate >= fd And maindate <= td Then
                                         
                                 

                                        Dim paramM_InsertDETAILS(7) As SqlParameter

                                        paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(0).Value = rid
                                        paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(1).Value = type_id

                                        paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(2).Value = ast_Id

                                        paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(3).Value = maindate

                                        paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(4).Value = schdtime

                                        paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(5).Value = 1

                                        paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

                                        paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(7).Value = uid

                                        ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

                                        Dim param_updateDetails(3) As SqlParameter
                                        param_updateDetails(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                        param_updateDetails(0).Value = selradfreq
                                        param_updateDetails(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                        param_updateDetails(1).Value = fd
                                        param_updateDetails(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                        param_updateDetails(2).Value = td
                                        param_updateDetails(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        param_updateDetails(3).Value = rid

                                        ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PVM_DtS", param_updateDetails)


                                    End If
                                End If
                                If mm = mm2 Then
                                    maindate = mm2 & "/" & dd2 & "/" & yy
                                    If maindate >= fd And maindate <= td Then
                                      




                                        Dim paramM_InsertDETAILS(7) As SqlParameter

                                        paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(0).Value = rid
                                        paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(1).Value = type_id

                                        paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(2).Value = ast_Id

                                        paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(3).Value = maindate

                                        paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(4).Value = schdtime

                                        paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(5).Value = 1

                                        paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

                                        paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(7).Value = uid

                                        ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

                                        Dim param_updateDetails(3) As SqlParameter
                                        param_updateDetails(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                        param_updateDetails(0).Value = selradfreq
                                        param_updateDetails(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                        param_updateDetails(1).Value = fd
                                        param_updateDetails(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                        param_updateDetails(2).Value = td
                                        param_updateDetails(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        param_updateDetails(3).Value = rid

                                        ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PVM_DtS", param_updateDetails)
 




                                    End If
                                End If
                                If mm = mm3 Then
                                    maindate = mm3 & "/" & dd3 & "/" & yy
                                    If maindate >= fd And maindate <= td Then




                                        Dim paramM_InsertDETAILS(7) As SqlParameter

                                        paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(0).Value = rid
                                        paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(1).Value = type_id

                                        paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(2).Value = ast_Id

                                        paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(3).Value = maindate

                                        paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(4).Value = schdtime

                                        paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(5).Value = 1

                                        paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

                                        paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(7).Value = uid

                                        ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

                                        Dim param_updateDetails(3) As SqlParameter
                                        param_updateDetails(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                        param_updateDetails(0).Value = selradfreq
                                        param_updateDetails(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                        param_updateDetails(1).Value = fd
                                        param_updateDetails(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                        param_updateDetails(2).Value = td
                                        param_updateDetails(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        param_updateDetails(3).Value = rid

                                        ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PVM_DtS", param_updateDetails)


                                    End If
                                End If
                                If mm = mm4 Then
                                    maindate = mm4 & "/" & dd4 & "/" & yy
                                    If maindate >= fd And maindate <= td Then


                                        Dim paramM_InsertDETAILS(7) As SqlParameter

                                        paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(0).Value = rid
                                        paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(1).Value = type_id

                                        paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(2).Value = ast_Id

                                        paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(3).Value = maindate

                                        paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(4).Value = schdtime

                                        paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(5).Value = 1

                                        paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

                                        paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(7).Value = uid

                                        ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

                                        Dim param_updateDetails(3) As SqlParameter
                                        param_updateDetails(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                        param_updateDetails(0).Value = selradfreq
                                        param_updateDetails(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                        param_updateDetails(1).Value = fd
                                        param_updateDetails(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                        param_updateDetails(2).Value = td
                                        param_updateDetails(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        param_updateDetails(3).Value = rid

                                        ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PVM_DtS", param_updateDetails)


                                    End If
                                End If

                                If mm = 12 Then
                                    mm = 1
                                    yy = yy + 1
                                Else
                                    mm = mm + 1
                                End If
                            Next

                        ElseIf radYearly.Checked = True Then
                            If Year(td) <> Year(fd) Then
                                diffmm = 12 - Month(fd)
                                diffyy = Year(td) - Year(fd)
                                If diffyy = 1 Then
                                    diffmm = diffmm + Month(td)
                                Else
                                    diffmm = diffmm + (diffyy * 12) + Month(td)
                                End If
                            Else
                                diffmm = Month(td) - Month(fd)
                            End If

                            mm = Y_DrpDwnMonth.SelectedItem.Value
                            yy = Year(fd)
                            dd = Y_DrpDwnDate.SelectedItem.Value

                            mm1 = Y_DrpDwnMonth.SelectedItem.Value

                            For i = 0 To diffmm

                                If mm = mm1 Then
                                    maindate = mm & "/" & dd & "/" & yy
                                    If maindate >= fd And maindate <= td Then


                                        Dim paramM_InsertDETAILS(7) As SqlParameter

                                        paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(0).Value = rid
                                        paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(1).Value = type_id

                                        paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(2).Value = ast_Id

                                        paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(3).Value = maindate

                                        paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(4).Value = schdtime

                                        paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                        paramM_InsertDETAILS(5).Value = 1

                                        paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                        paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

                                        paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                        paramM_InsertDETAILS(7).Value = uid

                                        ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

                                        Dim param_updateDetails(3) As SqlParameter
                                        param_updateDetails(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                        param_updateDetails(0).Value = selradfreq
                                        param_updateDetails(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                        param_updateDetails(1).Value = fd
                                        param_updateDetails(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                        param_updateDetails(2).Value = td
                                        param_updateDetails(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        param_updateDetails(3).Value = rid

                                        ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PVM_DtS", param_updateDetails)

                                    End If
                                End If

                                If mm1 = 12 Then
                                    mm1 = 1
                                    yy = yy + 1
                                Else
                                    mm1 = mm1 + 1
                                End If
                            Next

                        ElseIf radDaily.Checked = True Then
                            schdtime = D_DrpDwnHour.SelectedItem.Text
                            maindate = fd
                            For i = 0 To DateDiff(DateInterval.Day, fd, td)
                                If maindate >= fd And maindate <= td Then


                                    Dim paramM_InsertDETAILS(7) As SqlParameter

                                    paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                    paramM_InsertDETAILS(0).Value = rid
                                    paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                    paramM_InsertDETAILS(1).Value = type_id

                                    paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                    paramM_InsertDETAILS(2).Value = ast_Id

                                    paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                    paramM_InsertDETAILS(3).Value = maindate

                                    paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                    paramM_InsertDETAILS(4).Value = schdtime

                                    paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                    paramM_InsertDETAILS(5).Value = 1

                                    paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                    paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

                                    paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                    paramM_InsertDETAILS(7).Value = uid

                                    ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

                                    Dim param_updateDetails(3) As SqlParameter
                                    param_updateDetails(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                    param_updateDetails(0).Value = selradfreq
                                    param_updateDetails(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                    param_updateDetails(1).Value = fd
                                    param_updateDetails(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                    param_updateDetails(2).Value = td
                                    param_updateDetails(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                    param_updateDetails(3).Value = rid

                                    ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PVM_DtS", param_updateDetails)

                                End If
                                maindate = DateAdd(DateInterval.Day, 1, maindate)
                            Next
                        End If
                    Next j

                    'Response.Redirect("frmPVMFinalpage.aspx?rid=" & rid & "&staid=Changed")
                    Response.Redirect("frmAssetThanks.aspx?rid=EditMaintSc&MReqId=" & rid)
                Else
                    Response.Write("<script language=javascript>alert(""Plan ID Details Not Changed"")</script>")
                End If
            Else
                Response.Write("<script language=javascript>alert(""Please Select The Valid Dates"")</script>")
            End If
        End If
    End Sub

    Private Function Check_Dates()
        Try
            Dim fd, td, maindate, tempdate As Date
            Dim diffmm, i, j, diffyy, dd, mm, yy, dd1, mm1, yy1, dd2, mm2, dd3, mm3, dd4, mm4, flag, dateflag

            fd = CDate(txtfromdate.SelectedDate)
            td = CDate(txttodate.SelectedDate)
            dateflag = 0

            If radWeekly.Checked = True Then
                flag = 1
                tempdate = fd
                While flag = 1
                    If W_DrpDwnWeek.SelectedItem.Value = DatePart(DateInterval.Weekday, tempdate) Then
                        flag = 2
                    Else
                        tempdate = DateAdd(DateInterval.Day, 1, tempdate)
                    End If
                End While
                fd = tempdate
                maindate = fd
                For i = 1 To DateDiff(DateInterval.Day, fd, td)
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                    maindate = DateAdd(DateInterval.Day, 7, maindate)
                Next

            ElseIf radMonthly.Checked = True Then

                If Year(td) <> Year(fd) Then
                    diffmm = 12 - Month(fd)
                    diffyy = Year(td) - Year(fd)
                    If diffyy = 1 Then
                        diffmm = diffmm + Month(td)
                    Else
                        diffmm = diffmm + (diffyy * 12) + Month(td)
                    End If
                Else
                    diffmm = Month(td) - Month(fd)
                End If

                mm = Month(fd)
                yy = Year(fd)
                dd = M_DrpDwnDate.SelectedItem.Value

                If mm = 12 Then
                    mm1 = 1
                    yy1 = yy + 1
                Else
                    mm1 = mm + 1
                    yy1 = yy
                End If

                For i = 0 To diffmm

                    maindate = mm & "/1/" & yy
                    tempdate = mm1 & "/1/" & yy1
                    If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                        maindate = mm & "/" & dd & "/" & yy
                        If maindate >= fd And maindate <= td Then
                            dateflag = 1
                            Exit For
                        End If
                    End If

                    If mm = 12 Then
                        mm = 1
                        yy = yy + 1
                    Else
                        mm = mm + 1
                    End If

                    If mm1 = 12 Then
                        mm1 = 1
                        yy1 = yy1 + 1
                    Else
                        mm1 = mm1 + 1
                    End If
                Next

            ElseIf radBiMonthly.Checked = True Then
                If Year(td) <> Year(fd) Then
                    diffmm = 12 - Month(fd)
                    diffyy = Year(td) - Year(fd)
                    If diffyy = 1 Then
                        diffmm = diffmm + Month(td)
                    Else
                        diffmm = diffmm + (diffyy * 12) + Month(td)
                    End If
                Else
                    diffmm = Month(td) - Month(fd)
                End If

                mm = B_DrpDwnMonth.SelectedItem.Value
                yy = Year(fd)
                dd = B_DrpDwnDate.SelectedItem.Value

                If mm = 12 Then
                    mm1 = 1
                    yy1 = yy + 1
                Else
                    mm1 = mm + 1
                    yy1 = yy
                End If

                For i = 0 To diffmm

                    maindate = mm & "/1/" & yy
                    tempdate = mm1 & "/1/" & yy1
                    If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                        maindate = mm & "/" & dd & "/" & yy
                        If maindate >= fd And maindate <= td Then
                            dateflag = 1
                            Exit For
                        End If
                    End If

                    If mm = 12 Then
                        mm = 2
                        yy = yy + 1
                    ElseIf mm = 11 Then
                        mm = 1
                        yy = yy + 1
                    Else
                        mm = mm + 2
                    End If

                    If mm = 12 Then
                        mm1 = 1
                        yy1 = yy + 1
                    Else
                        mm1 = mm + 1
                        yy1 = yy
                    End If
                Next

            ElseIf radQuarterly.Checked = True Then
                If Year(td) <> Year(fd) Then
                    diffmm = 12 - Month(fd)
                    diffyy = Year(td) - Year(fd)
                    If diffyy = 1 Then
                        diffmm = diffmm + Month(td)
                    Else
                        diffmm = diffmm + (diffyy * 12) + Month(td)
                    End If
                Else
                    diffmm = Month(td) - Month(fd)
                End If

                mm1 = Q1_DrpDwnMonth.SelectedItem.Value
                dd1 = Q1_DrpDwnDate.SelectedItem.Value

                mm2 = Q2_DrpDwnMonth.SelectedItem.Value
                dd2 = Q2_DrpDwnDate.SelectedItem.Value

                mm3 = Q3_DrpDwnMonth.SelectedItem.Value
                dd3 = Q3_DrpDwnDate.SelectedItem.Value

                mm4 = Q4_DrpDwnMonth.SelectedItem.Value
                dd4 = Q4_DrpDwnDate.SelectedItem.Value

                mm = Month(fd)
                yy = Year(fd)
                For i = 0 To diffmm

                    If mm = mm1 Then
                        maindate = mm1 & "/" & dd1 & "/" & yy
                        If maindate >= fd And maindate <= td Then
                            dateflag = 1
                            Exit For
                        End If
                    End If
                    If mm = mm2 Then
                        maindate = mm2 & "/" & dd2 & "/" & yy
                        If maindate >= fd And maindate <= td Then
                            dateflag = 1
                            Exit For
                        End If
                    End If
                    If mm = mm3 Then
                        maindate = mm3 & "/" & dd3 & "/" & yy
                        If maindate >= fd And maindate <= td Then
                            dateflag = 1
                            Exit For
                        End If
                    End If
                    If mm = mm4 Then
                        maindate = mm4 & "/" & dd4 & "/" & yy
                        If maindate >= fd And maindate <= td Then
                            dateflag = 1
                            Exit For
                        End If
                    End If

                    If mm = 12 Then
                        mm = 1
                        yy = yy + 1
                    Else
                        mm = mm + 1
                    End If
                Next

            ElseIf radYearly.Checked = True Then
                If Year(td) <> Year(fd) Then
                    diffmm = 12 - Month(fd)
                    diffyy = Year(td) - Year(fd)
                    If diffyy = 1 Then
                        diffmm = diffmm + Month(td)
                    Else
                        diffmm = diffmm + (diffyy * 12) + Month(td)
                    End If
                Else
                    diffmm = Month(td) - Month(fd)
                End If

                mm = Y_DrpDwnMonth.SelectedItem.Value
                yy = Year(fd)
                dd = Y_DrpDwnDate.SelectedItem.Value

                mm1 = Y_DrpDwnMonth.SelectedItem.Value

                For i = 0 To diffmm

                    If mm = mm1 Then
                        maindate = mm & "/" & dd & "/" & yy
                        If maindate >= fd And maindate <= td Then
                            dateflag = 1
                            Exit For
                        End If
                    End If

                    If mm1 = 12 Then
                        mm1 = 1
                        yy = yy + 1
                    Else
                        mm1 = mm1 + 1
                    End If
                Next

            ElseIf radDaily.Checked = True Then
                maindate = fd
                For i = 0 To DateDiff(DateInterval.Day, fd, td)
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                    maindate = DateAdd(DateInterval.Day, 1, maindate)
                Next
            End If
            Check_Dates = dateflag
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Function



    Private Function Check_Plan_Id(ByVal planid As String)
        Try
            Dim insertflag
            insertflag = 0


            Dim rid, freq, fdate, tdate, pmdate
            Dim nfreq, nfdate, ntdate, npmdate
            rid = Request.QueryString("Req_id")


            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = rid

            ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_DTLS_BYPLAN", param)

            Do While ObjDR.Read()
                freq = ObjDR("PVM_PLAN_FREQ").ToString()
                fdate = FormatDateTime(ObjDR("PVM_PLAN_FDATE").ToString(), DateFormat.ShortDate)
                tdate = FormatDateTime(ObjDR("PVM_PLAN_TDATE").ToString(), DateFormat.ShortDate)
            Loop
            ObjDR.Close()


            nfdate = txtfromdate.SelectedDate
            ntdate = txttodate.SelectedDate
            If radWeekly.Checked = True Then
                nfreq = "Weekly"
            ElseIf radMonthly.Checked = True Then
                nfreq = "Monthly"
            ElseIf radBiMonthly.Checked = True Then
                nfreq = "BiMonthly"
            ElseIf radQuarterly.Checked = True Then
                nfreq = "Quarterly"
            ElseIf radYearly.Checked = True Then
                nfreq = "Yearly"
            Else
                nfreq = "Daily"
            End If

            If (nfreq = freq) And (nfdate = fdate) And (ntdate = tdate) Then

                If freq = "Weekly" Then

                    Dim param1(0) As SqlParameter
                    param1(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                    param1(0).Value = rid

                    ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_SCHDL_DT_BYPLAN", param1)
                  
                    Do While ObjDR.Read()
                        pmdate = ObjDR("pmdate").ToString()
                    Loop
                    ObjDR.Close()


                    Dim day, i
                    day = Weekday(pmdate)
                    If (day <> W_DrpDwnWeek.SelectedItem.Value) Then
                        insertflag = 1
                    End If

                ElseIf freq = "Monthly" Then
                    panMonthly.Visible = True
                    radMonthly.Checked = True


                    Dim param1(0) As SqlParameter
                    param1(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                    param1(0).Value = rid

                    ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_SCHDL_DT_BYPLAN", param1)



                    Do While ObjDR.Read()
                        pmdate = ObjDR("pmdate").ToString()
                    Loop
                    ObjDR.Close()


                    Dim dd, i
                    dd = day(pmdate)

                    If (dd <> M_DrpDwnDate.SelectedItem.Value) Then
                        insertflag = 1
                    End If

                ElseIf freq = "BiMonthly" Then
                    panBiMonthly.Visible = True
                    radBiMonthly.Checked = True

                    Dim param1(0) As SqlParameter
                    param1(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                    param1(0).Value = rid

                    ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_SCHDL_DT_BYPLAN", param1)


                    Do While ObjDR.Read()
                        pmdate = ObjDR("pmdate").ToString()
                    Loop
                    ObjDR.Close()


                    Dim dd, mm, i
                    dd = day(pmdate)
                    mm = Month(pmdate)

                    If (dd <> B_DrpDwnDate.SelectedItem.Value) Then
                        insertflag = 1
                    End If

                    If (mm <> B_DrpDwnMonth().SelectedItem.Value) Then
                        insertflag = 1
                    End If


                ElseIf freq = "Quarterly" Then
                    panQuarterly.Visible = True
                    radQuarterly.Checked = True

                    strSQL = "select min(PVD_PLANSCHD_DT) as pmdate from " & HttpContext.Current.Session("TENANT") & "." & "PVM_DETAILS where PVD_PLAN_ID='" & rid & "' and month(PVD_PLANSCHD_DT) in (4,5,6)"
                    ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                    Do While ObjDR.Read()
                        pmdate = ObjDR("pmdate").ToString()
                    Loop
                    ObjDR.Close()


                    Dim dd, mm, i
                    dd = day(pmdate)
                    mm = Month(pmdate)


                    If (dd <> Q1_DrpDwnDate.SelectedItem.Value) Then
                        insertflag = 1
                    End If

                    If (mm <> Q1_DrpDwnMonth().SelectedItem.Value) Then
                        insertflag = 1
                    End If



                    strSQL = "select min(PVD_PLANSCHD_DT) as pmdate from " & HttpContext.Current.Session("TENANT") & "." & "PVM_DETAILS where PVD_PLAN_ID='" & rid & "' and month(PVD_PLANSCHD_DT) in (7,8,9)"
                    ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                    Do While ObjDR.Read()
                        pmdate = ObjDR("pmdate").ToString()
                    Loop
                    ObjDR.Close()


                    dd = day(pmdate)
                    mm = Month(pmdate)

                    If (dd <> Q2_DrpDwnDate.SelectedItem.Value) Then
                        insertflag = 1
                    End If

                    If (mm <> Q2_DrpDwnMonth().SelectedItem.Value) Then
                        insertflag = 1
                    End If



                    strSQL = "select min(PVD_PLANSCHD_DT) as pmdate from " & HttpContext.Current.Session("TENANT") & "." & "PVM_DETAILS where PVD_PLAN_ID='" & rid & "' and month(PVD_PLANSCHD_DT) in (10,11,12)"
                    ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                    Do While ObjDR.Read()
                        pmdate = ObjDR("pmdate").ToString()
                    Loop
                    ObjDR.Close()


                    dd = day(pmdate)
                    mm = Month(pmdate)

                    If (dd <> Q3_DrpDwnDate.SelectedItem.Value) Then
                        insertflag = 1
                    End If

                    If (mm <> Q3_DrpDwnMonth().SelectedItem.Value) Then
                        insertflag = 1
                    End If



                    strSQL = "select min(PVD_PLANSCHD_DT) as pmdate from " & HttpContext.Current.Session("TENANT") & "." & "PVM_DETAILS where PVD_PLAN_ID='" & rid & "' and month(PVD_PLANSCHD_DT) in (1,2,3)"
                    ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                    Do While ObjDR.Read()
                        pmdate = ObjDR("pmdate").ToString()
                    Loop
                    ObjDR.Close()


                    dd = day(pmdate)
                    mm = Month(pmdate)

                    If (dd <> Q4_DrpDwnDate.SelectedItem.Value) Then
                        insertflag = 1
                    End If

                    If (mm <> Q4_DrpDwnMonth().SelectedItem.Value) Then
                        insertflag = 1
                    End If

                ElseIf freq = "Yearly" Then
                    panYearly.Visible = True
                    radYearly.Checked = True

                    strSQL = "exec FAM_GET_PVM_SCHDL_DT_BYPLAN '" & rid & "'"
                    ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                    Do While ObjDR.Read()
                        pmdate = ObjDR("pmdate").ToString()
                    Loop
                    ObjDR.Close()


                    Dim dd, mm, i
                    dd = day(pmdate)
                    mm = Month(pmdate)

                    If (dd <> Y_DrpDwnDate.SelectedItem.Value) Then
                        insertflag = 1
                    End If

                    If (mm <> Y_DrpDwnMonth().SelectedItem.Value) Then
                        insertflag = 1
                    End If


                ElseIf freq = "Daily" Then
                    panDaily.Visible = True
                    radDaily.Checked = True
                    Dim tt, i

                    '  StrSQL = "select (PVD_PLANSCHD_TIME) as pmdate from PVM_DETAILS where PVD_PLAN_ID='" & rid & "'"
                    strSQL = "exec FAM_GET_PVM_SCHDL_DT_BYPLAN '" & rid & "'"
                    ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                    Do While ObjDR.Read()
                        tt = ObjDR("pmdate").ToString()
                    Loop
                    ObjDR.Close()


                    If (tt <> D_DrpDwnHour.SelectedItem.Value) Then
                        insertflag = 1
                    End If

                End If
            Else
                insertflag = 1
            End If
            Check_Plan_Id = insertflag
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Function
 

    Private Sub B_DrpDwnMonth_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles b_drpdwnmonth.SelectedIndexChanged
        Dim i, j As Int16
        Dim dt As DateTime
        j = B_DrpDwnMonth.SelectedIndex
        B_DrpDwnDate.Items.Clear()
        Select Case j
            Case 1
                dt = txttodate.SelectedDate
                If dt.IsLeapYear(dt.Year) = True Then
                    For i = 1 To 29
                        B_DrpDwnDate.Items.Add(i)
                    Next
                Else
                    For i = 1 To 28
                        B_DrpDwnDate.Items.Add(i)
                    Next
                End If
            Case 3
                For i = 1 To 30
                    B_DrpDwnDate.Items.Add(i)
                Next
            Case 5
                For i = 1 To 30
                    B_DrpDwnDate.Items.Add(i)
                Next
            Case 8
                For i = 1 To 30
                    B_DrpDwnDate.Items.Add(i)
                Next
            Case 10
                For i = 1 To 30
                    B_DrpDwnDate.Items.Add(i)
                Next
        End Select
    End Sub
    Private Sub Y_DrpDwnMonth_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles y_drpdwnmonth.SelectedIndexChanged
        Dim i, j As Int16
        Dim dt As DateTime
        j = Y_DrpDwnMonth.SelectedIndex
        Y_DrpDwnDate.Items.Clear()
        Select Case j
            Case 1
                dt = txttodate.SelectedDate
                If dt.IsLeapYear(dt.Year) = True Then
                    For i = 1 To 29
                        Y_DrpDwnDate.Items.Add(i)
                    Next
                Else
                    For i = 1 To 28
                        Y_DrpDwnDate.Items.Add(i)
                    Next
                End If
            Case 3
                For i = 1 To 30
                    Y_DrpDwnDate.Items.Add(i)
                Next
            Case 5
                For i = 1 To 30
                    Y_DrpDwnDate.Items.Add(i)
                Next
            Case 8
                For i = 1 To 30
                    Y_DrpDwnDate.Items.Add(i)
                Next
            Case 10
                For i = 1 To 30
                    Y_DrpDwnDate.Items.Add(i)
                Next
        End Select
    End Sub

    Private Sub Q1_DrpDwnMonth_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles q1_drpdwnmonth.SelectedIndexChanged
        Dim i, j As Int16
        Dim dt As DateTime
        j = Q1_DrpDwnMonth.SelectedIndex
        Q1_DrpDwnDate.Items.Clear()
        Select Case j
            Case 0
                For i = 1 To 30
                    Q1_DrpDwnDate.Items.Add(i)
                Next
            Case 1
                For i = 1 To 31
                    Q1_DrpDwnDate.Items.Add(i)
                Next
            Case 2
                For i = 1 To 30
                    Q1_DrpDwnDate.Items.Add(i)
                Next
        End Select
    End Sub

    Private Sub Q2_DrpDwnMonth_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles q2_drpdwnmonth.SelectedIndexChanged
        Dim i, j As Int16
        Dim dt As DateTime
        j = Q2_DrpDwnMonth.SelectedIndex
        Q2_DrpDwnDate.Items.Clear()
        Select Case j
            Case 0
                For i = 1 To 31
                    Q2_DrpDwnDate.Items.Add(i)
                Next
            Case 0
                For i = 1 To 31
                    Q2_DrpDwnDate.Items.Add(i)
                Next
            Case 2
                For i = 1 To 30
                    Q2_DrpDwnDate.Items.Add(i)
                Next
        End Select
    End Sub

    Private Sub Q3_DrpDwnMonth_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles q3_drpdwnmonth.SelectedIndexChanged
        Dim i, j As Int16
        Dim dt As DateTime
        j = Q3_DrpDwnMonth.SelectedIndex
        Q3_DrpDwnDate.Items.Clear()
        Select Case j
            Case 0
                For i = 1 To 31
                    Q3_DrpDwnDate.Items.Add(i)
                Next
            Case 1
                For i = 1 To 30
                    Q3_DrpDwnDate.Items.Add(i)
                Next
            Case 2
                For i = 1 To 31
                    Q3_DrpDwnDate.Items.Add(i)
                Next
        End Select
    End Sub

    Private Sub Q4_DrpDwnMonth_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles q4_drpdwnmonth.SelectedIndexChanged
        Dim i, j As Int16
        Dim dt As DateTime
        j = Q4_DrpDwnMonth.SelectedIndex
        Q4_DrpDwnDate.Items.Clear()
        Select Case j
            Case 0
                For i = 1 To 31
                    Q4_DrpDwnDate.Items.Add(i)
                Next

            Case 1
                dt = txttodate.SelectedDate
                If dt.IsLeapYear(dt.Year) = True Then
                    For i = 1 To 29
                        Q4_DrpDwnDate.Items.Add(i)
                    Next
                Else
                    For i = 1 To 28
                        Q4_DrpDwnDate.Items.Add(i)
                    Next
                End If
            Case 2
                For i = 1 To 31
                    Q4_DrpDwnDate.Items.Add(i)
                Next
        End Select
    End Sub

     

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("View_Maintanance_Schedule.aspx")
    End Sub
End Class
