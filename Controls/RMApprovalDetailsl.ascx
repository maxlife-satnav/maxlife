<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RMApprovalDetailsl.ascx.vb"
    Inherits="Controls_RMApprovalDetailsl" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">                  
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Requisition Id</label>
                <div class="col-md-7">
                    <asp:TextBox ID="lblReqId" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Raised By</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Category</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Sub Category</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Brand/Make</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Model </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

</div>
<div id="pnlItems" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">

                <fieldset>
                    <legend>Assets List</legend>
                </fieldset>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 320px">
           <asp:GridView ID="gvItems" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                EmptyDataText="No request(s) found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:BoundField DataField="AST_MD_id" HeaderText="Item Code" ItemStyle-HorizontalAlign="left" Visible="false" />
                    <asp:BoundField DataField="AST_MD_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left" />

                    <asp:BoundField DataField="Category" HeaderText="Category Name" ItemStyle-HorizontalAlign="left">
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="manufacturer" HeaderText="Brand Name" ItemStyle-HorizontalAlign="left">
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AID_QTY")%>'></asp:TextBox>
                            <asp:Label ID="lblProductid" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblReq_status" runat="server" Visible="false" Text='<%# Eval("AIR_STA_ID") %>'></asp:Label>
                            <asp:CheckBox ID="chkSelect" runat="server" Checked='<%#Bind("ticked") %>' Enabled="false"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
              
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<br />
<br />
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Status</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div id="tr3" runat="Server" class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Requestor Remarks</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div id="tr1" runat="Server" class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Admin/IT Approval Remarks</label>
                <div class="col-md-7">
                    <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRMRemarks"
                        Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"
                        Enabled="true"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div id="tr2" runat="server" visible="false" class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Admin Remarks</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="MultiLine" CssClass="form-control"
                        ReadOnly="True"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnApprove" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnCancel" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnBack_Click" />
        </div>
    </div>
</div>
