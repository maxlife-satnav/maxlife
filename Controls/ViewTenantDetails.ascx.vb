Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ViewTenantDetails
    Inherits System.Web.UI.UserControl
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'gvPropType.PageIndex = 0
        If ddlproptype.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 And ddlProp.SelectedIndex > 0 Then
            gvPropType.Visible = True
            fillgrid()
            If gvPropType.Rows.Count > 0 Then
                'tab.Visible = True
                btnexporttoexcel.Enabled = True
                btnexporttoexcel.Visible = True
            Else
                'tab.Visible = False
                btnexporttoexcel.Enabled = False
            End If
        Else
            gvPropType.Visible = False
            'tab.Visible = False
        End If

    End Sub
    Public Sub fillgrid()
        Try
            Val = "B"
            Session("value") = Val
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_DETAILS")
            sp2.Command.AddParameter("@FLAG", 1, DbType.Int32)
            Session("dataset") = sp2.GetDataSet()
            gvPropType.DataSource = Session("dataset")
            'sp2.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String)
            'sp2.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
            'sp2.Command.AddParameter("@PN_NAME", ddlProp.SelectedItem.Value, DbType.String)
            'sp2.Command.AddParameter("@PROPERTY_TYPE", ddlproptype.SelectedItem.Value, DbType.String)
            'sp2.Command.AddParameter("@USER", Session("uid"), DbType.String)
            'sp2.Command.AddParameter("@GET_ALL_REC", Val, DbType.String)
            'gvPropType.DataSource = sp2.GetDataSet()
            gvPropType.DataBind()
            gvPropType.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
            LBTN1.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            BindPropType()
            BindCity()
            Session("CurrentPageIndex") = 0
            fillgrid()
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_DETAILS_FOR_PI")
            'sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            'Dim ds As New DataSet()
            'ds = sp.GetDataSet()
            'ddlCity.SelectedValue = ds.Tables(0).Rows(0).Item("CITY_ID")
            'ddlCity.Enabled = False

        End If
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_CITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@USR_ID", Session("UID"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
        LBTN1.Visible = False
        btnexporttoexcel.Enabled = False
        'tab.Visible = False
    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

        Catch ex As Exception

        End Try

    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'Dim SP3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TENANT_RECORD")
        'SP3.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String)
        'SP3.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
        'SP3.Command.AddParameter("@PN_NAME", ddlProp.SelectedItem.Value, DbType.String)
        'gvPropType.DataSource = SP3.GetDataSet()
        'gvPropType.DataBind()
        'SP3.ExecuteScalar()
        ' ddlproptype.SelectedIndex = 0
        'ddlCity.SelectedIndex = 0
        'If ddlProp.Items.Count > 0 Then
        'ddlProp.SelectedIndex = 0
        'End If
        'Try
        ' Val = "1"
        ' Session("value") = Val
        'SearchFn(Val)
        'Catch ex As Exception
        'Response.Write(ex.Message)
        'End Try
        'LBTN1.Visible = True
        'tab.Visible = True
        'btnexporttoexcel.Enabled = True
        Try
            fillgridOnTenantCodeSearch()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        btnexporttoexcel.Enabled = True

    End Sub
    Dim Val As String
    Private Sub SearchFn(Val)
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_DETAILS")
            sp2.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_ADM_CODE", String.Empty, DbType.String)
            sp2.Command.AddParameter("@PN_NAME", String.Empty, DbType.String)
            sp2.Command.AddParameter("@PROPERTY_TYPE", String.Empty, DbType.String)
            sp2.Command.AddParameter("@USER", Session("uid"), DbType.String)
            sp2.Command.AddParameter("@GET_ALL_REC", Val, DbType.String)
            gvPropType.Visible = True
            gvPropType.DataSource = sp2.GetDataSet()
            gvPropType.DataBind()
            gvPropType.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
            LBTN1.Visible = True
            If gvPropType.Rows.Count <> 0 Then
                btnexporttoexcel.Visible = True
            Else
                btnexporttoexcel.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub fillgridOnTenantCodeSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_DETAILS")
            sp2.Command.AddParameter("@FLAG", 2, DbType.Int32)
            sp2.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String)
            Session("dataset") = sp2.GetDataSet()
            gvPropType.DataSource = Session("dataset")
            gvPropType.DataBind()
            'gvPropType.DataSource = sp2.GetDataSet()
            'gvPropType.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvPropType_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPropType.RowCommand
        If e.CommandName = "Delete" Then
            Dim currentpageindex As Integer = 0
            If Session("CurrentPageIndex") <> Nothing Then
                currentpageindex = (Integer.Parse(Session("CurrentPageIndex")))
            End If
            'Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString()) - (currentpageindex * 8)
            Dim lblid As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblid"), Label)
            Dim SP3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "DEL_TENANT_RECORD")
            SP3.Command.AddParameter("@TEN_CODE", lblid.Text, DbType.Int32)
            SP3.ExecuteScalar()
            If (Session("value").ToString() = "B") Then
                fillgrid()
            Else
                SearchFn(Session("value").ToString())
            End If
        End If
        ' fillgrid()
    End Sub

    Protected Sub gvPropType_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPropType.PageIndexChanging
        gvPropType.PageIndex = e.NewPageIndex()
        gvPropType.DataSource = Session("dataset")
        gvPropType.DataBind()
        'gvPropType.PageIndex = e.NewPageIndex
        'SearchFn(Session("value").ToString())
        'Session("CurrentPageIndex") = e.NewPageIndex
        ' tab.Visible = True
    End Sub

    Protected Sub LBTN1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBTN1.Click
        gvPropType.Visible = False
        If Not IsPostBack Then
            gvPropType.PageIndex = 0
        End If
        gvPropType.Visible = True
        'fillgrid()
        txtSearch.Text = String.Empty
        Val = "A"
        Session("value") = Val
        SearchFn(Val)
        gvPropType.Visible = True
        ' tab.Visible = True
        'txtSearch.Text = ""
        btnexporttoexcel.Enabled = True
    End Sub

    Protected Sub btnexporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexporttoexcel.Click
        exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        exportpanel1.FileName = "Tenant Details Report"
    End Sub

    Protected Sub gvPropType_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPropType.RowDeleting

    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Try
            gvPropType.Visible = False
            btnexporttoexcel.Visible = False
            txtSearch.Text = String.Empty
            If ddlCity.SelectedIndex > 0 And ddlproptype.SelectedIndex > 0 Then
                BindCityLoc()
                'BindProp()
                gvPropType.DataSource = Nothing
                gvPropType.DataBind()
            Else
                ddlProp.Items.Clear()
                ddlProp.Items.Insert(0, New ListItem("--Select--", "0"))
            End If


        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlproptype.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
            BindProp()
            gvPropType.DataSource = Nothing
            gvPropType.DataBind()

        Else
            ddlProp.Items.Clear()
            ddlProp.Items.Insert(0, New ListItem("--Select--", "0"))
        End If
    End Sub

    Private Sub BindProp()
        Try


            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTY_CITY")
            sp1.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@PROPERTYTYPE", ddlproptype.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@USER", Session("uid"), DbType.String)
            ddlProp.DataSource = sp1.GetDataSet()
            ddlProp.DataTextField = "PN_NAME"
            ddlProp.DataValueField = "BDG_ID"
            ddlProp.DataBind()
            ddlProp.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlproptype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' txtSearch.Text = String.Empty
        'btnexporttoexcel.Visible = False
        'gvPropType.Visible = False

    End Sub

    Protected Sub ddlProp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProp.SelectedIndexChanged
        txtSearch.Text = String.Empty
        btnexporttoexcel.Visible = False
        gvPropType.Visible = False
    End Sub
End Class
