﻿Imports System.Data
Imports System.Data.SqlClient


Partial Class Controls_ConsumableItemIssuance
    Inherits System.Web.UI.UserControl

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Dim id As String = Request.QueryString("id")
            'lblTemp.Text = id
            dispdata()
        End If

    End Sub

    Private Sub dispdata()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_VIEW_REQDETAILS")
        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_FOR_ASSETGRIDVR")
        SP.Command.AddParameter("@Req_id", Request.QueryString("RID"), DbType.String)
        Dim ds As New DataSet
        ds = SP.GetDataSet()
        Dim req_status As Integer = 0

        If ds.Tables(0).Rows.Count > 0 Then
            lblReqId.Text = ds.Tables(0).Rows(0).Item("AIR_REQ_TS")
            lblLoc.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
            'lblTwr.Text = ds.Tables(0).Rows(0).Item("TWR_NAME")
            'lblFlr.Text = ds.Tables(0).Rows(0).Item("FLR_NAME")
            lblRem.Text = ds.Tables(0).Rows(0).Item("AIR_REMARKS")
            lblRMRem.Text = ds.Tables(0).Rows(0).Item("AIR_RM_REMARKS")
            lblSMRem.Text = ds.Tables(0).Rows(0).Item("AIR_ADM_REMARKS")
            'lblLocCode.Text = ds.Tables(0).Rows(0).Item("LCM_CODE")
            'lblTwrCode.Text = ds.Tables(0).Rows(0).Item("TWR_CODE")
            'lblFlrCode.Text = ds.Tables(0).Rows(0).Item("FLR_CODE")
            req_status = ds.Tables(0).Rows(0).Item("AIR_STA_ID")

        End If



        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        ObjSubsonic.BindGridView(gvItems, "AST_GET_CONSUMABLES_FOR_ASSETGRIDVR", param)

    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try

            Validate(Request.QueryString("RID"))
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Validate(ByVal ReqId As String)


        Dim count As Integer = 0
        Dim Message As String = String.Empty

        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)


            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtQty.Text) Then
                    lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                    Exit Sub
                ElseIf IsNumeric(txtQty.Text) = True Then
                    count = count + 1
                    If count = 0 Then
                        lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
                        Exit Sub
                    End If
                Else
                    lblMsg.Text = "Please enter Quantity in Numerics Only"
                    Exit Sub
                End If
            End If
        Next
        If count > 0 Then
            UpdateData(ReqId)

            For Each row As GridViewRow In gvItems.Rows

                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
                Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
                Dim lblMinOrdQty As Label = DirectCast(row.FindControl("lblMinOrdQty"), Label)
                If chkSelect.Checked Then
                    If String.IsNullOrEmpty(txtQty.Text) Then
                        lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                        Exit Sub
                    ElseIf IsNumeric(txtQty.Text) = True Then
                        If CInt(lblMinOrdQty.Text) <= CInt(txtQty.Text) Then
                            count = count + 1
                            If count > 0 Then
                                InsertDetails(ReqId, lblProductid.Text, CInt(Trim(txtQty.Text)))
                            End If
                        Else
                            lblMsg.Text = "Minimum Qty. should be  " + lblMinOrdQty.Text
                            Exit Sub
                        End If

                    End If

                End If
            Next
            '' 20 is for sending for mail to issued asset
            sendMail(20)
            ''
            Page.ClientScript.RegisterStartupScript(GetType(Page), "Javascript", "javascript: printreq('../../FAM/FAM_WEBfiles/frmGenerateIssuePrint.aspx?id=" & Request.QueryString("RID") & "','frmAssetThanks.aspx?RID=" & Request.QueryString("RID") & "');", True)
            'Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString("RID"))
        ElseIf count = 0 Then
            lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
        Else
            lblMsg.Text = "Please enter Quantity for Selected Checkbox"
        End If
    End Sub

    Private Sub UpdateData(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_UpdateByReqIdISSUANCE")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@StatusId", 1509, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_update")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_UPDATEBYREQID_ISSUECANCEL")
        sp.Command.AddParameter("@ReqId", Request.QueryString("RID"), DbType.String)
        sp.Command.AddParameter("@StatusId", 1510, DbType.Int32)
        sp.ExecuteScalar()
        Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString("RID"))

    End Sub

    Public Sub sendMail(ByVal id As Integer)
        Try


            Dim drmail As SqlDataReader
            Dim drdata As SqlDataReader

            Dim mail_body As String = String.Empty
            Dim subject As String = String.Empty
            Dim sm_email As String = String.Empty
            Dim emp_email As String = String.Empty

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MAILMASTER_ID")
            sp.Command.AddParameter("@MAIL_ID", id, DbType.Int32)
            drmail = sp.GetReader()
            If drmail.Read() Then
                mail_body = drmail("MAIL_BODY").ToString()
                subject = drmail("MAIL_SUBJECT").ToString()
            End If

            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_FOR_ASSETISSUEMAIL")
            sp2.Command.AddParameter("@Req_id", lblReqId.Text, DbType.String)
            drdata = sp2.GetReader()

            If drdata.Read() Then
                mail_body = Replace(mail_body, "@@USER", drdata("AIR_APP_AUTH").ToString())
                mail_body = Replace(mail_body, "@@REQID", drdata("AIR_REQ_TS").ToString())
                mail_body = Replace(mail_body, "@@LOC", lblLoc.Text)
                mail_body = Replace(mail_body, "@@ITEMS", drdata("QTY").ToString())
                mail_body = Replace(mail_body, "@@EMP", drdata("AIR_AUR_ID").ToString())
                mail_body = Replace(mail_body, "@@RAISE_DATE", drdata("AIR_REQ_DATE").ToString())
                mail_body = Replace(mail_body, "@@ISSU_DATE", drdata("AIR_UPT_DATE").ToString())
                sm_email = drdata("SM_EMAIL").ToString()
                emp_email = drdata("EMP_MAIL").ToString()
            Else
                lblMsg.Text = "Mail Id Does Not Exists for the User"
                Exit Sub
            End If
            If mail_body IsNot Nothing Then
                Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_INSERT_AMTMAIL")
                sp3.Command.AddParameter("@AMT_ID", lblReqId.Text, DbType.String)
                sp3.Command.AddParameter("@AMT_MESSAGE", mail_body, DbType.String)
                sp3.Command.AddParameter("@AMT_MAIL_TO", emp_email, DbType.String)
                sp3.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
                sp3.Command.AddParameter("@AMT_MAIL_TIME", getoffsetdatetime(DateTime.Now), DbType.String)
                sp3.Command.AddParameter("@AMT_FLAG", "Request Submitted", DbType.String)
                sp3.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
                sp3.Command.AddParameter("@AMT_MAIL_CC", sm_email, DbType.String)
                sp3.Execute()
            End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Asset Issuance", "Load", ex)
        End Try

    End Sub

End Class
