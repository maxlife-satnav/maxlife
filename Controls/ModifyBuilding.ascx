<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ModifyBuilding.ascx.vb" Inherits="Controls_ModifyBuilding" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Edit Property
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Edit Building</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <table id="table2" cellspacing="0" cellpadding="1" width="100%" border="1">
                    <tr>
                        <td align="left" width="25%">
                            <asp:Label ID="lbllocation" runat="server" CssClass="bodytext" Text="Select Location"></asp:Label>
                            <asp:RequiredFieldValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlLocation"
                                Display="Dynamic" ErrorMessage="Select Location" ValidationGroup="Val1"
                                initialvalue="--Select Location--"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="dropDown" Width="99%">
                              
                            </asp:DropDownList>
                            <br />
                        </td>
                        <td align="left" width="25%">
                            <asp:Label ID="lblBuildingCode" runat="server" CssClass="bodytext" Text="Building Code"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBuildingCode"
                                Display="Dynamic" ErrorMessage="Required Building Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="25%">
                            <asp:TextBox ID="txtBuildingCode" CssClass="textBox" runat="server" MaxLength="50"
                                Width="97%"></asp:TextBox>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="25%">
                            <asp:Label ID="lblBuilding" runat="server" CssClass="bodytext" Text="Building Name"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBuilding"
                                Display="Dynamic" ErrorMessage="Required Building Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="25%">
                            <asp:TextBox ID="txtBuilding" CssClass="textBox" runat="server" Width="97%"></asp:TextBox>
                            <br />
                        </td>
                        <td align="left" width="25%">
                            <asp:Label ID="lblPreQID" runat="server" CssClass="bodytext" Text="Building PREQ ID"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvID" runat="server" ControlToValidate="TXTPREQID"
                                Display="Dynamic" ErrorMessage="Required Building PREQID" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            
                        </td>
                        <td align="left" width="25%">
                            
                                <asp:TextBox ID="TXTPREQID" CssClass="textBox" runat="server" Width="97%"></asp:TextBox>
                                <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="25%">
                            <asp:Label ID="lblGSCID" runat="server" CssClass="bodytext" Text="Building GSCID"></asp:Label>
                            <asp:RequiredFieldValidator ID="RFVGSCID" runat="server" ControlToValidate="txtGSCID"
                                Display="Dynamic" ErrorMessage="Required Building GSC ID" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revgscid" runat="server" ControlToValidate="txtGSCID"
                                ErrorMessage="Please enter valid GSCID" ValidationExpression="^[0-9]*$" ValidationGroup="Val1">
                            </asp:RegularExpressionValidator>
                        </td>
                        <td align="left" width="25%">
                            <div onmouseover="Tip('Enter ID in numbers only)" onmouseout="UnTip()">
                                <asp:TextBox ID="txtGSCID" CssClass="textBox" runat="server" Width="97%"></asp:TextBox>
                                <br />
                        </td>
                        
                    <td align="left" width="25%">
                            <asp:Label ID="lblAddress" runat="server" CssClass="bodytext" Text="Building Address"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                                Display="Dynamic" ErrorMessage="Required Building Address" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="25%">
                            <asp:TextBox ID="txtAddress" CssClass="textBox" runat="server" Width="97%" TextMode="MultiLine" Rows="5"></asp:TextBox>
                            <br />
                        </td>
                    </tr>
                   
                    <tr>
                        <td align="left" width="25%">
                            <asp:Label ID="Label1" runat="server" CssClass="bodytext" Text="Select Building Category"></asp:Label>
                        <asp:RequiredFieldValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlcategory"
                                Display="Dynamic" ErrorMessage="Select Building Category" ValidationGroup="Val1"
                                initialvalue="0"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlcategory" runat="server" CssClass="dropDown" Width="99%">
                               
                              
                            </asp:DropDownList>
                            <br />
                        </td>
                        <td align="left" width="25%">
                            <asp:Label ID="lblStatus" runat="server" CssClass="bodytext" Text="Select Status"></asp:Label>
                        <asp:RequiredFieldValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlstatus"
                                Display="Dynamic" ErrorMessage="Please Select Status" ValidationGroup="Val1"
                                initialvalue="--Select Status--"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlstatus" runat="server" CssClass="dropDown" Width="99%">
                              <asp:ListItem>--Select Status--</asp:ListItem>
                                <asp:ListItem Value="0">InActive</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>
                            </asp:DropDownList>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        
                       
                    </tr>
                    <tr>
                        <td align="left" width="25%">
                            <asp:Label ID="lbCpArea" runat="server" CssClass="bodytext" Text="CarpetArea"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvCarpetArea" runat="server" ControlToValidate="txtcparea"
                                Display="Dynamic" ErrorMessage="Required Building Carpet-Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtcparea" ErrorMessage="Please enter valid Carpet Area" ValidationGroup="Val1" Type="double" Operator="DataTypeCheck"></asp:CompareValidator>
                        </td>
                        <td align="left" width="25%">
                            <div onmouseover="Tip('Enter CarpetArea in numbers and decimals)" onmouseout="UnTip()">
                                <asp:TextBox ID="txtcparea" CssClass="textBox" runat="server" Width="97%" MaxLength="5"></asp:TextBox>
                                <br />
                        </td>
                        <td align="left" width="25%">
                            <asp:Label ID="lblBUArea" runat="server" CssClass="bodytext" Text="Built-Up Area"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvBuiltuparea" runat="server" ControlToValidate="txtBuiltuparea"
                                Display="Dynamic" ErrorMessage="Required Building Carpet-Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtBuiltuparea" ErrorMessage="Please enter valid Builtup Area" ValidationGroup="Val1" Type="double" Operator="DataTypeCheck"></asp:CompareValidator>
                        </td>
                        <td align="left" width="25%">
                            <div onmouseover="Tip('Enter BuiltupArea in numbers and decimals)" onmouseout="UnTip()">
                                <asp:TextBox ID="txtBuiltuparea" CssClass="textBox" runat="server" Width="97%" AutoPostBack="True" MaxLength="5"></asp:TextBox>
                                <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="25%">
                            <asp:Label ID="lblPMSID" runat="server" CssClass="bodytext" Text="Building PMS_ID"></asp:Label>
                            <asp:RequiredFieldValidator ID="RFVPMSID" runat="server" ControlToValidate="txtPMSID"
                                Display="Dynamic" ErrorMessage="Required Building PMSID" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="25%">
                            <asp:TextBox ID="txtPMSID" CssClass="textBox" runat="server" MaxLength="500" Width="97%"></asp:TextBox>
                        </td>
                        <td align="left" width="25%">
                            <asp:Label ID="lblLatLng" runat="server" CssClass="bodytext" Text="Enter Latitude Longitude for Building"></asp:Label>
                        </td>
                        <td align="left" width="25%">
                            <asp:TextBox ID="txtLat" CssClass="textBox" runat="server" MaxLength="8" Width="80px"></asp:TextBox>
                            <asp:TextBox ID="txtLon" CssClass="textBox" runat="server" MaxLength="8" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="25%">
                            <asp:Label ID="lblBldgDesc" runat="server" CssClass="bodytext" Text="Building Description"></asp:Label>
                        </td>
                        <td align="left" width="25%">
                            <asp:TextBox ID="txtBldgDesc" CssClass="textBox" runat="server" MaxLength="500" TextMode="MultiLine"
                                Width="97%" Rows="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">
                            <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="Val1" CausesValidation="true"  />
                             <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" CausesValidation="false" />
                            </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left" style="height: 17px">
                            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage" Visible="True"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
        &nbsp;</td>
    </tr>
    <tr>
        <td style="width: 10px; height: 17px;">
            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
        <td style="height: 17px; width: 17px;">
            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
    </tr>
    </table>
</div>