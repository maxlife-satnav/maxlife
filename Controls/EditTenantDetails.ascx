<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditTenantDetails.ascx.vb"
    Inherits="Controls_EditTenantDetails" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Property Type <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                    Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlproptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select City<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Location <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Property<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvbuilding" runat="server" ControlToValidate="ddlBuilding"
                    Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>



</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Tenant Occupied Area<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfarea" runat="server" ControlToValidate="txtTenantOccupiedArea"
                    Display="None" ErrorMessage="Please enter Tenant Occupied Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revarea" runat="server" ControlToValidate="txtTenantOccupiedArea"
                    ErrorMessage="Please Enter Valid Tenant Occupied Area in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                    ValidationExpression="((\d+)((\.\d{1,2})?))$" Display="None"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Tenant Occupied Area in numbers upto 2 decimal places.')"
                        onmouseout="UnTip()">
                        <asp:TextBox ID="txtTenantOccupiedArea" runat="server" CssClass="form-control"
                            MaxLength="20"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Tenant Rent<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfRent" runat="server" ControlToValidate="txtRent"
                    Display="None" ErrorMessage="Please enter Tenant Rent" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRent"
                    ErrorMessage="Please Enter Valid Tenant Rent in Numbers or Decimal Number with 2 Decimal Places." Display="None" ValidationGroup="Val1" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter tenant rent in numbers upto 2 decimal places.')"
                        onmouseout="UnTip()">
                        <asp:TextBox ID="txtRent" runat="server" AutoPostBack="true" CssClass="form-control" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Joining Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Joining Date"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Next Payable Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvPayDate" runat="server" ControlToValidate="txtPayableDate"
                    Display="none" ValidationGroup="Val1" ErrorMessage="Please Select Next Payable Date"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='Payabledate'>
                        <asp:TextBox ID="txtPayableDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('Payabledate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Security Deposit<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                    Display="None" ErrorMessage="Please enter Security Deposit" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revdeposit" runat="server" Display="None" ControlToValidate="txtSecurityDeposit"
                    ErrorMessage="Please Enter Valid Security Deposit in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Security Deposit only in numbers upto 2 decimal places.')"
                        onmouseout="UnTip()">
                        <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control"
                            MaxLength="20"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Payment Terms<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlPaymentTerms"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Payment Terms"
                    InitialValue="--Select Payment Terms--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlPaymentTerms" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem>--Select Payment Terms--</asp:ListItem>
                        <asp:ListItem>Weekly</asp:ListItem>
                        <asp:ListItem>Monthly</asp:ListItem>
                        <asp:ListItem>Half-Yearly</asp:ListItem>
                        <asp:ListItem>Annual</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Tenant Status<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlstatus"
                    Display="None" ErrorMessage="Please Select Payment Status" ValidationGroup="Val1"
                    InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem>--Select Status--</asp:ListItem>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Number of Parking<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfparking" runat="server" ControlToValidate="txtNoofparking"
                    Display="None" ValidationGroup="Val1" ErrorMessage="Please enter Number of parking"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revNoofParking" Display="none" ValidationGroup="Val1" runat="server"
                    ControlToValidate="txtNoofparking" ErrorMessage="Please enter Valid Number of Parking"
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter numbers only with maximum length 5')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtNoofparking" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Maintenance Fees<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvfees" runat="server" ControlToValidate="txtfees"
                    Display="None" ValidationGroup="Val1" ErrorMessage="Please enter Maintenance Fees"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revfees" Display="None" runat="server" ControlToValidate="txtfees"
                    ErrorMessage="Please Enter Valid Maintenance fees in Numbers or Decimal Number with 2 Decimal Places." ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Maintenance Fees in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtfees" runat="server" AutoPostBack="true" CssClass="form-control" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Outstanding amount<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvamt" runat="server" ControlToValidate="txtamount"
                    Display="None" ValidationGroup="Val1" ErrorMessage="Please enter Outstanding amount"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revamt" Display="none" runat="server" ControlToValidate="txtamount"
                    ErrorMessage="Please enter Valid Outstanding amount in Numbers or Decimal Number with 2 Decimal Places" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter numbers with maximum length 20 only')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select User<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvuser" runat="server" ControlToValidate="ddluser"
                    Display="None" ErrorMessage="Please Select User" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddluser" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Remarks<span style="color: red;">*</span></label>

                <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                        Rows="3" MaxLength="500"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <div class="row">
                <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />&nbsp;
                            <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" />
            </div>
        </div>
    </div>
</div>
