Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_Mapped_Unmapped_AstDispose
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
                panel1.Visible = False
                lblMsg.Text = ""

            Else
                BindLocation()
                getassetcategory()
                getsubcategorybycat(ddlastCat.SelectedItem.Value)
                ddlastsubCat.SelectedIndex = If(ddlastsubCat.Items.Count > 1, 1, 0)
                getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
                ddlAstBrand.SelectedIndex = If(ddlAstBrand.Items.Count > 1, 1, 0)
                getModelbycatsubcat()
                ddlAstModel.SelectedIndex = If(ddlAstModel.Items.Count > 1, 1, 0)

                BindStockReport()
                lblMsg.Text = ""
                'ddlLocation.Items.Insert(1, New ListItem("--All--", "1"))
                panel1.Visible = True
                If gvAstList.Rows.Count = 0 Then
                    gvAstList.DataSource = Nothing
                    gvAstList.DataBind()
                End If
            End If
        End If
    End Sub

    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")

        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.Items.Insert(0, New ListItem("--All--", "All"))
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)

        'ddlLocation.Items.Remove("--All--")
    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        'ddlastCat.Items.Insert(0, "--Select--")
        ddlastCat.Items.Remove("--Select--")
        ddlastCat.Items.Insert(0, New ListItem("--All--", "All"))
        ddlastCat.SelectedIndex = If(ddlastCat.Items.Count > 1, 1, 0)


    End Sub
    Private Sub getbrandbycatsubcat(ByVal astcatcode As String, ByVal astsubcatcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlastsubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--All--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstModel.Items.Insert(0, New ListItem("--All--", ""))


    End Sub
    Private Sub getModelbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlastsubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))


    End Sub


    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlastsubCat.DataSource = sp.GetDataSet()
        ddlastsubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlastsubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlastsubCat.DataBind()
        ' ddlastsubCat.Items.Insert(0, "--Select--")
        ddlastsubCat.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub
    Protected Sub ddlastCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlastCat.SelectedIndexChanged
        getsubcategorybycat(ddlastCat.SelectedItem.Value)
        getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
        getModelbycatsubcat()
        BindStockReport()
    End Sub
    Protected Sub ddlastsubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlastsubCat.SelectedIndexChanged
        If ddlastsubCat.SelectedIndex > 0 Then
            getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
            getModelbycatsubcat()
            BindStockReport()
        End If

    End Sub
    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex > 0 Then
            getModelbycatsubcat()
            BindStockReport()
        End If

    End Sub


    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex = 1 Then

            ElseIf ddlLocation.SelectedIndex > 1 Then

            Else
                gvAstList.DataSource = Nothing
                gvAstList.DataBind()

            End If
            BindStockReport()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindStockReport()
        'Dim Lcm_code As String = ""
        'Dim Twr_code As String = ""
        'Dim Flr_code As String = ""
        'Dim Aim_code As String = ""
        'If ddlLocation.SelectedItem.Value = "--Select--" Then
        '    lblMsg.Text = "Please Select Location"
        '    Exit Sub
        'ElseIf ddlLocation.SelectedItem.Value = "1" Then
        '    Lcm_code = ""
        '    Twr_code = ""
        '    Flr_code = ""
        '    lblMsg.Text = ""
        'Else
        '    lblMsg.Text = ""
        '    Lcm_code = ddlLocation.SelectedItem.Value

        'End If



        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@AST_CAT_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlastCat.SelectedItem.Value
        param(1) = New SqlParameter("@AST_SUBCAT_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlastsubCat.SelectedItem.Value
        param(2) = New SqlParameter("@AST_BRND", SqlDbType.NVarChar, 200)
        param(2).Value = ddlAstBrand.SelectedItem.Value
        param(3) = New SqlParameter("@AST_MODEL", SqlDbType.NVarChar, 200)
        param(3).Value = ddlAstModel.SelectedItem.Value
        param(4) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(4).Value = ddlLocation.SelectedItem.Value
        param(5) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(5).Value = Session("uid")
        ObjSubsonic.BindGridView(gvAstList, "GET_ALLASTS", param)
        panel1.Visible = True
        If gvAstList.Rows.Count = 0 Then
            gvAstList.DataSource = Nothing
            gvAstList.DataBind()
            remarks.Visible = False
        Else
            remarks.Visible = True

        End If
    End Sub

    Protected Sub gvAstList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAstList.PageIndexChanging
        gvAstList.PageIndex = e.NewPageIndex
        BindStockReport()
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        BindStockReport()
    End Sub


    Protected Sub DisposeSelected_Click(sender As Object, e As EventArgs) Handles DisposeSelected.Click

        Try

            Dim Req_id As String
            Req_id = ObjSubsonic.RIDGENARATION("Admin/Ast/Dispose/")
            Dim AstTaggedId As Integer = 0

            Dim count As Integer = 0
            For Each gvRow As GridViewRow In gvAstList.Rows
                Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    count = count + 1
                    Exit For
                End If
            Next
            If count > 0 Then
                For Each gvRow As GridViewRow In gvAstList.Rows
                    Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
                    Dim lblAstCode As Label = DirectCast(gvRow.FindControl("lblAstCode"), Label)
                    Dim lbl_salvge As Label = DirectCast(gvRow.FindControl("lbl_salvge"), Label)
                    Dim lblEmpAstTaggedId As Label = DirectCast(gvRow.FindControl("lbl_emp_tag_id"), Label)
                    lblMsg.Text = ""

                    If chkSelect.Checked = True Then

                        If String.IsNullOrEmpty(lblEmpAstTaggedId.Text) = True Then
                        Else
                            AstTaggedId = lblEmpAstTaggedId.Text
                        End If

                        Dim param(12) As SqlParameter
                        param(0) = New SqlParameter("@DREQ_TS", SqlDbType.NVarChar, 200)
                        param(0).Value = Req_id
                        param(1) = New SqlParameter("@DREQ_ID", SqlDbType.NVarChar, 200)
                        param(1).Value = Req_id
                        param(2) = New SqlParameter("@AST_TAGGED_ID", SqlDbType.Int)
                        param(2).Value = AstTaggedId
                        param(3) = New SqlParameter("@DREQ_REQUESITION_DT", SqlDbType.DateTime)
                        param(3).Value = getoffsetdatetime(DateTime.Now)
                        param(4) = New SqlParameter("@DREQ_REQUESTED_BY", SqlDbType.NVarChar, 200)
                        param(4).Value = Session("uid")
                        param(5) = New SqlParameter("@DREQ_REQUESTED_REMARKS", SqlDbType.NVarChar, 2000)
                        param(5).Value = txtRemarks.Text
                        param(6) = New SqlParameter("@DREQ_STATUS", SqlDbType.Int)
                        param(6).Value = 1040
                        param(7) = New SqlParameter("@AST_SALAVAGE_VALUE", SqlDbType.Int)
                        param(7).Value = lbl_salvge.Text
                        param(8) = New SqlParameter("@AST_DISPOSE", SqlDbType.Bit)
                        param(8).Value = True
                        param(9) = New SqlParameter("@DREQ_ADMIN_DT", SqlDbType.Int)
                        param(9).Value = ""
                        param(10) = New SqlParameter("@DREQ_ADMIN_BY", SqlDbType.Int)
                        param(10).Value = ""
                        param(11) = New SqlParameter("@DREQ_ADMIN_REMARKS", SqlDbType.Int)
                        param(11).Value = ""
                        param(12) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
                        param(12).Value = lblAstCode.Text
                        ObjSubsonic.GetSubSonicExecute("INSERT_DISPOSE_REQ", param)
                        send_mail(Req_id, lblAstCode.Text)
                    End If

                Next
                lblMsg.Visible = True
                lblMsg.Text = "Dispose request has been raised Successfully"
                panel1.Visible = False
                remarks.Visible = False
            Else
                lblMsg.Text = "Please select the assets to Dispose"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub send_mail(ByVal reqid As String, ByVal astcode As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_DISPOSE_REQUISITION")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@AST_CODE", astcode, DbType.String)
        sp1.Execute()
    End Sub
    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
    Public Sub cleardata()
        ddlastCat.ClearSelection()
        ddlastsubCat.ClearSelection()
        ddlLocation.ClearSelection()
        ddlAstBrand.ClearSelection()
        ddlAstModel.ClearSelection()
        gvAstList.DataSource = Nothing
        gvAstList.DataBind()
        remarks.Visible = False
    End Sub

   
End Class
