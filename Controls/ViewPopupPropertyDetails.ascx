<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewPopupPropertyDetails.ascx.vb" Inherits="Controls_ViewPopupPropertyDetails" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>

<asp:ScriptManager ID="scriptmanager1" runat="server"></asp:ScriptManager>

<div class="row">
    <cc1:ExportPanel ID="exportpanel1" runat="server" ScrollBars="Auto">
        <asp:GridView ID="gvDetails" runat="server" EmptyDataText="Sorry! No Available Records..."
            RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Left" Width="100%"
            AllowPaging="True" AllowSorting="false" PageSize="3" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-condensed table-responsive">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <asp:TemplateField HeaderText="Property Details">
                    <ItemTemplate>
                        <table class="table table-bordered table-condensed table-responsive" align="right">
                            <tr>
                                <td>ID</td>
                                <td>
                                    <asp:Label ID="lblPN_LOCID" runat="server" Text='<%#Eval("sno")%>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Last Update Date</td>
                                <td>
                                    <asp:Label ID="lblLAST_UPDATE_DATE" runat="server" Text='<%#Eval("LAST_UPDATE_DATE")%>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Last Updated By</td>
                                <td>
                                    <asp:Label ID="lblLAST_UPDATED_BY" runat="server" Text='<%#Eval("LAST_UPDATED_BY")%>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Creation Date</td>
                                <td>
                                    <asp:Label ID="lblCREATION_DATE" runat="server" Text='<%#Eval("CREATION_DATE")%>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Created By</td>
                                <td>
                                    <asp:Label ID="lblCREATED_BY" runat="server" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("STATUS")%>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Property Description</td>
                                <td>
                                    <asp:Label ID="lblPROPERTY_DESCRIPTION" runat="server" Text='<%#Eval("PROPERTY_DESCRIPTION")%>'></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Property">
                    <ItemTemplate>
                        <asp:Label ID="lblPN_NAME" runat="server" Text='<%#Eval("PN_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblBDG_ADM_CODE" runat="server" Text='<%#Eval("BDG_ADM_CODE")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Max Capacity">
                    <ItemTemplate>
                        <asp:Label ID="lblMAX_CAPACITY" runat="Server" Text='<%#Eval("MAX_CAPACITY")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Optimum Capacity">
                    <ItemTemplate>
                        <asp:Label ID="lblOPTIMUM_CAPACITY" runat="Server" Text='<%#Eval("OPTIMUM_CAPACITY")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Rentable Area">
                    <ItemTemplate>
                        <asp:Label ID="lblRENTABLE_AREA" runat="Server" Text='<%#Eval("RENTABLE_AREA")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Usable Area">
                    <ItemTemplate>
                        <asp:Label ID="lblUSABLE_AREA" runat="Server" Text='<%#Eval("USABLE_AREA")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Carpet Area">
                    <ItemTemplate>
                        <asp:Label ID="lblCARPET_AREA" runat="Server" Text='<%#Eval("CARPET_AREA")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Builtup Area">
                    <ItemTemplate>
                        <asp:Label ID="lblBUILTUP_AREA" runat="Server" Text='<%#Eval("BUILTUP_AREA")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Common Area">
                    <ItemTemplate>
                        <asp:Label ID="lblCOMMON_AREA" runat="Server" Text='<%#Eval("COMMON_AREA")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="UOM Code">
                    <ItemTemplate>
                        <asp:Label ID="lblUOM_CODE" runat="Server" Text='<%#Eval("UOM_CODE")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Govt Property Code">
                    <ItemTemplate>
                        <asp:Label ID="lblGOVT_PROPERTY_CODE" runat="Server" Text='<%#Eval("GOVT_PROPERTY_CODE")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Insurance Type">
                    <ItemTemplate>
                        <asp:Label ID="lblInsType" runat="Server" Text='<%#Eval("IN_TYPE")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Insurance Vendor">
                    <ItemTemplate>
                        <asp:Label ID="lblInsVendor" runat="Server" Text='<%#Eval("IN_VENDOR")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Insurance Amount">
                    <ItemTemplate>
                        <asp:Label ID="lblInsAmt" runat="Server" Text='<%#Eval("IN_AMOUNT")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Insurance Policy Number">
                    <ItemTemplate>
                        <asp:Label ID="lblInsPolNum" runat="Server" Text='<%#Eval("IN_POLICY_NUMBER")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Insurance Start Date">
                    <ItemTemplate>
                        <asp:Label ID="lblInsSdate" runat="Server" Text='<%#Eval("IN_START_DATE")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Insurance End Date">
                    <ItemTemplate>
                        <asp:Label ID="lblInsEdate" runat="Server" Text='<%#Eval("IN_END_DATE")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
            <RowStyle HorizontalAlign="Center" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:GridView>
    </cc1:ExportPanel>
</div>

<div class="row">
    <div class="col-md-7 text-center">
        <asp:Button ID="btnexporttoexcel" runat="Server" Text="Export to Excel" CssClass="btn btn-primary custom-button-color" />
    </div>
</div>
