<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGGroupModify.ascx.vb"
    Inherits="Controls_AMGGroupModify" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

        <div>
            <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">ASSET GROUP<hr align="center" width="60%" /> </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                    </td>
                </tr>
            </table>
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="85%" Height="100%">
                <table id="Table3" cellspacing="0" cellpadding="0" style="vertical-align: top; width: 95%;"
                    align="center" border="0">
                    <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                   
                    <tr>
                        <td style="width: 10px">
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                        <td width="100%" class="tableHEADER" align="left">
                            <strong>&nbsp; Group Modification Details </strong>
                        </td>
                        <td style="width: 17px">
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                            &nbsp;</td>
                        <td align="left">
                         <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                            <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                                <tr>
                                    <td align="left" style="width: 50%; height: 26px;">
                                        Group Name <font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtName"
                                            Display="None" ErrorMessage="Please enter Name !" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 50%; height: 26px;">
                                        <asp:TextBox ID="txtName" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 50%; height: 26px;">
                                        Select Status 
                                    </td>
                                    <td align="left" style="width: 50%; height: 26px;">
                                        <asp:RadioButtonList ID="rdbtnGroupID" runat="server" CssClass="clsRadioButton" Width="97%"
                                            RepeatDirection="Horizontal" CellPadding="0" CellSpacing="0">
                                            <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                            <asp:ListItem Value="0">InActive</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 50%; height: 26px;">
                                         Remarks<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfRemarks" runat="server" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Please enter Remarks" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 50%; height: 26px;">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" Width="97%" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 50%; height: 26px;">
                                         EXHCode <font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvExhCode" runat="server" ControlToValidate="txtEXHCode"
                                            Display="None" ErrorMessage="Please enter EXHCode" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 50%; height: 26px;">
                                        <asp:TextBox ID="txtEXHCode" runat="server" CssClass="clsTextField" Width="97%" TextMode="MultiLine" Rows="3" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 50%; height: 26px;">
                                         MFType <font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfMFType" runat="server" ControlToValidate="txtMFType"
                                            Display="none" ErrorMessage="Please enter MFType" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 50%; height: 26px;">
                                        <asp:TextBox ID="txtMFType" runat="server" CssClass="clsTextField" Width="97%" TextMode="MultiLine" Rows="3" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="button" Width="76px" Text="Submit"
                                            ValidationGroup="Val1" />
                                        &nbsp;
                                        <asp:Button ID="btnBack" runat="server" CssClass="button" Width="76px" Text="Back" />
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px; width: 17px;">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
        <asp:PostBackTrigger ControlID="btnBack" />
    </Triggers>
</asp:UpdatePanel>
