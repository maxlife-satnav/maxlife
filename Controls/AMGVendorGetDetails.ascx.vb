Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGVendorGetDetails
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvDetails_AVR.PageIndex = 0
            fillgrid()
            lbtn1.Visible = False
        End If
    End Sub
    Public Sub fillgrid()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_AMG_VENDOR")
        sp.Command.AddParameter("@AVR_NAME", txtfindcode.Text, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvDetails_AVR.DataSource = ds
        gvDetails_AVR.DataBind()
        For i As Integer = 0 To gvDetails_AVR.Rows.Count - 1
            Dim lblStatus_AVR As Label = CType(gvDetails_AVR.Rows(i).FindControl("lblStatus_AVR"), Label)
            If lblStatus_AVR.Text = "0" Then
                lblStatus_AVR.Text = "Inactive"
            Else
                lblStatus_AVR.Text = "Active"
            End If
           
        Next


    End Sub
    Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click
        

        ' Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_VENDOR_RECORD")
        fillgrid()
        lbtn1.Visible = True

        
    End Sub

    Protected Sub lbtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtn1.Click
        txtfindcode.Text = ""
        fillgrid()
        lbtn1.Visible = False

    End Sub

   

    Protected Sub gvDetails_AVR_RowCommand1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDetails_AVR.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblCode_AVR As Label = DirectCast(gvDetails_AVR.Rows(rowIndex).FindControl("lblCode_AVR"), Label)
            Dim id As String = lblCode_AVR.Text
            Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMG_AVR_DEL")
            SP1.Command.AddParameter("@AVR_CODE", id, DbType.String)
            SP1.ExecuteScalar()
        End If
        fillgrid()
    End Sub

    Protected Sub gvDetails_AVR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails_AVR.PageIndexChanging
        gvDetails_AVR.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub gvDetails_AVR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetails_AVR.SelectedIndexChanged
        Dim rowIndex As Integer = gvDetails_AVR.SelectedIndex
        Dim lbl As Label = DirectCast(gvDetails_AVR.Rows(rowIndex).FindControl("lbl"), Label)
    End Sub

    Protected Sub gvDetails_AVR_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDetails_AVR.RowDeleting

    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/FAM/FAM_Webfiles/frmAMGVendorNewRecord1.aspx")
    End Sub
End Class
