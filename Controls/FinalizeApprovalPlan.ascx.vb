﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_FinalizeApprovalPlan
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            btnAdd.Visible = False
            btnModify.Visible = False

        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_REQDETAILS_SECONDLEVEL")
            gvitems1.DataSource = sp.GetDataSet()
            gvitems1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
        gvitems1.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub



    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            For i As Integer = 0 To gvItems.Rows.Count - 1

                Dim lblID1 As Label = DirectCast(gvItems.Rows(i).FindControl("lblID1"), Label)
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_UPDATE_TOTALAMT_SJP")
                sp1.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
                sp1.Command.AddParameter("@DETAILSID", Convert.ToInt32(lblID1.Text), DbType.Int32)
                sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
                sp1.ExecuteScalar()
            Next
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_SECONDLEVELAPPROVAL")
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            sp.Command.AddParameter("@aur_id", Session("UID"), DbType.String)
            sp.ExecuteScalar()

            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks2.aspx?id=63")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_SECONDLEVELREJECT")
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            sp.Command.AddParameter("@aur_id", Session("UID"), DbType.String)
            sp.ExecuteScalar()

            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=64")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvitems1.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvitems1.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id As String = lblID.Text
                txtStore.Text = id
                BindDetails(txtStore.Text)
                pnlreqdetails.Visible = True
                pnladd.Visible = False
                btnAdd.Visible = False
                btnModify.Visible = False
                pnltraveladvance.Visible = False
            ElseIf e.CommandName = "AddTravelAdv" Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvitems1.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id As String = lblID.Text
                txtStore.Text = id
                pnltraveladvance.Visible = True
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_GET_TRAVELADVAMOUNT")
                sp1.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
                Dim ds As New DataSet()
                ds = sp1.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    txttraveladvamt.Text = ds.Tables(0).Rows(0).Item("TRAVELADVANCE")
                End If

            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try


    End Sub
    Private Sub BindDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_DETAILS_BYREQ")
            sp.Command.AddParameter("@REQUESTID", id, DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            If e.CommandName = "Select" Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID1"), Label)
                Dim Detailsid As Integer = Convert.ToInt32(lblID.Text)
                lblsno.Text = lblID.Text
                pnladd.Visible = True
                btnAdd.Visible = True
                btnModify.Visible = True
                BINDEXISTEDPAYDETAILS(Detailsid)

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BINDEXISTEDPAYDETAILS(ByVal id As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_GET_PAYDETAILS_SJPBYID")
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            sp.Command.AddParameter("@DETAILSID", id, DbType.Int32)
            Dim ds As DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtstay.Text = ds.Tables(0).Rows(0).Item("STAY")
                txtfood.Text = ds.Tables(0).Rows(0).Item("FOOD")
                txtentertainment.Text = ds.Tables(0).Rows(0).Item("ENTERTAINMENT")
                txtexpensesfamily.Text = ds.Tables(0).Rows(0).Item("EXPOFFAMILY")
                txtremamount.Text = ds.Tables(0).Rows(0).Item("REMAMOUNT")
                txtConveyance.Text = ds.Tables(0).Rows(0).Item("CONVEYANCE")
                txttelephone.Text = ds.Tables(0).Rows(0).Item("TELEPHONE")
                txtlaundry.Text = ds.Tables(0).Rows(0).Item("LAUNDRYEXP")
                txtkms.Text = ds.Tables(0).Rows(0).Item("NOOFKMS")
            Else
                clear()
                lblMsg.Text = ""
            End If



        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnclose_Click(sender As Object, e As EventArgs) Handles btnclose.Click
        pnltraveladvance.Visible = False
        lblMsg.Text = ""
    End Sub

    Protected Sub btnAddtravelamt_Click(sender As Object, e As EventArgs) Handles btnAddtravelamt.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_UPDATE_TRAVELADV_SJ")
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            sp.Command.AddParameter("@MODIFIED_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@TRAVELADVANCE", Convert.ToInt32(txttraveladvamt.Text), DbType.Int32)
            sp.ExecuteScalar()
            lblMsg.Text = "Travel Amount Added Successfully"
            txttraveladvamt.Text = ""
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub clear()
        Try
            txtstay.Text = ""
            txtfood.Text = ""
            txtentertainment.Text = ""
            txtexpensesfamily.Text = ""
            txtremamount.Text = ""
            txtConveyance.Text = ""
            txttelephone.Text = ""
            txtlaundry.Text = ""
            txtkms.Text = ""


        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnAddpay_Click(sender As Object, e As EventArgs) Handles btnAddpay.Click
        '---PAY DETAILS
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_INSERT_PAYDETAILS")
        sp2.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
        sp2.Command.AddParameter("@DETAILSID", Convert.ToInt32(lblsno.Text), DbType.Int32)
        sp2.Command.AddParameter("@CREATED_BY", Session("UID"), DbType.String)
        If txtstay.Text <> "" Then
            sp2.Command.AddParameter("@STAY", Convert.ToInt32(txtstay.Text), DbType.Int32)
        Else
            sp2.Command.AddParameter("@STAY", Convert.ToInt32(0), DbType.Int32)
        End If

        If txtfood.Text <> "" Then
            sp2.Command.AddParameter("@FOOD", Convert.ToInt32(txtfood.Text), DbType.Int32)
        Else
            sp2.Command.AddParameter("@FOOD", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtentertainment.Text <> "" Then
            sp2.Command.AddParameter("@ENTERTAINMENT", Convert.ToInt32(txtentertainment.Text), DbType.Int32)
        Else
            sp2.Command.AddParameter("@ENTERTAINMENT", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtexpensesfamily.Text <> "" Then
            sp2.Command.AddParameter("@EXPOFFAMILY", Convert.ToInt32(txtexpensesfamily.Text), DbType.Int32)
        Else
            sp2.Command.AddParameter("@EXPOFFAMILY", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtremamount.Text <> "" Then
            sp2.Command.AddParameter("@REMAMOUNT", Convert.ToInt32(txtremamount.Text), DbType.Int32)
        Else
            sp2.Command.AddParameter("@REMAMOUNT", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtConveyance.Text <> "" Then
            sp2.Command.AddParameter("@CONVEYANCE", Convert.ToInt32(txtConveyance.Text), DbType.Int32)
        Else
            sp2.Command.AddParameter("@CONVEYANCE", Convert.ToInt32(0), DbType.Int32)
        End If
        If txttelephone.Text <> "" Then
            sp2.Command.AddParameter("@TELEPHONE", Convert.ToInt32(txttelephone.Text), DbType.Int32)
        Else
            sp2.Command.AddParameter("@TELEPHONE", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtlaundry.Text <> "" Then
            sp2.Command.AddParameter("@LAUNDRYEXP", Convert.ToInt32(txtlaundry.Text), DbType.Int32)
        Else
            sp2.Command.AddParameter("@LAUNDRYEXP", Convert.ToInt32(0), DbType.Int32)
        End If
        If txtkms.Text <> "" Then
            sp2.Command.AddParameter("@NOOFKMS", Convert.ToInt32(txtkms.Text), DbType.Int32)
        Else
            sp2.Command.AddParameter("@NOOFKMS", Convert.ToInt32(0), DbType.Int32)
        End If


        sp2.ExecuteScalar()
        lblMsg.Text = "Pay Details Added Successfully"
        clear()
        '---END
    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindDetails(txtStore.Text)
    End Sub
End Class
