Imports System.Data
Imports System.IO
Imports System.Data.OleDb

Partial Class Controls_AddSpace
    Inherits System.Web.UI.UserControl
    Dim lcmtemp As String
    Dim bdgtemp As String
    Dim twrtemp As String
    Dim spctemp As String
    Dim flrtemp As String
    Dim sertemp As String
    Dim sertemp1 As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack Then
            lblMsg.Text = ""
            Get_Building()
            Get_Spacetype()
            Get_Seattype()
            btnsubmit.Visible = True
            btnExport.Visible = False
        End If
    End Sub
    Private Sub Get_Spacetype()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_GET_SPACETYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlSPCType.DataSource = sp.GetDataSet()
        ddlSPCType.DataTextField = "SPACE_TYPE"
        ddlSPCType.DataValueField = "SPACE_LAYER"
        ddlSPCType.DataBind()
        ddlSPCType.Items.Insert(0, "--Select Space Type--")
    End Sub
    Private Sub Get_Seattype()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EFM_GET_SEATTYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlseattype.DataSource = sp.GetDataSet()
        ddlseattype.DataValueField = "SNO"
        ddlseattype.DataTextField = "SPACE_TYPE"
        ddlseattype.DataValueField = "STA_ID"
        ddlseattype.DataBind()
        ddlseattype.Items.Insert(0, "--Select--")
    End Sub
    Private Sub Cleardata()
        txtSpcName.Text = String.Empty
        txtArea.Text = String.Empty
        txtCost.Text = String.Empty
        txtDesc.Text = String.Empty
        txtRem.Text = String.Empty
        ddlBDG.SelectedIndex = 0
        ddlFLR.SelectedIndex = 0
        ddlWNG.SelectedIndex = 0
        ddlSPCType.SelectedIndex = 0
        ddlTWR.SelectedIndex = 0
        ddlseattype.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        'gvspace.DataSource = Nothing
        'gvspace.DataBind()

    End Sub
    Protected Sub ddlBDG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBDG.SelectedIndexChanged
        lblMsg.Text = ""
        If ddlBDG.SelectedIndex = 0 Then
            'ddlTWR.SelectedIndex = 0
            ddlFLR.SelectedIndex = 0
            ddlWNG.SelectedIndex = 0
        End If
        Get_Tower(ddlBDG.SelectedItem.Value)
    End Sub
    Private Sub Get_Tower(ByVal bdgtemp As String)
        'Retrieving Tower name,id and binding into the Tower dropdown based on selected building in building dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_TOWER")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        ddlTWR.DataSource = sp.GetDataSet()
        ddlTWR.DataTextField = "TWR_NAME"
        ddlTWR.DataValueField = "TWR_CODE"
        ddlTWR.DataBind()
        ddlTWR.Items.Insert(0, "--Select Tower--")
        'If twrtemp <> "" Then
        '    ddlTWR.Items.FindByValue(twrtemp).Selected = True
        'End If
    End Sub
    Protected Sub ddlTWR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTWR.SelectedIndexChanged
        ddlWNG.SelectedIndex = 0
        Get_Floor(ddlBDG.SelectedItem.Value, ddlTWR.SelectedItem.Value)
    End Sub
    Private Sub Get_Floor(ByVal bdgtemp As String, ByVal twrtemp As String)
        'Retrieving Floor name,id and binding into the Floor dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_FLOOR")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrtemp, DbType.String)
        ddlFLR.DataSource = sp.GetDataSet()
        ddlFLR.DataTextField = "FLR_NAME"
        ddlFLR.DataValueField = "FLR_CODE"
        ddlFLR.DataBind()
        ddlFLR.Items.Insert(0, "--Select Floor--")
        'If flrtemp <> "" Then
        '    ddlFLR.Items.FindByValue(flrtemp).Selected = True
        'End If
    End Sub
    Protected Sub ddlFLR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFLR.SelectedIndexChanged
        Get_Wing(ddlBDG.SelectedItem.Value, ddlTWR.SelectedItem.Value, ddlFLR.SelectedItem.Value)
    End Sub
    Private Sub Get_Wing(ByVal bdgtemp As String, ByVal twrtemp As String, ByVal flrtemp As String)
        'Retrieving Space name,id and binding into the Space dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_WING")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrtemp, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", flrtemp, DbType.String)
        ddlWNG.DataSource = sp.GetDataSet()
        ddlWNG.DataTextField = "WNG_NAME"
        ddlWNG.DataValueField = "WNG_CODE"
        ddlWNG.DataBind()
        ddlWNG.Items.Insert(0, "--Select Wing--")
        'If spctemp <> "" Then
        '    ddlWNG.Items.FindByValue(spctemp).Selected = True
        'End If
    End Sub
    
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim validatename As Integer
        validatename = ValidateSpaceCode()
        If validatename = "0" Then
            lblMsg.Text = "Space Name Already Exist,Please enter another space name"
        ElseIf validatename = "1" Then
            insertnewrecord()
        End If
    End Sub
    Private Function ValidateSpaceCode()
        Dim validatename As Integer
        'Dim totbkt As String
        'totbkt = ddlBDG.SelectedItem.Value & "/" & ddlTWR.SelectedItem.Value & "/" & ddlFLR.SelectedItem.Value & "/" & ddlWNG.SelectedItem.Value & "/" & txtSpcName.Text
        Dim totbkt As String
        Dim twrid As String
        Dim rmvbdg As String
        'Ex: Before Twrid is "NA/AX0025"
        twrid = ddlTWR.SelectedItem.Value
        'Removing buildingid from Tower 
        rmvbdg = twrid.Replace(ddlBDG.SelectedItem.Value, "")
        'After Twrid is NA
        totbkt = ddlBDG.SelectedItem.Value & "/" & ddlTWR.SelectedItem.Value & "/" & ddlFLR.SelectedItem.Value & "/" & ddlWNG.SelectedItem.Value & "/" & ddlSPCType.SelectedItem.Value & "-" & txtSpcName.Text



        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_ValidateSpaceName")
        'sp.Command.AddParameter("@SPC_NAME", txtSpcName.Text, DbType.String)
        'validatename = sp.ExecuteScalar()
        'Return validatename
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_ValidateSpaceName")
        sp.Command.AddParameter("@SPC_VIEW_NAME", totbkt, DbType.String)
        validatename = sp.ExecuteScalar()
        Return validatename
    End Function
    Private Sub insertnewrecord()
        
        Dim spcidtemp As String = ddlWNG.SelectedItem.Value
        
        Dim totbkt As String
        Dim twrid As String
        Dim rmvbdg As String
        Dim lyrnm As String
        lyrnm = ddlSPCType.SelectedItem.Value & "-" & txtSpcName.Text
        'Ex: Before Twrid is "NA/AX0025"
        twrid = ddlTWR.SelectedItem.Value
        'Removing buildingid from Tower 
        rmvbdg = twrid.Replace(ddlBDG.SelectedItem.Value, "")
        'After Twrid is NA
        totbkt = ddlBDG.SelectedItem.Value & "/" & ddlTWR.SelectedItem.Value & "/" & ddlFLR.SelectedItem.Value & "/" & ddlWNG.SelectedItem.Value & "/" & ddlSPCType.SelectedItem.Value & "-" & txtSpcName.Text

        Try
            'Inserting the data into SPACE Table
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_INSERT_SPACE")
            sp.Command.AddParameter("@SPC_BDG_ID", ddlBDG.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_TWR_ID", ddlTWR.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_FLR_ID", ddlFLR.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_WNG_ID", ddlWNG.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_ID", totbkt, DbType.String)
            sp.Command.AddParameter("@SPC_DESC", txtDesc.Text, DbType.String)
            sp.Command.AddParameter("@SPC_REM", txtRem.Text, DbType.String)
            'sp.Command.AddParameter("@SER_UP_BY", Session("Uid"), DbType.String)
            sp.Command.AddParameter("@SPC_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@SPC_TYPE", ddlSPCType.SelectedItem.Text, DbType.String)
            sp.Command.AddParameter("@SPC_LAYER", ddlSPCType.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_COST_SQFT", txtCost.Text, DbType.String)
            sp.Command.AddParameter("@SPC_AREA", txtArea.Text, DbType.String)
            sp.Command.AddParameter("@SPC_NAME", lyrnm, DbType.String)
            sp.Command.AddParameter("@SPC_VIEW_NAME", totbkt, DbType.String)
            sp.Command.AddParameter("@SPACE_TYPE", ddlseattype.SelectedItem.Value, DbType.String)

            sp.ExecuteScalar()
            lblMsg.Visible = True
            lblMsg.Text = "New Space added successfully"
            Cleardata()
            'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?ID=7")


        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Get_Building()
        'Retrieving Building name,id and binding into the Building dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_BUILDING")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlBDG.DataSource = sp.GetDataSet()
        ddlBDG.DataTextField = "BDG_NAME"
        ddlBDG.DataValueField = "BDG_ADM_CODE"
        ddlBDG.DataBind()
        ddlBDG.Items.Insert(0, "--Select Location--")


    End Sub
    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx")
    End Sub
    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile = True Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                'Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & "." & s(1).ToString()
                Dim filename As String = Path.GetFileNameWithoutExtension(fpBrowseDoc.FileName) & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & "." & s(1).ToString()
                'Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename
                Dim filepath As String = Replace(Server.MapPath("~\UploadFiles\"), "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                'fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                fpBrowseDoc.SaveAs(filepath)
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                'For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                'If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "Column name should be SNO"
                '    Exit Sub
                If LCase(ds.Tables(0).Columns(0).ToString) <> "location code" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be Location Code"
                    Exit Sub
                ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "tower code" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be Tower Code"
                    Exit Sub

                ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "floor code" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be Floor Code"
                    Exit Sub

                ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "wing code" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be Wing Code"
                    Exit Sub

                ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "city code" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be City Code"
                    Exit Sub

                ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "space type" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be Space Type"
                    Exit Sub
                ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "space name" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be Space Name"
                    Exit Sub
                ElseIf LCase(ds.Tables(0).Columns(7).ToString) <> "seat type" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be Space Type"
                    Exit Sub
                ElseIf LCase(ds.Tables(0).Columns(8).ToString) <> "space area" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be Space Area"
                    Exit Sub
                ElseIf LCase(ds.Tables(0).Columns(9).ToString) <> "space cost" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Column name should be Space Cost"
                    Exit Sub
                End If

                'Next
                ' Next

                Dim loc_codecnt As Integer = 1
                Dim spc_namecnt As Integer = 1
                Dim twr_codecnt As Integer = 1
                Dim spc_typecnt As Integer = 1
                Dim flr_codecnt As Integer = 1
                Dim seat_typecnt As Integer = 1
                Dim wng_codecnt As Integer = 1
                Dim spc_areacnt As Integer = 1
                Dim cty_codecnt As Integer = 1
                Dim spc_costcnt As Integer = 1

                Dim loc_remarks As String = ""
                Dim twr_remarks As String = ""
                Dim flr_remarks As String = ""
                Dim wng_remarks As String = ""
                Dim cty_remarks As String = ""
                Dim spctyp_remarks As String = ""
                Dim spcnam_remarks As String = ""
                Dim seat_remarks As String = ""
                Dim spcarea_remarks As String = ""
                Dim spcost_remarks As String = ""
                Dim remarks As String = ""

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "location code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Location code is null or empty, "
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "tower code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Tower code is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "floor code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Floor code is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "wing code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Wing code is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "city code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " City code is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "space type" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Space type is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "space name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Space name is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "seat type" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Seat type is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "space area" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Space area is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "space cost" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Space cost is null or empty, "
                            End If
                        End If


                    Next
                    'Dim space_id As String = ds.Tables(0).Rows(i).Item(0).ToString & "/NA/" & ds.Tables(0).Rows(i).Item(2).ToString & "/NA/" & ds.Tables(0).Rows(i).Item(6).ToString

                    If loc_codecnt <> 0 And twr_codecnt <> 0 And flr_codecnt <> 0 And wng_codecnt <> 0 And cty_codecnt <> 0 And spc_typecnt <> 0 And spc_namecnt <> 0 And seat_typecnt <> 0 And spc_areacnt <> 0 And spc_costcnt <> 0 Then
                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPLOAD_SPACE_DATA")
                        sp.Command.AddParameter("@LOC_CODE", ds.Tables(0).Rows(i).Item(0).ToString, DbType.String)
                        sp.Command.AddParameter("@TWR_CODE", ds.Tables(0).Rows(i).Item(1).ToString, DbType.String)
                        sp.Command.AddParameter("@FLR_CODE", ds.Tables(0).Rows(i).Item(2).ToString, DbType.String)
                        sp.Command.AddParameter("@WNG_CODE", ds.Tables(0).Rows(i).Item(3).ToString, DbType.String)
                        sp.Command.AddParameter("@CTY_CODE", ds.Tables(0).Rows(i).Item(4).ToString, DbType.String)
                        sp.Command.AddParameter("@SPC_TYPE", ds.Tables(0).Rows(i).Item(5).ToString, DbType.String)
                        sp.Command.AddParameter("@SPC_NAME", ds.Tables(0).Rows(i).Item(6).ToString, DbType.String)
                        sp.Command.AddParameter("@SEAT_TYPE", ds.Tables(0).Rows(i).Item(7).ToString, DbType.String)
                        sp.Command.AddParameter("@SPC_AREA", ds.Tables(0).Rows(i).Item(8).ToString, DbType.String)
                        sp.Command.AddParameter("@SPC_COST", ds.Tables(0).Rows(i).Item(9).ToString, DbType.Decimal)
                        sp.Command.AddParameter("@UID", Session("Uid"), DbType.String)
                        sp.Command.AddParameter("@REMARKS", remarks, DbType.String)
                        sp.ExecuteScalar()

                    End If
                    remarks = String.Empty
                Next
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_SPACE_DATA")
                sp1.Command.AddParameter("@dummy", 1, DbType.Int32)

                Dim ds1 As New DataSet
                ds1 = sp1.GetDataSet()
                If ds1.Tables(0).Rows.Count > 0 Then
                    gvspace.DataSource = ds1.Tables(0)
                    gvspace.DataBind()
                    gvspace.Visible = True
                    btnExport.Visible = True
                End If

                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
            Else
                lblMsg.Text = ""

                'lblMsg.Visible = True
                'lblMsg.Text = "Please select file..."
            End If
        Catch ex As Exception
            CommonModules.PopUpMessage(ex.Message, Page)
        End Try
    End Sub
    Private Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click

        Try

            ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel

            'If ddlTower.SelectedValue = "0" Then
            ExportPanel1.FileName = "SpaceData" & "_" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls"

            'Else
            'lblheading.Text = ddlverticals.SelectedValue & "_" & ddlTower.SelectedItem.Text & "_Report"
            'End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Projectwise Allocation", "Load", ex)
        End Try

        'panel1.Visible = True
        'gvspace.Visible = False
        ''panel2.Visible = False

        'Export("Space_Data_Master.xls", gvspace, panel1)


    End Sub
    Public Sub Export(ByVal fileName As String, ByVal gv As GridView, pn As Panel)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        For Each row As GridViewRow In gv.Rows
            row.Cells(0).Style.Add("mso-number-format", "\@")
            row.Cells(5).Style.Add("mso-number-format", "\@")
            PrepareControlForExport(row)
            table.Rows.Add(row)

        Next

        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        table.RenderControl(htw)
        Dim style As String = "<style>.textmode{mso-number-format:\@;}</style>"
        'clear(gv)
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()

    End Sub
End Class

