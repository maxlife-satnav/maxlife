Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_frmModifyPropertyDetails
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim PN_LOCID As String = Request.QueryString("id")
        If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
            lblMsg.Text = "Carpet Area cannot be more than builtup area"
            lblMsg.Visible = True
        ElseIf CInt(txtCommonArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtCommonArea.Text) > CInt(txtBuiltupArea.Text) Then
            lblMsg.Text = "Common Area cannot be more than builtup area and Carpet"
            lblMsg.Visible = True

        ElseIf CInt(txtRentableArea.Text) > CInt(txtBuiltupArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtUsableArea.Text) Then
            lblMsg.Text = "Rentable area cannot be more than built up, carpet area and usable area"
            lblMsg.Visible = True
        Else
            Dim validatecode As Integer
            validatecode = validatepncode()
            If validatecode = 0 Then
                lblMsg.Text = "Code already exist please enter another code"
                lblMsg.Visible = True
            Else
                UpdatePropertydetails()
                UpdateRecord()
            End If

        End If

    End Sub

    Private Sub UpdatePropertydetails()

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_PROPERTY_DETAILS_LEASE")
        If txtpdate.Text = "" Then
            sp1.Command.AddParameter("@PURCHASE_DATE", DBNull.Value, DbType.DateTime)
        Else
            sp1.Command.AddParameter("@PURCHASE_DATE", txtpdate.Text, DbType.DateTime)
        End If


        sp1.Command.AddParameter("@PURCHASE_PRICE", txtpprice.Text, DbType.Decimal)
        sp1.Command.AddParameter("@BASIC_PURCHASE_PRICE", txtbprice.Text, DbType.Decimal)
        sp1.Command.AddParameter("STAMP_DUTY", txtsduty.Text, DbType.Decimal)
        sp1.Command.AddParameter("@REG_CHARGES", txtregcharges.Text, DbType.Decimal)
        sp1.Command.AddParameter("@VAT_DEPOSIT", txtvat.Text, DbType.Decimal)
        sp1.Command.AddParameter("@SERVICE_TAX", txtstax.Text, DbType.Decimal)
        sp1.Command.AddParameter("@PROFESSIONAL_FEES", txtpfees.Text, DbType.Decimal)
        sp1.Command.AddParameter("@BROKERAGE_AMOUNT", txtbrokerage.Text, DbType.Decimal)
        sp1.Command.AddParameter("@LEASEHOLD_IMPROVEMENTS", txtimp.Text, DbType.String)
        sp1.Command.AddParameter("@FURNITURE", txtfurniture.Text, DbType.String)
        sp1.Command.AddParameter("@OFFICE", txtofcequip.Text, DbType.String)
        sp1.Command.AddParameter("@LATITUDE", txtlat.Text, DbType.String)
        sp1.Command.AddParameter("@LONGITUDE", txtlong.Text, DbType.String)
        sp1.Command.AddParameter("@property", txtpropcode.Text, DbType.String)
        sp1.Command.AddParameter("@MODE", "MODIFY", DbType.String)
        sp1.ExecuteScalar()
    End Sub

    Private Function validatepncode()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "validatepncode")
        sp.Command.AddParameter("@code", txtpropcode.Text, DbType.String)
        sp.Command.AddParameter("@sno", Request.QueryString("id"), DbType.Int32)
        Dim validatecode As Integer = sp.ExecuteScalar()
        Return validatecode
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtpdate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then
            BindPropType()
            BindCity()
            BindDetails()
            'BindInsuranceType()

            If ddlPropertyType.SelectedValue = 1 Then
                panelvisible(True)
            ElseIf ddlPropertyType.SelectedValue = 2 Then
                panelvisible(True)
            Else
                Clearpaneldata()
                panelvisible(False)
            End If

        End If
    End Sub
    Private Sub BindInsuranceType()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_INSURANCE_TYPE")
        ddlInsuranceType.DataSource = sp.GetDataSet()
        ddlInsuranceType.DataTextField = "INSURANCE_TYPE"
        ddlInsuranceType.DataValueField = "ID"
        ddlInsuranceType.DataBind()
        ddlInsuranceType.Items.Insert(0, New ListItem("--Select Insurance Type--", "--Select Insurance Type--"))

    End Sub


    Private Sub BindDetails()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PRPTY_DETAILS")
        sp3.Command.AddParameter("@sno", Request.QueryString("id"), DbType.Int32)
        Dim ds3 As New DataSet
        ds3 = sp3.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            Dim li As ListItem = Nothing
            li = ddlPropertyType.Items.FindByValue(ds3.Tables(0).Rows(0).Item("PROPERTY_TYPE"))
            If Not li Is Nothing Then
                li.Selected = True
            End If
            Dim li1 As ListItem = Nothing
            li1 = ddlCity.Items.FindByValue(ds3.Tables(0).Rows(0).Item("BDG_ADM_CODE"))
            If Not li1 Is Nothing Then
                li1.Selected = True
                'ddlCity.Enabled = False
            End If
            BindLocaiton()
            ddlLocation.Items.FindByValue(ds3.Tables(0).Rows(0).Item("LOC_ID")).Selected = True
            'ddlLocation.Enabled = False
            Dim li2 As Integer = ds3.Tables(0).Rows(0).Item("STATUS")
            If li2 = 0 Then
                ddlStatus.SelectedValue = 0
            Else
                ddlStatus.SelectedValue = 1
            End If
            BindInsuranceType()
            If ds3.Tables(0).Rows(0).Item("IN_TYPE") <> "" Then

                ddlInsuranceType.Items.FindByValue(ds3.Tables(0).Rows(0).Item("IN_TYPE")).Selected = True
            End If
            'ddlInsuranceType.Enabled = False

            txtpropcode.Text = ds3.Tables(0).Rows(0).Item("BDG_ID")
            txtPropIDName.Text = ds3.Tables(0).Rows(0).Item("PN_NAME")
            txtMaxCapacity.Text = ds3.Tables(0).Rows(0).Item("MAX_CAPACITY")
            txtOptCapacity.Text = ds3.Tables(0).Rows(0).Item("OPTIMUM_CAPACITY")
            txtCarpetArea.Text = ds3.Tables(0).Rows(0).Item("CARPET_AREA")
            txtBuiltupArea.Text = ds3.Tables(0).Rows(0).Item("BUILTUP_AREA")
            txtCommonArea.Text = ds3.Tables(0).Rows(0).Item("COMMON_AREA")
            txtRentableArea.Text = ds3.Tables(0).Rows(0).Item("RENTABLE_AREA")
            txtUsableArea.Text = ds3.Tables(0).Rows(0).Item("USABLE_AREA")
            txtUOM_CODE.Text = ds3.Tables(0).Rows(0).Item("UOM_CODE")
            txtGovtPropCode.Text = ds3.Tables(0).Rows(0).Item("GOVT_PROPERTY_CODE")
            txtownrname.Text = ds3.Tables(0).Rows(0).Item("OWNERNAME")
            txtphno.Text = ds3.Tables(0).Rows(0).Item("PHNO")
            txtemail.Text = ds3.Tables(0).Rows(0).Item("EMAIL")
            txtPropDesc.Text = ds3.Tables(0).Rows(0).Item("PROPERTY_DESCRIPTION")
            txtlat.Text = ds3.Tables(1).Rows(0).Item("LATITUDE")
            txtlong.Text = ds3.Tables(1).Rows(0).Item("LONGITUDE")
            txtInsuranceVendor.Text = ds3.Tables(0).Rows(0).Item("IN_VENDOR")
            txtInsuranceAmt.Text = ds3.Tables(0).Rows(0).Item("IN_AMOUNT")
            txtInsurancePolNum.Text = ds3.Tables(0).Rows(0).Item("IN_POLICY_NUMBER")
            '01/01/1900
            If ds3.Tables(0).Rows(0).Item("IN_START_DATE") <> "01/01/1900" Then
                txtInsuranceStartdate.Text = ds3.Tables(0).Rows(0).Item("IN_START_DATE")
            End If
            If ds3.Tables(0).Rows(0).Item("IN_END_DATE") <> "01/01/1900" Then
                txtInsuranceEnddate.Text = ds3.Tables(0).Rows(0).Item("IN_END_DATE")
            End If


        End If
            If ds3.Tables(1).Rows.Count > 0 Then
                txtpdate.Text = ds3.Tables(1).Rows(0).Item("PURCHASE_DATE")
                txtpprice.Text = ds3.Tables(1).Rows(0).Item("PURCHASE_PRICE")
                txtbprice.Text = ds3.Tables(1).Rows(0).Item("BASIC_PURCHASE_PRICE")
                txtsduty.Text = ds3.Tables(1).Rows(0).Item("STAMP_DUTY")
                txtregcharges.Text = ds3.Tables(1).Rows(0).Item("REG_CHARGES")
                txtvat.Text = ds3.Tables(1).Rows(0).Item("VAT_DEPOSIT")
                txtstax.Text = ds3.Tables(1).Rows(0).Item("SERVICE_TAX")
                txtpfees.Text = ds3.Tables(1).Rows(0).Item("PROFESSIONAL_FEES")
                txtbrokerage.Text = ds3.Tables(1).Rows(0).Item("BROKERAGE_AMOUNT")
                txtimp.Text = ds3.Tables(1).Rows(0).Item("LEASEHOLD_IMPROVEMENTS")
                txtfurniture.Text = ds3.Tables(1).Rows(0).Item("FURNITURE")
                txtofcequip.Text = ds3.Tables(1).Rows(0).Item("OFFICE")
                'txtpropcode.Text = ds3.Tables(1).Rows(0).Item("PROPERTY_ID")
            End If
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select City--", "--Select City--"))

    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTYTYPE")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", "--Select Property Type--"))
    End Sub

    Public Sub UpdateRecord()

        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_PROPERTY_DETAILS")
            sp1.Command.AddParameter("@sno", Request.QueryString("id"), DbType.Int32)
            sp1.Command.AddParameter("@PROPERTY_TYPE", ddlPropertyType.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@BDG_ID", txtpropcode.Text, DbType.String)
            sp1.Command.AddParameter("@PN_NAME", txtPropIDName.Text, DbType.String)
            sp1.Command.AddParameter("@MAX_CAPACITY", txtMaxCapacity.Text, DbType.Int32)
            sp1.Command.AddParameter("@OPTIMUM_CAPACITY", txtOptCapacity.Text, DbType.Int32)
            sp1.Command.AddParameter("@CARPET_AREA", txtCarpetArea.Text, DbType.Decimal)
            sp1.Command.AddParameter("@BUILTUP_AREA", txtBuiltupArea.Text, DbType.Decimal)
            sp1.Command.AddParameter("@COMMON_AREA", txtCommonArea.Text, DbType.Decimal)
            sp1.Command.AddParameter("@RENTABLE_AREA", txtRentableArea.Text, DbType.Int32)
            sp1.Command.AddParameter("@USABLE_AREA", txtUsableArea.Text, DbType.Int32)
            sp1.Command.AddParameter("@UOM_CODE", txtUOM_CODE.Text, DbType.String)
            sp1.Command.AddParameter("@STATUS", ddlStatus.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@LAST_UPDATED_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@GOVT_PROPERTY_CODE", txtGovtPropCode.Text, DbType.String)
            sp1.Command.AddParameter("@OWNER", txtownrname.Text, DbType.String)
            sp1.Command.AddParameter("@PHNO", txtphno.Text, DbType.String)
            sp1.Command.AddParameter("@EMAIL", txtemail.Text, DbType.String)
            sp1.Command.AddParameter("@PROPERTY_DESCRIPTION", txtPropDesc.Text, DbType.String)
            sp1.Command.AddParameter("@CREATED_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@LOCATION", ddlLocation.SelectedItem.Value, DbType.String)
            If ddlInsuranceType.SelectedItem.Text <> "--Select Insurance Type--" Then
                sp1.Command.AddParameter("@IN_TYPE", ddlInsuranceType.SelectedItem.Value, DbType.String)
            Else
                sp1.Command.AddParameter("@IN_TYPE", "", DbType.String)
            End If
            sp1.Command.AddParameter("@IN_VENDOR", txtInsuranceVendor.Text, DbType.String)
            sp1.Command.AddParameter("@IN_AMOUNT", txtInsuranceAmt.Text, DbType.Decimal)
            sp1.Command.AddParameter("@IN_POLICY_NUMBER", txtInsurancePolNum.Text, DbType.String)
            sp1.Command.AddParameter("@IN_SDATE", txtInsuranceStartdate.Text, DbType.String)
            sp1.Command.AddParameter("@IN_EDATE", txtInsuranceEnddate.Text, DbType.String)
            sp1.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=5")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmEditPropertyDetails.aspx")
    End Sub

    Protected Sub ddlPropertyType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPropertyType.SelectedIndexChanged

        'If ddlPropertyType.SelectedItem.Value = 1 Then
        '    panelvisible(True)
        'ElseIf ddlPropertyType.SelectedItem.Value = 2 Then
        '    panelvisible(True)
        'Else
        '    Clearpaneldata()
        '    panelvisible(False)
        'End If

        If ddlPropertyType.SelectedItem.Value <> "--Select Property Type--" Then
            If ddlPropertyType.SelectedItem.Value = 1 Then
                panelvisible(True)
            ElseIf ddlPropertyType.SelectedItem.Value = 2 Then
                panelvisible(True)
            End If
        Else
            Clearpaneldata()
            panelvisible(False)
        End If
    End Sub

    Private Sub Clearpaneldata()
        txtpdate.Text = ""
        txtpprice.Text = 0
        txtbprice.Text = 0
        txtsduty.Text = 0
        txtregcharges.Text = 0
        txtvat.Text = 0
        txtstax.Text = 0
        txtpfees.Text = 0
        txtbrokerage.Text = 0
        txtimp.Text = ""
        txtfurniture.Text = ""
        txtofcequip.Text = ""
        'txtlat.Text = ""
        ' txtlong.Text = ""
        'txtpropcode.Text = ""
    End Sub

    Private Sub panelvisible(ByVal stat As Boolean)
        t1.Visible = stat
        t2.Visible = stat
        t3.Visible = stat
        t4.Visible = stat
        t5.Visible = stat
        t6.Visible = stat
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        BindLocaiton()
    End Sub

    Protected Sub BindLocaiton()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value)
        sp.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub
End Class