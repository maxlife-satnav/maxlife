Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_ITApproval
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim UID As String = ""
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                UID = Session("uid")
            End If
            'Dim chkIT_GLOBALuser As Boolean = False

            'Dim DR As SqlDataReader
            'DR = ObjSubsonic.GetSubSonicDataReader("CHK_USERROLE")
            'While DR.Read
            '    If UID = DR.Item("URL_USR_ID") Then
            '        chkIT_GLOBALuser = True
            '    End If
            'End While
            'If DR.IsClosed = False Then
            '    DR.Close()
            'End If

            'If chkIT_GLOBALuser = True Then
            FillReqIds()
            FillInterMVMTRequests()
            'Else
            '    PopUpMessageNormal("Authorised for IT Administrator & Global Administrator roles.", Me.Page)
            'End If



        End If
    End Sub

    Public Sub FillReqIds()
        'ObjSubsonic.BindGridView(gvReqIds, "GET_IT_MOVEMENTS")
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
        param(0).Value = Session("UID")
        ObjSubsonic.BindGridView(gvReqIds, "GET_IT_INTRA_MOVEMENTS_NP", param)

    End Sub

    Public Sub FillInterMVMTRequests()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
        param(0).Value = Session("UID")
        ObjSubsonic.BindGridView(gvInterMVMT, "GET_REQID_InterIT_MOVEMENTS_NP", param)
    End Sub

    Protected Sub gvReqIds_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReqIds.PageIndexChanging
        gvReqIds.PageIndex = e.NewPageIndex
        FillReqIds()
    End Sub

    Protected Sub gvReqIds_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqIds.RowCommand
        If e.CommandName = "Details" Then
            Response.Redirect("ITApprovalDtls.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub

    Protected Sub gvInterMVMT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvInterMVMT.PageIndexChanging
        gvInterMVMT.PageIndex = e.NewPageIndex
        FillInterMVMTRequests()
    End Sub

    Protected Sub gvInterMVMT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvInterMVMT.RowCommand
        If e.CommandName = "Details" Then
            Response.Redirect("InterMVMT_ITApprovalDtls.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub
End Class
