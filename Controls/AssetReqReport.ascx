<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetReqReport.ascx.vb"
    Inherits="Controls_AssetReqReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div id="panel1" runat="server">
            <%-- <asp:GridView ID="gvitems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                AllowPaging="True" PageSize="5" EmptyDataText="No Records Found" Width="100%">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" CssClass="lblID" Text='<%#Eval("staid")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Assets">
                        <ItemTemplate>
                            <asp:Label ID="lblreq" runat="server" CssClass="lblreq" Text='<%#Eval("Assets")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkstatus" runat="Server" Text='<%#Bind("Stat")%>' CommandArgument='<%#Bind("Stat")%>'
                                CommandName="Status"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>--%>

            <div class="row">
                <div class="col-md-12">
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                </div>
            </div>
        </div>
        <%--  <div id="panel2" runat="server">
            <asp:GridView ID="gvitems1" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                AllowPaging="True" PageSize="5" EmptyDataText="No Records Found" Width="100%">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField HeaderText="RequisitionID">
                        <ItemTemplate>
                            <a href="#" onclick="showPopWin('frmAssetReqReportDtls.aspx?REQID=<%# Eval("AIR_REQ_ID") %>',550,480,null)">
                                <asp:Label ID="lbntreqid" runat="server" Text='<%#Bind("AIR_REQ_ID")%>'></asp:Label>
                            </a>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblstat" runat="server" CssClass="bodytext" Text='<%#Eval("sta")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
           <div class="row table table table-condensed table-responsive">
                <div class="form-group">
                    <div class="col-md-12">
                        <rsweb:ReportViewer ID="ReportViewer2" runat="server" Width="100%"></rsweb:ReportViewer>
                    </div>
                </div>
            </div>
        </div>--%>
    </div>
</div>

