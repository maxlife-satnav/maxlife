Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_EditPropertyDocuments
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvEditDoc.PageIndex = 0
            Try
                Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITYNAME")
                sp3.Command.AddParameter("@DUMMY", "", DbType.String)
                ddlCity.DataSource = sp3.GetDataSet()
                ddlCity.DataTextField = "CTY_NAME"
                ddlCity.DataValueField = "CTY_CODE"
                ddlCity.DataBind()
                ddlCity.Items.Insert(0, New ListItem("---Select City---", "0"))
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
        'fillgrid()
        gvEditDoc.Visible = True
    End Sub


    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PRPIDNAME")
            sp4.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            ddlPropIDName.DataSource = sp4.GetDataSet()
            ddlPropIDName.DataTextField = "PN_NAME"
            ddlPropIDName.DataValueField = "BDG_ID"
            ddlPropIDName.DataBind()
            ddlPropIDName.Items.Insert(0, New ListItem("--Select Property--", "0"))
        Else
            ddlPropIDName.Items.Clear()
            ddlPropIDName.Items.Insert(0, New ListItem("--Select Property--", "0"))
            ddlPropIDName.SelectedIndex = 0
            gvEditDoc.Visible = False
        End If
    End Sub
    Public Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PRTY_DOCSGRID")
        sp.Command.AddParameter("@RPT_CITY", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@RPT_FOR", ddlPropIDName.SelectedItem.Value, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvEditDoc.DataSource = ds
        gvEditDoc.DataBind()
        For i As Integer = 0 To gvEditDoc.Rows.Count - 1
            Dim hypbtnDocs As HyperLink = CType(gvEditDoc.Rows(i).FindControl("hypbtnDocs"), HyperLink)
            Dim lblViewFilename As Label = CType(gvEditDoc.Rows(i).FindControl("lblViewFilename"), Label)
            Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & lblViewFilename.Text
            hypbtnDocs.NavigateUrl = filePath
        Next
    End Sub

    Protected Sub gvEditDoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEditDoc.PageIndexChanging
        gvEditDoc.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub gvEditDoc_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvEditDoc.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            For i As Integer = 0 To gvEditDoc.Rows.Count - 1
                Dim lblID As Label = DirectCast(gvEditDoc.Rows(rowIndex).FindControl("lblID"), Label)
                Dim lblUploadedFilename As Label = DirectCast(gvEditDoc.Rows(rowIndex).FindControl("lblUploadedFilename"), Label)
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & lblUploadedFilename.Text

                If System.IO.File.Exists(filePath) = True Then
                    System.IO.File.Delete(filePath)
                End If

                Dim id As String = Integer.Parse(lblID.Text)
                Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EDP_DOCS_DEL")
                SP1.Command.AddParameter("@RPT_ID", id, DbType.Int32)
                SP1.ExecuteScalar()
            Next
        End If
        fillgrid()
        gvEditDoc.Visible = True
    End Sub


    Protected Sub gvEditDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvEditDoc.SelectedIndexChanged

    End Sub

    Protected Sub gvEditDoc_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles gvEditDoc.SelectedIndexChanging

    End Sub

    Protected Sub gvEditDoc_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEditDoc.RowDeleting
        
    End Sub

    Protected Sub gvEditDoc_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles gvEditDoc.RowDeleted
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlCity.SelectedIndex > 0 And ddlPropIDName.SelectedIndex > 0 Then
            fillgrid()
            gvEditDoc.Visible = True
        Else
            gvEditDoc.Visible = False
        End If
    End Sub
End Class
