Imports System.Data
Partial Class Controls_ModifySpace
    Inherits System.Web.UI.UserControl
    Dim lcmtemp As String
    Dim bdgtemp As String
    Dim twrtemp As String
    Dim spctemp As String
    Dim flrtemp As String
    Dim sertemp As String
    Dim sertemp1 As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Get_Building()
            Get_Spacetype()
            Dim id As String = Request.QueryString("id")
            lblTemp.Text = id
            dispdata()
        End If
    End Sub
    Private Sub Get_Spacetype()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_GET_SPACETYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlSPCType.DataSource = sp.GetDataSet()
        ddlSPCType.DataTextField = "SPACE_TYPE"
        ddlSPCType.DataValueField = "SPACE_LAYER"
        ddlSPCType.DataBind()
        ddlSPCType.Items.Insert(0, "--Select Space Type--")
    End Sub
    Protected Sub ddlBDG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBDG.SelectedIndexChanged
        If ddlBDG.SelectedIndex = 0 Then
            'ddlTWR.SelectedIndex = 0
            ddlFLR.SelectedIndex = 0
            ddlWNG.SelectedIndex = 0
        End If
        Get_Tower(ddlBDG.SelectedItem.Value)
    End Sub
    Private Sub Get_Tower(ByVal bdgtemp As String)
        'Retrieving Tower name,id and binding into the Tower dropdown based on selected building in building dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_TOWER")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        ddlTWR.DataSource = sp.GetDataSet()
        ddlTWR.DataTextField = "TWR_NAME"
        ddlTWR.DataValueField = "TWR_CODE"
        ddlTWR.DataBind()
        ddlTWR.Items.Insert(0, "--Select Tower--")
        If twrtemp <> "" Then
            ddlTWR.Items.FindByValue(twrtemp).Selected = True
        End If
    End Sub
    Protected Sub ddlTWR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTWR.SelectedIndexChanged
        Get_Floor(ddlBDG.SelectedItem.Value, ddlTWR.SelectedItem.Value)
    End Sub
    Private Sub Get_Floor(ByVal bdgtemp As String, ByVal twrtemp As String)
        'Retrieving Floor name,id and binding into the Floor dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_FLOOR")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrtemp, DbType.String)
        ddlFLR.DataSource = sp.GetDataSet()
        ddlFLR.DataTextField = "FLR_NAME"
        ddlFLR.DataValueField = "FLR_CODE"
        ddlFLR.DataBind()
        ddlFLR.Items.Insert(0, "--Select Floor--")
        If flrtemp <> "" Then
            ddlFLR.Items.FindByValue(flrtemp).Selected = True
        End If
    End Sub
    Protected Sub ddlFLR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFLR.SelectedIndexChanged
        Get_Wing(ddlBDG.SelectedItem.Value, ddlTWR.SelectedItem.Value, ddlFLR.SelectedItem.Value)
    End Sub
    Private Sub Get_Wing(ByVal bdgtemp As String, ByVal twrtemp As String, ByVal flrtemp As String)
        'Retrieving Space name,id and binding into the Space dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_WING")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrtemp, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", flrtemp, DbType.String)
        ddlWNG.DataSource = sp.GetDataSet()
        ddlWNG.DataTextField = "WNG_NAME"
        ddlWNG.DataValueField = "WNG_CODE"
        ddlWNG.DataBind()
        ddlWNG.Items.Insert(0, "--Select Wing--")
        If spctemp <> "" Then
            ddlWNG.Items.FindByValue(spctemp).Selected = True
        End If
    End Sub
    Private Sub Get_Building()
        'Retrieving Building name,id and binding into the Building dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_BUILDING")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlBDG.DataSource = sp.GetDataSet()
        ddlBDG.DataTextField = "BDG_NAME"
        ddlBDG.DataValueField = "BDG_ADM_CODE"
        ddlBDG.DataBind()
        ddlBDG.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub
    Private Sub dispdata()
        'Retrieving data from SRQ_EMP_REQ for editing
        Get_Building()
        Get_Spacetype()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SPACE_EDIT")
        sp.Command.AddParameter("@id", lblTemp.Text, DbType.String)
        Dim ds3 As New DataSet
        ds3 = sp.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            ddlBDG.ClearSelection()
            ddlBDG.Items.FindByValue(ds3.Tables(0).Rows(0).Item("LCM_CODE")).Selected = True
            ddlSPCType.ClearSelection()
            ddlSPCType.Items.FindByValue(ds3.Tables(0).Rows(0).Item("SPC_LAYER")).Selected = True
            ddlStatus.ClearSelection()
            ddlStatus.Items.FindByValue(ds3.Tables(0).Rows(0).Item("SPC_STA_ID")).Selected = True
            bdgtemp = ds3.Tables(0).Rows(0).Item("SPC_BDG_ID")
            twrtemp = ds3.Tables(0).Rows(0).Item("SPC_TWR_ID")
            flrtemp = ds3.Tables(0).Rows(0).Item("SPC_FLR_ID")
            spctemp = ds3.Tables(0).Rows(0).Item("SPC_WNG_ID")
            txtCost.Text = ds3.Tables(0).Rows(0).Item("SPC_COST_SQFT")
            txtArea.Text = ds3.Tables(0).Rows(0).Item("SPC_AREA")
            txtDesc.Text = ds3.Tables(0).Rows(0).Item("SPC_DESC")
            txtRem.Text = ds3.Tables(0).Rows(0).Item("SPC_REM")
            txtSpcName.Text = ds3.Tables(0).Rows(0).Item("SPC_NAME")
        End If
        Get_Tower(bdgtemp)
        Get_Floor(bdgtemp, twrtemp)
        Get_Wing(bdgtemp, twrtemp, flrtemp)
    End Sub
    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        Dim spcidtemp As String = ddlWNG.SelectedItem.Value
        'Dim totbkt As String
        'totbkt = ddlBDG.SelectedItem.Value & "/" & ddlTWR.SelectedItem.Value & "/" & ddlFLR.SelectedItem.Value & "/" & ddlWNG.SelectedItem.Value & "/" & txtSpcName.Text

        Dim totbkt As String
        Dim twrid As String
        Dim rmvbdg As String
        'Ex: Before Twrid is "NA/AX0025"
        twrid = ddlTWR.SelectedItem.Value
        'Removing buildingid from Tower 
        rmvbdg = twrid.Replace(ddlBDG.SelectedItem.Value, "")
        'After Twrid is NA
        'totbkt = ddlBDG.SelectedItem.Value & "/" & rmvbdg & "/" & ddlFLR.SelectedItem.Value & "/" & ddlWNG.SelectedItem.Value & "/" & txtSpcName.Text


        totbkt = ddlBDG.SelectedItem.Value & "/" & ddlFLR.SelectedItem.Value & "/" & txtSpcName.Text
        Try
            btnModify.Visible = True
            Dim validatecode1 As Integer
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_UPDATE_SPACE")
            sp.Command.AddParameter("@SPC_ID", lblTemp.Text, DbType.String)
            sp.Command.AddParameter("@SPC_BDG_ID", ddlBDG.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_TWR_ID", ddlTWR.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_FLR_ID", ddlFLR.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_WNG_ID", ddlWNG.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_DESC", txtDesc.Text, DbType.String)
            sp.Command.AddParameter("@SPC_REM", txtRem.Text, DbType.String)
            sp.Command.AddParameter("@SPC_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@SPC_TYPE", ddlSPCType.SelectedItem.Text, DbType.String)
            sp.Command.AddParameter("@SPC_LAYER", ddlSPCType.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SPC_COST_SQFT", txtCost.Text, DbType.String)
            sp.Command.AddParameter("@SPC_AREA", txtArea.Text, DbType.String)
            sp.Command.AddParameter("@SPC_NAME", txtSpcName.Text, DbType.String)
            sp.Command.AddParameter("@SPC_VIEW_NAME", lblTemp.Text, DbType.String)
            validatecode1 = sp.ExecuteScalar()
            If validatecode1 = "0" Then
                'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=8")
                'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmEditSpace.aspx")
                lblMsg.Visible = True
                lblMsg.Text = "Space Modified successfully"
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Space Name already exist, Please enter another space name"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/Masters/Mas_webfiles/frmmasmasters.aspx")
    End Sub
End Class

