Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_AdminAstDisposableDTLS
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim Req_id As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Req_id = Request.QueryString("Req_id")
            GetDetailsByRequistion(Req_id)
        End If
    End Sub

    Private Sub GetDetailsByRequistion(ByVal Req_id As String)
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Req_id
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ADMINASTDISPOSE_DETAILS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblAstName.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
            lblModelName.Text = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
            lblLocation.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
            lblAstSurDt.Text = ds.Tables(0).Rows(0).Item("DREQ_REQUESITION_DT")
            lblSurReq_id.Text = Req_id
        
            lblAstSalvage.Text = FormatNumber(ds.Tables(0).Rows(0).Item("AST_SALAVAGE_VALUE"), 2)
        End If
    End Sub

    Protected Sub btnApprov_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprov.Click

        Dim REQ = Request.QueryString("REQ_ID")
        ' ------- Asset Dispose -----------------
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = REQ
        param(1) = New SqlParameter("@DREQ_ADMIN_BY", SqlDbType.NVarChar, 200)
        param(1).Value = Session("UID")
        param(2) = New SqlParameter("@DREQ_ADMIN_REMARKS", SqlDbType.NVarChar, 2000)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
        param(3).Value = 1041
        
        ObjSubsonic.GetSubSonicDataSet("UPDATEDISPOSABLE_REQUISTION_byAdmin", param)
        send_mail(REQ)
        'Response.Redirect("frmDisposableAssetsRequisition.aspx")
        Response.Redirect("frmAssetThanks.aspx?RID=outwardapp&MReqId=" + Request.QueryString("REQ_ID"))

    End Sub
    Public Sub send_mail(ByVal reqid As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_DISPOSE_REQUISITION_APPROVAL")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Execute()
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmDisposableAssetsRequisition.aspx")
    End Sub

End Class

