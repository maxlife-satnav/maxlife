Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_SurrenderLease
    Inherits System.Web.UI.UserControl
    Shared refund As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindGrid()
            If gvItems.Rows.Count > 0 Then
                lblMessage.Visible = True
            Else
                lblMessage.Visible = False
            End If
            GetDeposit()
            pnl1.Visible = False
            txtsurrender.Text = Date.Today.ToString("MM/dd/yyyy")
        End If
        txtsurrender.Attributes.Add("readonly", "readonly")
        txtrefund.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub GetDeposit()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "LEASE_DEPOSIT_NOTIFICATION")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            'lblMessage.Text = "Pending Security Deposit Amount to refund :" & ds.Tables(0).Rows(0).Item("PENDING_SECURITY_DEPOSIT_AMOUNT") & "  Rs. for lease " & ds.Tables(0).Rows(0).Item("LEASE_NAME") & " "
            refund = CInt(ds.Tables(0).Rows(0).Item("PENDING_SECURITY_DEPOSIT_AMOUNT"))
        End If
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_LEASE_DESK_SURRENDER_LEASE1")
        sp.Command.AddParameter("@EMPID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "Surrender" Then

            Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
            Dim lblLseName As Label = DirectCast(gvRow.FindControl("lblLseName"), Label)
            txtstore.Text = lblLseName.Text

            pnl1.Visible = True

        Else
            pnl1.Visible = False
        End If
    End Sub

    Private Sub Update_Lease_Surrender_Status()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AXIS_PN_LEASE_SURENDER_STATUS_MODIFY")
            sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
            sp.Command.AddParameter("@SURRENDERED_DATE", txtsurrender.Text, DbType.Date)
            sp.Command.AddParameter("@REMARKS", txtremarks.Text, DbType.String)
            sp.Command.AddParameter("@PAID_AMOUNT", txtrefundamount.Text, DbType.Decimal)
            sp.Command.AddParameter("@PAID_DATE", txtrefund.Text, DbType.Date)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        'If CDate(txtrefund.Text) > CDate(txtsurrender.Text) Then

        '    lblMsg.Text = "Refund Date Should be less than or equal to Surrender Date"
        '    Exit Sub

        'Else
        If CDate(txtsurrender.Text) < getoffsetdatetime(DateTime.Now) Or CDate(txtrefund.Text) < getoffsetdatetime(DateTime.Now) Then
            lblMsg.Text = "Past date Not allowed"
            Exit Sub
        End If

        'If (refund < CInt(txtrefundamount.Text)) Then
        'lblMsg.Text = "the refund amount should be less or equal to" + refund.ToString()
        'Exit Sub
        If (Convert.ToDecimal(txtrefundamount.Text) = 0) Then
            'lblMsg.Text = "the refund amount should be less or equal to" + refund.ToString() + " and greater than 0"
            lblMsg.Text = "the refund amount should be greater than 0"
            Exit Sub
        Else
            Update_Lease_Surrender_Status()
        End If
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=47")
    End Sub

    Private Function ValidateSurrenderDate()
        Dim validdate As Integer
        Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "LEASE_SUREENDER_LEASE_VALIDATION")
        sp4.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
        sp4.Command.AddParameter("@SURRENDER_DATE", txtsurrender.Text, DbType.Date)
        validdate = sp4.ExecuteScalar()
        Return validdate
    End Function

End Class