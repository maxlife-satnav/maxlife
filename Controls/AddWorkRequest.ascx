<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddWorkRequest.ascx.vb"
    Inherits="Controls_AddWorkRequest" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Property Type<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                    Display="none" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                    InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlproptype" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select City<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlcity"
                    Display="None" ErrorMessage="Please Select city" ValidationGroup="Val1" InitialValue="0"
                    Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlcity" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Location <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvloc" runat="server" ControlToValidate="ddlLocation"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue=""></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Property <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvProperty" runat="server" ControlToValidate="ddlProperty"
                    Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1" InitialValue="0"
                    Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlProperty" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Title<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfTitle" runat="server" ControlToValidate="txtWorkTitle"
                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Work Title"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control"
                        MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Specifications<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvspec" runat="server" ControlToValidate="txtWorkSpec"
                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Work Specification"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control" Rows="3"
                        TextMode="MultiLine" MaxLength="250"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Estimated Amount<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvamount" runat="server" ControlToValidate="txtamount"
                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Estimated Amount"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revamount" Display="None" runat="server" ControlToValidate="txtamount"
                    ValidationGroup="Val1" ErrorMessage="Invalid Amount" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Vendor Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvVendor" runat="server" ControlToValidate="txtVendor"
                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Vendor Name"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Vendor Phone Number<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvPhno" runat="server" ControlToValidate="txtPhno"
                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Vendor Phone Number"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None"
                    runat="server" ControlToValidate="txtPhno" ValidationGroup="Val1" ErrorMessage="Invalid Phone Number"
                    ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPhno" runat="server" CssClass="form-control" MaxLength="12"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Vendor Address<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                    Display="none" ValidationGroup="Val1" ErrorMessage="Please Enter Vendor Address"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TextMode="MultiLine"
                        Rows="3"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Remarks<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                    Display="none" ValidationGroup="Val1" ErrorMessage="Please Enter Remarks"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                        Rows="3" MaxLength="250"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                CausesValidation="true" />&nbsp;            
        </div>
    </div>
</div>
