<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditPropertyType.ascx.vb"
    Inherits="Controls_EditPropertyType" %>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Edit Property Type
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Edit Property Type</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="1">
                    <tr>
                        <td align="left" width="25%">
                            <asp:Label ID="lblPropType" runat="server" CssClass="bodytext" Text="Property Type"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtPropType"
                                Display="none" ErrorMessage="Please enter Property Type !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtPropType"
                                ErrorMessage="Please enter valid Property Type" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                                ValidationGroup="Val1">
                            </asp:RegularExpressionValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <div onmouseover="Tip('Enter code in alphabets,numbers and /-,')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtPropType" runat="server" CssClass="textBox" Width="99%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblStatus" runat="Server" CssClass="bodytext" Text="Select Status"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                                Display="none" ErrorMessage="Please Status !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="clsComboBox" Width="99%">
                                <asp:ListItem>--Select--</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">InActive</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblrole" runat="server" CssClass="bodytext" Text="Select Role"></asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:ListBox ID="lirole" runat="server" Width="99%" SelectionMode="Multiple"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" style="height: 39px">
                            <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Modify" ValidationGroup="Val1"
                                CausesValidation="true" />
                            <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Back" />&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" />
            </td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
