Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions

Partial Class Controls_AMGVendorNewRecord1
    Inherits System.Web.UI.UserControl

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else


            Dim ValidateCode As Integer
            ValidateCode = ValidateVendorCode()

            Dim cntSelect As Integer = 0

            For i As Integer = 0 To lstTypes.Items.Count - 1
                If lstTypes.Items(i).Selected = True Then
                    cntSelect += 1
                End If
            Next
            If cntSelect = 0 Then
                lblMsg.Text = "Please select Vendor Type."
                Exit Sub
            End If

            Dim cntVenCat As Integer = 0

            For i As Integer = 0 To chkVendorCat.Items.Count - 1
                If chkVendorCat.Items(i).Selected = True Then
                    cntVenCat += 1
                End If
            Next
            If cntVenCat = 0 Then
                lblMsg.Text = "Please select Vendor Category."
                Exit Sub
            End If


            If ValidateCode = 0 Then
                lblMsg.Text = "Vendor Code Already Exist Please Select Another Code"
            ElseIf ValidateCode = 1 Then
                insertnewrecord()
                fillgrid()

            End If
        End If
    End Sub
    Public Sub fillgrid()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMG_VENDOR")
        'sp.Command.AddParameter("@AVR_NAME", txtfindcode.Text, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvDetails_AVR.DataSource = ds
        gvDetails_AVR.DataBind()
        For i As Integer = 0 To gvDetails_AVR.Rows.Count - 1
            Dim lblStatus_AVR As Label = CType(gvDetails_AVR.Rows(i).FindControl("lblStatus_AVR"), Label)
            If lblStatus_AVR.Text = "0" Then
                lblStatus_AVR.Text = "Inactive"
            Else
                lblStatus_AVR.Text = "Active"
            End If

        Next


    End Sub
    Public Function ValidateVendorCode()
        Dim ValidateCode As Integer
        Dim AVR_CODE As String = txtCode.Text
        Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_VENDOR_CODE")
        sp5.Command.AddParameter("@AVR_CODE", AVR_CODE, DbType.String)
        ValidateCode = sp5.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub insertnewrecord()
        Try

            For i As Integer = 0 To lstTypes.Items.Count - 1
                If lstTypes.Items(i).Selected = True Then
                    InsertVendorType(txtCode.Text, lstTypes.Items(i).Text, lstTypes.Items(i).Value)
                End If
            Next

            For j As Integer = 0 To chkVendorCat.Items.Count - 1
                If chkVendorCat.Items(j).Selected = True Then
                    InsertVendorCategory(txtCode.Text, chkVendorCat.Items(j).Value)
                End If
            Next

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_AMG_VENDOR")
            sp1.Command.AddParameter("@AVR_CODE", txtCode.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_NAME", txtName.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_GRADE", "A", DbType.String)
            sp1.Command.AddParameter("@AVR_ADDR", txtAddress.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_CITY", ddlCity.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@AVR_STATE", ddlState.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@AVR_COUNTRY", txtCountry.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_PHNO", txtPhone.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_MOBILE_PHNO", txtMobile.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_EMAIL", txtEmail.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_STATUS", rdbtnStatus.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_STA_ID", rdbtnStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AVR_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AVR_REMARKS", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_ACCNO", txtBankAcNo.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_BRN_NAME", txtBranchName.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_IFSC_CODE", txtIFSCCode.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_BANK_NAME", txtBankName.Text, DbType.String)


            sp1.Command.AddParameter("@AVR_PAN_NO", txtPANno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_GIR_NO", txtGIRno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_TAN_NO", txtTANno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_LSTNO", txtlstno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_WSTNO", txtwctno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_CSTNO", txtcstno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_STCIRCLE", txtstcircle.Text, DbType.String)

            sp1.ExecuteScalar()


            lblMsg.Text = "Vendor details added successfully"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub InsertVendorType(ByVal VendorCode As String, ByVal VendorTypeCode As String, ByVal VendorTypeId As Integer)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@AMG_VENDOR_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = VendorCode
        param(1) = New SqlParameter("@TYPE_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = VendorTypeCode
        param(2) = New SqlParameter("@TYPE_ID", SqlDbType.Int)
        param(2).Value = VendorTypeId
        ObjSubsonic.GetSubSonicExecute("INSERT_VERDORTYPE", param)

    End Sub


    Private Sub InsertVendorCategory(ByVal VendorCode As String, ByVal VendorCatName As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AMG_VEN_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = VendorCode
        param(1) = New SqlParameter("@CAT_NAME", SqlDbType.NVarChar, 200)
        param(1).Value = VendorCatName
        ObjSubsonic.GetSubSonicExecute("INSERT_VERDOR_CATEGORY", param)
    End Sub

    Public Sub Cleardata()
        txtCode.Text = ""
        txtName.Text = ""
        txtAddress.Text = ""
        txtPhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        txtRemarks.Text = ""
        ddlCity.SelectedIndex = 0
        txtBankAcNo.Text = ""
        txtBankName.Text = ""
        txtBranchName.Text = ""
        txtIFSCCode.Text = ""
        'ddlState.SelectedItem.Value = 0
        lstTypes.ClearSelection()
        chkVendorCat.ClearSelection()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else

                If Session("ViewOrModify") = "1" Then
                    btnSubmit.Enabled = False
                End If
                FillTypes()
                gvDetails_AVR.PageIndex = 0
                fillgrid()
                Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY")
                sp2.Command.AddParameter("@dummy", " ", DbType.String)
                ddlCity.DataSource = sp2.GetDataSet()
                ddlCity.DataTextField = "CTY_NAME"
                ddlCity.DataValueField = "CTY_CODE"
                ddlCity.DataBind()
                ddlCity.Items.Insert(0, "--Select--")
            End If
        End If
    End Sub
    Protected Sub gvDetails_AVR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails_AVR.PageIndexChanging
        gvDetails_AVR.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub gvDetails_AVR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetails_AVR.SelectedIndexChanged
        Dim rowIndex As Integer = gvDetails_AVR.SelectedIndex
        Dim lbl As Label = DirectCast(gvDetails_AVR.Rows(rowIndex).FindControl("lbl"), Label)
    End Sub

    Protected Sub gvDetails_AVR_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDetails_AVR.RowDeleting

    End Sub

    Private Sub FillTypes()
        ObjSubsonic.BindListBox(lstTypes, "GET_TYPES", "VT_TYPE", "ID")
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        '  Response.Redirect("frmAMGVendorGetDetails.aspx")
        Response.Redirect("~/Masters/MAS_Webfiles/frmMASMasters.aspx")
    End Sub

    'Protected Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlState.SelectedIndexChanged
    '    Dim AVR_STATE As String = ddlState.SelectedItem.Value
    '    Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITY")
    '    sp4.Command.AddParameter("@AVR_STATE", AVR_STATE, DbType.String)
    '    Dim ds4 As New DataSet
    '    ds4 = sp4.GetDataSet()
    '    ddlCity.DataSource = ds4
    '    ddlCity.DataTextField = "CTY_NAME"
    '    ddlCity.DataValueField = "CTY_CODE"
    '    ddlCity.DataBind()
    '    ddlCity.Items.Insert(0, "--Select--")
    'End Sub

    'Protected Sub btnview_Click(sender As Object, e As EventArgs) Handles btnview.Click
    '    Response.Redirect("frmAMGVendorGetDetails.aspx")
    'End Sub


End Class
