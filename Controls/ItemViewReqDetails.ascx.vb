Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions
Imports System.Data

Partial Class Controls_ItemViewReqDetails
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim CatId As String
    Dim asstsubcat As String
    Dim asstbrand As String
    Dim astmodel As String
    Dim LOC As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not IsPostBack Then
        '    Dim id As String = Request.QueryString("RID")
        'lblTemp.Text = id
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                BindUsers(UID)
                'BindCategories()
                getassetcategory()
                Cache.Remove("CIRGrid")
                'BindRequisition()
                BindBasicReqDetails()
                fillgrid()
                'dispdata()
            End If
        End If
    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GETCHECK_CONSUMABLES")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        '  ddlastCat.Items.Insert(0, New ListItem("--All--", ""))
    End Sub
    'Private Sub modifyqty()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_ITEMREQUISITION_DETAILS_MODIFY")
    '    sp.Command.AddParameter("@ReqId", lblTemp.Text, DbType.String)
    '    sp.Command.AddParameter("@Qty", txtQty.Text, DbType.Int32)
    '    sp.ExecuteScalar()
    'End Sub

    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return ""
        Else
            Return Session("UID")
        End If
    End Function
    Dim dsCIRGrid As New DataSet()
    Private Sub fillgrid()
        'Dim tickedcount = 0
        Dim ReqId As String = Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_FOR_VIEW_CONSUMABLE_REQUISITION")
        sp.Command.AddParameter("@AST_MD_CATID", CatId, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", asstsubcat, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", asstbrand, DbType.String)
        sp.Command.AddParameter("@AST_MD_MODEL_ID", astmodel, DbType.String)
        sp.Command.AddParameter("@AST_LOC_ID", LOC, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        dsCIRGrid = sp.GetDataSet
        gvItems.DataSource = dsCIRGrid

        gvItems.DataBind()
      
    End Sub

  
    Private Sub BindRequisition()
        Dim ReqId As String = Request("RID")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_REQUISITION_GetByReqId_NP")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                'Dim RaisedBy As Integer = 0
                'Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim RaisedBy As String
                RaisedBy = dr("AIR_AUR_ID")

                BindUsers(RaisedBy)

                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim CatId As Integer = 0
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)
                Dim CatId As String

                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = True


                getassetsubcategory(CatId)
                ddlAstSubCat.Enabled = True
                Dim asstsubcat As String = dr("AIR_ITEM_SUBCAT")

                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)

                Dim asstbrand As String = dr("AIR_ITEM_BRD")
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                ddlAstBrand.Enabled = True
                txtRemarks.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")
                'getmakebycatsubcat()
                'ddlModel.Items.FindByValue(dr("AIR_ITEM_MOD")).Selected = True
                'ddlModel.Enabled = False

                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                    tr1.Visible = False
                Else
                    tr1.Visible = True

                End If
                If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                    tr2.Visible = False
                Else
                    tr2.Visible = True
                End If

                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                'If RaisedBy = GetCurrentUser() Then
                If StatusId = 1501 Or StatusId = 1502 Then
                    btnModify.Enabled = True
                    btnCancel.Enabled = True
                Else
                    btnModify.Enabled = False
                    btnCancel.Enabled = False
                End If
                If StatusId = 1506 Then
                    btnClose.Visible = True
                Else
                    btnClose.Visible = False
                End If
                'Else
                'btnModify.Enabled = False
                'btnCancel.Enabled = False
                'End If

                'BindGrid()
                dispdata()
                For Each row As GridViewRow In gvItems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
                    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)

                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqIdAndProductId")
                    sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
                    sp1.Command.AddParameter("@ProductId", CInt(lblProductId.Text), DbType.Int32)
                    Dim dr1 As SqlDataReader = sp1.GetReader
                    If dr1.Read() Then
                        chkSelect.Checked = True
                        txtQty.Text = dr1("AID_QTY")
                        If txtQty.Text = "NULL" Or txtQty.Text = "" Or txtQty.Text = 0 Then
                            row.Visible = False
                        Else
                            row.Visible = True
                        End If
                    Else
                        row.Visible = False
                    End If
                    '
                Next
            Else
                lblMsg.Text = "No such requisition found."
        End If
        End If
    End Sub


    Private Sub BindBasicReqDetails()
        Dim ReqId As String = Request("RID")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_REQUISITION_GetByReqId_NP")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                'Dim RaisedBy As Integer = 0
                'Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim RaisedBy As String
                RaisedBy = dr("AIR_AUR_ID")

                BindUsers(RaisedBy)

                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim CatId As Integer = 0
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)


                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = True


                getassetsubcategory(CatId)
                ddlAstSubCat.Enabled = True
                asstsubcat = dr("AIR_ITEM_SUBCAT")

                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)

                asstbrand = dr("AIR_ITEM_BRD")
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                ddlAstBrand.Enabled = True


                getmakebycatsubcat()

                BindLocation()

                astmodel = dr("AIR_ITEM_MOD")
                ddlAstModel.Items.FindByValue(astmodel).Selected = True


                LOC = dr("AIR_REQ_LOC")
                ddlLocation.Items.FindByValue(LOC).Selected = True

                txtRemarks.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")
                'getmakebycatsubcat()
                'ddlModel.Items.FindByValue(dr("AIR_ITEM_MOD")).Selected = True
                'ddlModel.Enabled = False

                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                    tr1.Visible = False
                Else
                    tr1.Visible = True

                End If
                If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                    tr2.Visible = False
                Else
                    tr2.Visible = True
                End If

                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                'If RaisedBy = GetCurrentUser() Then
                If StatusId = 1501 Or StatusId = 1502 Then
                    btnModify.Enabled = True
                    btnCancel.Enabled = True
                Else
                    btnModify.Enabled = False
                    btnCancel.Enabled = False
                End If
                If StatusId = 1506 Then
                    btnClose.Visible = True
                Else
                    btnClose.Visible = False
                End If
            End If
        End If
    End Sub


    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex > 0 Then
            getmakebycatsubcat()
        End If

    End Sub

    Private Sub BindGrid()

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@ReqId", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_REQUISITION", param)

        pnlItems.Visible = True
    End Sub

    Private Sub BindUsers(ByVal AUR_ID As String)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = aur_id
        ObjSubsonic.Binddropdown(ddlEmp, "AMT_BINDUSERS_SP", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(aur_id)
        If Not li Is Nothing Then
            li.Selected = True
        End If
        'Dim sp As New SubSonic.StoredProcedure("AMT_GetUserDtls_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        'ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindCategories()
        'GetChildRows("0")
        ''ddlAstCat.DataSource = CategoryController.CategoryList
        ''ddlAstCat.DataTextField = "CategoryName"
        ''ddlAstCat.DataValueField = "CategoryID"
        ''ddlAstCat.DataBind()
        ''For Each li As ListItem In ddlAstCat.Items

        ''Next
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))

        'GetChildRows("0")
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")

    End Sub

    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ' ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    'Private Sub getmakebycatsubcat()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
    '    sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
    '    ddlModel.DataSource = sp.GetDataSet()
    '    ddlModel.DataTextField = "AST_MD_NAME"
    '    ddlModel.DataValueField = "AST_MD_CODE"
    '    ddlModel.DataBind()
    '    ddlModel.Items.Insert(0, "--Select--")

    'End Sub

    Private Sub modifyrem()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_ITEM_UPDATE_STATUS_USR")
        sp.Command.AddParameter("@id", Request.QueryString("RID"), DbType.String)
        sp.Command.AddParameter("@Rem", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("frmAssetThanks.aspx?RID=itemreqmodified&reqid=" + Request.QueryString("id"))
    End Sub

    Private Sub dispdata()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_VIEW_REQDETAILS")
        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_FOR_ASSETGRIDVR")
        SP.Command.AddParameter("@Req_id", Request.QueryString("RID"), DbType.String)
        Dim ds As New DataSet
        ds = SP.GetDataSet()
        Dim req_status As Integer = 0

        If ds.Tables(0).Rows.Count > 0 Then
            lblReqId.Text = ds.Tables(0).Rows(0).Item("AIR_REQ_TS")
            'lblLoc.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
            'lblTwr.Text = ds.Tables(0).Rows(0).Item("TWR_NAME")
            'lblFlr.Text = ds.Tables(0).Rows(0).Item("FLR_NAME")
            txtRemarks.Text = ds.Tables(0).Rows(0).Item("AIR_REMARKS")
            'lblLocCode.Text = ds.Tables(0).Rows(0).Item("LCM_CODE")
            'lblTwrCode.Text = ds.Tables(0).Rows(0).Item("TWR_CODE")
            'lblFlrCode.Text = ds.Tables(0).Rows(0).Item("FLR_CODE")
            req_status = ds.Tables(0).Rows(0).Item("AIR_STA_ID")
        End If
        If req_status = 1501 Or req_status = 1502 Then
            btnModify.Visible = True
            btnCancel.Enabled = True
        Else
            btnModify.Enabled = False
            btnCancel.Enabled = False
            txtRemarks.Enabled = False
        End If

        If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
            tr1.Visible = False
        Else
            tr1.Visible = True

        End If
        If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
            tr2.Visible = False
        Else
            tr2.Visible = True
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        'ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_REQUISITION", param)
        ObjSubsonic.BindGridView(gvItems, "AST_GET_CONSUMABLES_FOR_ASSETGRIDVR", param)
        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ITEM_REQUEST")
        'sp1.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        'Dim ds1 As New DataSet
        'ds1 = sp1.GetDataSet()
        'gvItemReceivedHistory.DataSource = ds1
        'gvItemReceivedHistory.DataBind()

        'If gvItemReceivedHistory.Rows.Count = 0 Then
        '    btnExpExcel.Visible = False
        'Else
        '    btnExpExcel.Visible = True
        'End If
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmItemViewReq.aspx")
    End Sub
    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        'Changing the status to modify after modified by  the user
        Validate(Request.QueryString("RID"))
        'modifyqty()
    End Sub

    Private Sub Validate(ByVal ReqId As String)


        Dim count As Integer = 0
        Dim Message As String = String.Empty

        Dim VT_Code As String
        Dim Ast_SubCat_Code As String
        Dim manufactuer_code As String
        Dim Ast_Md_code As String

        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
        

            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtQty.Text) Then
                    lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                    Exit Sub
                ElseIf IsNumeric(txtQty.Text) = True Then
                    count = count + 1
                    If count = 0 Then
                        lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
                        Exit Sub
                    End If
                Else
                    lblMsg.Text = "Please enter Quantity in Numerics Only"
                    Exit Sub
                End If
            End If
        Next
        If count > 0 Then
            UpdateData(ReqId, Trim(txtRemarks.Text), ddlAstCat.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value, ddlAstBrand.SelectedItem.Value, ddlAstModel.SelectedItem.Value, ddlLocation.SelectedItem.Value)
            For Each row As GridViewRow In gvItems.Rows

                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
                Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
                Dim AST_MD_NAME As Label = DirectCast(row.FindControl("AST_MD_NAME"), Label)

                Dim lblMinOrdQty As Label = DirectCast(row.FindControl("lblMinOrdQty"), Label)


                Dim lbl_vt_code As Label = DirectCast(row.FindControl("lbl_vt_code"), Label)
                Dim lbl_ast_subcat_code As Label = DirectCast(row.FindControl("lbl_ast_subcat_code"), Label)
                Dim lbl_manufactuer_code As Label = DirectCast(row.FindControl("lbl_manufactuer_code"), Label)
                Dim lbl_ast_md_code As Label = DirectCast(row.FindControl("lbl_ast_md_code"), Label)

                VT_Code = lbl_vt_code.Text
                Ast_SubCat_Code = lbl_ast_subcat_code.Text
                manufactuer_code = lbl_manufactuer_code.Text
                Ast_Md_code = lbl_ast_md_code.Text

                If chkSelect.Checked Then
                    If String.IsNullOrEmpty(txtQty.Text) Then
                        lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                        Exit Sub
                    ElseIf IsNumeric(txtQty.Text) = True Then
                        If CInt(lblMinOrdQty.Text) <= CInt(txtQty.Text) Then
                            count = count + 1
                            If count > 0 Then
                                InsertDetails(ReqId, lblProductid.Text, CInt(Trim(txtQty.Text)), VT_Code, Ast_SubCat_Code, manufactuer_code, Ast_Md_code)
                            End If
                        Else
                            lblMsg.Text = "Minimum Qty. should be  " + lblMinOrdQty.Text + " for " + AST_MD_NAME.Text
                            Exit Sub
                        End If

                    End If

                End If
            Next
            send_mail(ReqId)
            Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
        ElseIf count = 0 Then
            lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
        Else
            lblMsg.Text = "Please enter Quantity for Selected Checkbox"
        End If
    End Sub
    Public Sub send_mail(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_UPDATE")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Execute()
    End Sub

    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String, ByVal astcat As String, ByVal subcat As String, ByVal brand As String, ByVal model As String, ByVal location As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UpdateByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@StatusId", 1502, DbType.Int32)

        sp.Command.AddParameter("@cat", astcat, DbType.String)
        sp.Command.AddParameter("@subcat", subcat, DbType.String)
        sp.Command.AddParameter("@brand", brand, DbType.String)
        sp.Command.AddParameter("@model", model, DbType.String)
        sp.Command.AddParameter("@location", location, DbType.String)
        sp.ExecuteScalar()
    End Sub
    'Private Sub DeleteDetails(ByVal ReqId As String)
    '    'DELETE_ITEMREQDTLS
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DELETE_ITEMREQDTLS")
    '    sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
    '    sp.ExecuteScalar()
    'End Sub

    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer, ByVal VT_CODE As String,
        ByVal AST_SUBCAT_CODE As String, ByVal manufactuer_code As String, ByVal AST_MD_CODE As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_update")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)

        sp.Command.AddParameter("@ITEM_TYPE", VT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_SUBCAT", AST_SUBCAT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_BRD", manufactuer_code, DbType.String)
        sp.Command.AddParameter("@ITEM_MOD", AST_MD_CODE, DbType.String)

        sp.ExecuteScalar()

    End Sub

    'Private Function checkstockavailability()
    '    Dim avlblstck As Integer
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_ITEM_STOCK_AVLBLTY")
    '    sp.Command.AddParameter("@LOC", lblLoc.Text, DbType.String)
    '    'sp.Command.AddParameter("@TWR", lblTwrCode.Text, DbType.String)
    '    'sp.Command.AddParameter("@FLR", lblFlrCode.Text, DbType.String)
    '    'sp.Command.AddParameter("ITMCODE", lblItemCode.Text, DbType.String)
    '    avlblstck = sp.ExecuteScalar()
    '    Return avlblstck
    'End Function
    'Check the minimum order quantity
    'Private Function checkminordrstock()
    '    Dim minqty As Integer
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_ITEM_MIN_ORDER_QTY")
    '    sp.Command.AddParameter("@ItemCode", lblItemCode.Text, DbType.String)
    '    minqty = sp.ExecuteScalar()
    '    Return minqty
    'End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Changing the status to cancel after cancelled by  the user
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_UpdateByReqId")
        sp.Command.AddParameter("@ReqId", Request.QueryString("RID"), DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@StatusId", 1503, DbType.Int32)
        sp.Command.AddParameter("@location", ddlLocation.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
        send_mail_cancel(Request.QueryString("RID"))
        Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString("RID"))
    End Sub

    Public Sub send_mail_cancel(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_CANCEL")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Execute()
    End Sub

    'Protected Sub gvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItems.RowDataBound
    '    If (e.Row.RowType = DataControlRowType.DataRow) Then
    '        Dim chkSelect As CheckBox = DirectCast(e.Row.FindControl("chkSelect"), CheckBox)
    '        Dim lblReq_status As Label = DirectCast(e.Row.FindControl("lblReq_status"), Label)
    '        Dim txtQty As TextBox = DirectCast(e.Row.FindControl("txtQty"), TextBox)
    '        'lblReq_status
    '        chkSelect.Checked = True

    '        If CInt(lblReq_status.Text) = 1027 Then
    '            txtQty.Enabled = True
    '            btnModify.Enabled = True
    '            txtRem.Enabled = True
    '            btnCancel.Enabled = True
    '            chkSelect.Enabled = True
    '        Else
    '            txtQty.Enabled = False
    '            btnModify.Enabled = False
    '            txtRem.Enabled = False
    '            btnCancel.Enabled = False
    '            chkSelect.Enabled = False
    '        End If
    '    End If


    'End Sub

    'Protected Sub gvItemReceivedHistory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemReceivedHistory.RowDataBound
    '    If (e.Row.RowType = DataControlRowType.DataRow) Then
    '        Dim gvItemReceivedHistoryDTLS As GridView = DirectCast(e.Row.FindControl("gvItemReceivedHistoryDTLS"), GridView)
    '        Dim lblAIM_CODE As Label = DirectCast(e.Row.FindControl("lblAIM_CODE"), Label)

    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ITEM_REQUEST_DTLS")
    '        sp1.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
    '        sp1.Command.AddParameter("@AIM_CODE", lblAIM_CODE.Text, DbType.String)
    '        Dim ds1 As New DataSet
    '        ds1 = sp1.GetDataSet()
    '        gvItemReceivedHistoryDTLS.DataSource = ds1
    '        gvItemReceivedHistoryDTLS.DataBind()


    '    End If
    'End Sub


    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UpdateByReqId")
        sp.Command.AddParameter("@ReqId", Request.QueryString("RID"), DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@StatusId", 1512, DbType.Int32)
        sp.Execute()
        Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString("RID"))
    End Sub


    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub
    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")

        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        'ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        If ddlastsubCat.SelectedIndex > 0 Then
            getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
        End If

    End Sub
    Protected Sub ddlastCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstCat.SelectedIndexChanged
        getsubcategorybycat(ddlastCat.SelectedItem.Value)
    End Sub

    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlastsubCat.DataSource = sp.GetDataSet()
        ddlastsubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlastsubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlastsubCat.DataBind()
        ' ddlastsubCat.Items.Insert(0, "--Select--")
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        fillgridOnSearch()

    End Sub

    Private Sub fillgridOnSearch()
        Dim ReqId As String = Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_FOR_VIEW_CONSUMABLE_REQUISITION")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_MODEL_ID", ddlAstModel.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        dsCIRGrid = sp.GetDataSet
        gvItems.DataSource = dsCIRGrid
        gvItems.DataBind()
        If gvItems.Rows.Count > 0 Then
            pnlItems.Visible = True
            remarksAndActionButtions.Visible = True
        End If
    End Sub
    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
    Public Sub cleardata()
        ddlastCat.ClearSelection()
        ddlastsubCat.ClearSelection()
        ddlLocation.ClearSelection()
        pnlItems.Visible = False
        getassetsubcategory()
        ddlAstBrand.Items.Clear()
        ddlAstModel.Items.Clear()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    Private Sub getassetsubcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
        pnlItems.Visible = False
        remarksAndActionButtions.Visible = False
    End Sub
End Class
