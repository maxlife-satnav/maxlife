<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAssetModel.ascx.vb" Inherits="Controls_AddAssetModel" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-bottom: 20px">
    <div class="col-md-6">
        <label class="col-md-2 btn btn-default pull-right">
            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                ToolTip="Please Select Add to add new Asset Model and Select Modify to modify the existing Asset Model" />
            Add
        </label>
    </div>
    <div class="col-md-6">
        <label class="btn btn-default" style="margin-left: 25px">
            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                ToolTip="Please Select Add to add new Asset Model and Select Modify to modify the existing Asset Model" />
            Modify
        </label>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row" id="pnlmodelcode" runat="server" visible="false">
                <asp:Label ID="lblAssetModel" runat="server" CssClass="col-md-5 control-label">Select Asset Model<span style="color: red;">*</span></asp:Label>
                <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlModel"
                    ErrorMessage="Please Select Asset Model" ValueToCompare="--Select--" ></asp:CompareValidator>

                <div class="col-md-7">
                    <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Asset Model"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Model Code<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtModel"
                    Display="none" ErrorMessage="Please Enter Model Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtModel"
                    ErrorMessage="Please Enter code in alphabets,numbers" Display="None" ValidationExpression="^[0-9a-zA-Z]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtModel" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Model Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtModelName"
                    Display="none" ErrorMessage="Please Enter Model Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtModelName"
                    ErrorMessage="Please Enter Model Name in alphabets,numbers" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter name in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtModelName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Asset Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                    Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Asset Category" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Asset Sub Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                    Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Asset Sub Category" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Asset Brand/Make<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Asset Brand/Make" AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Status</label>
                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                    Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Status">
                        <%--<asp:ListItem>--Select--</asp:ListItem>--%>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="pnlMinQtyCon" runat="server" visible="false">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Quantity Unit <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlunit"
                        Display="none" ErrorMessage="Please Select Quantity Unit" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlunit" runat="server" CssClass="selectpicker" data-live-search="true"
                            ToolTip="Select Quantity Unit">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Min order Qty.<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtModel"
                        Display="none" ErrorMessage="Please Enter Min Order Qty." InitialValue="" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMinOrdQty"
                        ErrorMessage="Please Enter Quantity In Numbers" Display="None" ValidationExpression="^[0-9]+"
                        ValidationGroup="Val1">
                    </asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter Quantity in numbers')" onmouseout="UnTip()">
                            <asp:TextBox ID="txtMinOrdQty" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Min stock Qty.<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtModel"
                        Display="none" ErrorMessage="Please Enter Min Stock Qty." ValidationGroup="Val1" InitialValue=""></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtMinEscQty"
                        ErrorMessage="Please Enter Minimum Stock In Numbers" Display="None" ValidationExpression="^[0-9]+"
                        ValidationGroup="Val1">
                    </asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter Minimum Stock In Numbers')" onmouseout="UnTip()">
                            <asp:TextBox ID="txtMinEscQty" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        


        <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks</label>
            <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtBrand"
                    Display="none" ErrorMessage="Please Enter Asset Category Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtBrand"
                    ErrorMessage="Please Enter Asset Category Code" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>--%>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>


    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" CausesValidation="true" />

            <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" CausesValidation="False" PostBackUrl="~/FAM/Masters/Mas_Webfiles/frmAssetMasters.aspx" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvBrand" runat="server" AllowPaging="True" AllowSorting="False"
            RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
            PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Asset Model Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%#Eval("AST_MD_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Model Name">
                    <ItemTemplate>
                        <asp:Label ID="lblCode" runat="server" CssClass="lblCode" Text='<%#BIND("AST_MD_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Model Code">
                    <ItemTemplate>
                        <asp:Label ID="lblMdlCode" runat="server" Text='<%#Eval("AST_MD_CODE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Brand/Make">
                    <ItemTemplate>
                        <asp:Label ID="lblAstBrnd" runat="server" CssClass="lblCat" Text='<%#BIND("manufacturer")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Sub Category">
                    <ItemTemplate>
                        <asp:Label ID="lblSubCat" runat="server" CssClass="lblCat" Text='<%#BIND("AST_SUBCAT_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Category">
                    <ItemTemplate>
                        <asp:Label ID="lblCat" runat="server" CssClass="lblCat" Text='<%#BIND("VT_TYPE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("AST_MD_STAID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
