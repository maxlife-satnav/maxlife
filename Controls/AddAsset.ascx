<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAsset.ascx.vb" Inherits="Controls_AddAsset" %>




<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row" >
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Upload Document(Only Excel)<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                    ControlToValidate="fpBrowseDoc" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" ControlToValidate="fpBrowseDoc"
                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                    ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                </asp:RegularExpressionValidator>
                <div>
                    <div class="btn btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="col-md-5  control-label">
                    <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val2" style="height: 26px" />
                </div>
                <div>
                    <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/NewAsset.xls"></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-bottom: 20px">
    <div class="col-md-6">
        <label class="col-md-2 btn btn-default pull-right">
            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                ToolTip="Please Select Add to add new Asset and Select Modify to modify the existing Asset" />
            Add
        </label>
    </div>
    <div class="col-md-6">
        <label class="btn btn-default" style="margin-left: 25px">
            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                ToolTip="Please Select Add to add new Asset and Select Modify to modify the existing Asset" />
            Modify
        </label>
    </div>
</div>

<div class="row" style="padding-left:30px">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <%--  <div class="row">--%>
            <asp:Label ID="lblAsset" runat="server" Visible="False">Select Asset</asp:Label>
            <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlAsset"
                ErrorMessage="Please Select the Asset" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>

            <%-- <div >--%>
            <asp:DropDownList ID="ddlAsset" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset"
                AutoPostBack="True" Visible="False">
            </asp:DropDownList>
            <%--  </div>--%>
            <%--</div>--%>
        </div>
    </div>
</div>
<div class="row" style="padding-left:30px">
  
  


    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <%-- <div class="row">--%>
            <label>Select Asset Category<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Category" AutoPostBack="True">
            </asp:DropDownList>
            <%--</div>--%>
        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <%--     <div class="row">--%>
            <label>Select Asset Sub Category<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlAstSubCat"
                Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--">
            </asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Sub Category" AutoPostBack="True">
            </asp:DropDownList>
            <%--</div>--%>
        </div>
    </div>
         <div class="col-md-1 col-sm-12 col-xs-12"></div>

        <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <%--       <div class="row">--%>
            <label>Select Asset Brand/Make<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlAstBrand"
                Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Brand/Make" AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
            </asp:DropDownList>
            <%-- </div>--%>
        </div>
    </div>
</div>
<div class="row" style="padding-left:30px">



   
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Select Asset Model<span style="color: red;">*</span></label>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlModel"
                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>


            <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Model"
                AutoPostBack="True">
            </asp:DropDownList>



        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
       <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <asp:Label ID="Label5" runat="server">Asset Type<span style="color: red;">*</span></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="ddlStatus"
                Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlAssetType" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Status">
                <%--<asp:ListItem>--Select--</asp:ListItem>--%>
               <%-- <asp:ListItem Value="1">Active</asp:ListItem>
                <asp:ListItem Value="0">InActive</asp:ListItem>--%>
            </asp:DropDownList>


        </div>
    </div>

    <div class="col-md-1 col-sm-12 col-xs-12"></div>
      <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <%-- <div class="row">--%>
            <label>Select Vendor</label>
           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlVendor"
                Display="none" ErrorMessage="Please Select Vendor" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Vendor" style="height: 22px" >
            </asp:DropDownList>
            <%-- </div>--%>
        </div>
    </div>
</div>
<div class="row" style="padding-left:30px">



     <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Select Location<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlLocation"
                Display="none" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>


            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Model" AutoPostBack="true">
            </asp:DropDownList>



        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Select Tower<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlTower"
                Display="none" ErrorMessage="Please Select Tower" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>


            <asp:DropDownList ID="ddlTower" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Model" AutoPostBack="true">
            </asp:DropDownList>



        </div>
    </div>
    <div class="col-md-1 col-sm-12 col-xs-12"></div>
      <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <%-- <div class="row">--%>
            <label>Select Floor<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="ddlFloor"
                Display="none" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlFloor" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Floor" style="height: 22px" >
            </asp:DropDownList>
            <%-- </div>--%>
        </div>
    </div>
</div>
<div class="row" style="padding-left:30px">
    <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label>Asset Id <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAssetName"
                    Display="none" ErrorMessage="Please Enter Asset Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>


                <div onmouseover="Tip('Enter id in alphabets,numbers')" onmouseout="UnTip()">
                    <asp:TextBox ID="txtAssetName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>


            </div>
        </div>

     <div class="col-md-1 col-sm-12 col-xs-12"></div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Asset Serial No.<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtAssetSerialNo"
                Display="none" ErrorMessage="Please Enter Asset Serial No" ValidationGroup="Val1"></asp:RequiredFieldValidator>


            <%--<div onmouseover="Tip('Enter code in numbers')" onmouseout="UnTip()">--%>
                <asp:TextBox ID="txtAssetSerialNo" runat="server" CssClass="form-control"></asp:TextBox>
     <%--       </div>--%>


        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
    
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Asset Price (In Cost)<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAssetPrice"
                Display="none" ErrorMessage="Please Enter  Asset Price" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtAssetPrice"
                ErrorMessage="Please Enter Asset Price in numbers" Display="None" ValidationExpression="^[0-9.]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>


            <div onmouseover="Tip('Enter price in numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtAssetPrice" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
            </div>

        </div>
    </div>



</div>
<div class="row" style="padding-left:30px">
    
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <asp:Label ID="Label2" runat="server">Purchase Date<span style="color: red;">*</span></asp:Label><br />


            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtPDate"
                ErrorMessage="Please Enter Purchase Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>


            <div class='input-group date' id='fromdate'>
                <asp:TextBox ID="txtPDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                </span>
            </div>


        </div>
    </div><div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <asp:Label ID="lblMfgDate" runat="server">Mfg. Date<span style="color: red;">*</span></asp:Label>
            <asp:RequiredFieldValidator ID="rfvHYDate" runat="server" ControlToValidate="txtMfgDate"
                ErrorMessage="Please Enter Mfg. Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>

            <div class='input-group date' id='Div1'>
                <asp:TextBox ID="txtMfgDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                </span>
            </div>


        </div>
    </div> <div class="col-md-1 col-sm-12 col-xs-12"></div>
      <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <asp:Label ID="Label1" runat="server">Warranty Date Up To<span style="color: red;">*</span></asp:Label>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtWarDate"
                ErrorMessage="Please Enter Warranty. Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>

            <div class='input-group date' id='Div4'>
                <asp:TextBox ID="txtWarDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div4')"></span>
                </span>
            </div>


        </div>
    </div>

    <div id="pnlAst" runat="server" visible="false">

        <div class="col-md-3 col-sm-12 col-xs-12" runat="server" visible="false">
            <div class="form-group" >

                <label>Asset Code <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtAssetCode"
                    Display="none" ErrorMessage="Please Enter Asset Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>



                <asp:TextBox ID="txtAssetCode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>


            </div>
        </div> 
    
    </div>
</div>
<div class="row" style="padding-left:30px">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Life Span(in Years)</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtAssetPrice"
                Display="none" ErrorMessage="Please Enter  Life Span" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
          <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAssetPrice"
                ErrorMessage="Please Enter Asset Price in numbers" Display="None" ValidationExpression="^[0-9.]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>--%>

          <%--  <div onmouseover="Tip('Enter code in numbers')" onmouseout="UnTip()">--%>
                <asp:TextBox ID="txtLifeSpan" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
            <%--</div>--%>


        </div>
    </div>      <div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>PO Number</label>
      <%--      <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtAssetPrice"
                Display="none" ErrorMessage="Please Enter  Asset Price" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtAssetPrice"
                ErrorMessage="Please Enter PO Number in numbers" Display="None" ValidationExpression="^[0-9.]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>--%>

    <%--        <div onmouseover="Tip('Enter code in numbers')" onmouseout="UnTip()">--%>
                <asp:TextBox ID="txtPONumber" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
      <%--      </div>--%>


        </div>
    </div>
    <div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Depriciation(in %)</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtAssetSerialNo"
                Display="none" ErrorMessage="Please Enter  Asset Serial No" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAssetPrice"
                ErrorMessage="Please Enter Depriciation in numbers" Display="None" ValidationExpression="^[0-9.]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>


<%--            <div onmouseover="Tip('Enter code in numbers')" onmouseout="UnTip()">--%>
                <asp:TextBox ID="txtDepriciation" runat="server" CssClass="form-control"></asp:TextBox>


        </div>
    </div>

</div>
<div class="row" style="padding-left:30px;" id="rbn1" runat="server" >
    <div class="col-md-3 col-sm-12 col-xs-12">
      <div class="form-group">          <br />
                    <label>AMC Included(Y/N)<span style="color: red;">*</span></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButtonList ID="rdAMC" runat="server" AutoPostBack="True"
                        Font-Bold="True" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:RadioButtonList>              

                </div>
    </div>      <div class="col-md-1 col-sm-12 col-xs-12"></div>
       <div class="col-md-3 col-sm-12 col-xs-12" id="rbnAmcDate" runat="server">
        <div class="form-group">

            <asp:Label ID="Label3" runat="server">AMC Date Up To <span style="color: red;">*</span></asp:Label>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtAMCDate"
                ErrorMessage="Please Enter AMC Date Up To" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>


            <div class='input-group date' id='Div2'>
                <asp:TextBox ID="txtAMCDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div2')"></span>
                </span>
            </div>


        </div>
    </div> <div class="col-md-1 col-sm-12 col-xs-12"></div>
   <div class="col-md-3 col-sm-12 col-xs-12" id="rbnAmcUpload" runat="server"> 
        <div class="form-group">

            <label>Upload Asset Image<span style="color: red;">*</span></label>
             <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="fpBrowseAMCDoc"
                ErrorMessage="Please Upload AMC Image" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>
            <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseAMCDoc"
                ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$"> 
                            
            </asp:RegularExpressionValidator>


            <div class="btn btn-default">
                <i class="fa fa-folder-open-o fa-lg"></i>
                <asp:FileUpload ID="fpBrowseAMCDoc" runat="Server" onchange="showselectedfiles(this)" Width="90%" />


            </div>
           

            <asp:LinkButton ID="hplAMC" runat="server" Visible="false"></asp:LinkButton>
            <asp:Label ID="lblhplAMC" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Literal ID="ltrAMC" Text="" runat="server"></asp:Literal>



        </div>
    </div> <div id="placehere" class="col-md-1 col-sm-12 col-xs-12">  </div>

</div>
<div class="row" style="padding-left:30px" id="rbn2" runat="server">
    <div class="col-md-3 col-sm-12 col-xs-12">
 <div class="form-group"><br />

                    <label>Insurance Included(Y/N)<span style="color: red;">*</span></label>&nbsp;

                    <asp:RadioButtonList ID="rdIns" runat="server" CssClass="clsRadioButton" AutoPostBack="True"
                        Font-Bold="True" RepeatDirection="Horizontal"  RepeatLayout="Flow">
                        <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:RadioButtonList>


                </div>
    </div>      
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-3 col-sm-12 col-xs-12" id="rbnInsDate" runat="server">
        <div class="form-group">

            <asp:Label ID="Label4" runat="server">Insurance Date Up To<span style="color: red;">*</span></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtIncDate"
                ErrorMessage="Please Enter Insurance Date Up To" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>


            <div class='input-group date' id='Div3'>
                <asp:TextBox ID="txtIncDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div3')"></span>
                </span>
            </div>


        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-3 col-sm-12 col-xs-12" id="rbnInsUpload" runat="server">
        <div class="form-group">

            <label>Upload Insurance Document(s)<span style="color: red;">*</span></label>
               <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="fpBrowseIncDoc"
                ErrorMessage="Please Upload Insurance Document" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" ControlToValidate="fpBrowseIncDoc"
                ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$"> 
                            
            </asp:RegularExpressionValidator>

            <div class="btn btn-default">
                <i class="fa fa-folder-open-o fa-lg"></i>
                <asp:FileUpload ID="fpBrowseIncDoc" runat="Server" onchange="show(this)" Width="90%" />

            </div>
            <div id="image-holder"></div>

            <asp:LinkButton ID="hplINC" runat="server" Visible="false"></asp:LinkButton>
            <asp:Label ID="lblINC" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Literal ID="litINC" Text="" runat="server"></asp:Literal>


        </div>
    </div> 

</div>
<div class="row" style="padding-left:30px">
        <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Salvage Value (In Cost)<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtSalvgValue"
                Display="none" ErrorMessage="Please Enter  Salvage Value" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtSalvgValue"
                ErrorMessage="Please Enter Salvage Value in numbers" Display="None" ValidationExpression="^[0-9.]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>

            <div onmouseover="Tip('Enter Salvage Value in numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtSalvgValue" runat="server" CssClass="form-control"></asp:TextBox>
            </div>


        </div>
    </div>
      <div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <asp:Label ID="Label6" runat="server">Status</asp:Label>
            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Status">
                <%--<asp:ListItem>--Select--</asp:ListItem>--%>
                <asp:ListItem Value="1">Active</asp:ListItem>
                <asp:ListItem Value="0">InActive</asp:ListItem>
            </asp:DropDownList>


        </div>
    </div><div class="col-md-1 col-sm-12 col-xs-12"></div>
        <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Asset Description </label>
&nbsp;<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAssetDescription"
                Display="none" ErrorMessage="Please Enter Description" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
          <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAssetDescription"
                ErrorMessage="Please Enter Asset Description" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>--%>

            <div onmouseover="Tip('Enter Asset Description')" onmouseout="UnTip()">
                <asp:TextBox ID="txtAssetDescription" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>


        </div>
    </div> 
</div>


<%--<div class="row">

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="row">
                <label >Asset Serial No.<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtAssetSerialNo"
                    Display="none" ErrorMessage="Please Enter  Asset Serial No" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                <div >
                    <div onmouseover="Tip('Enter code in numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>--%>
 <%--<div class="col-md-1 col-sm-12 col-xs-12"></div>--%>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" OnClick="btnBack_Click" CausesValidation="False" />

        </div>
    </div>
</div> 
<%--<div class="col-md-1 col-sm-12 col-xs-12"></div>--%>
<div class="row table table table-condensed table-responsive">
    <div class="form-group">
        <div  class="col-md-12">
            <asp:GridView ID="gvBrand" runat="server" AllowPaging="True" AllowSorting="False"
                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Records Found" CssClass="table table-condensed table-bordered table-hover table-striped">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField HeaderText="Asset Id">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("AST_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("LOCATION")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AST_CATEGORY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Sub Category">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AST_SUBCAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Brand">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AST_BRAND")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText="Model Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("MODEL_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> <asp:TemplateField HeaderText="Asset Cost">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("COST")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> <asp:TemplateField HeaderText="Asset Serial Number">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("SERIAL_NO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> <asp:TemplateField HeaderText="Purchase Date">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("PURCHASEDATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> <asp:TemplateField HeaderText="Asset Type">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AST_TYPE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> <asp:TemplateField HeaderText="Vendor Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AVR_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> <asp:TemplateField HeaderText="Added By">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AUR_KNOWN_AS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

<script>

    function showselectedfiles(fu) {
        document.getElementById('placehere').innerHTML = "";
        //var filename=fu.files[0].name;
        //var extn = filename.split('.').pop();

        //if (extn == jpg) {


        var input = document.getElementById("<%= fpBrowseAMCDoc.ClientID%>");

               var fReader = new FileReader();
               fReader.readAsDataURL(input.files[0]);
               fReader.onloadend = function (event) {
                   var img = document.createElement("img");
                   img.id = "picture"
                   img.style.height = "50px";
                   img.style.width = "50px";
                   img.style.border = "1px solid yellow";
                   img.src = event.target.result;
                   document.getElementById("placehere").appendChild(img);
               }
           }
           //}


           //function show(fu) {
           //    document.getElementById('image-holder').innerHTML = "";

           // var input = document.getElementById("<%= fpBrowseIncDoc.ClientID%>");
    //    var fReader = new FileReader();
    //    fReader.readAsDataURL(input.files[0]);
    //    fReader.onloadend = function (event) {
    //        var img = document.createElement("img");
    //        img.id = "picture"
    //        img.style.height = "60px";
    //        img.style.width = "60px";
    //        img.style.border = "1px solid yellow";
    //        img.src = event.target.result;
    //        document.getElementById("image-holder").appendChild(img);        

    //    }

    //}

</script>

