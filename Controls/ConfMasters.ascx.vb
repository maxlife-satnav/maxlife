Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ConfMasters
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            tablegrid1.Visible = False
            
        End If
        txtFrmDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindGrid()
        'Get Attendance For 1 week From Yesterday
        Try
            Dim yesterday As String = getoffsetdate(Date.Today).AddDays(-1).ToShortDateString()
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_HISTORY_ATTENDANCE_REGISTER")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If CDate(txtFrmDate.Text) > CDate(txtToDate.Text) Then
            lblMsg.Text = "To Date Should be Greater then From Date"
        ElseIf txtFrmDate.Text <> "" And txtToDate.Text <> "" Then
            BindGrid1()
            tablegrid1.Visible = True
            gvItems.Visible = False
        Else
            tablegrid1.Visible = False
        End If
    End Sub
    Private Sub BindGrid1()
        Try
            'Get Attendance Between Those Dates
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_HISTORY_ATTENDANCE_REGISTER1")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@FROMDATE", txtFrmDate.Text, DbType.Date)
            sp.Command.AddParameter("@TODATE", txtToDate.Text, DbType.Date)
            gvItems1.DataSource = sp.GetDataSet()
            gvItems1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems1.PageIndexChanging
        gvItems1.PageIndex = e.NewPageIndex()
        BindGrid1()
    End Sub
End Class
