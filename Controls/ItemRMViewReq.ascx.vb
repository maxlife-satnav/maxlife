Imports System.Data

Partial Class Controls_ItemRMViewReq
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fillgrid()
        End If
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_FOR_RMAPPROVAL")
        'sp.Command.AddParameter("@dummy", 1, Data.DbType.Int32)
        sp.Command.AddParameter("@Cuser", Session("Uid"), Data.DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
        If gvItems.Rows.Count = 0 Then
            RemarksNApprNRejPanel.Visible = False
        End If
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UpdateByReqIdRM")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@StatusId", 1504, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Private Sub Validate(ByVal ReqId As String)


        Dim count As Integer = 0
        Dim Message As String = String.Empty

        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked Then
                count = count + 1
            End If
        Next
        If count > 0 Then
            UpdateData(ReqId, Trim(txtRM.Text))
            For Each row As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
                Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
                Dim lblMinOrdQty As Label = DirectCast(row.FindControl("lblMinOrdQty"), Label)
                Dim lblqty As Label = DirectCast(row.FindControl("lblqty"), Label)
                If chkSelect.Checked Then
                    If String.IsNullOrEmpty(txtQty.Text) Then
                        lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                        Exit Sub
                    ElseIf IsNumeric(txtQty.Text) = True Then
                        If CInt(lblMinOrdQty.Text) <= CInt(txtQty.Text) Then
                            If lblqty.Text >= txtQty.Text Then
                                count = count + 1
                                If count > 0 Then

                                    InsertDetails(ReqId, lblProductid.Text, CInt(Trim(txtQty.Text)))
                                End If
                            Else
                                lblMsg.Text = "Maximum quantity to approve is only " & lblqty.Text
                                Exit Sub
                            End If

                        Else
                            lblMsg.Text = "Minimum Qty. should be  " + lblMinOrdQty.Text
                            Exit Sub
                        End If

                    End If

                End If
            Next
            ' send_mail(ReqId)
            Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString("RID"))
        ElseIf count = 0 Then
            lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
        Else
            lblMsg.Text = "Please enter Quantity for Selected Checkbox"
        End If
    End Sub
    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_updatereq1")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.ExecuteScalar()

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim ReqIDsForRejectMsg = ""
        For Each row As GridViewRow In gvItems.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim bf As CheckBox = TryCast(row.Cells(0).FindControl("chkSelect"), CheckBox)
                Dim id As String = TryCast(row.Cells(1).FindControl("hLinkDetails"), HyperLink).Text

                If bf.Checked Then

                    RejectRequest(id)
                    ReqIDsForRejectMsg = ReqIDsForRejectMsg + ", " + id
                End If
            End If
        Next
        fillgrid()
        txtRM.Text = ""
        lblMsg.Text = "Requisition(s) Rejected Successfully ( " + ReqIDsForRejectMsg.Remove(0, 1) + " ) "
     
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim ReqIDsForMsg = ""
        For Each row As GridViewRow In gvItems.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim bf As CheckBox = TryCast(row.Cells(0).FindControl("chkSelect"), CheckBox)
                Dim id As String = TryCast(row.Cells(1).FindControl("hLinkDetails"), HyperLink).Text
                If bf.Checked Then
                    UpdateData(id, txtRM.Text)
                    send_mail(id)
                    ReqIDsForMsg = ReqIDsForMsg + ", " + id
                End If
            End If
        Next
        fillgrid()
        txtRM.Text = ""
        lblMsg.Text = "Requisition(s) Approved Successfully ( " + ReqIDsForMsg.Remove(0, 1) + " ) "

    End Sub

    Protected Sub RejectRequest(ByVal ReqID As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UpdateByReqIdRMCANCEL")
        sp.Command.AddParameter("@ReqId", ReqID, DbType.String)
        sp.Command.AddParameter("@Remarks", txtRM.Text, DbType.String)
        sp.Command.AddParameter("@StatusId", 1505, DbType.Int32)

        sp.ExecuteScalar()
        send_mail_reject(ReqID)
        ' Response.Redirect("frmAssetThanks.aspx?RID=" + ReqID)
    End Sub


    Public Sub send_mail_reject(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_ADMIN_REJECT")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Execute()
    End Sub

    Public Sub send_mail(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_ADMIN_APPROVAL")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Execute()
    End Sub
End Class
