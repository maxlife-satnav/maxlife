Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_RequesitionReport
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        BindGridView()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindGridView()
            End If
        End If
    End Sub

    Private Sub BindGridView()

        Dim Sdate As DateTime
        Dim Edate As DateTime

        If String.IsNullOrEmpty(txtsdate.Text) = False Then
            Sdate = CDate(txtsdate.Text)
        End If

        If String.IsNullOrEmpty(txtEdate.Text) = False Then
            Edate = CDate(txtEdate.Text)
        End If

        If String.IsNullOrEmpty(txtsdate.Text) = False And String.IsNullOrEmpty(txtsdate.Text) = False Then
            If CDate(txtsdate.Text) > CDate(txtEdate.Text) Then
                lblMsg.Text = "Invalid Date Range"

                btnExpExcel.Visible = False
               
                gvItem.DataSource = Nothing
                gvItem.DataBind()

                Exit Sub
            End If
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@SDATE", SqlDbType.DateTime)
            param(0).Value = txtsdate.Text
            param(1) = New SqlParameter("@EDATE", SqlDbType.DateTime)
            param(1).Value = txtEdate.Text
            ObjSubsonic.BindGridView(gvItem, "GET_REQUESTBETDATES", param)

            If gvItem.Rows.Count = 0 Then
                btnExpExcel.Visible = False
                lblMsg.Text = ""
            Else
                btnExpExcel.Visible = True
                lblMsg.Text = ""
            End If
        Else
            btnExpExcel.Visible = False
        End If
        
      
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindGridView()
    End Sub


    Protected Sub btnExpExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExpExcel.Click
        exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        exportpanel1.FileName = "ItemRequisitionReport"
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        If e.CommandName = "ReqId" Then
            Response.Redirect("frmItemRequisitionDetails.aspx?id=" & e.CommandArgument)
        End If
    End Sub

    Protected Sub gvItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItem.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lblPending As Label = CType(e.Row.Cells(0).FindControl("lblPending"), Label)
            Dim lnkReq_id As LinkButton = CType(e.Row.Cells(0).FindControl("lnkReq_id"), LinkButton)
            If CInt(lblPending.Text) > 0 Then
                lnkReq_id.Enabled = True
            Else
                lnkReq_id.Enabled = False
            End If
        End If
    End Sub
End Class
