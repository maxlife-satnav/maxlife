Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common

Partial Class Controls_ApprovetoProcure
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_REQUISITION_GetByStatusId3")
        sp.Command.AddParameter("@dummy", 1008, Data.DbType.Int32) '--1010
        sp.Command.AddParameter("@Cuser", Session("uid"), Data.DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub

    'Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
    '    gvItems.PageIndex = e.NewPageIndex
    '    BindGrid()
    'End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Private Sub UpdateAssetRequisitionData(ByVal ReqId As String, ByVal ProductId As String, ByVal StockQty As Integer, ByVal PurchaseQty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_UpdateStockPurchaseQtyByReqIdAndProductId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@SQty", StockQty, DbType.Int32)
        sp.Command.AddParameter("@PQty", PurchaseQty, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Private Sub UpdateData(ByVal ReqId As String, ByVal StatusId As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionStatus_UpdateByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
        sp.Command.AddParameter("@UID", Session("UID"), DbType.Int32)
        sp.Command.AddParameter("@REM", txtRM.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim ds As DataSet
        Dim lblReqID As Label
        Dim FlagStock As Boolean = False
        Dim FlagPurchase As Boolean = False
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblReqID = DirectCast(row.FindControl("lblReqID"), Label)
            If chkSelect.Checked Then
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
                sp1.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
                ds = sp1.GetDataSet
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim lblProductId As String = dr("AST_MD_CODE")
                    Dim txtQty As String = dr("AID_QTY")
                    Dim txtStockQty As String = dr("AID_MVM_QTY")
                    Dim txtPurchaseQty As String = dr("AID_ORD_QTY")
                    If CInt(Trim(txtPurchaseQty)) > 0 Then
                        FlagPurchase = True
                    End If
                    If CInt(Trim(txtStockQty)) > 0 Then
                        FlagStock = True
                    End If
                    UpdateAssetRequisitionData(lblReqID.Text, Trim(lblProductId), CInt(Trim(txtStockQty)), CInt(Trim(txtPurchaseQty)))
                Next
                Dim StatusId As Integer = 0
                If FlagPurchase = True And FlagStock = True Then
                    StatusId = 1016
                ElseIf FlagPurchase = True Then
                    StatusId = 1012
                ElseIf FlagStock = True Then
                    StatusId = 1015
                End If
                If StatusId > 0 Then
                    UpdateData(lblReqID.Text, StatusId)
                End If
                send_mail_PO(lblReqID.Text, 1)
            End If
        Next
        Response.Redirect("frmAssetThanks.aspx?RID=" + lblReqID.Text)
    End Sub

    Public Sub send_mail_PO(ByVal ReqId As String, ByVal MODE As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_APPROVE")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MODE", MODE, DbType.Int32)
        sp.Execute()
    End Sub

    'Public Sub send_mail_PO(ByVal ReqId As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_Reject")
    '    sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
    '    sp.Execute()
    'End Sub


    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dim ds As DataSet
        Dim lblReqID As Label
        Dim FlagStock As Boolean = False
        Dim FlagPurchase As Boolean = False
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblReqID = DirectCast(row.FindControl("lblReqID"), Label)
            If chkSelect.Checked Then
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
                sp1.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
                ds = sp1.GetDataSet
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim lblProductId As String = dr("AST_MD_CODE")
                    Dim txtQty As String = dr("AID_QTY")
                    Dim txtStockQty As String = dr("AID_MVM_QTY")
                    Dim txtPurchaseQty As String = dr("AID_ORD_QTY")
                    If CInt(Trim(txtPurchaseQty)) > 0 Then
                        FlagPurchase = True
                    End If
                    If CInt(Trim(txtStockQty)) > 0 Then
                        FlagStock = True
                    End If
                    UpdateAssetRequisitionData(lblReqID.Text, Trim(lblProductId), CInt(Trim(txtStockQty)), CInt(Trim(txtPurchaseQty)))
                Next
                Dim StatusId As Integer = 0
                If FlagPurchase = True And FlagStock = True Then
                    StatusId = 1016
                ElseIf FlagPurchase = True Then
                    StatusId = 1012
                ElseIf FlagStock = True Then
                    StatusId = 1013
                End If
                If StatusId > 0 Then
                    UpdateData(lblReqID.Text, StatusId)
                End If
                send_mail_PO(lblReqID.Text, 2)
            End If
        Next
        Response.Redirect("frmAssetThanks.aspx?RID=" + lblReqID.Text)
    End Sub

End Class
