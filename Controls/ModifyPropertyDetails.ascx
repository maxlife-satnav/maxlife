<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ModifyPropertyDetails.ascx.vb"
    Inherits="Controls_frmModifyPropertyDetails" %>

<script type="text/javascript">
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
    function CheckDate() {
        var dtFrom = document.getElementById("txtInsuranceStartdate").Value;
        var dtTo = document.getElementById("txtInsuranceEnddate").Value;
        if (dtFrom < dtTo) {
            alert("Invalid Dates");
        }
    }
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" Visible="True" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Property Type<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rvpropertyType" runat="server" ControlToValidate="ddlPropertyType"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Type"
                    InitialValue="--Select Property Type--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlPropertyType" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select City <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="--Select City--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Location <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvddlLocation" runat="server" ControlToValidate="ddlLocation"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Property Code <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpropcode" runat="server" ControlToValidate="txtpropcode"
                    Display="None" ErrorMessage="Please Enter Property Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revpropcode" runat="server" ControlToValidate="txtpropcode"
                    Display="None" ErrorMessage="Invalid Property Code" ValidationExpression="^[A-Za-z0-9//\s/-]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtpropcode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Property Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="revPropIDName" runat="server" ControlToValidate="txtPropIDName"
                    Display="None" ErrorMessage="Please Enter Property Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revpropertyname" runat="server" ControlToValidate="txtPropIDName"
                    Display="None" ErrorMessage="Invalid Property Name" ValidationExpression="^[A-Za-z0-9//\s/-]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtPropIDName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Maximum Capacity <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvMaxCapacity" runat="server" ControlToValidate="txtMaxCapacity"
                    Display="None" ErrorMessage="Please Enter Maximum Capacity" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revmaxcapacity" runat="server" ControlToValidate="txtMaxCapacity"
                    Display="None" ErrorMessage="Invalid Maximum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter only numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtMaxCapacity" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Optimum Capacity <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvOptimumCapacity" runat="server" ControlToValidate="txtOptCapacity"
                    Display="None" ErrorMessage="Please Enter Optimum Capacity" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revOptCapacity" runat="server" ControlToValidate="txtOptCapacity"
                    Display="None" ErrorMessage="Invalid Optimum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter only numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtOptCapacity" runat="Server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Carpet Area <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfCarpetarea" runat="server" ControlToValidate="txtCarpetArea"
                    Display="None" ErrorMessage="Please Enter Carpet area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revCarpetarea" runat="server" ControlToValidate="txtOptCapacity"
                    Display="None" ErrorMessage="Invalid Carpet Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter only numbers and decimals')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtCarpetArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Built up Area <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvBArea" runat="server" ControlToValidate="txtBuiltupArea"
                    Display="None" ErrorMessage="Please Enter Bulitup Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revBuiltupArea" runat="server" ControlToValidate="txtBuiltupArea"
                    Display="None" ErrorMessage="Invalid Builtup Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter only numbers and decimals')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtBuiltupArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Common Area <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvCommonarea" runat="server" ControlToValidate="txtCommonArea"
                    Display="None" ErrorMessage="Please Enter Common Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revCommon" runat="server" ControlToValidate="txtCommonArea"
                    Display="None" ErrorMessage="Invalid Common Area" ValidationExpression="^[0-9]*\.?[0-9]*$"
                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter only numbers and decimals')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtCommonArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Rentable Area <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvRentable" runat="server" ControlToValidate="txtRentableArea"
                    Display="None" ErrorMessage="Please Enter Rentable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRentableArea"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Rentable Area"
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter only numbers and decimals')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtRentableArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Usable Area <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvUsable" runat="server" ControlToValidate="txtUsableArea"
                    Display="None" ErrorMessage="Please Enter Usable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revUsable" runat="server" ControlToValidate="txtUsableArea"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Usable Area" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter only numbers and decimals')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtUsableArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">UOM CODE<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfUOM_CODE" runat="server" ControlToValidate="txtUOM_CODE"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter UOM CODE" ToolTip="Enter only numbers"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtUOM_CODE" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Status <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvStatus" runat="server" ControlToValidate="ddlStatus"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Status"
                    InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem>--Select Status--</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Government Property Code<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfGovt" runat="server" ControlToValidate="txtGovtPropCode"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Government Property Code"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revgovt" runat="server" ControlToValidate="txtGovtPropCode"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Government Property Code"
                    ValidationExpression="^[A-Za-z0-9//(/)\s/-]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtGovtPropCode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Owner Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvownname" runat="server" ControlToValidate="txtownrname"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Owner Name"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtownrname" runat="SERVER" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Phone Number<span style="color: red;">*</span></label>
                <asp:RegularExpressionValidator ID="REVPHNO" runat="server" ControlToValidate="txtphno"
                    ErrorMessage="Please enter valid Phone Number" ValidationGroup="Val1" Display="None"
                    ValidationExpression="^[0-9\-\(\)\, ]+$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtphno" runat="server" CssClass="form-control" MaxLength="15">0</asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Email<span style="color: red;">*</span></label>
                <asp:RegularExpressionValidator ID="revEmail2" runat="server" ControlToValidate="txtemail"
                    ValidationGroup="Val1" ErrorMessage="Please enter valid Email" Display="None"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Property Address<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtPropDesc"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Property Address"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPropDesc" runat="server" CssClass="form-control"
                        ToolTip="No Special characters allowed except / - , )(" Rows="3" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" runat="server" id="t1">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Date of Purchase Agreement<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtpdate"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Date of Purchase Agreement"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <%-- <asp:TextBox ID="txtpdate" runat="server" CssClass="form-control"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtpdate"
                        Format="dd-MMM-yyyy">
                    </cc1:CalendarExtender>--%>
                    <div class='input-group date' id='effdate'>
                        <asp:TextBox ID="txtpdate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Total Purchase Price :<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpprice"
                    ValidationGroup="Val1" Display="None" ErrorMessage=" Please Enter Total Purchase Price"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revPropdesc" runat="server" ControlToValidate="txtpprice"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid  Total Purchase Price"
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtpprice" runat="server" CssClass="form-control" MaxLength="12">0</asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" runat="server" id="t2">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Basic Purchase Price:<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtbprice"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Basic Purchase Price"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtbprice"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid  Basic Purchase Price"
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtbprice" runat="server" CssClass="form-control">0</asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Stamp duty :<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtsduty"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Stamp duty"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtsduty"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid  Stamp duty" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtsduty" runat="server" CssClass="form-control" Width="97%" MaxLength="12">0</asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" runat="server" id="t3">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Registration charges:<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtregcharges"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Registration charges"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtregcharges"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid  Registration charges"
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtregcharges" runat="server" CssClass="form-control">0</asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">VAT Deposit :<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtvat"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter VAT Deposit"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtvat"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid  VAT Deposit" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtvat" runat="server" CssClass="form-control" MaxLength="3">0</asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" runat="server" id="t4">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Service Tax:<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtstax"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Service Tax"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtstax"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid  Service Tax" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtstax" runat="server" CssClass="form-control">0</asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Professional Fees :<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtpfees"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Professional Fees"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtpfees"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Professional Fees"
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtpfees" runat="server" CssClass="form-control" MaxLength="12">0</asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" runat="server" id="t5">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Consultancy / Brokerage:<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtbrokerage"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Brokerage Fees"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtbrokerage"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid     Brokerage Fees"
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtbrokerage" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Leasehold Improvements :<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtimp"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Leasehold Improvements"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtimp" runat="server" Rows="3" CssClass="form-control" TextMode="Multiline"
                        MaxLength="500"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" runat="server" id="t6">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Furniture & Fixtures:<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtfurniture"
                    ValidationGroup="Val1" Display="None" ErrorMessage=" Please Enter Furniture & Fixtures"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtfurniture" runat="server" CssClass="form-control" Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Office Equipments :<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtofcequip"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Office Equipments"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtofcequip" runat="server" CssClass="form-control" Rows="3" TextMode="Multiline" MaxLength="250"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" runat="server" id="Tr1">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Latitude:<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtlat"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Latitude"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtlat" ValidationGroup="Val1"
                    Display="None" ErrorMessage="Invalid Latitude" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtlat" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Longitude :<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtlong"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Longitude"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtlong" ValidationGroup="Val1"
                    Display="None" ErrorMessage="Invalid Longitude" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtlong" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div>
        <div class="panel-heading">
            <h3 class="panel-title">Insurance Details</h3>
        </div>
        <div>&nbsp;</div>
        <div id="Div1" class="row" runat="server">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Type</label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlInsuranceType" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Vendor</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtInsuranceVendor" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="Div2" class="row" runat="server">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Amount</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtInsuranceAmt" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Policy Number</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtInsurancePolNum" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Start Date</label>
                        <div class="col-md-7">
                            <div class='input-group date' id='fromdate'>
                                <asp:TextBox ID="txtInsuranceStartdate" runat="server" CssClass="form-control"> </asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">End Date</label>
                        <div class="col-md-7">
                            <div class='input-group date' id='todate'>
                                <asp:TextBox ID="txtInsuranceEnddate" runat="server" CssClass="form-control"> </asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="row">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true"
                ValidationGroup="Val1" />
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" CausesValidation="false" />
        </div>
    </div>
</div>





