Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AssetTagNewRecord
    Inherits System.Web.UI.UserControl
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim AAT_AST_CODE As String = ddlCode.SelectedItem.Value
        Dim AAT_EMP_ID As String = ddlEmpID_AATTAG.SelectedItem.Value
        Dim AAT_TAG_STATUS As Integer = 0
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AAT_TAG_NewRecord")
            sp1.Command.AddParameter("@AAT_AST_CODE", AAT_AST_CODE, DbType.String)
            sp1.Command.AddParameter("@AAT_EMP_ID", AAT_EMP_ID, DbType.String)
            sp1.Command.AddParameter("@AAT_TAG_STATUS", AAT_TAG_STATUS, DbType.Int32)
            sp1.ExecuteScalar()
            'lblMsg.Visible = True
            lblMsg.Text = "Data Tagged Succesfully"
            lblMsg.Visible = True
            ClearData()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try



    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblMsg.Visible = False
            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_VendorValues")
            sp4.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlVendorCode.DataSource = sp4.GetDataSet()
            ddlVendorCode.DataTextField = "AVR_NAME"
            ddlVendorCode.DataValueField = "AVR_CODE"
            ddlVendorCode.DataBind()
            ddlVendorCode.Items.Insert(0, New ListItem("--Select--", "0"))

            Dim sp7 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_EmployeeTag")
            sp7.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlEmpID_AATTAG.DataSource = sp7.GetDataSet()
            ddlEmpID_AATTAG.DataTextField = "AUR_FIRST_NAME"
            ddlEmpID_AATTAG.DataValueField = "AUR_ID"
            ddlEmpID_AATTAG.DataBind()
            ddlEmpID_AATTAG.Items.Insert(0, New ListItem("--Select--", "0"))
        End If
    End Sub
    Private Sub ClearData()

        ddlVendorCode.SelectedIndex = 0

        ddlGrpCode.Items.Clear()
        ddlGrpCode.Items.Insert(0, New ListItem("--Select--", "0"))
        ddlGrpCode.SelectedIndex = 0

        ddlBrandCode.Items.Clear()
        ddlBrandCode.Items.Insert(0, New ListItem("--Select--", "0"))
        ddlBrandCode.SelectedIndex = 0

        ddlCode.Items.Clear()
        ddlCode.Items.Insert(0, New ListItem("--Select--", "0"))
        ddlCode.SelectedIndex = 0

        ddlEmpID_AATTAG.SelectedIndex = 0
    End Sub
    Protected Sub ddlGrpCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrpCode.SelectedIndexChanged
        If ddlGrpCode.SelectedIndex > 0 Then
            Dim SP5 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_BRAND_GROUP")
            SP5.Command.AddParameter("@AAT_AAG_CODE", ddlGrpCode.SelectedItem.Value, DbType.String)
            SP5.Command.AddParameter("@AAT_AVR_CODE", ddlVendorCode.SelectedItem.Value, DbType.String)
            ddlBrandCode.DataSource = SP5.GetDataSet()
            ddlBrandCode.DataTextField = "AAB_NAME"
            ddlBrandCode.DataValueField = "AAB_CODE"
            ddlBrandCode.DataBind()
            ddlBrandCode.Items.Insert(0, New ListItem("--Select--", "0"))
        Else
            ddlBrandCode.Items.Clear()
            ddlBrandCode.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBrandCode.SelectedIndex = 0

            ddlCode.Items.Clear()
            ddlCode.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlCode.SelectedIndex = 0
        End If
       
    End Sub

    Protected Sub ddlVendorCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendorCode.SelectedIndexChanged
        If ddlVendorCode.SelectedIndex > 0 Then
            Dim SP3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_GRP_VENDOR")
            SP3.Command.AddParameter("@AAT_AVR_CODE", ddlVendorCode.SelectedItem.Value, DbType.String)
            ddlGrpCode.DataSource = SP3.GetDataSet()
            ddlGrpCode.DataTextField = "AAG_NAME"
            ddlGrpCode.DataValueField = "AAG_CODE"
            ddlGrpCode.DataBind()
            ddlGrpCode.Items.Insert(0, New ListItem("--Select--", "0"))
        Else
            ddlGrpCode.Items.Clear()
            ddlGrpCode.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlGrpCode.SelectedIndex = 0

            ddlBrandCode.Items.Clear()
            ddlBrandCode.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBrandCode.SelectedIndex = 0

            ddlCode.Items.Clear()
            ddlCode.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlCode.SelectedIndex = 0

        End If

    End Sub
    Protected Sub ddlBrandCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrandCode.SelectedIndexChanged
        If ddlBrandCode.SelectedIndex > 0 Then
            Dim sp8 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_Code_Asset")
            sp8.Command.AddParameter("@AAT_AAB_CODE", ddlBrandCode.SelectedItem.Value, DbType.String)
            sp8.Command.AddParameter("@AAT_AAG_CODE", ddlGrpCode.SelectedItem.Value, DbType.String)
            sp8.Command.AddParameter("@AAT_AVR_CODE", ddlVendorCode.SelectedItem.Value, DbType.String)
            ddlCode.DataSource = sp8.GetDataSet()
            ddlCode.DataTextField = "AAT_NAME"
            ddlCode.DataValueField = "AAT_CODE"
            ddlCode.DataBind()
            ddlCode.Items.Insert(0, New ListItem("--Select--", "0"))
        Else
            ddlCode.Items.Clear()
            ddlCode.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlCode.SelectedIndex = 0
        End If
        
    End Sub
End Class
