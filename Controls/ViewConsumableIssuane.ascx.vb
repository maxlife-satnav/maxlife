﻿
Partial Class Controls_ConsumableIssuane
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fillgrid()
        End If
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_FOR_ISSUANCE")
        sp.Command.AddParameter("@Cuser", Session("Uid"), Data.DbType.String)
        gvItemStock.DataSource = sp.GetDataSet()
        gvItemStock.DataBind()
    End Sub

    Protected Sub gvItemStock_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItemStock.PageIndexChanging
        gvItemStock.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub


End Class
