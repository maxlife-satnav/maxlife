<%@ Control Language="VB" AutoEventWireup="false" CodeFile="WorkRequestReport.ascx.vb"
    Inherits="Controls_WorkRequestReport" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>


<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <fieldset>
        <legend>Property Total Requests
        </legend>
    </fieldset>
        <asp:GridView ID="gvitems" runat="server" AllowPaging="true" EmptyDataText="No Property Request Found."
            AutoGenerateColumns="false" AllowSorting="true" PagerSettings-Position="Top" CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblreq" runat="server" Text='<%#EVAL("PN_LOC_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Property Code">
                    <ItemTemplate>
                        <asp:Label ID="lblbdgid" runat="server" Text='<%#EVAL("PN_LOC_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Requests">
                    <ItemTemplate>
                        <asp:Label ID="lbltotreqs" runat="server" Text='<%#EVAL("TOTAL") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Property Name">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkstatus" runat="server" Text='<%#Eval("PN_NAME")%>'
                            CommandName="Update"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<div id="panel2" runat="server">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <fieldset>
        <legend>Property Request Details
        </legend>
    </fieldset>
            <asp:GridView ID="gvitems1" runat="server" AllowPaging="true" AllowSorting="true"
                AutoGenerateColumns="false" EmptyDataText="No Property Details Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblreq1" runat="server" Text='<%#Bind("PN_WORKREQUEST_REQ") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Request ID">
                        <ItemTemplate>
                            <a href="#" onclick="showPopWin('<%#Eval("PN_WORKREQUEST_REQ")%>')"><%#Eval("PN_WORKREQUEST_REQ")%></a>
                            <%--<a href="#" onclick="showPopWin('frmWorkRequestDetails.aspx?REQID=<%#EVAL("PN_WORKREQUEST_REQ")%>',550,480,null)">--%>
                            <%--<asp:Label ID="lbntreqid" runat="server" Text='<%#Bind("PN_WORKREQUEST_REQ")%>'></asp:Label></a>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Raised By">
                        <ItemTemplate>
                            <asp:Label ID="lblraisedby" runat="server" Text='<%#EVAL("REQUEST_RAISED_BY") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblraisedby" runat="server" Text='<%#Eval("STS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Requested Date">
                        <ItemTemplate>
                            <asp:Label ID="lblraisedby" runat="server" Text='<%#Eval("REQUESTED_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Last Updated Date">
                        <ItemTemplate>
                            <asp:Label ID="lblraisedby" runat="server" Text='<%#Eval("LAST_UPDATED_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">            
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"></asp:Button>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="modelcontainer">
                <%-- Content loads here --%>
            </div>
        </div>
    </div>
</div>
<script>
    function showPopWin(id) {

        //$("#modalcontentframe").attr("src", "frmViewPopupPropertyDetails.aspx?id=" + id);
        //$("#myModal").modal().fadeIn();
        $("#modelcontainer").load("frmWorkRequestDetails.aspx?REQID=" + id, function (responseTxt, statusTxt, xhr) {
            $("#myModal").modal().fadeIn();
        });
    }
</script>
