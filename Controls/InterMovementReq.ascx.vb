Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_InterMovementReq
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim AAS_SNO, AAS_AAT_CODE, AAS_BDG_ID, AAS_FLR_ID, LCM_CODE, LCM_NAME, AAT_EMP_ID As String

    Dim Twr_code, Flr_code As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindGrid()
            End If
            tab1.Visible = False
            tabinter.Visible = False
            tabdetails.Visible = False
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ASSET_GET_INTERREQUISITIONS")
            'sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvgriditems.DataSource = sp.GetDataSet()
            gvgriditems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
     

    'Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@AST_AAT_CODE", SqlDbType.NVarChar, 200)
    '    param(0).Value = Trim(txtSearch.Text)

    '    Dim dr As SqlDataReader
    '    dr = ObjSubsonic.GetSubSonicDataReader("GET_AST_DETAILS", param)
    '    If dr.Read Then
    '        lblastcode.Text = dr.Item("AAS_AAT_CODE")
    '        AAS_BDG_ID = dr.Item("STWR_NAME")
    '        AAS_FLR_ID = dr.Item("SFLR_NAME")
    '        LCM_CODE = dr.Item("LCM_CODE")
    '        LCM_NAME = dr.Item("LCM_NAME")
    '        Twr_code = dr.Item("AAS_BDG_ID")
    '        Flr_code = dr.Item("AAS_FLR_ID")
    '        lblLocation.Text = LCM_NAME
    '        lblTower.Text = AAS_BDG_ID
    '        lblFloor.Text = AAS_FLR_ID

    '        lblLCM_CODE.Text = LCM_CODE
    '        lblTwr_code.Text = Twr_code
    '        lblFLR_CODE.Text = Flr_code
    '        pnlSearch.Visible = True
    '        BindEmp()
    '    End If
    '    If dr.IsClosed = False Then
    '        dr.Close()
    '    End If

    'End Sub

    Private Sub BindEmp()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AST_AAT_CODE", DbType.String)
        param(0).Value = Trim(lblastcode.Text)
        ObjSubsonic.Binddropdown(ddlEmp, "GET_EMPLOYES", "AUR_FIRST_NAME", "AUR_ID", param)

        Dim param1(0) As SqlParameter
        param1(0) = New SqlParameter("@AST_AAT_CODE", DbType.String)
        param1(0).Value = Trim(lblastcode.Text)
        Dim DR As SqlDataReader

        Dim EmpId As Integer = 0

        DR = ObjSubsonic.GetSubSonicDataReader("GET_TAGGED_EMPLOYEE", param1)
        If DR.Read Then
            EmpId = DR.Item("AAT_EMP_ID")
        End If
        If DR.IsClosed = False Then
            DR.Close()
        End If

        If EmpId <> 0 Then
            ddlEmp.ClearSelection()
            ddlEmp.Items.FindByValue(EmpId).Selected = True
            lblFEmp.Text = EmpId
        End If
    End Sub

    

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim strASSET_LIST As New ArrayList

        Dim Location As String = ""
        Dim Tower As String = ""
        Dim Floor As String = ""
        Dim DLocation As String = ""
        Dim DTower As String = ""
        Dim DFloor As String = ""
        Dim PersonName As String = ddlEmp.SelectedItem.Value
        Dim Remarks As String = Trim(txtRemarks.Text)
        Dim ReqId As String = ""

        If ddlEmp.SelectedIndex > 0 Then

        Else
            lblMsg.Text = "Please select a Employee."
            Exit Sub
        End If

        ReqId = GenerateRequestId(lblLCM_CODE.Text, lblTwr_code.Text, lblFLR_CODE.Text, ddlEmp.SelectedItem.Value)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_INTRA_MVMT_REQ_Insert")
        sp.Command.AddParameter("@MMR_REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MMR_AST_CODE", lblastcode.Text, DbType.String)
        sp.Command.AddParameter("@MMR_FROMBDG_ID", lblTwr_code.Text, DbType.String)
        sp.Command.AddParameter("@MMR_FROMFLR_ID", lblFLR_CODE.Text, DbType.String)
        sp.Command.AddParameter("@MMR_RAISEDBY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@MMR_RECVD_BY", ddlEmp.SelectedItem.Value.ToString, DbType.String)
        sp.Command.AddParameter("@MMR_MVMT_TYPE", "", DbType.String)
        sp.Command.AddParameter("@MMR_MVMT_PURPOSE", "", DbType.String)
        sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
        sp.Command.AddParameter("@MMR_COMMENTS", Remarks, DbType.String)
        sp.Command.AddParameter("@MMR_FROMEMP_ID", Val(lblFEmp.Text), DbType.Int32)
        sp.Command.AddParameter("@MMR_ITEM_REQUISITION", txtstore.Text, DbType.String)

        sp.ExecuteScalar()



        Dim i As Integer = 0

        strASSET_LIST.Insert(i, lblastcode.Text & "," & lblTwr_code.Text & "," & lblFLR_CODE.Text & "," & ddlEmp.SelectedItem.Text & "," & txtRemarks.Text & "," & Val(lblFEmp.Text))

        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetInterMovementRequisition_employee"))
        'getRequestDetails(ReqId, strASSET_LIST, MailTemplateId, False)

        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetInterMovementRequisition_it_appoval_reject"))
        'getRequestDetails(ReqId, strASSET_LIST, MailTemplateId, True)

        Response.Redirect("frmAssetThanks.aspx?RID=intermovement")

    End Sub


    Private Sub getRequestDetails(ByVal ReqId As String, ByVal strAst As ArrayList, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        'Dim ReqId As String = GetRequestId()
        'Dim strASSET_LIST As New ArrayList
        'Dim i As Integer = 0
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
        '    Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
        '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
        '    If chkSelect.Checked Then
        '        strASSET_LIST.Insert(i, lblProductId.Text & "," & lblProductname.Text & "," & txtQty.Text)
        '    End If
        '    i += 1
        'Next
        SendMail(ReqId, strAst, Session("uid"), txtRemarks.Text, MailStatus, App_Rej_status)
    End Sub


    Private Sub SendMail(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal strRemarks As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        'lblMsg.Text = st
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet
            Dim strRMAur_id As String
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = ObjSubsonic.GetSubSonicDataSet("GET_ASSET_INTER_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strRMAur_id = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_ID")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If


            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty

            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = ObjSubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If

            'strAstList = "<table width='200' border='1'><tr><td><strong>Asset Name </strong></td><td><strong>Quantity</strong></td></tr>"
            'For i As Integer = 0 To AstList.Count - 1
            '    Dim p1
            '    p1 = AstList.Item(i).Split(",")

            '    strAstList = strAstList & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            'Next
            'strAstList = strAstList & "</table> "


            Dim p1
            p1 = AstList.Item(0).ToString.Split(",")
            strAstList = "<table width='100%' border='1'>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Asset Code</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblLocation.Text & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblTwr_code.Text & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblFLR_CODE.Text & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Person To Receive Assets</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlEmp.SelectedItem.Text & "</td></tr>"


            Dim strApproveList As String = String.Empty
            Dim strRejectList As String = String.Empty

            Dim AppLink As String = String.Empty
            AppLink = ConfigurationManager.AppSettings("AppLink").ToString


            strApproveList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=ap&pg=interit> Click to Approve </a></td></tr></table>"

            strRejectList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=rj&pg=interit>  Click to Reject </a></td></tr></table>"

            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strRemarks)
            body = body.Replace("@@RM", strKnownas)
            body = body.Replace("@@Approve", strApproveList)
            body = body.Replace("@@Reject", strRejectList)



            '1. Request Approved copy goes to request raised person.


            '2. One copy for IT ADMIN to approve or reject.

            If App_Rej_status = False Then
                Insert_AmtMail(body, strRR, strSubject, strRM)
            Else
                Insert_AmtMail(body, strRM, strSubject, "")
            End If

        Catch ex As Exception

            Throw (ex)
        End Try


    End Sub

    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "InterMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = getoffsetdatetime(DateTime.Now)
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub


    Private Function GenerateRequestId(ByVal LocId As String, ByVal TowerId As String, ByVal FloorId As String, ByVal EmpId As String) As String
        Dim ReqId As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_INTRA_MVMT_REQ_GetMaxMMR_ID")
        Dim SNO As String = CStr(sp.ExecuteScalar())

        ReqId = "MMR/" + LocId + "/" + TowerId + "/" + FloorId + "/" + EmpId + "/" + SNO
        Return ReqId
    End Function

    'Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
    '    If ddlSLoc.SelectedIndex > 0 Then
    '        Dim LocCode As String = ddlSLoc.SelectedItem.Value
    '        BindTowersByLocation(LocCode, ddlSTower)
    '    End If
    'End Sub

    'Protected Sub ddlSTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTower.SelectedIndexChanged
    '    If ddlSTower.SelectedIndex > 0 Then
    '        Dim TwrCode As String = ddlSTower.SelectedItem.Value
    '        BindFloorsByTower(TwrCode, ddlSFloor)
    '    End If
    'End Sub

    Protected Sub gvgriditems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvgriditems.PageIndexChanging
        gvgriditems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvgriditems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvgriditems.RowCommand

        If e.CommandName = "ViewDetails" Then
            tab1.Visible = True
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblasset As Label = DirectCast(gvgriditems.Rows(rowIndex).FindControl("lblasset"), Label)
            txtstore.Text = lblasset.Text
            BindGrid1()
            tabinter.Visible = False
            tabdetails.Visible = True

            BindCategories()

            BindRequisition()

            
        End If
    End Sub
    Private Sub BindRequisition()
        Dim ReqId As String = txtstore.Text
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetByReqId")
            sp.Command.AddParameter("@ReqId", txtstore.Text, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                Dim RaisedBy As Integer = 0
                Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                ddlEmp1.Items.Insert(0, New ListItem(CStr(RaisedBy), "0"))
               

                Dim CatId As Integer = 0
                Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)

                Dim li As ListItem
                li = ddlAstCat.Items.FindByValue(CStr(CatId))
                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = False

                txtRemarks1.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")

                txtRemarks1.ReadOnly = True
                txtRMRemarks.ReadOnly = True
                txtAdminRemarks.ReadOnly = True
                txtStatus.ReadOnly = True

                If String.IsNullOrEmpty(Trim(txtRemarks.Text)) Then
                    trRemarks.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                    trRMRemarks.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                    trAdminRemarks.Visible = False
                End If

                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                'If RaisedBy = GetCurrentUser() Then
                '    If StatusId = 1001 Or StatusId = 1002 Then
                '        btnSubmit.Enabled = True
                '        btnCancel.Enabled = True
                '    Else
                '        btnSubmit.Enabled = False
                '        btnCancel.Enabled = False
                '    End If
                'Else
                '    btnSubmit.Enabled = False
                '    btnCancel.Enabled = False
                'End If

                
            Else
                lblMsg.Text = "No such requisition found."
            End If
        End If
    End Sub
    Private Sub BindCategories()
        GetChildRows("0")
        'ddlAstCat.DataSource = CategoryController.CategoryList
        'ddlAstCat.DataTextField = "CategoryName"
        'ddlAstCat.DataValueField = "CategoryID"
        'ddlAstCat.DataBind()
        'For Each li As ListItem In ddlAstCat.Items

        'Next
        ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub GetChildRows(ByVal i As String)
        Dim str As String = ""
        Dim id
        If i = "0" Then
            id = CType(i, Integer)
        Else
            Dim id1 As Array = Split(i, "~")
            str = id1(0).ToString & "  --"
            id = CType(id1(1), Integer)
        End If


        Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
        Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id, objConn)
        Dim ds As New DataSet
        da.Fill(ds)



        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim j As Integer = CType(dr("CategoryId"), Integer)
                If id = 0 Then
                    str = ""
                End If
                Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
                ddlAstCat.Items.Add(li)
                GetChildRows(str & "~" & j)
            Next
        End If
    End Sub
    
    Private Sub BindGrid1()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ASSET_GET_ASSETS_INTER_ITEMCODE")
            sp.Command.AddParameter("@REQ_ID", txtstore.Text, DbType.String)
            gvassets.DataSource = sp.GetDataSet()
            gvassets.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindFromDetails()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AST_AAT_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = Trim(txtstore1.Text)

        Dim dr As SqlDataReader
        dr = ObjSubsonic.GetSubSonicDataReader("GET_AST_DETAILS", param)
        If dr.Read Then


            lblastcode.Text = dr.Item("AAS_AAT_CODE")
            AAS_BDG_ID = dr.Item("STWR_NAME")
            AAS_FLR_ID = dr.Item("SFLR_NAME")
            LCM_CODE = dr.Item("LCM_CODE")
            LCM_NAME = dr.Item("LCM_NAME")
            Twr_code = dr.Item("AAS_BDG_ID")
            Flr_code = dr.Item("AAS_FLR_ID")
            lblLocation.Text = LCM_NAME
            lblTower.Text = AAS_BDG_ID
            lblFloor.Text = AAS_FLR_ID

            lblLCM_CODE.Text = LCM_CODE
            lblTwr_code.Text = Twr_code
            lblFLR_CODE.Text = Flr_code
            tabinter.Visible = True
            BindEmp()
        End If
       
    End Sub

    Protected Sub gvassets_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvassets.PageIndexChanging
        gvassets.PageIndex = e.NewPageIndex()
        BindGrid1()
    End Sub

    Protected Sub gvassets_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvassets.RowCommand
        If e.CommandName = "ViewDetails" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lbtnaas_aat_code As Label = DirectCast(gvassets.Rows(rowIndex).FindControl("lbtnaas_aat_code"), Label)
            txtstore1.Text = lbtnaas_aat_code.Text
            tabinter.Visible = True
            BindEmp()
            BindFromDetails()
           
        End If
    End Sub
End Class
