Imports System.Data
Imports System.Data.SqlClient
Partial Class Controls_ItemRMViewReqDetails
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindCategories()
            dispdata()
        End If
    End Sub
    Private Sub BindUsers(ByVal AUR_ID As String)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = aur_id
        ObjSubsonic.Binddropdown(ddlEmp, "AMT_BINDUSERS_SP", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(aur_id)
        If Not li Is Nothing Then
            li.Selected = True
        End If
        'Dim sp As New SubSonic.StoredProcedure("AMT_GetUserDtls_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        'ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindCategories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")

    End Sub

    Private Sub dispdata()
        Dim ReqId As String = Request("RID")
        'Dim sp As New SubSonic.StoredProcedure("AST_GET_VIEW_REQDETAILS")
        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_FOR_ASSETGRIDVR_NP")
        SP.Command.AddParameter("@Req_id", Request.QueryString("RID"), DbType.String)
        SP.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim dr As SqlDataReader = SP.GetReader()
        If dr.Read() Then
            lblReqId.Text = ReqId

            BindUsers(dr("AIR_AUR_ID"))

            'Dim CatId As Integer = 0
            'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)
            Dim CatId As String

            CatId = dr("AIR_ITEM_TYPE")
            Dim li As ListItem = ddlAstCat.Items.FindByValue(CStr(CatId))

            If Not li Is Nothing Then
                li.Selected = True
            End If
            ddlAstCat.Enabled = False


            getassetsubcategory(CatId)
            ddlAstSubCat.Enabled = False
            Dim asstsubcat As String = dr("AIR_ITEM_SUBCAT")

            ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

            getbrandbycatsubcat(CatId, asstsubcat)

            Dim asstbrand As String = dr("AIR_ITEM_BRD")
            ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
            ddlAstBrand.Enabled = False

            getmakebycatsubcat()
            ddlAstModel.Items.FindByValue(dr("AIR_ITEM_MOD")).Selected = True
            ddlAstModel.Enabled = False
            lblRem.Text = dr("AIR_REMARKS")
            BindLocation()

            ddlLocation.Items.FindByValue(dr("AIR_REQ_LOC")).Selected = True
            ddlLocation.Enabled = False
            'Dim ds As New DataSet
            'ds = SP.GetDataSet()
            'Dim req_status As Integer = 0

            'If ds.Tables(0).Rows.Count > 0 Then
            '    lblReqId.Text = ds.Tables(0).Rows(0).Item("AIR_REQ_TS")
            '    lblLoc.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
            '    'lblTwr.Text = ds.Tables(0).Rows(0).Item("TWR_NAME")
            '    'lblFlr.Text = ds.Tables(0).Rows(0).Item("FLR_NAME")
            '    lblRem.Text = ds.Tables(0).Rows(0).Item("AIR_REMARKS")
            '    'lblLocCode.Text = ds.Tables(0).Rows(0).Item("LCM_CODE")
            '    'lblTwrCode.Text = ds.Tables(0).Rows(0).Item("TWR_CODE")
            '    'lblFlrCode.Text = ds.Tables(0).Rows(0).Item("FLR_CODE")
            '    req_status = ds.Tables(0).Rows(0).Item("AIR_STA_ID")
            'End If
            'If req_status = 1001 Or req_status = 1002 Then
            '    btnModify.Visible = True
            '    btnCancel.Enabled = True
            'Else
            '    btnModify.Enabled = False
            '    btnCancel.Enabled = False
            '    txtRem.Enabled = False
            'End If
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        'ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_REQUISITION", param)
        ObjSubsonic.BindGridView(gvItems, "AST_GET_CONSUMABLES_FOR_ASSETGRIDVR", param)
        'Dim sp1 As New SubSonic.StoredProcedure("GET_ITEM_REQUEST")
        'sp1.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        'Dim ds1 As New DataSet
        'ds1 = sp1.GetDataSet()
        'gvItemReceivedHistory.DataSource = ds1
        'gvItemReceivedHistory.DataBind()

        'If gvItemReceivedHistory.Rows.Count = 0 Then
        '    btnExpExcel.Visible = False
        'Else
        '    btnExpExcel.Visible = True
        'End If
    End Sub

    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        'ddlAstSubCat.Items.Insert(0, "--Select--")
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    'Private Sub getmakebycatsubcat()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
    '    sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
    '    ddlModel.DataSource = sp.GetDataSet()
    '    ddlModel.DataTextField = "AST_MD_NAME"
    '    ddlModel.DataValueField = "AST_MD_CODE"
    '    ddlModel.DataBind()
    '    ddlModel.Items.Insert(0, "--Select--")

    'End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmItemRMViewReq.aspx")
    End Sub


    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        ''Changing the status to approve after approving the raised request
        'Dim sp As New SubSonic.StoredProcedure("AST_ITEM_UPDATE_STATUS_RM")
        'sp.Command.AddParameter("@id", lblTemp.Text, DbType.String)
        'sp.Command.AddParameter("@RMRem", txtRMRem.Text, DbType.String)
        'sp.ExecuteScalar()
        'Response.Redirect("frmAssetThanks.aspx?RID=itemreqrmapproved&reqid=" + lblTemp.Text)
        Validate(Request.QueryString("RID"))
    End Sub

    Private Sub Validate(ByVal ReqId As String)


        Dim count As Integer = 0
        Dim Message As String = String.Empty

        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)


            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtQty.Text) Then
                    lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                    Exit Sub
                ElseIf IsNumeric(txtQty.Text) = True Then
                    count = count + 1
                    If count = 0 Then
                        lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
                        Exit Sub
                    End If
                Else
                    lblMsg.Text = "Please enter Quantity in Numerics Only"
                    Exit Sub
                End If
            End If
        Next
        If count > 0 Then
            UpdateData(ReqId, Trim(txtRM.Text))
            For Each row As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
                Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
                Dim lblMinOrdQty As Label = DirectCast(row.FindControl("lblMinOrdQty"), Label)
                Dim lblqty As Label = DirectCast(row.FindControl("lblqty"), Label)
                If chkSelect.Checked Then
                    If String.IsNullOrEmpty(txtQty.Text) Then
                        lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                        Exit Sub
                    ElseIf IsNumeric(txtQty.Text) = True Then
                        If CInt(lblMinOrdQty.Text) <= CInt(txtQty.Text) Then
                            If lblqty.Text >= txtQty.Text Then
                                count = count + 1
                                If count > 0 Then

                                    InsertDetails(ReqId, lblProductid.Text, CInt(Trim(txtQty.Text)))
                                End If
                            Else
                                lblMsg.Text = "Maximum quantity to approve is only " & lblqty.Text
                                Exit Sub
                            End If

                        Else
                            lblMsg.Text = "Minimum Qty. should be  " + lblMinOrdQty.Text
                            Exit Sub
                        End If

                    End If

                End If
            Next
            send_mail(ReqId)
            Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString("RID"))
        ElseIf count = 0 Then
            lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
        Else
            lblMsg.Text = "Please enter Quantity for Selected Checkbox"
        End If
    End Sub
    Public Sub send_mail(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_ADMIN_APPROVAL")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Execute()
    End Sub
    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UpdateByReqIdRM")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@StatusId", 1504, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_updatereq1")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.ExecuteScalar()

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UpdateByReqIdRMCANCEL")
        sp.Command.AddParameter("@ReqId", Request.QueryString("RID"), DbType.String)
        sp.Command.AddParameter("@Remarks", lblRem.Text, DbType.String)
        sp.Command.AddParameter("@StatusId", 1505, DbType.Int32)

        sp.ExecuteScalar()
        send_mail_reject(Request.QueryString("RID"))
        Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString("RID"))
    End Sub
    Public Sub send_mail_reject(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_ADMIN_REJECT")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Execute()
    End Sub
    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        'ddlAstModel.Items.Insert(0, "--All--")
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub

    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")

        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ' ddlLocation.Items.Insert(0, New ListItem("--All--", ""))
        ddlLocation.Items.Remove("--Select--")
        'ddlLocation.Items.Remove("--All--")
        '  ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)
    End Sub

End Class
