<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucAssetLocationReport.ascx.vb"
    Inherits="Controls_ucAssetLocationReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<%--<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Location</label>

                <div class="col-md-7">
                    <asp:DropDownList ID="ddllocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>--%>
<%--<div class="row table-responsive">
    <div class="col-md-12">
        <asp:GridView ID="gvAssetLoc" runat="server" AllowSorting="true" AllowPaging="true" Width="100%" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="Asset_Count" SortExpression="Asset Count">
                    <ItemTemplate>
                        <a href="#" onclick="showPopWin('frmMapped_unMapped_Summary.aspx?Prd_id=<%#Eval("[Asset_ID]")%>~<%#Eval("[Location_Code]")%>',700,400,null)">
                            <%#Eval("[Asset_Count]")%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Asset Name" SortExpression="Asset_Name" DataField="Asset_Name" />
                <asp:BoundField HeaderText="Location" SortExpression="Location" DataField="Location" />
            </Columns>
        </asp:GridView>

    </div>
</div>--%>
<%--<div class="row">
    <div class="form-group">
    </div>
</div>--%>
<div class="row" style="padding-left:30px">
    
     <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Location</label>


            <asp:DropDownList ID="ddllocation" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Model">
            </asp:DropDownList>
        </div>
    </div>
  
  
     <div class="col-md-1 col-sm-12 col-xs-12"></div>


    <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">
            <%-- <div class="row">--%>
            <label>Asset Category</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Category" AutoPostBack="True">
            </asp:DropDownList>
            <%--</div>--%>
        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>

    <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">
            <%--     <div class="row">--%>
            <label>Asset Sub Category</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlAstSubCat"
                Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--">
            </asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Sub Category" AutoPostBack="True">
            </asp:DropDownList>
            <%--</div>--%>
        </div>
    </div>
         <div class="col-md-1 col-sm-12 col-xs-12"></div>

        <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">
            <%--       <div class="row">--%>
            <label>Asset Brand/Make</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlAstBrand"
                Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Brand/Make" AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
            </asp:DropDownList>
            <%-- </div>--%>
        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
     
</div>
<div class="row" style="padding-left:30px">
       <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>&nbsp;Asset Model</label>

            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlModel"
                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>


            <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Model"
               >
            </asp:DropDownList>



        </div>
    </div>

     <div class="col-md-1 col-sm-12 col-xs-12"></div>
 <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

          <label>Duration</label>
            <br />

        
                                        <select id="ddlRange"  class="selectpicker" onhange="getDate(this)">
                                            <option value="">Select Range</option>
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    


        </div>
    </div><div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>From Date</label>

            <%--<asp:RequiredFieldValidator ID="rfvHYDate" runat="server" ControlToValidate="FromDate"
                ErrorMessage="Please From Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>

            <div class='input-group date' id='Div1'>
                <asp:TextBox ID="FromDate" runat="server" CssClass="form-control"  placeholder="mm/dd/yyyy" MaxLength="10" > </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                </span>
            </div>


        </div>
    </div> <div class="col-md-1 col-sm-12 col-xs-12"></div>
      <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

         <label>To Date</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ToDate"
                ErrorMessage="Please Enter To Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>

            <div class='input-group date' id='Div4'>
                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div4')"></span>
                </span>
            </div>


        </div>
    </div>
        <div class="col-md-1 col-sm-12 col-xs-12 text-right" style="padding-left:30px">
            <div class="form-group"><br />
                <asp:Button ID="btnSubmit" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                <%--<asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary custom-button-color" />--%>
            </div>
        </div>
</div>

<div class="row table table table-condensed table-responsive">
    <div class="col-md-12">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
    </div>
</div>




