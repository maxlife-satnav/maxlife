<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApprovePayment.ascx.vb"
    Inherits="Controls_ApprovePayment" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Work Request <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvProperty" runat="server" ControlToValidate="ddlWorkRequest"
                    Display="none" ErrorMessage="Please Select Work Request" ValidationGroup="Val1"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlWorkRequest" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Work Status
                </label>
                <asp:RequiredFieldValidator ID="cvworkstatus" runat="server" ControlToValidate="ddlwstatus"
                    Display="none" ErrorMessage="Please Select Work status" ValidationGroup="Val1"
                    InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlwstatus" runat="server" CssClass="selectpicker" data-live-search="true"
                        Enabled="False">
                        <asp:ListItem>--Select Status--</asp:ListItem>
                        <asp:ListItem Value="0">Pending</asp:ListItem>
                        <asp:ListItem Value="1">InProgress</asp:ListItem>
                        <asp:ListItem Value="2">Completed</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Title
                </label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control"
                        MaxLength="50" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Specifications
                </label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control"
                        Rows="3" TextMode="MultiLine" MaxLength="250" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Estimated Amount
                </label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Start Date
                </label>
                <div class="col-md-7">
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
                <%--<asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control"
                        Enabled="False"></asp:TextBox>--%>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Expected End Date
                </label>
                <div class="col-md-7">
                    <div class='input-group date' id='todate'>
                        <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
                <%--<asp:TextBox ID="txtExpiryDate" runat="server" CssClass="form-control" 
                        Enabled="False"></asp:TextBox>--%>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Paid Amount
                </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtoamount"
                    Display="none" ErrorMessage="Please Enter Paid Amount" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="none" runat="server"
                    ControlToValidate="txtoamount" ValidationGroup="Val1" ErrorMessage="Invalid Amount"
                    ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtpamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Outstanding Amount 
                </label>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtoamount"
                    Display="none" ValidationGroup="Val1" ErrorMessage="Please Enter Outstanding Amount"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rev2" Display="none" runat="server" ControlToValidate="txtoamount"
                    ErrorMessage="Invalid Amount" ValidationGroup="Val1" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtoamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Item Status
                </label>
                <asp:RequiredFieldValidator ID="cvstatus" runat="server" ControlToValidate="ddlstatus"
                    Display="none" ErrorMessage="Please Select Item status" ValidationGroup="Val1"
                    InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"
                        Enabled="False">
                        <asp:ListItem>--Select Status--</asp:ListItem>
                        <asp:ListItem Value="0">Pending</asp:ListItem>
                        <asp:ListItem Value="1">InProgress</asp:ListItem>
                        <asp:ListItem Value="2">Completed</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Condition<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                    Display="none" ValidationGroup="Val1" ErrorMessage="Please Enter Work Condition"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                        Rows="3" MaxLength="250"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnApprove" CssClass="btn btn-primary custom-button-color" runat="server" ValidationGroup="Val1" Text="Approve" />

            <asp:Button ID="btnReject" CssClass="btn btn-primary custom-button-color" runat="server" ValidationGroup="Val1" Text="Reject" />

        </div>
    </div>
</div>
