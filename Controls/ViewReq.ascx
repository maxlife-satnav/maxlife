<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewReq.ascx.vb" Inherits="Controls_ViewReq" %>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
            EmptyDataText="No View Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>

                <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                  <ItemTemplate>
                        <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmAssetRequisitionDetails.aspx?RID={0}") %>'
                            Text='<%#Eval("AIR_REQ_ID")%>' ></asp:HyperLink>
                        <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                
                <asp:BoundField DataField="AUR_NAME" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
               
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>


