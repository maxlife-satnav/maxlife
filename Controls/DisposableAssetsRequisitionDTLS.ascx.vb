Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_DisposableAssetsRequisitionDTLS
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim Req_id As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Req_id = Request.QueryString("Req_id")
            GetDetailsByRequistion(Req_id)
        End If
    End Sub

    Private Sub GetDetailsByRequistion(ByVal Req_id As String)
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Req_id
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ALLAST_DISPOSABLEREQ_BYREQ", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblAstName.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
            lblModelName.Text = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
            lblLocation.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
            lblAstAllocDt.Text = ds.Tables(0).Rows(0).Item("AAT_ALLOCATED_DATE")
            lblAstSurDt.Text = ds.Tables(0).Rows(0).Item("AAT_SURRENDERED_DATE")
            lblSurReq_id.Text = Req_id
            lblAstDate.Text = ds.Tables(0).Rows(0).Item("AAT_UPT_DT")
            lblAdminDate.Text = ds.Tables(0).Rows(0).Item("SREQ_ADMINAPPROVAL_DATE")
            lblAdminName.Text = ds.Tables(0).Rows(0).Item("ADMIN_NAME")
            lblAdminRemarks.Text = ds.Tables(0).Rows(0).Item("SREQ_ADMIN_REMARKS")
            lblAstSalvage.Text = FormatNumber(ds.Tables(0).Rows(0).Item("AST_SALAVAGE_VALUE"), 2)
        End If
    End Sub

    Protected Sub btnApprov_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprov.Click
        Dim REQ = Request.QueryString("REQ_ID")
        ' ------- Asset Dispose -----------------
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = REQ
        param(1) = New SqlParameter("@RM_AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("UID")
        param(2) = New SqlParameter("@RM_REMARKS", SqlDbType.NVarChar, 2000)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
        param(3).Value = 1041
        param(4) = New SqlParameter("@AST_SALAVAGE_VALUE", SqlDbType.NVarChar, 2000)
        param(4).Value = CDbl(lblAstSalvage.Text)
        param(5) = New SqlParameter("@AST_DISPOSE", SqlDbType.Bit)
        param(5).Value = True
        ObjSubsonic.GetSubSonicDataSet("UPDATESURRENDER_REQUISTION_byAdmin", param)
        send_mail(REQ)
        'Response.Redirect("frmDisposableAssetsRequisition.aspx")
        Response.Redirect("frmAssetThanks.aspx?RID=outwardapp&MReqId=" + Request.QueryString("REQ_ID"))

    End Sub
    Public Sub send_mail(ByVal reqid As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_DISPOSE_REQUISITION_APPROVAL")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Execute()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmDisposableAssetsRequisition.aspx")
    End Sub

End Class
