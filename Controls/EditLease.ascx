<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditLease.ascx.vb" Inherits="Controls_EditLease" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

        <table width="98%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">View Lease Details
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <asp:Panel ID="PanelGridview" runat="server" Width="90%">
            <table cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%" align="center"
                border="0">
                <tr>
                    <td style="width: 10px; height: 27px;">
                        <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left" style="height: 27px">
                        &nbsp;<strong>Lease Details</strong></td>
                    <td style="height: 27px">
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                       <br>


			<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table width="100%" cellpadding="1" cellspacing="0" align="center" border="1">
                            <tr runat="server" visible="false" >
                                <td align="left" style="height: 26px; width: 50%">
                                    Select Lease Type <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvLtype" runat="server" ControlToValidate="ddlLtype"
                                        Display="None" ErrorMessage="Please select LeaseType" ValidationGroup="Val1"
                                        InitialValue="0"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:DropDownList ID="ddlLtype" runat="Server" CssClass="clsComboBox" Width="99%"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <table id="t1" runat="server" width="80%" cellpadding="3" cellspacing="0" align="center" border="0" visible="false" >
                           
                            <tr>
                                <td align="left" style="height: 30px" width="30%">
                                    Enter Lease Name to find Lease
                                </td>
                                <td align="left" style="height: 26px; width: 30%">
                                    <asp:TextBox ID="txtfindcode" runat="server" CssClass="textBox"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvcode" runat="server" ControlToValidate="txtfindcode"
                                        ErrorMessage="Please enter LeaseType !" ValidationGroup="Val2" SetFocusOnError="True"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 10%" valign="top">
                                    <asp:Button ID="btnfincode" runat="server" CssClass="button" Text="GO" ValidationGroup="Val2" />
                                </td>
                                <td align="LEFT" style="height: 26px; width: 30%">
                                    <asp:LinkButton ID="lbtn1" runat="Server" Text="Get All Records"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="0" runat="server">
                            <tr>
                                <td align="center" style="height: 20px">
                                    <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                        AllowPaging="True" PageSize="5" EmptyDataText="You cannot Edit the Lease.">
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <Columns>
                                          
                                            <asp:TemplateField HeaderText="Lease">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllname" runat="server"  Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CTS Number">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCode" runat="server" Text='<%#Eval("CTS_NUMBER")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="city">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcity" runat="server" Text='<%#Eval("CITY")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee No">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmpNo" runat="server"  Text='<%#Eval("LESSE_ID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lesse">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLesseName" runat="server"  Text='<%#Eval("LESSE_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Start Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsdate" runat="server"  Text='<%#Eval("LEASE_START_DATE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEdate" runat="server"  Text='<%#Eval("LEASE_EXPIRY_DATE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLstatus" runat="server"  Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created By">
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbluser" runat="server"  Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <a href='frmModifyLease.aspx?id=<%#Eval("LEASE_NAME")%>'>EDIT</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                                        </Columns>
                                        <FooterStyle CssClass="GVFixedFooter" />
                                        <HeaderStyle CssClass="GVFixedHeader" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="ddlLtype" />
        <asp:PostBackTrigger ControlID="gvLDetails_Lease" />
        <asp:PostBackTrigger ControlID="btnfincode" />
        <asp:PostBackTrigger ControlID="lbtn1" />
    </Triggers>
</asp:UpdatePanel>
