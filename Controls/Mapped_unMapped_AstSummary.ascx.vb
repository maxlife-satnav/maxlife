Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class Controls_Mapped_unMapped_AstSummary
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Dim Prd_id As Integer
    Dim strLocation As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Prd_id = Request.QueryString("prd_id")
        strLocation = Request.QueryString("lcm_code")
        If Not IsPostBack Then


            BindMappedAssets(Prd_id, strLocation)
            BindUnMappedAssets(Prd_id, strLocation)
        End If
    End Sub

    Private Sub BindMappedAssets(ByVal PrdId As Integer, ByVal LCM_Code As String)

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = PrdId
        param(1) = New SqlParameter("@LCM_Code", SqlDbType.NVarChar, 200)
        param(1).Value = LCM_Code
        ObjSubSonic.BindGridView(gvItems, "GET_MAPPED_ASTSLIST", param)

    End Sub

    Private Sub BindUnMappedAssets(ByVal PrdId As Integer, ByVal LCM_Code As String)

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = PrdId
        param(1) = New SqlParameter("@LCM_Code", SqlDbType.NVarChar, 200)
        param(1).Value = LCM_Code
        ObjSubSonic.BindGridView(gvUnmappedAst, "GET_UNMAPPED_ASTSLIST", param)

    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindMappedAssets(Prd_id, strLocation)

    End Sub

    Protected Sub gvUnmappedAst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUnmappedAst.PageIndexChanging
        gvUnmappedAst.PageIndex = e.NewPageIndex
        BindUnMappedAssets(Prd_id, strLocation)
    End Sub
End Class
