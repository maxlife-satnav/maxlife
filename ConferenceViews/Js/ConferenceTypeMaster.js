﻿app.service("ConferenceTypeMasterService", function ($http, $q) {
    this.SaveConfDetails = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceTypeMaster/SaveConferenceDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.ModifyConference = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceTypeMaster/ModifyConferenceDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.GetConfGrid = function () {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceTypeMaster/GetGridData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('ConferenceTypeMasterController', function ($scope, $q, ConferenceTypeMasterService, UtilityService, $filter) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.ConferenceType = {};
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;

    $scope.Save = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            ConferenceTypeMasterService.ModifyConference($scope.ConferenceType).then(function (response) {
                var savedobj = {};
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.IsInEdit = false;
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', resposne.Message);
                $scope.LoadData();
                $scope.ConferenceType = {};
            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.ConferenceType.CONF_STA_ID = "1";
            ConferenceTypeMasterService.SaveConfDetails($scope.ConferenceType).then(function (response) {
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', response.Message);
                var savedobj = {};
                angular.copy($scope.ConferenceType, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.ConferenceType = {};
            }, function (error) {
                console.log(error);
            });
        }
    }
    var columnDefs = [
               { headerName: "Conference Code", field: "CONF_CODE", width: 190, cellClass: 'grid-align' },
               { headerName: "Conference Name", field: "CONF_NAME", width: 280, cellClass: 'grid-align' },
               { headerName: "Status", template: "{{ShowStatus(data.CONF_STA_ID)}}", width: 170, cellClass: 'grid-align' },
               { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110 }];


    $scope.LoadData = function () {
        ConferenceTypeMasterService.GetConfGrid().then(function (data) {
            $scope.gridata = data;
            $scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
            console.log(error);
        });
    }
    $scope.pageSize = '10';    $scope.createNewDatasource = function () {
        var dataSource = {
            pageSize: parseInt($scope.pageSize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($scope.gridata.length <= params.endRow) {
                        lastRow = $scope.gridata.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };        $scope.gridOptions.api.setDatasource(dataSource);
    }    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableSorting: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }

    };    $scope.LoadData();
    $scope.EditData = function (data) {
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        angular.copy(data, $scope.ConferenceType);
    }
    $scope.EraseData = function () {
        $scope.ConferenceType = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
    }

    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }
});