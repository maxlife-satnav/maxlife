﻿app.service("ConferenceRoomMasterService", function ($http, $q, UtilityService) {

    this.BindConfData = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomMaster/BindConferenceDetails')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.SaveConferenceRoomDetails = function (response) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomMaster/SaveConferenceRoomDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.ModifyConferenceRoomDetails = function (response) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomMaster/ModifyConferenceRoomDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.BindGridData = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomMaster/BindGridData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('ConferenceRoomMasterController', function ($scope, $q, $location, ConferenceRoomMasterService, UtilityService, $filter) {
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.ConfType = [];
    $scope.ConferenceRoom = {};
    $scope.ActionStatus = 0;
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.IsInEdit = false;
    $scope.LocationHierarchy = [];
    $scope.ConferenceRoom.City = [];
    $scope.ConferenceRoom.Location = [];
    $scope.ConferenceRoom.Tower = [];
    $scope.ConferenceRoom.Floor = [];
    $scope.ConferenceTypeArray = [];
    $scope.CityArray = [];
    $scope.LocationArray = [];
    $scope.TowerArray = [];
    $scope.FloorArray = [];

    UtilityService.getCities(2).then(function (response) {
        if (response.data != null) {
            $scope.City = response.data;
        }
    });

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
        }
    });

    UtilityService.getTowers(2).then(function (response) {
        if (response.data != null) {
            $scope.Tower = response.data;
        }
    });

    UtilityService.getFloors(2).then(function (response) {
        if (response.data != null) {
            $scope.Floor = response.data;
        }
    });

    $scope.CtyChangeAll = function () {
        $scope.ConferenceRoom.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.ConferenceRoom.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.ConferenceRoom.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceRoom.Country.push(cny);
            }
        });
    }

    $scope.LcmChangeAll = function () {
        $scope.ConferenceRoom.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.ConferenceRoom.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.ConferenceRoom.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceRoom.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceRoom.City.push(cty);
            }
        });
    }

    $scope.TwrChangeAll = function () {
        $scope.ConferenceRoom.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.ConferenceRoom.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.ConferenceRoom.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceRoom.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceRoom.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ConferenceRoom.Location[0].push(lcm);
            }
        });



    }

    $scope.FlrChangeAll = function () {
        $scope.ConferenceRoom.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.ConferenceRoom.Floor = [];
        $scope.FlrChanged();
    }

    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceRoom.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceRoom.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ConferenceRoom.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.ConferenceRoom.Tower.push(twr);
            }
        });

    }
    
    $scope.Clear = function () {
        $scope.ConferenceRoom = {};
        angular.forEach($scope.ConfType, function (ConfType) {
            ConfType.ticked = false;
        });

        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });

        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Location, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Tower, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floor, function (floor) {
            floor.ticked = false;
        });
        $scope.ConferenceRoom.ConfType = [];
        $scope.ConferenceRoom.Country = [];
        $scope.ConferenceRoom.City = [];
        $scope.ConferenceRoom.Location = [];
        $scope.ConferenceRoom.Tower = [];
        $scope.ConferenceRoom.Floor = [];
    }
    
    $scope.SaveData = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            var selctdVal = _.find($scope.ConfType, { ticked: true });
            $scope.ConferenceRoom.CONFERENCE_TYPE = selctdVal.CONF_CODE;
            var savedobj = { Structure: $scope.ConferenceRoom.Floor, Model: $scope.ConferenceRoom };
            ConferenceRoomMasterService.ModifyConferenceRoomDetails(savedobj).then(function (resposne) {
                var mod = {};
                angular.copy($scope.ConferenceRoom, mod);
                $scope.gridata.unshift(mod);
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.IsInEdit = false;
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', resposne.Message);
            }, function (error) {
                console.log(error);
            });
        }
        else {
            $scope.ConferenceRoom.CONFERENCE_STATUS = "1";
            var selctdVal = _.find($scope.ConfType, { ticked: true });
            $scope.ConferenceRoom.CONFERENCE_TYPE = selctdVal.CONF_CODE;
            var savedobj = { Structure: $scope.ConferenceRoom.Floor, Model: $scope.ConferenceRoom };
            ConferenceRoomMasterService.SaveConferenceRoomDetails(savedobj).then(function (resposne) {
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', resposne.Message);
                var obj = {};
                angular.copy($scope.ConferenceRoom, obj);
                $scope.gridata.unshift(obj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.ConferenceRoom = {};
            }, function (error) {
                console.log(error);
            });
        }
    }

    var columnDefs = [
               //{ headerName: "Conference Type Code", field: "CONFERENCE_TYPE", width: 320, cellClass: 'grid-align' },
               { headerName: "Conference Type", field: "CONFERENCE_TYPE_NAME", width: 320, cellClass: 'grid-align' },
               { headerName: "Conference Code", field: "CONFERENCE_ROOM_CODE", width: 280, cellClass: 'grid-align' },
               { headerName: "Conference Name", field: "CONFERENCE_ROOM_NAME", width: 280, cellClass: 'grid-align' },
               { headerName: "City", field: "CONFERENCE_CITY", width: 280, cellClass: 'grid-align' },
               { headerName: "Location", field: "CONFERENCE_LOCATION", width: 280, cellClass: 'grid-align' },
               { headerName: "Tower", field: "CONFERENCE_TOWER", width: 280, cellClass: 'grid-align' },
               { headerName: "Floor", field: "CONFERENCE_FLOOR", width: 280, cellClass: 'grid-align' },
               { headerName: "Capacity", field: "CONFERENCE_CAPACITY", width: 280, cellClass: 'grid-align', suppressMenu: true, },
               { headerName: "Status", template: "{{ShowStatus(data.CONFERENCE_STATUS)}}", width: 170, cellClass: 'grid-align', suppressMenu: true, },
               { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110, suppressMenu: true, }];

    $scope.LoadData = function () {
        ConferenceRoomMasterService.BindGridData().then(function (data) {
            $scope.gridata = data;
            $scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
            console.log(error);
        });
        
    }

    $scope.BindConferenceDropDown = function () {
        ConferenceRoomMasterService.BindConfData().then(function (response) {
            if (response != null) {
                $scope.ConfType = response;
            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.pageSize = '10';

    $scope.createNewDatasource = function () {
        var dataSource = {
            pageSize: parseInt($scope.pageSize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($scope.gridata.length <= params.endRow) {
                        lastRow = $scope.gridata.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };        $scope.gridOptions.api.setDatasource(dataSource);
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }

    };
    $scope.BindConferenceDropDown();
   
    setTimeout(function () { $scope.LoadData(); }, 1000);

    $scope.EditData = function (data) {
        console.log(data);
        $scope.ActionStatus = 1;
        $scope.ConferenceTypeArray = data.CONFERENCE_TYPE.split(',');
        $scope.CityArray = data.CONFERENCE_CITY_CODE.split(',');
        $scope.LocationArray = data.CONFERENCE_LOCATION_CODE.split(',');
        $scope.TowerArray = data.CONFERENCE_TOWER_CODE.split(',');
        $scope.FloorArray = data.CONFERENCE_FLOOR_CODE.split(',');

        _($scope.ConfType).forEach(function (value) {
            value.ticked = false;
        });

        _($scope.City).forEach(function (value) {
            value.ticked = false;
        });
        _($scope.Location).forEach(function (value) {
            value.ticked = false;
        });
        _($scope.Tower).forEach(function (value) {
            value.ticked = false;
        });
        _($scope.Floor).forEach(function (value) {
            value.ticked = false;
        });

        _($scope.ConferenceTypeArray).forEach(function (value) {
            var conf = _.find($scope.ConfType, { CONF_CODE: value });
            conf.ticked = true;
        });

        _($scope.CityArray).forEach(function (value) {
            var cty = _.find($scope.City, { CTY_CODE: value });
            cty.ticked = true;
        });

        _($scope.LocationArray).forEach(function (value) {
            var lcm = _.find($scope.Location, { LCM_CODE: value });
            lcm.ticked = true;
        });

        _($scope.FloorArray).forEach(function (value) {
            var flr = _.find($scope.Floor, { FLR_CODE: value });
            flr.ticked = true;
        });

        _($scope.TowerArray).forEach(function (value) {
            var twr = _.find($scope.Tower, { TWR_CODE: value });
            twr.ticked = true;
        });
        $scope.IsInEdit = true;
        angular.copy(data, $scope.ConferenceRoom);
    }

    $scope.EraseData = function () {
        $scope.ConferenceRoom = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
    }

    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

});