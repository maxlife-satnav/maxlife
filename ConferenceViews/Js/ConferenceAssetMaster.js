﻿app.service("ConferenceAssetMasterService", function ($http, $q) {
    this.SaveAstData = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceAssetMaster/SaveAssetDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.ModifyAstData = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceAssetMaster/ModifyAssetDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.GetAstData = function () {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceAssetMaster/GetGridData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('ConferenceAssetMasterController', function ($scope, $q, ConferenceAssetMasterService, UtilityService, $filter) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.AssetType = {};
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.Save = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            ConferenceAssetMasterService.ModifyAstData($scope.AssetType).then(function (response) {
                var savedobj = {};
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.IsInEdit = false;
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', resposne.Message);
                $scope.LoadData();
                $scope.AssetType = {};
            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.AssetType.AST_STA_ID = "1";
            ConferenceAssetMasterService.SaveAstData($scope.AssetType).then(function (response) {
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', response.Message);
                var savedobj = {};
                angular.copy($scope.AssetType, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.AssetType = {};
            }, function (error) {
                console.log(error);
            });
        }
    }
    var columnDefs = [
               { headerName: "Asset Code", field: "AST_CODE", width: 190, cellClass: 'grid-align' },
               { headerName: "Asset Name", field: "AST_NAME", width: 280, cellClass: 'grid-align' },
               { headerName: "Status", template: "{{ShowStatus(data.AST_STA_ID)}}", width: 170, cellClass: 'grid-align' },
               { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110 }];


    $scope.LoadData = function () {
        ConferenceAssetMasterService.GetAstData().then(function (data) {
            $scope.gridata = data;
            $scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
            console.log(error);
        });
    }
    $scope.pageSize = '10';    $scope.createNewDatasource = function () {
        var dataSource = {
            pageSize: parseInt($scope.pageSize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($scope.gridata.length <= params.endRow) {
                        lastRow = $scope.gridata.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };        $scope.gridOptions.api.setDatasource(dataSource);
    }    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableSorting: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }

    };    $scope.LoadData();
    $scope.EditData = function (data) {
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        angular.copy(data, $scope.AssetType);
    }
    $scope.EraseData = function () {
        $scope.AssetType = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
    }

    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    
});