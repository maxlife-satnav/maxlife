﻿app.service("FacilityMasterService", function ($http, $q, UtilityService) {

    this.SaveFacility = function (response) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/FacilityMaster/SaveFacilityDetails', response)
         .then(function (response) {
             console.log(response);
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/FacilityMaster/GetGridData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.ModifyFacility = function (response) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/FacilityMaster/ModifyFacilityDetails', response)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });

    };

});

app.controller('FacilityMasterController', function ($scope, $q, FacilityMasterService, UtilityService, $filter) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.Facility = {};
    $scope.Locations = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.LocationArray = [];
    $scope.ShowMessage = false;

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;
        }
    });

    $scope.SaveData = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            var savedobj = { LocatinoList: $scope.Facility.Locations, FMastr: $scope.Facility };
            FacilityMasterService.ModifyFacility(savedobj).then(function (resposne) {
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.IsInEdit = false;
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', resposne.Message);
            }, function (error) {
                console.log(error);
            });
        }
        else {
            $scope.Facility.FACILITY_STATUS = "1";
            var savedobj = { LocatinoList: $scope.Facility.Locations, FMastr: $scope.Facility };
            FacilityMasterService.SaveFacility(savedobj).then(function (resposne) {
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', resposne.Message);
                var savedobj = {};
                angular.copy($scope.Facility, savedobj);
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.Facility = {};
            }, function (error) {
                console.log(error);
            });
        }
    }

    var columnDefs = [
               { headerName: "Facility Code", field: "FACILITY_CODE", width: 190, cellClass: 'grid-align' },
               { headerName: "Facility Name", field: "FACILITY_NAME", width: 280, cellClass: 'grid-align' },
               { headerName: "Location", field: "LCM_NAME", width: 280, cellClass: 'grid-align' },
               { headerName: "Unit Cost", field: "FACILITY_UNIT_COST", width: 280, cellClass: 'grid-align', suppressMenu: true, },
               { headerName: "Status", template: "{{ShowStatus(data.FACILITY_STATUS)}}", width: 170, cellClass: 'grid-align', suppressMenu: true, },
               { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110, suppressMenu: true, }];

    $scope.LoadData = function () {
        FacilityMasterService.GetGriddata().then(function (data) {
            $scope.gridata = data;
            $scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.pageSize = '10';

    $scope.createNewDatasource = function () {
        var dataSource = {
            pageSize: parseInt($scope.pageSize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($scope.gridata.length <= params.endRow) {
                        lastRow = $scope.gridata.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };        $scope.gridOptions.api.setDatasource(dataSource);
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }

    };

    $scope.LoadData();

    $scope.EditData = function (data) {
        $scope.ActionStatus = 1;
        $scope.LocationArray = data.LCM_CODE.split(',');
        console.log(data);
        _($scope.Locations).forEach(function (value) {
            value.ticked = false;
        });

        _($scope.LocationArray).forEach(function (value) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value });
            lcm.ticked = true;
        });
        $scope.IsInEdit = true;
        angular.copy(data, $scope.Facility);
    }

    $scope.Clear = function () {
        angular.forEach($scope.Locations, function (Lcm) {
            Lcm.ticked = false;
        });
        $scope.Facility.Locations = [];
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

});