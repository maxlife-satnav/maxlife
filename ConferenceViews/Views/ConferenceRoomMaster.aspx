﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="ConferenceRoomMasterController" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Conference Room Master</legend>
                    </fieldset>
                    <form role="form" id="form1" name="frm" class="form-horizontal well" data-valid-submit="SaveData()" novalidate>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_TYPE.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Select Conference Type<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                             <div isteven-multi-select data-input-model="ConfType" data-output-model="ConferenceRoom.CONFERENCE_TYPE" data-button-label="icon CONF_NAME"
                                                data-item-label="icon CONF_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_TYPE.$invalid" style="color: red">Please Select Conference Type</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_ROOM_CODE.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Conference Code<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <input id="txtCName" name="CONFERENCE_ROOM_CODE" type="text" data-ng-model="ConferenceRoom.CONFERENCE_ROOM_CODE" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_ROOM_CODE.$invalid" style="color: red">Please Enter Valid Conference Code</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_ROOM_NAME.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Conference Name<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <input id="Text1" name="CONFERENCE_ROOM_NAME" type="text" data-ng-model="ConferenceRoom.CONFERENCE_ROOM_NAME" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_ROOM_NAME.$invalid" style="color: red">Please Enter Valid Conference Name</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_CAPACITY.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Conference Capacity<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Name numbers')"
                                                onmouseout="UnTip()">
                                                <input id="Text2" name="CONFERENCE_CAPACITY" type="number" min="0" data-ng-model="ConferenceRoom.CONFERENCE_CAPACITY" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_CAPACITY.$invalid" style="color: red">Please Enter Valid Conference Capacity</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_CITY.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">City<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div isteven-multi-select data-input-model="City" data-output-model="ConferenceRoom.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="ConferenceRoom.City" name="CONFERENCE_CITY" style="display: none" required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_CITY.$invalid" style="color: red">Please Select City </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_LOCATION.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Location<span style="color: red;">**</span></label>
                                        <div class="col-md-8">
                                            <div isteven-multi-select data-input-model="Location" data-output-model="ConferenceRoom.Location" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="ConferenceRoom.Location" name="CONFERENCE_LOCATION" style="display: none" required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_LOCATION.$invalid" style="color: red">Please Select Location </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_TOWER.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Tower<span style="color: red;">**</span></label>
                                        <div class="col-md-8">
                                            <div isteven-multi-select data-input-model="Tower" data-output-model="ConferenceRoom.Tower" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="ConferenceRoom.Tower" name="CONFERENCE_TOWER" style="display: none" required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_TOWER.$invalid" style="color: red">Please Select Tower </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_FLOOR.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Floor<span style="color: red;">**</span></label>
                                        <div class="col-md-8">
                                            <div isteven-multi-select data-input-model="Floor" data-output-model="ConferenceRoom.Floor" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()"
                                                data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="ConferenceRoom.Floor" name="CONFERENCE_FLOOR" style="display: none" required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_FLOOR.$invalid" style="color: red">Please Select Floor </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_STATUS.$invalid}">
                                        <label class="col-md-4 control-label">Status<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div>
                                                <select id="ddlsta" name="Status" data-ng-model="ConferenceRoom.CONFERENCE_STATUS" class="form-control">
                                                    <option value="">--Select--</option>
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_STATUS.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please Select Status</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                    <input type="button" class="btn btn-primary custom-button-color" data-ng-click="Clear()" value="Clear" />
                                    <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="height: 320px">
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 329px; width: 100%"></div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../Js/ConferenceRoomMaster.js"></script>

</body>
</html>
