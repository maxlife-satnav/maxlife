﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="ConferenceBookingRequestController" class="amantra">

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Conference Booking Request</legend>
                    </fieldset>
                    <form role="form" id="form1" name="frm" class="form-horizontal well" data-valid-submit="SaveData()" novalidate>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_CITY.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">City<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div isteven-multi-select data-input-model="City" data-output-model="BookingRequest.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="BookingRequest.City" name="CONFERENCE_CITY" style="display: none" required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_CITY.$invalid" style="color: red">Please Select City </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_LOCATION.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Location<span style="color: red;">**</span></label>
                                        <div class="col-md-8">
                                            <div isteven-multi-select data-input-model="Location" data-output-model="BookingRequest.Location" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="BookingRequest.Location" name="CONFERENCE_LOCATION" style="display: none" required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_LOCATION.$invalid" style="color: red">Please Select Location </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_TOWER.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Tower<span style="color: red;">**</span></label>
                                        <div class="col-md-8">
                                            <div isteven-multi-select data-input-model="Tower" data-output-model="BookingRequest.Tower" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="BookingRequest.Tower" name="CONFERENCE_TOWER" style="display: none" required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_TOWER.$invalid" style="color: red">Please Select Tower </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_FLOOR.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Floor<span style="color: red;">**</span></label>
                                        <div class="col-md-8">
                                            <div isteven-multi-select data-input-model="Floor" data-output-model="BookingRequest.Floor" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()"
                                                data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="BookingRequest.Floor" name="CONFERENCE_FLOOR" style="display: none" required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_FLOOR.$invalid" style="color: red">Please Select Floor </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_CAPACITY.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Capacity<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div isteven-multi-select data-input-model="Capacity" data-output-model="BookingRequest.Capacity" data-button-label="icon CONFERENCE_CAPACITY "
                                                data-item-label="icon TWR_NAME maker" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="BookingRequest.Capacity" name="CONFERENCE_CAPACITY" style="display: none" required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_CAPACITY.$invalid" style="color: red">Please Select Capacity</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Check Availability" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../Js/ConferenceBookingRequest.js"></script>
</body>
</html>
