﻿<%@ WebHandler Language="VB" Class="SearchEmployeeName" %>

Imports System
Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Script.Serialization

Public Class SearchEmployeeName : Implements IHttpHandler, IRequiresSessionState
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        
        Dim conn As SqlConnection = New SqlConnection
        Dim prefixText As String = context.Request.QueryString("prefixText")
        
        context.Response.ContentType = "application/json"
        conn.ConnectionString = ConfigurationManager _
         .ConnectionStrings("CSAmantraFAM").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandType = Data.CommandType.StoredProcedure
        cmd.CommandText = HttpContext.Current.Session("TENANT") & "." & "getsearchEmpName"
        'cmd.CommandText = "getsearchEmpName"
        cmd.Parameters.AddWithValue("@EmpName", prefixText)
        cmd.Connection = conn
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("EMP_NAME").ToString)
        End While
        Dim jsSerializer As JavaScriptSerializer = New JavaScriptSerializer()
        context.Response.Write(jsSerializer.Serialize(customers))
        conn.Close()
        
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class