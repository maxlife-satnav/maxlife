<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMASAssetCategoryMaster.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASAssetCategoryMaster"
    Title="Assets Category Master" %>

<%@ Register Src="../../../Modules/ResultMessage.ascx" TagName="ResultMessage" TagPrefix="uc2" %>
<%@ Register Src="../../../Modules/ImageManager.ascx" TagName="ImagePicker" TagPrefix="uc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Asset Category Master
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Asset Category Master</strong></td>
                <td>
                    <img alt="" height="27" src="../../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="center" valign="top" height="100%">
                    <table width="95%">
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td>
                                            Select an Asset Category:<br />
                                            <br />
                                            &nbsp;<asp:ListBox ID="lstCats" runat="server"  AutoPostBack="True"
                                                 OnSelectedIndexChanged="lstCats_SelectedIndexChanged"></asp:ListBox></td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCatNew" runat="server" Width="182px"></asp:TextBox></td>
                                        <td>
                                            <asp:Button ID="btnAdd" runat="server" ToolTip="Add" Text="Add" CssClass="button" /></td>
                                    </tr>
                                </table>
                                <br />
                            </td>
                            <td>
                                <table class="admintable" cellpadding="5" cellspacing="0">
                                    <tr>
                                        <td class="adminlabel">
                                            ID</td>
                                        <td class="adminitem" align="left">
                                            <asp:Label ID="lblID" runat="server" Text=""></asp:Label>
                                            <uc2:ResultMessage ID="ResultMessage1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="adminlabel">
                                            Parent Asset Category</td>
                                        <td class="adminitem" align="left">
                                            <asp:DropDownList ID="ddlParentID" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td class="adminlabel">
                                            Asset Category Name</td>
                                        <td class="adminitem" align="left">
                                            <asp:TextBox ID="txtCategoryName" runat="server" Width="353px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="trImage" runat="server">
                                        <td class="adminlabel">
                                            Image</td>
                                        <td class="adminitem" align="left">
                                            &nbsp;<uc1:ImagePicker ID="ImagePicker1" runat="server" ImageFolder="images/" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="adminlabel">
                                            Short Description</td>
                                        <td class="adminitem" align="left">
                                            <asp:TextBox ID="txtShortDescription" runat="server" TextMode="MultiLine" Height="75px"
                                                Width="400px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="adminlabel">
                                            Long Description</td>
                                        <td class="adminitem" align="left">
                                            <asp:TextBox ID="txtLongDescription" runat="server" MaxLength="2500" Height="75px"
                                                TextMode="MultiLine" Width="400px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="adminlabel">
                                            List Order</td>
                                        <td class="adminitem" align="left">
                                            <ew:NumericBox ID="txtListOrder" runat="server" MaxLength="3" Width="24px"></ew:NumericBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 32px">
                                        </td>
                                        <td align="right" style="height: 32px">
                                       <asp:Button ID="btnDelete" runat="server"
                                                    CausesValidation="False" Text="Delete" ToolTip="Delete" OnClick="btnDelete_Click"
                                                    CssClass="button"></asp:Button>
                                                      &nbsp;&nbsp;
                                                       <asp:Button ID="btnSave" runat="server" ToolTip="Save" Text="Save" OnClick="btnSave_Click"
                                                CssClass="button"></asp:Button>
                                                    </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img height="17" src="../../../Images/table_left_bot_corner.gif" width="9" alt="" /></td>
                <td style="height: 17px" background="../../../Images/table_bot_mid_bg.gif">
                    <img height="17" src="../../../Images/table_bot_mid_bg.gif" width="25" alt="" /></td>
                <td style="height: 17px; width: 17px;">
                    <img height="17" src="../../../Images/table_right_bot_corner.gif" width="16" alt="" /></td>
            </tr>
        </table>
    </div>

    <script type="text/ecmascript">
	function CheckDelete(){

		return confirm("Delete this record? This action cannot be undone...");

	}

    </script>

</asp:Content>
