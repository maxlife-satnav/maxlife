<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmAssetsList.aspx.vb" Inherits="FAM_Masters_Mas_WebFiles_frmAssetsList"
    Title="Assets List" %>

<%@ Register Src="../../../Modules/ImageManager.ascx" TagName="ImagePicker" TagPrefix="uc2" %>
<%@ Register Src="../../../Modules/ResultMessage.ascx" TagName="ResultMessage" TagPrefix="uc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Asset Master
            </asp:Label> <hr align="center" width="60%" />
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Asset Master</strong></td>
                <td>
                    <img alt="" height="27" src="../../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="center" valign="top" height="100%">
                    <asp:GridView id="dg" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PageSize="50" Width="803px" EmptyDataText="No Asset(s) Found" >
                        <columns>

			<asp:BoundField DataField="ProductID" SortExpression="ProductID" HeaderText="ProductID"  Visible="False"></asp:BoundField>
			<asp:TemplateField HeaderText="Product">

				<ItemTemplate>
					<a href="frmAssetDetails.aspx?id=<%#Eval("productID")%>"><%#Eval("ProductName")%></a>
				</ItemTemplate>

			</asp:TemplateField>
			<asp:BoundField DataField="SKU" SortExpression="SKU" HeaderText="SKU" ></asp:BoundField>

			<asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-CssClass="rightAlign"></asp:BoundField>
			<asp:BoundField DataField="ProductType" HeaderText="Product Type" ItemStyle-CssClass="rightAlign"></asp:BoundField>
			<asp:BoundField DataField="OurPrice" SortExpression="OurPrice" HeaderText="Salvage Value" DataFormatString="Rs. {0:N2}" HtmlEncode="false" ItemStyle-CssClass="rightAlign"></asp:BoundField>
			<asp:BoundField DataField="RetailPrice" SortExpression="RetailPrice" HeaderText="Retail Price" DataFormatString="Rs. {0:N2}" HtmlEncode="false" ItemStyle-CssClass="rightAlign"></asp:BoundField>
			<asp:BoundField DataField="ListOrder" SortExpression="ListOrder" HeaderText="List Order" ItemStyle-CssClass="rightAlign"></asp:BoundField>
		</columns>
                        <pagerstyle horizontalalign="Right" forecolor="Black" backcolor="#C6C3C6"></pagerstyle>
                    </asp:GridView>
                    <br />
                  <%--  <input type="button"  onclick="location.href='frmMASAssetMaster.aspx'" value="Add"
                        id="Button1" class="button" />
                    <br />--%>
                     <asp:Button ID="Button1" runat="server" Text="Add" CssClass="button"  />
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAll"
                        TypeName="ProductController"></asp:ObjectDataSource>
                    <br />
                </td>
                <td background="../../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img height="17" src="../../../Images/table_left_bot_corner.gif" width="9" alt="" /></td>
                <td style="height: 17px" background="../../../Images/table_bot_mid_bg.gif">
                    <img height="17" src="../../../Images/table_bot_mid_bg.gif" width="25" alt="" /></td>
                <td style="height: 17px; width: 17px;">
                    <img height="17" src="../../../Images/table_right_bot_corner.gif" width="16" alt="" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
