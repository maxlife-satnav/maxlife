Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports System.Collections.Generic

Partial Class Masters_Mas_Webfiles_frmMASAssetCategoryMaster
    Inherits System.Web.UI.Page
    Private catList As CategoryCollection = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim delFlag As String = String.Empty
        If Not Request.QueryString("did") Is Nothing Then
            delFlag = Request.QueryString("did").ToString()
        End If

        If delFlag <> String.Empty Then
            Dim delID As Integer = Integer.Parse(delFlag)
            Response.Redirect("admin_folders.aspx", False)
        End If

        If (Not Page.IsPostBack) Then
            LoadCatBox()
            ddlParentID.DataSource = catList
            ddlParentID.DataTextField = "CategoryName"
            ddlParentID.DataValueField = "categoryID"
            ddlParentID.DataBind()
            Dim item As ListItem = New ListItem("<Top Level>", "0")
            ddlParentID.Items.Insert(0, item)

            lstCats.SelectedIndex = 0
            If lstCats.Items.Count > 0 Then
                LoadCategory(Integer.Parse(lstCats.SelectedValue))
            End If
            trImage.Visible = False
        End If

    End Sub

    Private Sub LoadCatBox()

        'get all of em
        catList = New CategoryCollection().Load()

        lstCats.Items.Clear()

        'figure out the levels
        'load the top ones first
        For Each thisCat As Category In catList
            Dim item As ListItem = New ListItem()
            item.Text = thisCat.CategoryName
            item.Value = thisCat.CategoryID.ToString()
            lstCats.Items.Add(item)
        Next thisCat



    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Dim cat As Category = New Category()
        cat.CategoryName = txtCatNew.Text
        cat.ListOrder = 99
        cat.ParentID = 0
        cat.Save(Utility.GetUserName())
        'cat.Save(Session("UID"))
        LoadCatBox()

        'reload the master list
        CategoryController.Load()

    End Sub


    Private Sub LoadCategory(ByVal categoryID As Integer)
        btnDelete.Attributes.Add("onclick", "return CheckDelete();")
        Dim cat As Category = New Category(categoryID)
        txtCategoryName.Text = cat.CategoryName
        txtLongDescription.Text = cat.LongDescription
        txtShortDescription.Text = cat.ShortDescription
        txtListOrder.Text = cat.ListOrder.ToString()
        lblID.Text = cat.CategoryID.ToString()
        ddlParentID.SelectedValue = cat.ParentID.ToString()

        'this is a hack - Atlas doesn't respect the ViewState for some reason
        ImagePicker1.ImageFolder = "images/"

        ImagePicker1.SetImage(cat.ImageFile)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim delCatID As Integer = Integer.Parse(lblID.Text)
        'see if this category has children

        'get all the categories
        Dim coll As CategoryCollection = New CategoryCollection().Load()

        'find the one to be deleted
        Dim hasKids As Boolean = False
        For Each cat As Category In coll
            If cat.ParentID = delCatID Then
                hasKids = True
                Exit For
            End If
        Next cat

        If (Not hasKids) Then
            Category.Delete(delCatID)
            LoadCatBox()
            If lstCats.Items.Count > 0 Then
                lstCats.SelectedIndex = 0
                LoadCategory(Integer.Parse(lstCats.SelectedValue))

                'Reload the master category list
                CategoryController.Load()
            End If
        Else
            ResultMessage1.ShowFail("You must delete all child categories first, or reassign them by dragging and dropping to another parent category")
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim parentID As Integer = 0
        parentID = Integer.Parse(ddlParentID.SelectedValue)
        Dim cat As Category = New Category(Integer.Parse(lblID.Text))
        cat.CategoryName = txtCategoryName.Text
        cat.ImageFile = "" 'ImagePicker1.GetSelectedImage()
        'reset the image file to use virtual root reference
        'Dim imageFile As String = System.IO.Path.GetFileName(cat.ImageFile)
        'cat.ImageFile = "~/images/" & imageFile

        cat.ListOrder = Integer.Parse(txtListOrder.Text)
        cat.ShortDescription = txtShortDescription.Text
        cat.LongDescription = txtLongDescription.Text
        cat.ParentID = parentID
        'cat.Save(Utility.GetUserName())
        cat.Save(Session("UID"))
        LoadCategory(cat.CategoryID)
        LoadCatBox()

        'Reload the master category list
        CategoryController.Load()

        'Response.Redirect("frmMASAssetCategoryMaster.aspx")

    End Sub
    Protected Sub lstCats_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        LoadCategory(Integer.Parse(lstCats.SelectedValue))
    End Sub
End Class
