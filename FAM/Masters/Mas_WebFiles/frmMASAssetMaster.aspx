<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMASAssetMaster.aspx.vb" Inherits="FAM_Masters_Mas_WebFiles_frmMASAssetMaster"
    Title="Assets Masters" %>

<%@ Register Src="../../../Modules/Admin/ProductCrossSells.ascx" TagName="ProductCrossSells"
    TagPrefix="uc6" %>
<%@ Register Src="../../../Modules/Admin/ProductAttributes.ascx" TagName="ProductAttributes"
    TagPrefix="uc5" %>
<%@ Register Src="../../../Modules/Admin/ProductCategories.ascx" TagName="ProductCategories"
    TagPrefix="uc4" %>
<%@ Register Src="../../../Modules/Admin/ProductImages.ascx" TagName="ProductImages"
    TagPrefix="uc3" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register Src="../../../Modules/ImageManager.ascx" TagName="ImagePicker" TagPrefix="uc2" %>
<%@ Register Src="../../../Modules/ResultMessage.ascx" TagName="ResultMessage" TagPrefix="uc1" %>
<%--<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
--%>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div id="divMain">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left">
                        <a href="frmAssetsList.aspx" class="clsHead">Assets</a> >>> Add an Asset
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Add an Asset
             <hr align="center" width="60%" /></asp:Label>
                        &nbsp;
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="../../../images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Add an Asset</strong></td>
                    <td>
                        <img alt="" height="27" src="../../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="center" valign="top" height="100%">
                        <table class="admintable">
                            <tr>
                                <td colspan="2" bgcolor="whitesmoke">
                                    <h4>
                                        <asp:Label ID="lblProductName" runat="server"></asp:Label></h4>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <uc1:ResultMessage ID="ResultMessage1" runat="server" />
                                    <asp:ValidationSummary runat="server" ID="vsResultMessage" DisplayMode="List" ShowSummary="true"
                                        HeaderText="Page Errors:" CssClass="adminitem" />
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" align="left">
                                    Asset Code</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:TextBox ID="txtSku" runat="server" MaxLength="50" Width="500px"></asp:TextBox>
                                    <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="adminlabel" valign="top" align="left">
                                    Asset Name
                                    </td>
                                <td class="adminitem" style="width: 465px" valign="top" align="left">
                                    <asp:TextBox ID="txtProductName" runat="server" Width="500px" MaxLength="150"></asp:TextBox><asp:RequiredFieldValidator
                                        runat="server" ID="rfvProductName" ControlToValidate="txtProductName" ErrorMessage="Asset Name is required."
                                        Text="*" />
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" align="left">
                                    Asset Description</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:TextBox ID="txtShortDescription" runat="server" Width="500px" MaxLength="250" TextMode="multiline"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" align="left">
                                    Salvage Value</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    Rs.<asp:TextBox ID="txtOurPrice" runat="server" Width="71px">0</asp:TextBox><asp:RequiredFieldValidator
                                        ID="rfvOurPrice" runat="server" ControlToValidate="txtOurPrice" ErrorMessage="Salvage Value is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" align="left">
                                    Asset Price</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    Rs.<asp:TextBox ID="txtRetailPrice" runat="server" Width="71px">0</asp:TextBox><asp:RequiredFieldValidator
                                        ID="rfvRetailPrice" runat="server" ControlToValidate="txtRetailPrice" ErrorMessage="Asset Price is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr id="trManufacture" runat="server" visible="true">
                                <td class="adminlabel" align="left" style="height: 26px">
                                    Brand</td>
                                <td class="adminitem" style="width: 465px; height: 26px;" align="left">
                                    <div>
                                        <asp:DropDownList ID="ddlManufacturerID" runat="server">
                                        </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="rfvManufacturerID"
                                            ControlToValidate="ddlManufacturerID" ErrorMessage="Manufacturer is required."
                                            Text="*" />
                                        <asp:TextBox ID="txtQuickMan" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnQuickMan" runat="server" OnClick="btnQuickMan_Click" Text="Quick Add"
                                            CausesValidation="false" CssClass="button" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" align="left">
                                    Status</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:DropDownList ID="ddlStatusID" runat="server">
                                    </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="rfvStatusID" ControlToValidate="ddlStatusID"
                                        ErrorMessage="Status is required." Text="*" />
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" align="left">
                                    Asset Type</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:DropDownList ID="ddlProductTypeID" runat="server">
                                    </asp:DropDownList><asp:RequiredFieldValidator ID="rfvProductTypeID" runat="server"
                                        ControlToValidate="ddlProductTypeID" ErrorMessage="Asset Type is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr id="trShipType" runat="server" visible="false">
                                <td class="adminlabel" align="left">
                                    Shipping Type</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:DropDownList ID="ddlShippingTypeID" runat="server">
                                    </asp:DropDownList><asp:RequiredFieldValidator ID="rfvShippingTypeID" runat="server"
                                        ControlToValidate="ddlShippingTypeID" ErrorMessage="Shipping Type is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr id="trShipEstimate" runat="server" visible="false">
                                <td class="adminlabel" align="left">
                                    Ship Estimate</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:DropDownList ID="ddlShipEstimateID" runat="server">
                                    </asp:DropDownList><asp:RequiredFieldValidator ID="rfvShipEstimateID" runat="server"
                                        ControlToValidate="ddlShipEstimateID" ErrorMessage="Ship Estimate is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" align="left">
                                    Tax Type</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:DropDownList ID="ddlTaxTypeID" runat="server">
                                    </asp:DropDownList><asp:RequiredFieldValidator ID="rfvTaxTypeID" runat="server" ControlToValidate="ddlTaxTypeID"
                                        ErrorMessage="Tax Type is required." Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" style="height: 118px" align="left">
                                    Stock Location</td>
                                <td class="adminitem" style="height: 118px; width: 465px;" align="left">
                                    <asp:TextBox ID="txtStockLocation" runat="server" Height="100px" TextMode="MultiLine"
                                        Width="400px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="trWeight" runat="server" visible="false">
                                <td class="adminlabel" align="left">
                                    Weight</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <ew:NumericBox ID="txtWeight" runat="server" Width="41px" DecimalPlaces="2">0</ew:NumericBox><asp:RequiredFieldValidator
                                        ID="rfvWeight" runat="server" ControlToValidate="txtWeight" ErrorMessage="Weight is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr id="trCurrency" runat="server" visible="false">
                                <td class="adminlabel" align="left">
                                    Currency Code</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:DropDownList ID="ddlCurrencyCodeID" runat="server" /><asp:RequiredFieldValidator
                                        ID="rfvCurrencyCodeID" runat="server" ControlToValidate="ddlCurrencyCodeID" ErrorMessage="Currency Code is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr id="trUnit" runat="server" visible="false">
                                <td class="adminlabel" align="left">
                                    Unit Of Measure</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:TextBox ID="txtUnitOfMeasure" runat="server" Text="single item" MaxLength="5"></asp:TextBox><br />
                                    (inches, cm, m, ft, yds)
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" style="height: 29px" align="left">
                                    Admin Comments</td>
                                <td class="adminitem" style="height: 29px; width: 465px;" align="left">
                                    <asp:TextBox ID="txtAdminComments" runat="server" TextMode="MultiLine" Height="47px"
                                        Width="400px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="trDimension" runat="server" visible="false">
                                <td class="adminlabel" style="height: 26px" align="left">
                                    Dimensions</td>
                                <td class="adminitem" style="width: 465px; height: 26px;" align="left">
                                    L
                                    <ew:NumericBox ID="txtLength" runat="server" Width="24px">0</ew:NumericBox><asp:RequiredFieldValidator
                                        ID="rfvLength" runat="server" ControlToValidate="txtLength" ErrorMessage="Dimensions:Length is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                    H
                                    <ew:NumericBox ID="txtHeight" runat="server" Width="24px">0</ew:NumericBox><asp:RequiredFieldValidator
                                        ID="rfvHeight" runat="server" ControlToValidate="txtHeight" ErrorMessage="Dimensions:Height is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                    W
                                    <ew:NumericBox ID="txtWidth" runat="server" Width="24px">0</ew:NumericBox><asp:RequiredFieldValidator
                                        ID="rfvWidth" runat="server" ControlToValidate="txtWidth" ErrorMessage="Dimensions:Width is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr id="trDimensionUnit" runat="server" visible="false">
                                <td class="adminlabel" style="height: 26px" align="left">
                                    Dimension Unit</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <asp:TextBox ID="txtDimensionUnit" runat="server" Text="inches"></asp:TextBox><asp:RequiredFieldValidator
                                        ID="rfvDimensionUnit" runat="server" ControlToValidate="txtDimensionUnit" ErrorMessage="Dimension Unit is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="adminlabel" style="height: 26px" align="left">
                                    List Order</td>
                                <td class="adminitem" style="width: 465px" align="left">
                                    <ew:NumericBox ID="txtListOrder" runat="server" Width="24px">99</ew:NumericBox><asp:RequiredFieldValidator
                                        ID="rfvListOrder" runat="server" ControlToValidate="txtListOrder" ErrorMessage="List Order is required."
                                        Text="*">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CausesValidation="true"
                                        CssClass="button"></asp:Button>
                                 <%--   <input type="button" onclick="location.href='frmAssetsList.aspx'" value="Return"
                                        class="button" />--%>
                              <%-- <asp:Button ID="Button1" runat="server" Text="Return" CssClass="button"></asp:Button>&nbsp;--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="../../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../../Images/table_left_bot_corner.gif" width="9" alt="" /></td>
                    <td style="height: 17px" background="../../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../../Images/table_bot_mid_bg.gif" width="25" alt="" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img height="17" src="../../../Images/table_right_bot_corner.gif" width="16" alt="" /></td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
