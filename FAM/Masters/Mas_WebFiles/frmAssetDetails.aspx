<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmAssetDetails.aspx.vb" Inherits="FAM_Masters_Mas_WebFiles_frmAssetDetails"
    Title="Asset Details" %>

<%@ Register Src="../../../Modules/Admin/ProductDescriptors.ascx" TagName="ProductDescriptors"
    TagPrefix="uc7" %>
<%@ Register Src="../../../Modules/Admin/ProductCrossSells.ascx" TagName="ProductCrossSells"
    TagPrefix="uc6" %>
<%@ Register Src="../../../Modules/Admin/ProductAttributes.ascx" TagName="ProductAttributes"
    TagPrefix="uc5" %>
<%@ Register Src="../../../Modules/Admin/ProductCategories.ascx" TagName="ProductCategories"
    TagPrefix="uc4" %>
<%@ Register Src="../../../Modules/Admin/ProductImages.ascx" TagName="ProductImages"
    TagPrefix="uc3" %>
<%@ Register Src="../../../Modules/ImageManager.ascx" TagName="ImagePicker" TagPrefix="uc2" %>
<%@ Register Src="../../../Modules/ResultMessage.ascx" TagName="ResultMessage" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <a href="frmAssetsList.aspx" class="clsHead">Assets</a> >>> Asset Details >>>
                    <asp:Label ID="lblProductName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Asset Details
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Asset Details</strong></td>
                <td>
                    <img alt="" height="27" src="../../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="center" valign="top" height="100%">
                    <uc1:ResultMessage ID="ResultMessage1" runat="server" />
                    <table>
                        <tr>
                            <td>
                                <table cellpadding="4" cellspacing="1">
                                    <tr>
                                        <td class="tableNavLink">
                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="lnkMain_Click" CausesValidation="false">Main</asp:LinkButton></td>
                                        <td class="tableNavLink">
                                            <asp:LinkButton ID="LinkButton3" runat="server" OnClick="lnkDesc_Click" CausesValidation="false">Descriptors</asp:LinkButton></td>
                                        <td class="tableNavLink">
                                            <asp:LinkButton ID="LinkButton4" runat="server" OnClick="lnkCat_Click" CausesValidation="false">Categories</asp:LinkButton></td>
                                        <td class="tableNavLink" id="tdAttributes" visible="false" runat="server">
                                            <asp:LinkButton ID="LinkButton5" runat="server" OnClick="lnkAtt_Click" CausesValidation="false">Attributes</asp:LinkButton></td>
                                        <td class="tableNavLink">
                                            <asp:LinkButton ID="LinkButton6" runat="server" OnClick="lnkImages_Click" CausesValidation="false">Images</asp:LinkButton></td>
                                        <td class="tableNavLink" id="tdCrossSells" visible="false" runat="server">
                                            <asp:LinkButton ID="LinkButton7" runat="server" OnClick="lnkCross_Click" CausesValidation="false">Cross-Sells</asp:LinkButton></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="pnlMain" runat="server">
                                    <table class="admintable">
                                        <tr>
                                            <td class="tableHEADER" colspan="2"><strong>Main</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:ValidationSummary runat="server" ID="vsResultMessage" DisplayMode="List" ShowSummary="true"
                                                    HeaderText="Page Errors:" CssClass="adminitem" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" align="left">
                                                Asset Code</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:TextBox ID="txtSku" runat="server" Width="500px"></asp:TextBox>
                                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" valign="top" align="left">
                                                Asset Name</td>
                                            <td class="adminitem" style="width: 475px" valign="top" align="left">
                                                <asp:TextBox ID="txtProductName" runat="server" Width="500px" MaxLength="150"></asp:TextBox><asp:RequiredFieldValidator
                                                    runat="server" ID="rfvProductName" ControlToValidate="txtProductName" ErrorMessage="Asset Name is required."
                                                    Text="*" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" align="left">
                                                Asset Description</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:TextBox ID="txtShortDescription" runat="server" Width="500px" MaxLength="150"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" align="left">
                                                Salvage Value</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                Rs.
                                                <asp:TextBox ID="txtOurPrice" runat="server" Width="60px">0</asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvOurPrice" runat="server" ControlToValidate="txtOurPrice"
                                                    ErrorMessage="Salvage value is required." Text="*">
                                                </asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtOurPrice"
                                                    Display="Dynamic" ErrorMessage="Invalid Price" MaximumValue="99999999" MinimumValue="0"
                                                    Type="Currency"></asp:RangeValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" style="height: 26px" align="left">
                                                Asset Price</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                Rs.
                                                <asp:TextBox ID="txtRetailPrice" runat="server" Width="60px">0</asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvRetailPrice" runat="server" ControlToValidate="txtRetailPrice"
                                                    ErrorMessage="Asset Price is required." Text="*">
                                                </asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtRetailPrice"
                                                    Display="Dynamic" ErrorMessage="Invalid Price" MaximumValue="99999999" MinimumValue="0"
                                                    Type="Currency"></asp:RangeValidator></td>
                                        </tr>
                                        <tr id="trOldPrice" runat="server" visible="false">
                                            <td class="adminlabel" style="height: 26px" align="left">
                                                Old Price</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                Rs.
                                                <asp:TextBox ID="txtOldPrice" runat="server" Width="60px">0</asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtOldPrice"
                                                    Display="Dynamic" ErrorMessage="Invalid Price" MaximumValue="99999999" MinimumValue="0"
                                                    Type="Currency"></asp:RangeValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" align="left">
                                                Service/Installation Charge</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                Rs.
                                                <asp:TextBox ID="txtServiceCharge" runat="server" Width="60px">0</asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtServiceCharge"
                                                    Display="Dynamic" ErrorMessage="Invalid Price" MaximumValue="99999999" MinimumValue="0"
                                                    Type="Currency"></asp:RangeValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" align="left">
                                                Department</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:TextBox ID="txtDepartment" runat="server" Width="500px"></asp:TextBox>
                                             </td>
                                        </tr>
                                        <tr id="trServiceTax" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Service Tax in Percentage</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                %
                                                <asp:TextBox ID="txtServiceTax" runat="server" Width="60px">0</asp:TextBox>
                                                of Service Charge
                                                <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtServiceTax"
                                                    Display="Dynamic" ErrorMessage="Invalid Price" MaximumValue="99999999" MinimumValue="0"
                                                    Type="Currency"></asp:RangeValidator></td>
                                        </tr>
                                        <tr id="trManufactures" runat="server" visible="true">
                                            <td class="adminlabel" align="left">
                                                Brand</td>
                                            <td class="adminitem" style="width: 465px" align="left">
                                                <div>
                                                    <asp:DropDownList ID="ddlManufacturerID" runat="server">
                                                    </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="rfvManufacturerID"
                                                        ControlToValidate="ddlManufacturerID" ErrorMessage="Manufacturer is required."
                                                        Text="*" />
                                                    <asp:TextBox ID="txtQuickMan" runat="server"></asp:TextBox>
                                                    <asp:Button ID="btnQuickMan" runat="server" OnClick="btnQuickMan_Click" Text="Quick Add" CssClass="button" CausesValidation="false" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" align="left">
                                                Status</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:DropDownList ID="ddlStatusID" runat="server">
                                                </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="rfvStatusID" ControlToValidate="ddlStatusID"
                                                    ErrorMessage="Status is required." Text="*" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" align="left">
                                                Asset Type</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:DropDownList ID="ddlProductTypeID" runat="server">
                                                </asp:DropDownList><asp:RequiredFieldValidator ID="rfvProductTypeID" runat="server"
                                                    ControlToValidate="ddlProductTypeID" ErrorMessage="Asset Type is required."
                                                    Text="*">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr  id="trShipType" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Shipping Type</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:DropDownList ID="ddlShippingTypeID" runat="server">
                                                </asp:DropDownList><asp:RequiredFieldValidator ID="rfvShippingTypeID" runat="server"
                                                    ControlToValidate="ddlShippingTypeID" ErrorMessage="Shipping Type is required."
                                                    Text="*">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="trShipEstimate" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Ship Estimate</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:DropDownList ID="ddlShipEstimateID" runat="server">
                                                </asp:DropDownList><asp:RequiredFieldValidator ID="rfvShipEstimateID" runat="server"
                                                    ControlToValidate="ddlShipEstimateID" ErrorMessage="Ship Estimate is required."
                                                    Text="*">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" align="left">
                                                Tax Type</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:DropDownList ID="ddlTaxTypeID" runat="server">
                                                </asp:DropDownList><asp:RequiredFieldValidator ID="rfvTaxTypeID" runat="server" ControlToValidate="ddlTaxTypeID"
                                                    ErrorMessage="Tax Type is required." Text="*">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" style="height: 118px" align="left">
                                                Stock Location</td>
                                            <td class="adminitem" style="height: 118px; width: 475px;" align="left">
                                                <asp:TextBox ID="txtStockLocation" runat="server" Height="100px" TextMode="MultiLine"
                                                    Width="400px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trWeight" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Weight</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:TextBox ID="txtWeight" runat="server" Width="41px" DecimalPlaces="2">0</asp:TextBox><asp:RequiredFieldValidator
                                                    ID="rfvWeight" runat="server" ControlToValidate="txtWeight" ErrorMessage="Weight is required."
                                                    Text="*">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr  id="trCurrency" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Currency Code</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:DropDownList ID="ddlCurrencyCodeID" runat="server" /><asp:RequiredFieldValidator
                                                    ID="rfvCurrencyCodeID" runat="server" ControlToValidate="ddlCurrencyCodeID" ErrorMessage="Currency Code is required."
                                                    Text="*" />
                                            </td>
                                        </tr>
                                        <tr  id="trUnit" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Unit Of Measure</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:TextBox ID="txtUnitOfMeasure" runat="server" Text="single item"></asp:TextBox><br />
                                                (inches, cm, m, ft, yds, etc)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" style="height: 29px" align="left">
                                                Admin Comments</td>
                                            <td class="adminitem" style="height: 29px; width: 475px;" align="left">
                                                <asp:TextBox ID="txtAdminComments" runat="server" TextMode="MultiLine" Height="47px"
                                                    Width="400px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr  id="trDimensions" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Dimensions</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                L
                                                <asp:TextBox ID="txtLength" runat="server" Width="24px">0</asp:TextBox><asp:RequiredFieldValidator
                                                    ID="rfvLength" runat="server" ControlToValidate="txtLength" ErrorMessage="Dimensions:Length is required."
                                                    Text="*">
                                                </asp:RequiredFieldValidator>
                                                H
                                                <asp:TextBox ID="txtHeight" runat="server" Width="24px">0</asp:TextBox><asp:RequiredFieldValidator
                                                    ID="rfvHeight" runat="server" ControlToValidate="txtHeight" ErrorMessage="Dimensions:Height is required."
                                                    Text="*">
                                                </asp:RequiredFieldValidator>
                                                W
                                                <asp:TextBox ID="txtWidth" runat="server" Width="24px">0</asp:TextBox><asp:RequiredFieldValidator
                                                    ID="rfvWidth" runat="server" ControlToValidate="txtWidth" ErrorMessage="Dimensions:Width is required."
                                                    Text="*">
                                                </asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr  id="trDimensionUnit" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Dimension Unit</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:TextBox ID="txtDimensionUnit" runat="server" Text="inches"></asp:TextBox><asp:RequiredFieldValidator
                                                    ID="rfvDimensionUnit" runat="server" ControlToValidate="txtDimensionUnit" ErrorMessage="Dimension Unit is required."
                                                    Text="*">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="adminlabel" align="left">
                                                List Order</td>
                                            <td class="adminitem" style="width: 475px" align="left">
                                                <asp:TextBox ID="txtListOrder" runat="server" Width="24px">99</asp:TextBox><asp:RequiredFieldValidator
                                                    ID="rfvListOrder" runat="server" ControlToValidate="txtListOrder" ErrorMessage="List Order is required."
                                                    Text="*">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="trTitle" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Page Title
                                            </td>
                                            <td class="adminitem" align="left">
                                                <asp:TextBox ID="txtProductTitle" runat="server" Text="Asset Management" MaxLength="1000" Height="40px" TextMode="MultiLine"
                                                    Width="400px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trKeywords" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Page Keywords
                                            </td>
                                            <td class="adminitem" align="left">
                                                <asp:TextBox ID="txtProductKeywords" runat="server" MaxLength="2000" Height="40px"
                                                    TextMode="MultiLine" Width="400px"  Text="Asset Management"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trMeta" runat="server" visible="false">
                                            <td class="adminlabel" align="left">
                                                Page Meta Description
                                            </td>
                                            <td class="adminitem" align="left">
                                                <asp:TextBox ID="txtProductMetaDescription" runat="server" MaxLength="2000" Height="40px"
                                                    TextMode="MultiLine" Width="400px"  Text="Asset Management"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="button">
                                                </asp:Button>&nbsp;
                                                <input type="button" onclick="frmAssetsList.aspx" value="Return"
                                                    class="button" />&nbsp;
                                                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" Text="Delete"
                                                    OnClick="btnDelete_Click" CssClass="button"></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlDescriptions" runat="server">
                                    <uc7:ProductDescriptors ID="ProductDescriptors1" runat="server"></uc7:ProductDescriptors>
                                </asp:Panel>
                                <asp:Panel ID="pnlImages" runat="server">
                                    <div id="divImages">
                                        <uc3:ProductImages ID="ProductImages1" runat="server"></uc3:ProductImages>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlCategories" runat="server">
                                    <uc4:ProductCategories ID="ProductCategories1" runat="server" />
                                </asp:Panel>
                                <asp:Panel ID="pnlAtts" runat="server" Visible="false">
                                    <uc5:ProductAttributes ID="ProductAttributes1" runat="server"></uc5:ProductAttributes>
                                </asp:Panel>
                                <asp:Panel ID="pnlCross" runat="server" Visible="false">
                                    <uc6:ProductCrossSells ID="ProductCrossSells1" runat="server"></uc6:ProductCrossSells>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img height="17" src="../../../Images/table_left_bot_corner.gif" width="9" alt="" /></td>
                <td style="height: 17px" background="../../../Images/table_bot_mid_bg.gif">
                    <img height="17" src="../../../Images/table_bot_mid_bg.gif" width="25" alt="" /></td>
                <td style="height: 17px; width: 17px;">
                    <img height="17" src="../../../Images/table_right_bot_corner.gif" width="16" alt="" /></td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">
			function CheckDelete(){
				return confirm("Delete this record? This action cannot be undone...");
		  }
    </script>

</asp:Content>
