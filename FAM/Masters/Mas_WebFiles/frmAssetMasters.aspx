<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmAssetMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmAssetMasters"
    Title="Asset Masters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function noway(go) {
            if (document.all) {
                if (event.button == 2) {
                    alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                    return false;
                }
            }
            if (document.layers) {
                if (go.which == 3) {
                    alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = noway;
    </script>
    <style>
        .btn {
            border-radius: 4px;
            background-color: #3A618F;
        }
    </style>

</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Asset Masters</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div class="clearfix">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink10" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAddAssetCategory.aspx">Asset Type Master</asp:HyperLink>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLinkSubCat" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAddAssetSubCategory.aspx">Asset Sub Type Master</asp:HyperLink>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink6" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAddAssetBrand.aspx">Brand/Make Master</asp:HyperLink>
                            </div>
                        </div>
                        <br />
                        <div class="clearfix">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLinkModel" class="btn btn-block btn-primary" runat="server" NavigateUrl="~/FAM/FAM_Webfiles/frmAddAssetModel.aspx">Asset Model Master</asp:HyperLink>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAMGVendorNewRecord1.aspx">Add Vendor</asp:HyperLink>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAddVendorRateContract.aspx">Vendor Rate Contract</asp:HyperLink>
                            </div>

                        </div>
                        <br />
                        <div class="clearfix">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink7AddAsse" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/Masters/Mas_Webfiles/frmAddAsset.aspx">Add Capital Asset </asp:HyperLink>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="hypconasset" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmaddConsumablesstock.aspx">Add Consumable Asset </asp:HyperLink>
                            </div>
                             <div class="col-md-4 col-sm-12 col-xs-12">                            
                                    <asp:HyperLink ID="HyperLink1" runat="server"  class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/AssetLabelFormat.aspx">Asset Label Format</asp:HyperLink>                            
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

