Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Partial Class FAM_Masters_Mas_WebFiles_frmAssetsList
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            LoadGrid()
        End If

    End Sub


    Private Sub LoadGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GetAllProducts")
        dg.DataSource = sp.GetDataSet  'Commerce.Common.Product.FetchAll()
        dg.DataBind()
    End Sub

    Protected Sub dg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dg.PageIndexChanging
        dg.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Response.Redirect("../../Masters/Mas_WebFiles/frmMASAssetMaster.aspx")
    End Sub
End Class
