Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common

Partial Class FAM_Masters_Mas_WebFiles_frmAssetDetails
    Inherits System.Web.UI.Page
    Private productID As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        productID = Utility.GetIntParameter("id")
        If (Not Page.IsPostBack) Then
            If productID <> 0 Then
                LoadEditor()
            End If
            TogglePanel(pnlMain)
        End If

    End Sub

#Region "Add Loader"
    Private Sub LoadAddForm()
        LoadDropDowns()
        btnDelete.Visible = False
        btnSave.Text = "Add"
        lblProductName.Text = "Add a New Asset"
    End Sub

#End Region
    Private Sub LoadDropDowns()
        Utility.LoadListItems(ddlManufacturerID.Items, Lookups.GetList("CSK_Store_Manufacturer"), "manufacturer", "manufacturerid", "", True)
        Utility.LoadListItems(ddlStatusID.Items, Lookups.GetList("CSK_Store_ProductStatus"), "status", "statusid", "", True)
        Utility.LoadListItems(ddlProductTypeID.Items, Lookups.GetList("CSK_Store_ProductType"), "producttype", "producttypeid", "", True)
        Utility.LoadListItems(ddlShippingTypeID.Items, Lookups.GetList("CSK_Store_ShippingType"), "shippingtype", "shippingtypeid", "", True)
        Utility.LoadListItems(ddlShipEstimateID.Items, Lookups.GetList("CSK_Store_ShippingEstimate"), "shippingestimate", "shipestimateid", "", True)
        Utility.LoadListItems(ddlTaxTypeID.Items, Lookups.GetList("CSK_Tax_Type"), "taxtype", "taxtypeid", "", True)
        Utility.LoadListItems(ddlCurrencyCodeID.Items, Lookups.GetList("CSK_Util_Currency"), "code", "code", ConfigurationManager.AppSettings("defaultCurrency"), True)

    End Sub

#Region "Editor Loader"
    Private Sub LoadEditor()
        'load the drops
        btnDelete.Visible = True
        btnDelete.Attributes.Add("onclick", "return CheckDelete();")
        btnSave.Text = "Update"
        'load the rest
        LoadDropDowns()
        LoadEditData()
        lblID.Text = productID.ToString()
        'load up the images
        LoadImages()
        LoadCategories()
        'LoadCatList();
        LoadCrossProducts()
        LoadDescriptors()

    End Sub
    Private Sub LoadDescriptors()
        ProductDescriptors1.ProductID = productID
    End Sub

    Private Sub LoadCategories()
        ProductCategories1.LoadCategories(productID)
    End Sub
    Private Sub LoadCrossProducts()
        ProductCrossSells1.LoadCrossProducts(productID)
    End Sub


    Private Sub LoadImages()
        ProductImages1.LoadImages(productID)

    End Sub


    Private Sub LoadEditData()
        Dim product As Commerce.Common.Product = New Commerce.Common.Product(productID)
        ProductAttributes1.LoadAttributes(product)
        LoadDropDowns()

        'Page Title ... 
        Me.Title = "Asset Details: " & product.ProductName

        lblID.Text = productID.ToString()
        txtSku.Text = product.Sku
        txtProductName.Text = product.ProductName
        lblProductName.Text = product.ProductName
        txtShortDescription.Text = product.ShortDescription

        ddlManufacturerID.SelectedValue = product.ManufacturerID.ToString()
        ddlStatusID.SelectedValue = Convert.ToInt16(product.Status).ToString()
        ddlProductTypeID.SelectedValue = Convert.ToInt16(product.ProductType).ToString()
        ddlShippingTypeID.SelectedValue = Convert.ToInt16(product.ShippingType).ToString()
        ddlShipEstimateID.SelectedValue = product.ShipEstimateID.ToString()
        ddlTaxTypeID.SelectedValue = product.TaxTypeID.ToString()

        txtStockLocation.Text = product.StockLocation
        product.OurPrice = Math.Round(product.OurPrice, 2)
        product.RetailPrice = Math.Round(product.RetailPrice, 2)

        txtOurPrice.Text = product.OurPrice.ToString()
        txtRetailPrice.Text = product.RetailPrice.ToString()
        txtOldPrice.Text = FormatNumber((product.OldPrice))

        txtServiceCharge.Text = FormatNumber(product.ServiceCharge, 2)
        txtServiceTax.Text = FormatNumber(product.ServiceTaxinPercentage, 2)

        txtWeight.Text = product.Weight.ToString()
        ddlCurrencyCodeID.SelectedValue = product.CurrencyCode.ToString()
        txtUnitOfMeasure.Text = product.UnitOfMeasure
        txtAdminComments.Text = product.AdminComments
        txtLength.Text = product.Length.ToString()
        txtHeight.Text = product.Height.ToString()
        txtWidth.Text = product.Width.ToString()
        txtDimensionUnit.Text = product.DimensionUnit
        txtListOrder.Text = product.ListOrder.ToString()
        txtProductTitle.Text = product.ProductPageTitle
        txtProductKeywords.Text = product.ProductPageKeywords
        txtProductMetaDescription.Text = product.ProductPageMetaDescription

    End Sub
#End Region


#Region "Event Handlers"

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(Request.Url.PathAndQuery, False)
    End Sub


    Protected Sub cmdAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadAddForm()
    End Sub
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Commerce.Common.Product.Delete(int.Parse(lblID.Text));
        ProductController.DeletePermanent(Integer.Parse(lblID.Text))
        Response.Redirect("frmAssetsList.aspx", False)
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim product As Commerce.Common.Product = Nothing
        If lblID.Text <> String.Empty Then
            product = New Commerce.Common.Product(Integer.Parse(lblID.Text))
        Else
            product = New Commerce.Common.Product()
        End If
        product.AdminComments = txtAdminComments.Text
        product.CurrencyCode = ddlCurrencyCodeID.SelectedValue.Trim()
        product.DimensionUnit = txtDimensionUnit.Text

        product.ProductName = txtProductName.Text
        product.ShortDescription = txtShortDescription.Text
        product.Sku = txtSku.Text
        product.StockLocation = txtStockLocation.Text
        product.Status = CType(Integer.Parse(ddlStatusID.SelectedValue), ProductStatus)
        product.ShippingType = CType(Integer.Parse(ddlShippingTypeID.SelectedValue), ShippingType)
        product.ProductType = CType(Integer.Parse(ddlProductTypeID.SelectedValue), ProductType)
        product.UnitOfMeasure = txtUnitOfMeasure.Text
        product.ManufacturerID = Integer.Parse(ddlManufacturerID.SelectedValue)
        product.ShipEstimateID = Integer.Parse(ddlShipEstimateID.SelectedValue)
        product.TaxTypeID = Integer.Parse(ddlTaxTypeID.SelectedValue)
        product.ProductPageTitle = txtProductTitle.Text
        product.ProductPageKeywords = txtProductKeywords.Text
        product.ProductPageMetaDescription = txtProductMetaDescription.Text
        Dim parsedInt As Integer = 0
        Dim parsedDec As Decimal = 0

        Integer.TryParse(txtListOrder.Text, parsedInt)
        product.ListOrder = parsedInt

        Decimal.TryParse(txtOurPrice.Text, parsedDec)
        product.OurPrice = Decimal.Parse(txtOurPrice.Text)

        Decimal.TryParse(txtRetailPrice.Text, parsedDec)
        product.RetailPrice = parsedDec

        Decimal.TryParse(txtOldPrice.Text, parsedDec)
        product.OldPrice = parsedDec

        parsedDec = 0
        Decimal.TryParse(txtServiceCharge.Text, parsedDec)
        product.ServiceCharge = parsedDec

        parsedDec = 0
        Decimal.TryParse(txtServiceTax.Text, parsedDec)
        product.ServiceTaxinPercentage = parsedDec

        Decimal.TryParse(txtHeight.Text, parsedDec)
        product.Height = parsedDec

        Decimal.TryParse(txtLength.Text, parsedDec)
        product.Length = parsedDec

        Decimal.TryParse(txtWeight.Text, parsedDec)
        product.Weight = parsedDec


        Decimal.TryParse(txtWidth.Text, parsedDec)
        product.Width = parsedDec


        Try
            product.Save(Utility.GetUserName())
            ResultMessage1.ShowSuccess("Update Successful")
        Catch x As Exception
            ThrowError(x.Message)
        End Try

    End Sub
#End Region
#Region "Error Handling"
    Private Sub ThrowError(ByVal message As String)
        ResultMessage1.ShowFail(message)
    End Sub
#End Region
    Protected Sub btnQuickMan_Click(ByVal sender As Object, ByVal e As EventArgs)
        If txtQuickMan.Text.Trim() <> String.Empty Then
            Lookups.QuickAdd("CSK_Store_Manufacturer", "manufacturer", txtQuickMan.Text)
            txtQuickMan.Text = ""
            Me.LoadDropDowns()
            ddlManufacturerID.SelectedIndex = ddlManufacturerID.Items.Count - 1

        End If
    End Sub
    Private Sub TogglePanelsOff()
        pnlMain.Visible = False
        pnlImages.Visible = False
        pnlCross.Visible = False
        pnlCategories.Visible = False
        pnlCategories.Visible = False
        pnlDescriptions.Visible = False
        pnlAtts.Visible = False
    End Sub
    Private Sub TogglePanel(ByVal pnl As Panel)
        TogglePanelsOff()
        pnl.Visible = True
    End Sub
    Protected Sub lnkMain_Click(ByVal sender As Object, ByVal e As EventArgs)
        TogglePanel(pnlMain)
    End Sub
    Protected Sub lnkCat_Click(ByVal sender As Object, ByVal e As EventArgs)
        TogglePanel(pnlCategories)

    End Sub
    Protected Sub lnkAtt_Click(ByVal sender As Object, ByVal e As EventArgs)
        TogglePanel(pnlAtts)


    End Sub
    Protected Sub lnkImages_Click(ByVal sender As Object, ByVal e As EventArgs)
        TogglePanel(pnlImages)

    End Sub
    Protected Sub lnkCross_Click(ByVal sender As Object, ByVal e As EventArgs)
        TogglePanel(pnlCross)

    End Sub
    Protected Sub lnkDesc_Click(ByVal sender As Object, ByVal e As EventArgs)
        TogglePanel(pnlDescriptions)
    End Sub
End Class
