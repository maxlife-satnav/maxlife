Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common

Partial Class FAM_Masters_Mas_WebFiles_frmMASAssetMaster
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            LoadDropDowns()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        'Thanks Herman (osi_ni) for portions of this code
        If Page.IsValid Then

            Try
                Dim manufacturerId As Integer = 0
                Integer.TryParse(ddlManufacturerID.SelectedValue, manufacturerId)
                Dim statusId As Integer = 0
                Integer.TryParse(ddlStatusID.SelectedValue, statusId)
                Dim productTypeId As Integer = 0
                Integer.TryParse(ddlProductTypeID.SelectedValue, productTypeId)
                Dim shippingTypeId As Integer = 0
                Integer.TryParse(ddlShippingTypeID.SelectedValue, shippingTypeId)
                Dim shipEstimateId As Integer = 0
                Integer.TryParse(ddlShipEstimateID.SelectedValue, shipEstimateId)
                Dim taxTypeId As Integer = 0
                Integer.TryParse(ddlTaxTypeID.SelectedValue, taxTypeId)
                Dim ourPrice As Decimal = 0
                Decimal.TryParse(txtOurPrice.Text.Trim(), ourPrice)
                Dim retailPrice As Decimal = 0
                Decimal.TryParse(txtRetailPrice.Text.Trim(), retailPrice)
                Dim weight As Decimal = 0
                Decimal.TryParse(txtWeight.Text.Trim(), weight)
                Dim length As Decimal = 0
                Decimal.TryParse(txtLength.Text.Trim(), length)
                Dim height As Decimal = 0
                Decimal.TryParse(txtHeight.Text.Trim(), height)
                Dim width As Decimal = 0
                Decimal.TryParse(txtWidth.Text.Trim(), width)
                Dim listOrder As Integer = 0
                Integer.TryParse(txtListOrder.Text.Trim(), listOrder)

                Dim product As Commerce.Common.Product = New Commerce.Common.Product()

                product.Sku = txtSku.Text.Trim()
                product.ProductName = txtProductName.Text.Trim()
                product.ShortDescription = txtShortDescription.Text.Trim()
                product.OurPrice = ourPrice
                product.RetailPrice = retailPrice
                product.ManufacturerID = manufacturerId
                product.Status = CType(statusId, ProductStatus)
                product.ProductType = CType(productTypeId, ProductType)
                product.ShippingType = CType(shippingTypeId, ShippingType)
                product.ShipEstimateID = shipEstimateId
                product.TaxTypeID = taxTypeId
                product.StockLocation = txtStockLocation.Text.Trim()
                product.Weight = weight
                product.CurrencyCode = ddlCurrencyCodeID.SelectedValue.Trim()
                product.UnitOfMeasure = txtUnitOfMeasure.Text.Trim()
                product.AdminComments = txtAdminComments.Text.Trim()
                product.Length = length
                product.Height = height
                product.Width = width
                product.DimensionUnit = txtDimensionUnit.Text.Trim()
                product.ListOrder = listOrder
                'default this to avoid division errors
                product.TotalRatingVotes = 1
                product.RatingSum = 4


                'save it up and redirect
                product.Save(Utility.GetUserName())
                'product.Save(Session("UID"))
                'send to the detail page
                Response.Redirect("frmAssetDetails.aspx?id=" & product.ProductID.ToString(), False)
            Catch x As Exception
                ResultMessage1.ShowFail(x.Message)
            End Try
        End If
    End Sub

    Private Sub LoadDropDowns()
        Utility.LoadListItems(ddlManufacturerID.Items, Lookups.GetList("CSK_Store_Manufacturer"), "manufacturer", "manufacturerid", "", True)
        Utility.LoadListItems(ddlStatusID.Items, Lookups.GetList("CSK_Store_ProductStatus"), "status", "statusid", "", True)
        Utility.LoadListItems(ddlProductTypeID.Items, Lookups.GetList("CSK_Store_ProductType"), "producttype", "producttypeid", "", True)
        Utility.LoadListItems(ddlShippingTypeID.Items, Lookups.GetList("CSK_Store_ShippingType"), "shippingtype", "shippingtypeid", "", True)
        Utility.LoadListItems(ddlShipEstimateID.Items, Lookups.GetList("CSK_Store_ShippingEstimate"), "shippingestimate", "shipestimateid", "", True)
        Utility.LoadListItems(ddlTaxTypeID.Items, Lookups.GetList("CSK_Tax_Type"), "taxtype", "taxtypeid", "", True)
        Utility.LoadListItems(ddlCurrencyCodeID.Items, Lookups.GetList("CSK_Util_Currency"), "code", "code", ConfigurationManager.AppSettings("defaultCurrency"), True)
    End Sub

    Protected Sub btnQuickMan_Click(ByVal sender As Object, ByVal e As EventArgs)
        If txtQuickMan.Text.Trim().Length > 0 Then
            Lookups.QuickAdd("CSK_Store_Manufacturer", "manufacturer", txtQuickMan.Text.Trim())
            txtQuickMan.Text = ""
            Me.LoadDropDowns()
            ddlManufacturerID.SelectedIndex = ddlManufacturerID.Items.Count - 1
        End If
    End Sub


    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Response.Redirect("../../Masters/Mas_WebFiles/frmAssetsList.aspx")
    'End Sub
End Class
