﻿Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports clsSubSonicCommonFunctions
Imports System.IO
Partial Class FAM_FAM_Webfiles_Exports_AssetLocation
    Inherits System.Web.UI.Page

    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim export As New Export
    Dim ds As DataSet

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ExportToExcel()
        End If
    End Sub

    Private Sub ExportToExcel()
        Dim param(5) As SqlParameter

        param(0) = New SqlParameter("@categoryID", SqlDbType.Int)
        param(0).Value = Request.QueryString("ct")
        param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(1).Value = Request.QueryString("page")
        param(2) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        param(2).Value = Request.QueryString("sidx").Replace("AssetCategory_list_", "")
        param(3) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        param(3).Value = Request.QueryString("sord")
        param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(4).Value = Request.QueryString("rows")
        param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(5).Value = 100
        ds = objsubsonic.GetSubSonicDataSet("GET_ASTCOUNT_BYCATEGORY_V", param)


        ds.Tables(0).Columns(1).ColumnName = "PRODUCT ID"
        ds.Tables(0).Columns(2).ColumnName = "PRODUCT NAME"
        ds.Tables(0).Columns(3).ColumnName = "ASSET COUNT"
        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        export.Export("Asset_Location_Summary_Report.xls", gv)

    End Sub
End Class
