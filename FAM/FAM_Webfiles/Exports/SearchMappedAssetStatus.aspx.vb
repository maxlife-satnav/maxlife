﻿Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports clsSubSonicCommonFunctions
Imports System.IO
Partial Class FAM_FAM_Webfiles_Exports_SearchMappedAssetStatus
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim export As New Export
    Dim ds As DataSet
    Private Sub ExportToExcel()
        Dim param(0) As SqlParameter

        param(0) = New SqlParameter("@SEARCH_QRY", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("ct")
        ds = objsubsonic.GetSubSonicDataSet("GET_EMPLOYEEASSETMAPPED", param)



        ds.Tables(0).Columns(1).ColumnName = "Employee Name"
        ds.Tables(0).Columns(2).ColumnName = "Employee Email"
        ds.Tables(0).Columns(4).ColumnName = "RM Name"
        ds.Tables(0).Columns(12).ColumnName = "LOCATION NAME"
        'ds.Tables(0).Columns(22).ColumnName = "TOWER"
        'ds.Tables(0).Columns(19).ColumnName = "FLOOR"
        ds.Tables(0).Columns(7).ColumnName = "Department Id"
        'ds.Tables(0).Columns(14).ColumnName = "SPACE ID"
        ds.Tables(0).Columns(9).ColumnName = "Product Name"
        ds.Tables(0).Columns(10).ColumnName = "Asset Code"

        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        export.Export("Search_Mapped_Asset_Status.xls", gv)

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ExportToExcel()
        End If
    End Sub
End Class
