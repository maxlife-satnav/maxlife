Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common

Partial Class FAM_FAM_Webfiles_frmAssetRequisitionDetails_PO
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtDOD.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then
            pnlItems.Visible = True
            'BindRequisition()
            BindVendorDetails()
            gvItems.Visible = False
        End If
    End Sub

    Private Sub BindVendorDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GETVENDOR_FORASSE")
        'sp.Command.AddParameter("@REQ_ID", Request.QueryString("RID"), DbType.String)
        ddlVendor.DataSource = sp.GetReader
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function

    Private Sub BindRequisition()
        Dim ReqId As String = Request("RID")
        'If String.IsNullOrEmpty(ReqId) Then
        '    lblMsg.Text = "No such requisition found."
        'Else
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetailsProcurement_GetDetailsByReqId")
        'sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@VEN_CODE", ddlVendor.SelectedItem.Value, DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
        pnlItems.Visible = True
        'BindUserDetails(ReqId)
        'End If
    End Sub

    'Private Sub BindUserDetails(ByVal ReqId As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_ITEM_REQUISITION_USERDETAILS_GETBYREQID")
    '    sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
    '    Dim dr As SqlDataReader = sp.GetReader()
    '    If dr.Read() Then
    '        txtPORaisedBy.Text = dr("AUR_KNOWN_AS")
    '        'txtRemarks.Text=
    '        txtRequesterId.Text = dr("AUR_ID")
    '        txtRequesterName.Text = dr("AUR_KNOWN_AS")
    '        txtRequestorRemarks.Text = dr("AIR_REMARKS")
    '        txtDeptId.Text = dr("AUR_DEP_ID")
    '        txtLocation.Text = dr("AUR_LOCATION")
    '    End If
    'End Sub

    Protected Sub btnTotalCost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTotalCost.Click
        Dim ItemSubTotal As Double = 0
        Dim FFIP As Double = 0
        Dim Advance As Double = 0
        Dim cst As Double = 0
        Dim OnDelivery As Double = 0
        Dim Wst As Double = 0
        Dim Installation As Double = 0
        Dim Octrai As Double = 0
        Dim Commissioning As Double = 0
        Dim ServiceTax As Double = 0
        Dim Retention As Double = 0
        Dim Others As Double = 0
        Dim Payments As Double = 0
        Dim TotalCost As Double = 0
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim txtPrice As TextBox = DirectCast(row.FindControl("txtPrice"), TextBox)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            Dim txtUnitPrice As TextBox = DirectCast(row.FindControl("txtUnitPrice"), TextBox)
            Dim unitprice As Double = 0
            Dim PurchaseQty As Double = 0
            Decimal.TryParse(Trim(txtUnitPrice.Text), unitprice)
            Decimal.TryParse(Trim(txtPurchaseQty.Text), PurchaseQty)
            If chkSelect.Checked Then
                ItemSubTotal += (PurchaseQty * unitprice)
            End If
        Next
        If Double.TryParse(txtFFIP.Text, FFIP) Then
        Else
            lblMsg.Text = "Invalid Tax"
            Exit Sub
        End If
        If Double.TryParse(txtCst.Text, cst) Then
        Else
            lblMsg.Text = "Invalid CST/ST"
            Exit Sub
        End If
        If Double.TryParse(txtOctrai.Text, Octrai) Then
        Else
            lblMsg.Text = "Invalid Octroi"
            Exit Sub
        End If
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        If Double.TryParse(txtDelivery.Text, OnDelivery) Then
        Else
            lblMsg.Text = "Invalid On Delivery"
            Exit Sub
        End If
        If Double.TryParse(txtWst.Text, Wst) Then
        Else
            lblMsg.Text = "Invalid VAT"
            Exit Sub
        End If
        If Double.TryParse(txtInstallation.Text, Installation) Then
        Else
            lblMsg.Text = "Invalid Installation"
            Exit Sub
        End If
        If Double.TryParse(txtCommissioning.Text, Commissioning) Then
        Else
            lblMsg.Text = "Invalid Commissioning"
            Exit Sub
        End If
        If Double.TryParse(txtServiceTax.Text, ServiceTax) Then
        Else
            lblMsg.Text = "Invalid ServiceTax"
            Exit Sub
        End If
        If Double.TryParse(txtRetention.Text, Retention) Then
        Else
            lblMsg.Text = "Invalid Retention"
            Exit Sub
        End If
        If Double.TryParse(txtOthers.Text, Others) Then
        Else
            lblMsg.Text = "Invalid Others"
            Exit Sub
        End If
        If Double.TryParse(txtPayments.Text, Payments) Then
        Else
            lblMsg.Text = "Invalid Payments"
            Exit Sub
        End If
        TotalCost = ItemSubTotal + FFIP + cst + Octrai + Wst + ServiceTax + Others
        txtTotalCost.Text = FormatNumber(TotalCost, 2)
        If TotalCost = 0 Then
            lblMsg.Text = "Please select asset to calculate the total cost..."
            Exit Sub
        Else
            lblMsg.Visible = False
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim PONumber As String = GeneratePO()
        InsertPOMaster(PONumber)
        If lblMsg.Text <> "" Then
            lblMsg.Visible = True
            Exit Sub
        Else
            InsertPOMasterDetails(PONumber)
            UpdateData(1014)
            send_mail_gen_PO(PONumber)
            Response.Redirect("frmAssetThanks.aspx?RID=" + Request("RID") + "&PO=" + PONumber)
        End If
    End Sub

    Private Function GeneratePO() As String
        Dim Po As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GETMAX_AMG_ITEM_PO")
        Dim Sno As Integer = CInt(sp.ExecuteScalar)
        Return "AST/PO/0000" + CStr(Sno)
        'Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId() + "&PO=" + Po)
    End Function

    Private Sub InsertPOMaster(ByVal PO As String)
        Dim FFIP As Double = 0
        Dim Advance As Double = 0
        Dim cst As Double = 0
        Dim OnDelivery As Double = 0
        Dim Wst As Double = 0
        Dim Installation As Double = 0
        Dim Octrai As Double = 0
        Dim Commissioning As Double = 0
        Dim ServiceTax As Double = 0
        Dim Retention As Double = 0
        Dim Others As Double = 0
        Dim Payments As Double = 0
        Dim TotalCost As Double = 0
        If Double.TryParse(txtFFIP.Text, FFIP) Then
        Else
            lblMsg.Text = "Invalid FFIP"
            Exit Sub
        End If
        If Double.TryParse(txtCst.Text, cst) Then
        Else
            lblMsg.Text = "Invalid CST/ST"
            Exit Sub
        End If
        If Double.TryParse(txtOctrai.Text, Octrai) Then
        Else
            lblMsg.Text = "Invalid Octroi"
            Exit Sub
        End If
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        If Double.TryParse(txtDelivery.Text, OnDelivery) Then
        Else
            lblMsg.Text = "Invalid On Delivery"
            Exit Sub
        End If
        If Double.TryParse(txtWst.Text, Wst) Then
        Else
            lblMsg.Text = "Invalid Wst"
            Exit Sub
        End If
        If Double.TryParse(txtInstallation.Text, Installation) Then
        Else
            lblMsg.Text = "Invalid Installation"
            Exit Sub
        End If
        If Double.TryParse(txtCommissioning.Text, Commissioning) Then
        Else
            lblMsg.Text = "Invalid Commissioning"
            Exit Sub
        End If
        If Double.TryParse(txtServiceTax.Text, ServiceTax) Then
        Else
            lblMsg.Text = "Invalid ServiceTax"
            Exit Sub
        End If
        If Double.TryParse(txtRetention.Text, Retention) Then
        Else
            lblMsg.Text = "Invalid Retention"
            Exit Sub
        End If
        If Double.TryParse(txtOthers.Text, Others) Then
        Else
            lblMsg.Text = "Invalid Others"
            Exit Sub
        End If
        If Double.TryParse(txtPayments.Text, Payments) Then
        Else
            lblMsg.Text = "Invalid Payments"
            Exit Sub
        End If
        Dim ExchangeCharges As Double = 0
        If Double.TryParse(txtExchange.Text, ExchangeCharges) Then
            lblMsg.Text = ""
        Else
            lblMsg.Text = "Invalid Exchange Charges"
            Exit Sub
        End If
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_Insert")
        sp.Command.AddParameter("@AIP_PO_ID", PO, DbType.String)
        'sp.Command.AddParameter("@AIP_AUR_ID", txtRequesterId.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_REMARKS", txtRequestorRemarks.Text, DbType.String)
        sp.Command.AddParameter("@AIP_APP_AUTH", Session("UID"), DbType.String)
        sp.Command.AddParameter("@AIP_APP_DATE", txtDOD.Text, DbType.DateTime)
        sp.Command.AddParameter("@AIP_APP_REM", txtRemarks.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_DEPT", txtDeptId.Text, DbType.String)
        sp.Command.AddParameter("@AIP_ITM_TYPE", "", DbType.String)
        'sp.Command.AddParameter("@AIP_LOCATION", txtLocation.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_TYPE", "", DbType.String)
        sp.Command.AddParameter("@AIPD_VENDORName", ddlVendor.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@AIPD_TOTALCOST", Double.Parse(Trim(txtTotalCost.Text)), DbType.Double)
        sp.Command.AddParameter("@AIPD_PLI", Advance, DbType.String)
        sp.Command.AddParameter("@AIPD_TAX_FFIP", FFIP, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_CST", cst, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_WST", Wst, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_OCTRAI", Octrai, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_SERVICETAX", ServiceTax, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_OTHERS", Others, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_ADVANCE", Advance, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_ONDELIVERY", OnDelivery, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_INSTALLATION", Installation, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_COMMISSION", Commissioning, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_RETENTION", Retention, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_OTHERS", Payments, DbType.Double)
        sp.Command.AddParameter("@AIPD_EXPECTED_DATEOF_DELIVERY", txtDOD.Text, DbType.DateTime)
        sp.Command.AddParameter("@AIPD_EXCHANGE_CHARGES", ExchangeCharges, DbType.Double)
        sp.Command.AddParameter("@AVR_ID", ddlVendor.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertPOMasterDetails(ByVal PO As String)
        For Each row As GridViewRow In gvItems.Rows
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            Dim txtUnitPrice As TextBox = DirectCast(row.FindControl("txtUnitPrice"), TextBox)
            Dim lblReqId As Label = DirectCast(row.FindControl("lblReqId"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim unitprice As Double = 0
            Dim PurchaseQty As Integer = 0
            Double.TryParse(Trim(txtUnitPrice.Text), unitprice)
            Integer.TryParse(Trim(txtPurchaseQty.Text), PurchaseQty)
            If chkSelect.Checked Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEMPO_DETAILS_Insert")
                sp.Command.AddParameter("@AIPD_PO_ID", PO, DbType.String)
                sp.Command.AddParameter("@AIPD_AST_CODE", lblProductId.Text, DbType.String)
                sp.Command.AddParameter("@AIPD_QTY", CInt(txtQty.Text), DbType.Int32)
                sp.Command.AddParameter("@AIPD_ITMREQ_ID", lblReqId.Text, DbType.String)
                sp.Command.AddParameter("@AIPD_ACT_QTY", PurchaseQty, DbType.Int32)
                sp.Command.AddParameter("@AIPD_RATE", txtTotalCost.Text, DbType.Double)
                sp.ExecuteScalar()
            End If
        Next
    End Sub

    Private Sub UpdateData(ByVal StatusId As Integer)
        For Each row As GridViewRow In gvItems.Rows
            Dim lblReqId As Label = DirectCast(row.FindControl("lblReqId"), Label)
            Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionStatus_UpdateByReqIdPOApproved")
                sp.Command.AddParameter("@ReqId", lblReqId.Text, DbType.String)
                sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
                sp.Command.AddParameter("@ProductId", lblProductid.Text, DbType.String)
                sp.ExecuteScalar()
            End If
        Next
    End Sub

    Public Sub send_mail_gen_PO(ByVal POnum As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_GENERATE")
        sp.Command.AddParameter("@REQ_ID", POnum, DbType.String)
        sp.Execute()
    End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Protected Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged
        If ddlVendor.SelectedIndex > 0 Then
            BindRequisition()
            gvItems.Visible = True
            txtTotalCost.Text = ""
        Else
            gvItems.Visible = False
            txtTotalCost.Text = ""
        End If
    End Sub

End Class
