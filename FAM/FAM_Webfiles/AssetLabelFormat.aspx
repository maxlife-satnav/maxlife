﻿    <%@ Page Language="VB" AutoEventWireup="false" CodeFile="AssetLabelFormat.aspx.vb" Inherits="FAM_FAM_Webfiles_AssetLabelFormat" %>

<!DOCTYPE html>

<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Asset Label Format
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-12 control-label">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <label class="btn btn-default">
                                        <asp:CheckBox ID="CheckLoc" runat="server" Text="Location" />

                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="btn btn-default">
                                        <asp:CheckBox ID="CheckCat" runat="server" Text="Category" />

                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="btn btn-default">
                                        <asp:CheckBox ID="CheckSubcat" runat="server" Text="SubCategory" />

                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="btn btn-default">
                                        <asp:CheckBox ID="CheckBrand" runat="server" Text="Brand" />

                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="btn btn-default">
                                        <asp:CheckBox ID="CheckModel" runat="server" Text="Model" />

                                    </label>
                                </div>
                            </div>

                            <%--<asp:CheckBox ID="CheckLoc" runat="server" Text="Location" />
                            <asp:CheckBox ID="CheckCat" runat="server" Text="Category" />--%>
                            <%--<asp:CheckBox ID="CheckSubcat" runat="server" Text="SubCategory" />
                            <asp:CheckBox ID="CheckBrand" runat="server" Text="Brand" />
                            <asp:CheckBox ID="CheckModel" runat="server" Text="Model" />--%>

                            <br />
                            <br />
                            <div class="row" style="padding-left: 20px">
                                <asp:Label ID="Label1" runat="server" Text="Label Format :"></asp:Label>
                                <b>
                                    <asp:Label ID="LabelFormat" runat="server" Visible="false"></asp:Label></b>

                            </div>

                            <div class="row" id="submitbtn" runat="server">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" Text="View" ValidationGroup="Val1" OnClick="btnView_Click" />
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
                                        <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" PostBackUrl="~/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
