﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_frmConsumableStockRequestSummary
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Dim Req_Id As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Req_Id = Request.QueryString("Prd_id")
        If Not IsPostBack Then
            BindGrid(Req_Id)
        End If
    End Sub
    Private Sub BindGrid(ByVal Req_id As String)

        Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        'param(0).Value = PrdId
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Req_id
        ObjSubSonic.BindGridView(gvItems, "GET_CONSUMABLE_STOCK_BY_REQID", param)

    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid(Req_Id)

    End Sub
End Class
