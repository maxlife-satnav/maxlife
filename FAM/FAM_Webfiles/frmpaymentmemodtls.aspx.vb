Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_frmpaymentmemodtls
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim MReq_id As String
    Dim MPAYMENT_MODE As String = ""


    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))


    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))


    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))


    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                GetDetails()
            End If
        End If
    End Sub


    Private Sub GetDetails()
        MReq_id = Request.QueryString("id")

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = MReq_id
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_AMG_ITEM_PO_MEMO_DTLS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            MPAYMENT_MODE = ds.Tables(0).Rows(0).Item("PAYMENT_MODE")
            lblDDChqNumber.Text = ds.Tables(0).Rows(0).Item("CHQNUMBER")
            lblchqDt.Text = ds.Tables(0).Rows(0).Item("CHQDT")
            lblBankName.Text = ds.Tables(0).Rows(0).Item("BANKNAME")
            lblBranchName.Text = ds.Tables(0).Rows(0).Item("BRANCHNAME")
            lblAccountNumber.Text = ds.Tables(0).Rows(0).Item("AC_NUMBER")

            If MPAYMENT_MODE = "Cheque" Then
                lblHeadTitle.Text = "Cheque Details"
                lblHeadTitle1.Text = "Cheque Details"
                trDDChkDetails.Visible = True
                lbltitle.Text = "Cheque Details"
                lblDt.Text = "Cheque Date"
                lblNumber.Text = "Cheque Number"
                TRACNUMBER.Visible = False
            ElseIf MPAYMENT_MODE = "DD" Then
                lblHeadTitle.Text = "DD Details"
                lblHeadTitle1.Text = "DD Details"
                trDDChkDetails.Visible = True
                lbltitle.Text = "DD Details"
                lblDt.Text = "DD Date"
                lblNumber.Text = "DD Number"
                TRACNUMBER.Visible = False
            ElseIf MPAYMENT_MODE = "NEFT_RTGS" Then
                lblHeadTitle.Text = "NEFT/RTGS Details"
                lblHeadTitle1.Text = "NEFT/RTGS Details"
                trDDChkDetails.Visible = True
                lbltitle.Text = "NEFT / RTGS Details"
                lblDt.Text = "NEFT/RTGS Date"
                lblNumber.Text = " IFCS CODE "
                TRACNUMBER.Visible = True
            End If


        End If



    End Sub

End Class
