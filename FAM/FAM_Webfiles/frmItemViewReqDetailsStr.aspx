<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmItemViewReqDetailsStr.aspx.vb" Inherits="FAM_FAM_Webfiles_frmItemViewReqDetailsStr" Title="SM Approval" %>

<%@ Register Src="../../Controls/ItemViewReqDetailsStr.ascx" TagName="ItemViewReqDetailsStr"
    TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Consumable Requisition Approval 2
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" ValidationGroup="Val1" />
                            <uc1:ItemViewReqDetailsStr ID="ItemViewReqDetailsStr1" runat="server" />                         
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



