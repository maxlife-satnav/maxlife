Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_MyAssets
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim Req_id As String

   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            Req_id = Request.QueryString("Req_id")
            GetDetailsByRequistion(Req_id)
        End If
    End Sub
    Private Sub GetDetailsByRequistion(ByVal Req_id As String)
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Req_id
        ds = objsubsonic.GetSubSonicDataSet("GET_AdminALLSURRENDERREQ_byReq", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblAstCode.Text = ds.Tables(0).Rows(0).Item("AAT_AST_CODE")
            lblAstName.Text = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
            lblLocation.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
            lblTower.Text = ds.Tables(0).Rows(0).Item("TWR_NAME")
            lblFloor.Text = ds.Tables(0).Rows(0).Item("FLR_NAME")
            lblAstAllocDt.Text = ds.Tables(0).Rows(0).Item("AAT_ALLOCATED_DATE")
            lblAstSurDt.Text = ds.Tables(0).Rows(0).Item("AAT_SURRENDERED_DATE")
            lblSREQ_RMAPPROVAL_DATE.Text = ds.Tables(0).Rows(0).Item("SREQ_RMAPPROVAL_DATE")
            lblRM_NAME.Text = ds.Tables(0).Rows(0).Item("RM_NAME")
            lblRMRemarks.Text = ds.Tables(0).Rows(0).Item("SREQ_RM_REMARKS")
            lblSurReq_id.Text = Req_id
            lblAstDate.Text = ds.Tables(0).Rows(0).Item("AAT_UPT_DT")

        End If
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmViewadminSurrenderRequisitions.aspx")
    End Sub
End Class
