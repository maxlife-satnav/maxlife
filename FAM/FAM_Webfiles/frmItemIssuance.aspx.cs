﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Commerce.Common;

partial class FAM_FAM_Webfiles_frmItemIssuance : System.Web.UI.Page
{

    clsSubSonicCommonFunctions ObjSubsonic = new clsSubSonicCommonFunctions();
    string ReqID;
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (Session["uid"].ToString() == "")
        {
            Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
        }
        else
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                string UID = Session["uid"].ToString();
                BindUsers(UID);
                BindLocation();
                BindCategories();
                btnsubmit.Visible = true;
                //pnlItems.Visible = False
                fillgrid();

            }
        }
    }


    private void BindUsers(string aur_id)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200);
        param[0].Value = aur_id;
        ObjSubsonic.Binddropdown(ref ddlEmp, "AMT_BINDUSERS_SP", "NAME", "AUR_ID", param);

        ListItem li = ddlEmp.Items.FindByValue(aur_id);
        if ((li != null))
        {
            li.Selected = true;
        }
    }

    private void BindCategories()
    {
        //GetChildRows("0")
        //ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "USP_GET_ASSETCATEGORIESSCON_CON");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlAstCat.DataSource = sp.GetDataSet();
        ddlAstCat.DataTextField = "VT_TYPE";
        ddlAstCat.DataValueField = "VT_CODE";
        ddlAstCat.DataBind();
        ddlAstCat.Items.Insert(0, "--Select--");
    }
    private void getassetsubcategory()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AST_GET_SUBCATBYVENDORS");
        sp.Command.AddParameter("@VT_CODE", ddlAstCat.SelectedItem.Value, DbType.String);
        ddlAstSubCat.DataSource = sp.GetDataSet();
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME";
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE";
        ddlAstSubCat.DataBind();
        ddlAstSubCat.Items.Insert(0, "--Select--");
        pnlItems.Visible = false;
    }
    private void getbrandbycatsubcat()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AST_GET_MAKEBYCATSUBCAT");
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAstCat.SelectedItem.Value, DbType.String);
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String);
        ddlAstBrand.DataSource = sp.GetDataSet();
        ddlAstBrand.DataTextField = "manufacturer";
        ddlAstBrand.DataValueField = "manufactuer_code";
        ddlAstBrand.DataBind();
        ddlAstBrand.Items.Insert(0, "--Select--");
        pnlItems.Visible = false;
    }

    protected void ddlAstCat_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (ddlAstCat.SelectedIndex > 0)
        {
            pnlItems.Visible = false;
            getassetsubcategory();
        }
    }
    protected void ddlAstSubCat_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (ddlAstCat.SelectedIndex > 0)
        {
            getbrandbycatsubcat();
        }

    }
    protected void ddlAstBrand_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (ddlAstCat.SelectedIndex > 0)
        {
            // getmakebycatsubcat()
            gvItems.Visible = true;
            pnlItems.Visible = true;
            fillgrid();
        }
    }

    public void bindgrid()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AST_GET_CONSUMABLES_FOR_ASSETGRID");
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String);
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String);
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String);
        gvItems.DataSource = sp.GetDataSet();
        gvItems.DataBind();
    }


    private void fillgrid()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AST_GET_CONSUMABLES_FOR_ASSET_ISSUANCE");
        pnlItems.Visible = true;
        gvItems.DataSource = sp.GetDataSet();
        gvItems.DataBind();

    }

    private void BindLocation()
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@dummy", SqlDbType.NVarChar, 100);
        param[0].Value = "1";
        ObjSubsonic.Binddropdown( ref ddlAstIssuence, "GET_LOCTION_ISSUANCES", "LCM_NAME", "LCM_CODE", param);
    }

    protected void txtDiscPer_TextChanged(object sender, EventArgs e)
    {
        TextBox txtDiscPer = (TextBox)sender;
        GridViewRow gvRow = (GridViewRow)txtDiscPer.Parent.Parent;
        TextBox txtQty = (TextBox)gvRow.FindControl("txtQty");
        Label lblTotPrice = (Label)gvRow.FindControl("lblTotPrice");
        TextBox txtprice = (TextBox)gvRow.FindControl("txtPrice");
        int price = 0;

        if (txtDiscPer != null)
        {
            price = Convert.ToInt32(txtQty.Text) * Convert.ToInt32(txtprice.Text);
            lblTotPrice.Text = (price - Convert.ToDouble(price * Convert.ToDouble(txtDiscPer.Text) / 100)).ToString();
        }
        else
        {
            lblMsg.Text = "Please enter discount percentage";
        }
    }

    protected void txtQty_TextChanged(object sender, EventArgs e)
    {
        TextBox txtQty = (TextBox)sender;
        GridViewRow gvRow = (GridViewRow)txtQty.Parent.Parent;
        TextBox txtDiscPer = (TextBox)gvRow.FindControl("txtDiscPer");
        Label lblTotPrice = (Label)gvRow.FindControl("lblTotPrice");
        TextBox txtprice = (TextBox)gvRow.FindControl("txtPrice");
        Label lblTotavblQty = (Label)gvRow.FindControl("lblTotavblQty");
        int price = 0;

        if (Convert.ToInt32(lblTotavblQty.Text) >= Convert.ToInt32(txtQty.Text))
        {
            if (txtDiscPer != null)
            {
                price = Convert.ToInt32(txtQty.Text) * Convert.ToInt32(txtprice.Text);
                double discount = price * Convert.ToDouble(!object.ReferenceEquals(txtDiscPer.Text, string.Empty) ? txtDiscPer.Text : "0");
                lblTotPrice.Text = (price - (discount / 100)).ToString();
            }
            else
            {
                lblMsg.Text = "Please enter discount percentage";
            }
        }
        else
        {
            lblMsg.Text = "Please enter quantity less than available quantity";
        }
    }


    protected void btnsubmit_Click(object sender, System.EventArgs e)
    {

        int count = 0;
        string Model1 = "0";
        string ReqID = "0";
        foreach (GridViewRow row in gvItems.Rows)
        {
            CheckBox chkSelect = (CheckBox)row.FindControl("chkSelect");
            Label lblITEMCODE = (Label)row.FindControl("AST_MD_ID");
            // model name
            TextBox txtQty = (TextBox)row.FindControl("txtQty");
            // qty requested
            Model1 = lblITEMCODE.Text;

            if (chkSelect.Checked)
            {
                count = count + 1;
                break; // TODO: might not be correct. Was : Exit For
            }
        }
        if (count > 0)
        {
            ReqID = InsertData(txtRem.Text, Model1);
            if (ReqID != "0")
            {
                foreach (GridViewRow row in gvItems.Rows)
                {
                    CheckBox chkSelect = (CheckBox)row.FindControl("chkSelect");
                    Label lblITEMCODE = (Label)row.FindControl("AST_MD_ID");
                    // model name
                    TextBox txtQty = (TextBox)row.FindControl("txtQty");
                    // qty requested
                    //Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)


                    if (chkSelect.Checked)
                    {
                        ////// need to write  the code
                    }
                }

                send_mail(ReqID);
                Response.Redirect("frmAssetThanks.aspx?RID=" + ReqID);
            }
            else
            {
                lblMsg.Text = "Error has occured! Please try again";
            }
        }
        else
        {
            lblMsg.Text = "Please select atleast one asset to assign";
        }
    }

    private string InsertData(string remarks, string mdid)
    {

        SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AST_CONSUMABLES_ISSUANCE");
        sp1.Command.AddParameter("@MODID", mdid, DbType.String);
        sp1.Command.AddParameter("@AurId", Session["UID"], DbType.String);
        sp1.Command.AddParameter("@Remarks", remarks, DbType.String);
        string retval = sp1.ExecuteScalar().ToString();
        return retval;
    }

    private void InsertDetails(string ReqId, string modid, int qty, int unitprice, double discount, double totprice)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AST_CONSUMABLES_ISSUANCE_DTLS");
        sp.Command.AddParameter("@REQID", ReqId, DbType.String);
        sp.Command.AddParameter("@MODID", modid, DbType.String);
        sp.Command.AddParameter("@Qty", qty, DbType.Int32);
        sp.Command.AddParameter("@UNITPRICE", unitprice, DbType.Int32);
        sp.Command.AddParameter("@DISCOUNT", discount, DbType.Double);
        sp.Command.AddParameter("@TOTPRICE", totprice, DbType.Double);
        sp.Command.AddParameter("@POSTAT", rdbpo.Checked, DbType.Boolean);
        sp.ExecuteScalar();
    }

    public void send_mail(string ReqId)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "SEND_MAIL_ASSET_REQUISITION");
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String);
        sp.Execute();
    }
   
}
