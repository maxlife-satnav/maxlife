<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMapped_Unmapped_AstDispose.aspx.vb"
    Inherits="FAM_FAM_Webfiles_frmMapped_Unmapped_AstDispose"
    Title="Mapped and Unmapped Asset Dispose" %>

<%@ Register Src="../../Controls/Mapped_Unmapped_AstDispose.ascx" TagName="Mapped_Unmapped_AstDispose" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Mapped & Unmapped Asset Dispose 
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                            <uc1:Mapped_Unmapped_AstDispose ID="Mapped_Unmapped_AstDispose1" runat="server"></uc1:Mapped_Unmapped_AstDispose>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Employee Details</h4>
                </div>
                <div id="modelcontainer">
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {
            //$("#modelcontainer").load("frmaddroleviewusers.aspx?ROL_ID=" + id, function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal().fadeIn();
            //});
            $("#modalcontentframe").attr("src", "frmMapped_Unmapped_AstDisposeDTLS.aspx?astid=" + id);
            $("#myModal").modal().fadeIn();

            return false;
        }
    </script>
</body>
</html>


