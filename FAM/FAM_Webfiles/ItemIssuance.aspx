﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Item Issuance</title>
    <link href="../../BootStrapCSS/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/amantra.min.css" rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/jtable/themes/jqueryui/jtable_jqueryui.css" rel="stylesheet" />
    <%--<link href="../../BootStrapCSS/jtable/themes/metro/blue/jtable.css" rel="stylesheet" />--%>


    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Consumable Item Issuance
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Employee Id</label>
                                            <div class="col-md-7">
                                                <select id="ddlEmp" class="selectpicker" data-live-search="true">
                                                    <option value='--Select--'>--Select--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Issuence Location<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <select id="ddlLocation" class="selectpicker" data-live-search="true" required="required">
                                                    <option value='--Select--'>--Select--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Category<span style="color: red;">*</span></label>

                                            <div class="col-md-7">
                                                <select id="ddlAstCat" class="selectpicker" data-live-search="true" required="required">
                                                    <option value='--Select--'>--Select--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Sub Category<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <select id="ddlAstSubCat" class="selectpicker" data-live-search="true" required="required">
                                                    <option value='--Select--'>--Select--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <select id="ddlAstBrand" class="selectpicker" data-live-search="true" required="required">
                                                    <option value='--Select--'>--Select--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Invoice If Needed</label>
                                            <div class="col-md-7">
                                                <input type="checkbox" id="rdbpo" value="Invoice" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pnlastlistmodel" class="modal fade">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Select List of Models</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="panel-body">
                                                <%--<table id="tblitemList" class="table table-condensed table-bordered table-hover table-striped">
                                                    <tr>
                                                        <th>Category</th>
                                                        <th>Sub-Category</th>
                                                        <th>Brand/Make</th>
                                                        <th>Model</th>
                                                        <th>
                                                            <input type="checkbox" id="chkall" class="check" />
                                                            Select All</th>
                                                    </tr>
                                                </table>--%>
                                                <div id="PersonTable" class="table table-condensed table-bordered table-hover table-striped" style="margin: auto;"></div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <a id="btnSubmit" class='btn btn-primary'><span class='glyphicon glyphicon-refresh'></span>Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <%--<table id="tblselectedlist" class="table table-condensed table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>Item Name</th>
                                            <th>Sub Category Name</th>
                                            <th>Price</th>
                                            <th>Total Available</th>
                                            <th>Quantity Issued</th>
                                            <th>Discount(%)</th>
                                            <th>Total Price</th>
                                            <th><a href="#" class="add"><span class='glyphicon glyphicon-refresh' aria-hidden="true"></span></a></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind='foreach: lines'>
                                        <tr>
                                            <td class='Item' data-bind='with: product'>
                                                <span data-bind='text: formatCurrency(Item)'></span>
                                            </td>
                                            <td class='Subcategory' data-bind='with: product'>
                                                <span data-bind='text: formatCurrency(Subcategory)'></span>
                                            </td>
                                            <td class='price' data-bind='with: product'>
                                                <span data-bind='text: formatCurrency(price)'></span>
                                            </td>
                                            <td class='Avbl' data-bind='with: product'>
                                                <span data-bind='text: formatCurrency(quantity)'></span>
                                            </td>
                                            <td class='quantity'>
                                                <input data-bind='visible: product, value: quantity, valueUpdate: "afterkeyup"' class="form-control" />
                                            </td>
                                            <td class='Discount'>
                                                <input data-bind='visible: product, value: Discount, valueUpdate: "afterkeyup"' class="form-control" />
                                            </td>
                                            <td class='TotalPrice'>
                                                <span data-bind='visible: product, text: formatCurrency(subtotal())'></span>
                                            </td>
                                            <td>
                                                <a href='#' data-bind='click: $parent.removeLine'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>--%>
                                <div id="divitemlist">
                                    <table id="tblselectedlist" data-bind='visible: Assets().length > 0' class="table table-condensed table-bordered table-hover table-striped">
                                        <tr>
                                            <th>Item Name</th>
                                            <th>Sub Category Name</th>
                                            <th>Price</th>
                                            <th>Total Available</th>
                                            <th>Quantity Issued</th>
                                            <th>Discount(%)</th>
                                            <th>Total Price</th>
                                            <th><a href="#" class="add"><span class='glyphicon glyphicon-refresh' aria-hidden="true"></span></a></th>
                                        </tr>
                                        <tbody data-bind='foreach: Assets'>
                                            <tr>
                                                <td class='item'>
                                                    <span data-bind='text: item'></span></td>

                                                <td class='subcat'>
                                                    <span data-bind='text: subcat'></span></td>

                                                <td class='price'>
                                                    <span data-bind='text: price'></span></td>

                                                <td class='totavbl'>
                                                    <span data-bind='text: totavbl'></span></td>
                                                <%-- <td>
                                                <input class="form-control required number" data-bind='value: AAC_CON_TOTAVBL, uniqueName: true' /></td>--%>

                                                <td class='quantity'>
                                                    <input data-bind='value:quantity , valueUpdate: "afterkeydown" ' class="form-control required number" />
                                                </td>
                                                <td class='Discount'>
                                                    <input data-bind='value:Discount , valueUpdate: "afterkeydown"' class="form-control required number" />
                                                </td>
                                                <td class='TotalPrice'>
                                                    <span data-bind='text: subtotal()' class="required number"></span>
                                                </td>
                                                <td class="id"><a href='#' data-bind='value:Id, click: $root.removeAsset'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a></td>
                                            </tr>
                                        </tbody>

                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button data-bind='enable: Assets().length > 0, click: save' type='submit' class='btn btn-primary'>Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="InvoiceModel" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Invoice for Consumable Item Issuance</h4>
                </div>
                <div id="modelcontainer">
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
<script src="../../BootStrapCSS/knockout-3.3.0.js"></script>
<script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
<script src="../../BootStrapCSS/jtable/jquery-ui-1.11.2.js"></script>
<script src="../../BootStrapCSS/jtable/jquery.jtable.js"></script>
<script src="../../BootStrapCSS/Scripts/bootstrap.min.js"></script>
<script src="../../BootStrapCSS/Scripts/bootstrap-select.min.js"></script>

<script>
    function showPopWin(id) {
        alert("done");
        //$("#modelcontainer").load("frmaddroleviewusers.aspx?ROL_ID=" + id, function (responseTxt, statusTxt, xhr) {
        //    $("#myModal").modal().fadeIn();
        //});
        $("#modalcontentframe").attr("src", "IssuanceInvoice.aspx?REQID=" + id);
        $("#InvoiceModel").modal().fadeIn();
        return false;
    }
</script>
<script>


    // ------------- Loading/Submitting values in the selected items using Knockout js ------------
    var AssetLine = function (Asset) {
        var self = this;
        self.Id = ko.observable(Asset.AST_MD_ID);
        self.item = ko.observable(Asset.AST_MD_NAME);
        self.subcat = ko.observable(Asset.AST_SUBCAT_NAME);
        self.price = ko.observable(Asset.AMG_VENCAN_RT);
        self.totavbl = ko.observable(Asset.AAC_CON_TOTAVBL);
        self.quantity = ko.observable(1);
        self.Discount = ko.observable(0);
        self.TotalPrice = ko.observable(0);
        self.subtotal = ko.pureComputed(function () {
            return self.price() * parseInt("0" + self.quantity(), 10) - (self.price() * parseInt("0" + self.quantity(), 10) * parseInt("0" + self.Discount())) / 100;
        });

        // Whenever the category changes, reset the product selection

    };

    var AssetModel = function (Assets) {
        var self = this;
        self.Assets = ko.observableArray();
        $.each(Assets, function (key, value) {
            self.Assets.push(new AssetLine(value))
            //self.Assets = ko.observableArray([new AssetLine(value)]);
        });

        self.removeAsset = function (Asset) {
            self.Assets.remove(Asset);
        };
        self.save = function () {
            var issuance = 0;
            if ($('#rdbpo').is(':checked')) {
                issuance = 1;
            }
            else {
                issuance = 0;
            }

            var dataToSave = $.map(self.Assets(), function (Asset) {
                return {
                    Id: Asset.Id(),
                    Item: Asset.item(),
                    Subcat: Asset.subcat(),
                    price: Asset.price(),
                    totavbl: Asset.totavbl(),
                    quantity: Asset.quantity(),
                    Discount: Asset.Discount(),
                    location: $("#ddlLocation").val(),
                    userid: $("#ddlEmp").val(),
                    IssueStat: issuance
                }
            });
            //alert("Could now send this to server: " + JSON.stringify(dataToSave));

            var dataObject = { Jsondata: (dataToSave) };
            $.ajax({
                type: "POST",
                url: '../../api/ItemIssuanceAPI/Submitdetails',
                contentType: 'application/json',
                data: JSON.stringify(dataObject),
                success: function (result) {
                    if ($('#rdbpo').is(':checked')) {
                        showPopWin(result);
                    }
                }
            });

        };
    };

    // ------------- Ends Here ------------
    $(document).ready(function () {
        $("#divitemlist").hide();
        $.getJSON('../../api/ItemIssuanceAPI/GetallEmployeeIDDS', function (result) {
            $.each(result, function (key, value) {
                $("#ddlEmp").append($("<option></option>").val(value.AUR_ID).html(value.Name));
            });
            $("#ddlEmp").selectpicker('refresh');
        });

        $.getJSON('../../api/ItemIssuanceAPI/GetallLocations', function (result) {
            $.each(result, function (key, value) {
                $("#ddlLocation").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
            $("#ddlLocation").selectpicker('refresh');
        });

        $.getJSON('../../api/ItemIssuanceAPI/GetallCategories', function (result) {
            $.each(result, function (key, value) {
                $("#ddlAstCat").append($("<option></option>").val(value.VT_CODE).html(value.VT_TYPE));
            });
            if ($('#ddlAstCat option').length > 2) {
                ($("<option></option>").val("--All--").html("--All--")).insertAfter("#ddlAstCat option:first");      // add items after the first item
            }
            $("#ddlAstCat").selectpicker('refresh');
        });

        $("#ddlAstCat").change(function () {
            var dataObject = { Category: $("#ddlAstCat").val() };
            $.ajax({
                type: "POST",
                url: '../../api/ItemIssuanceAPI/Getallsubcatbycat',
                contentType: 'application/json',
                data: JSON.stringify(dataObject),
                success: function (result) {
                    $('#ddlAstSubCat option').remove()                                           // remove the contents of the dropdownlist
                    $("#ddlAstSubCat").append($("<option></option>").val("--Select--").html("--Select--"));
                    $.each(result, function (key, value) {
                        $("#ddlAstSubCat").append($("<option></option>").val(value.AST_SUBCAT_CODE).html(value.AST_SUBCAT_NAME));
                    });
                    if ($('#ddlAstSubCat option').length > 2) {
                        ($("<option></option>").val("--All--").html("--All--")).insertAfter("#ddlAstSubCat option:first");      // add items after the first item
                    }
                    $("#ddlAstSubCat").selectpicker('refresh');
                },
                dataType: "json"
            });
        });

        $("#ddlAstSubCat").change(function () {
            var dataObject = { Category: $("#ddlAstCat").val(), SubCategory: $("#ddlAstSubCat").val() };
            $('#ddlAstBrand option').remove()                                           // remove the contents of the dropdownlist
            $.post("../../api/ItemIssuanceAPI/GetallBrandbysubcat", dataObject, function (result) {
                $("#ddlAstBrand").append($("<option></option>").val("--Select--").html("--Select--"));

                $.each(result, function (key, value) {
                    $("#ddlAstBrand").append($("<option></option>").val(value.manufactuer_code).html(value.manufacturer));
                });
                if ($('#ddlAstBrand option').length > 2) {
                    ($("<option></option>").val("--All--").html("--All--")).insertAfter("#ddlAstBrand option:first");      // add items after the first item
                }
                $("#ddlAstBrand").selectpicker('refresh');      // refresh the css to bind drop down
            });
        });

        $("#ddlAstBrand").change(function () {
            if ($("#ddlAstBrand").val() != '--Select--') {
                reloadtbl();
            }
        });

        //$('#tblselectedlist').on('click', 'tr a.remove', function (e) {
        //    e.preventDefault();
        //    $(this).closest('tr').remove();
        //});

        $('#tblselectedlist').on('click', 'tr a.add', function (e) {
            reloadtbl()
        });

        function reloadtbl() {
            $('#PersonTable').jtable('load', { Category: $("#ddlAstCat").val(), SubCategory: $("#ddlAstSubCat").val(), Brand: $("#ddlAstBrand").val() });
            $("#pnlastlistmodel").modal('show');
        };

        // ------------ Jtable Loading --------------------
        var arr = new Array();
        $('#PersonTable').jtable({
            title: 'Item List',
            paging: true, //Enables paging
            pageSize: 10, //Actually this is not needed since default value is 10.
            selecting: true, //Enable selecting
            multiselect: true, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column
            sorting: true, //Enable sorting
            defaultSorting: 'MDNAME ASC', //Sort by Name by default

            actions: {
                listAction: function (postData, jtParams) {
                    return $.Deferred(function ($dfd) {
                        $.ajax({
                            url: '../../api/ItemIssuanceAPI/GetallItemstotbl?jtStartIndex= ' + jtParams.jtStartIndex + '&jtPageSize=' + jtParams.jtPageSize + '&jtSorting=' + jtParams.jtSorting,
                            type: 'POST',
                            dataType: 'json',
                            data: postData,
                            success: function (data) {
                                $dfd.resolve(data);
                            },
                            error: function () {
                                $dfd.reject();
                            }
                        });
                    });
                }
            },

            fields: {
                AST_MD_ID: {
                    title: 'ID',
                    width: '15%',
                    list: false
                },
                BRAND: {
                    title: 'Brand'
                },
                MDCODE: {
                    title: 'Model code',
                    width: '15%',
                    list: false
                },
                MDNAME: {
                    title: 'Model Name'
                },
                SUBCAT: {
                    title: 'Sub Category'
                },
                SUBCATID: {
                    title: 'Sub category ID',
                    width: '15%',
                    list: false
                },
                UNITS: {
                    title: 'Units',
                    width: '15%'
                }
            },

            selectionChanged: function () {
                //Get all selected rows
                var $selectedRows = $('#PersonTable').jtable('selectedRows');

                //$('#SelectedRowList').empty();
                if ($selectedRows.length > 0) {
                    //Show selected rows
                    $selectedRows.each(function () {
                        var record = $(this).data('record');

                        arr.push(+record.AST_MD_ID);
                        //$('#SelectedRowList').append(
                        //    'AST_MD_ID: ' + record.AST_MD_ID +
                        //    'MDNAME:' + record.MDNAME
                        //    );
                    });
                } else {
                    //No rows selected
                    //$('#SelectedRowList').append('No row selected! Select rows to see here...');
                }
            }
        });
        // ------------ Jtable ends here--------------------

        $('#btnSubmit').click(function () {
            var objarrayvalues = { modelarray: arr };
            $.post("../../api/ItemIssuanceAPI/Getallselectedmodels", objarrayvalues, function (result) {
                alert(JSON.parse(result));
                var viewModel = new AssetModel(JSON.parse(result));
                ko.applyBindings(viewModel);
            });
            $("#pnlastlistmodel").modal('hide');
            $("#divitemlist").show("slow");
        });
    });


</script>
