<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewAssetsinStock.aspx.vb" Inherits="FAM_FAM_Webfiles_frmViewAssetsinStock" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>

    <form id="form1" class="form-horizontal well" runat="server">

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
                </div>
            </div>
        </div>
        <div id="pnlItems" runat="server"> 
            <div class="row" style="margin-top: 10px">
                <div class="col-md-11">
                    <fieldset>
                        <legend>Asset Intra Movement</legend>
                        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false" EmptyDataText="No Asset(s) Found."
                            CssClass="table table-condensed table-bordered table-hover table-striped">
                            <Columns>
                                <asp:TemplateField HeaderText="Asset">
                                    <ItemStyle Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblasset" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Asset Name">
                                    <ItemStyle Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblassetName" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Floor" Visible="false">
                                    <ItemStyle Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblfloor" runat="server" Text='<%#Eval("FLR_NAME") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tower" Visible="false">
                                    <ItemStyle Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbltower" runat="server" Text='<%#Eval("TWR_NAME") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Location">
                                    <ItemStyle Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbllocation" runat="server" Text='<%#Eval("LCM_CODE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lbllocationname" runat="server" Text='<%#Eval("LOCATION") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="City">
                                    <ItemStyle Width="10%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblcity" runat="server" Text='<%#Eval("CITY") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Employee">
                                    <ItemStyle Width="10%" />
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlemp" runat="server" CssClass="selectpicker" data-live-search="true" Width="100%">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                                    <ItemStyle Width="5%" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                    </fieldset>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="pnlItemsInter" runat="server" visible="false">
            <div class="row" style="margin-top: 10px">
                <div class="col-md-11 ">
                    <fieldset>
                        <legend>Asset Inter Movement</legend>
                        <asp:GridView ID="gvItemsInter" runat="server" AllowPaging="true" AutoGenerateColumns="false" EmptyDataText="No Asset(s) Found."
                            CssClass="table table-condensed table-bordered table-hover table-striped">
                            <Columns>
                                <asp:TemplateField HeaderText="Location">
                                    <ItemStyle Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbllocation" runat="server" Text='<%#Eval("LCM_CODE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lbllocationname" runat="server" Text='<%#Eval("LOCATION") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Asset">
                                    <ItemStyle Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblasset" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Asset Name">
                                    <ItemStyle Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblassetName" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                                    <ItemStyle Width="5%" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                    </fieldset>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <asp:Button ID="btnSubmitInter" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

