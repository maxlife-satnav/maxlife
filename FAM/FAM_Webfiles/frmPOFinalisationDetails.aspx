<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPOFinalisationDetails.aspx.vb" Inherits="FAM_FAM_Webfiles_frmPOFinalisationDetails"
    Title="PO Finalisation" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Finalize PO
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>

                            <div id="pnlItems" runat="server">
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:BoundField DataField="sku" HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="productname" HeaderText="Asset Name" ItemStyle-HorizontalAlign="left" />
                                                <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQty" runat="server" MaxLength="10" Text='<%#Eval("AIPD_QTY") %>'
                                                            ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                        <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("ProductId") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unit Price" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtUnitPrice" runat="server" ReadOnly="true" CssClass="form-control" MaxLength="10" Text='<%#formatnumber(Eval("AIPD_RATE"),2) %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty To Purchase" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPurchaseQty" ReadOnly="true" runat="server" CssClass="form-control" MaxLength="10" Text='<%#Eval("AIPD_ACT_QTY") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-md-3">
                                            </div>
                                            <label class="col-md-3 control-label"><strong>TAX DETAILS</strong></label>
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-3">
                                                <strong>PAYMENT TERMS</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Entry Tax</label>
                                            <asp:RequiredFieldValidator ID="rfvtax" runat="server" Display="Dynamic" ErrorMessage="Enter Tax"
                                                ValidationGroup="Val1" ControlToValidate="txtFFIP"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtFFIP" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Advance</label>
                                            <asp:RequiredFieldValidator ID="rfvadvance" runat="server" Display="Dynamic"
                                                ErrorMessage="Advance" ValidationGroup="Val1" ControlToValidate="txtAdvance" ReadOnly="true"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtAdvance" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">CST/ST</label>
                                            <asp:RequiredFieldValidator ID="rfvcst" runat="server" Display="Dynamic" ErrorMessage="CST/ST"
                                                ValidationGroup="Val1" ControlToValidate="txtCst"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtCst" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">On Delivery</label>
                                            <asp:RequiredFieldValidator ID="rfvdelivery" runat="server" Display="Dynamic" ErrorMessage="On Delivery"
                                                ValidationGroup="Val1" ControlToValidate="txtDelivery"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtDelivery" runat="server" CssClass="form-control" ReadOnly="true"> </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">VAT</label>
                                            <asp:RequiredFieldValidator ID="rfvvat" runat="server" Display="Dynamic" ErrorMessage="VAT"
                                                ValidationGroup="Val1" ControlToValidate="txtWst"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtWst" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Installation</label>
                                            <asp:RequiredFieldValidator ID="rfvinstallation" runat="server" Display="Dynamic" ErrorMessage="Installation"
                                                ValidationGroup="Val1" ControlToValidate="txtInstallation"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtInstallation" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Octroi</label>
                                            <asp:RequiredFieldValidator ID="rfvoctroi" runat="server" Display="Dynamic" ErrorMessage="Octrai"
                                                ValidationGroup="Val1" ControlToValidate="txtOctrai"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtOctrai" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Commissioning</label>
                                            <asp:RequiredFieldValidator ID="rfvcomm" runat="server" Display="Dynamic" ErrorMessage="Commissioning"
                                                ValidationGroup="Val1" ControlToValidate="txtCommissioning"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtCommissioning" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Service Tax</label>
                                            <asp:RequiredFieldValidator ID="rfvservice" runat="server" Display="Dynamic" ErrorMessage="Service Tax"
                                                ValidationGroup="Val1" ControlToValidate="txtServiceTax"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtServiceTax" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Retention</label>
                                            <asp:RequiredFieldValidator ID="rfvretention" runat="server" Display="Dynamic" ErrorMessage="Retention"
                                                ValidationGroup="Val1" ControlToValidate="txtRetention"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRetention" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Others</label>
                                            <asp:RequiredFieldValidator ID="rfvothers" runat="server" Display="Dynamic" ErrorMessage="Others"
                                                ValidationGroup="Val1" ControlToValidate="txtOthers"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtOthers" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Payments</label>
                                            <asp:RequiredFieldValidator ID="rfvpayments" runat="server" Display="Dynamic" ErrorMessage="Payments"
                                                ValidationGroup="Val1" ControlToValidate="txtPayments"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtPayments" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Button ID="btnTotalCost" runat="server" Text="Get Total Cost" Visible="false" CssClass="btn btn-primary custom-button-color" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Total Cost</label>
                                            <asp:RequiredFieldValidator ID="rfvcose" runat="server" Display="Dynamic" ErrorMessage="Total Cost"
                                                ValidationGroup="Val1" ControlToValidate="txtTotalCost"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtTotalCost" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Vendor Name</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                                <asp:TextBox ID="txtVenCode" runat="server" CssClass="form-control" Visible="false" ReadOnly="True"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Expected Date Of Delivery</label>
                                            <%--  <asp:RequiredFieldValidator ID="rfvdate"  runat="server" Display="Dynamic" ErrorMessage="Expected Date"
                                                    ValidationGroup="Val1" ControlToValidate="txtDOD"></asp:RequiredFieldValidator>--%>
                                            <div class="col-md-7">
                                                <%--<ew:CalendarPopup ID="txtDOD" runat="server">
                                                    </ew:CalendarPopup>--%>
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="txtDOD" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Exchange Charges</label>

                                            <asp:RequiredFieldValidator ID="rfvcharges" runat="server" Display="Dynamic" ErrorMessage="Extra Charges"
                                                ValidationGroup="Val1" ControlToValidate="txtExchange"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtExchange" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvremarks" runat="server" Display="None" ErrorMessage="Please Enter Remarks"
                                                ValidationGroup="Val1" ControlToValidate="txtRemarks"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="multiline" ValidationGroup="Val1"> </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color" Visible="false" />
                                        <asp:Button ID="btnUpdate" Text="Update and Approve" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                                        <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnBack_Click" />
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
