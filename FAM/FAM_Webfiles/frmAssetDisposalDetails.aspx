<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetDisposalDetails.aspx.vb" Inherits="frmAssetDisposalDetails" %>

<%@ Register Src="~/Controls/ucAssetDisposalDetails.ascx" TagName="ucDisposaldetails" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Asset Disposal Report 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" />
                        <uc1:ucDisposaldetails ID="ucDisposaldetaisl" runat="server" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

<script src="../../Scripts/moment.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ddlRange").val('');
            rangeChanged();
        });

        function rangeChanged() {
            var selVal = $("#ddlRange").val();
            switch (selVal) {
                case 'TODAY':
                    $("#ucDisposaldetaisl_FromDate").val(moment().format('DD-MMM-YYYY'));
                    $("#ucDisposaldetaisl_txtToDate").val(moment().format('DD-MMM-YYYY'));
                    break;
                case 'YESTERDAY':
                    $("#ucDisposaldetaisl_FromDate").val(moment().subtract(1, 'days').format('DD-MMM-YYYY'));
                    $("#ucDisposaldetaisl_txtToDate").val(moment().subtract(1, 'days').format('DD-MMM-YYYY'));
                    break;
                case '7':
                    $("#ucDisposaldetaisl_FromDate").val(moment().subtract(6, 'days').format('DD-MMM-YYYY'));
                    $("#ucDisposaldetaisl_txtToDate").val(moment().format('DD-MMM-YYYY'));
                    break;
                case '30':
                    $("#ucDisposaldetaisl_FromDate").val(moment().subtract(29, 'days').format('DD-MMM-YYYY'));
                    $("#ucDisposaldetaisl_txtToDate").val(moment().format('DD-MMM-YYYY'));
                    break;
                case 'THISMONTH':
                    $("#ucDisposaldetaisl_FromDate").val(moment().startOf('month').format('DD-MMM-YYYY'));
                    $("#ucDisposaldetaisl_txtToDate").val(moment().endOf('month').format('DD-MMM-YYYY'));
                    break;
                case 'LASTMONTH':
                    $("#ucDisposaldetaisl_FromDate").val(moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY'));
                    $("#ucDisposaldetaisl_txtToDate").val(moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY'));
                    break;
                case '':
                    $("#ucDisposaldetaisl_FromDate").val('');
                    $("#ucItemViewSMReport1_txtToDate").val('');
                    break;
            }
        }

        $("#ddlRange").change(function () {
            rangeChanged();
        });
    </script>

