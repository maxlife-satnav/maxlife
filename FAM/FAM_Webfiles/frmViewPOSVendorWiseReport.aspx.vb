﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization


Partial Class FAM_FAM_Webfiles_frmViewPOSVendorWiseReport
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getvendors()
            BindGrid()
        End If
    End Sub
    Private Sub getvendors()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_VENDORS")
        sp.Command.AddParameter("@dummy", 0, DbType.Int32)
        ddlVendor.DataSource = sp.GetDataSet()
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, "--All--")

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        BindGrid()


    End Sub
    Public Sub BindGrid()

        Dim Mven As String = ""
        If ddlVendor.SelectedValue = "--All--" Then
            Mven = ""
        Else
            Mven = ddlVendor.SelectedValue
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@VEN_CODE", SqlDbType.NVarChar)
        param(0).Value = Mven
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("USP_AMG_ITEM_PO_GetByStatusIdFinalPOS_VendorWise", param)
        Dim rds As New ReportDataSource()
        'rds.Name = "ViewPOsVendorWise"
        rds.Name = "ViewPORptDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        'ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/ViewPOsVendorWiseReport.rdlc")
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/ViewPOReport.rdlc")
        ReportViewer1.LocalReport.EnableHyperlinks = True

        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p2 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())

        ReportViewer1.LocalReport.SetParameters(p2)
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub
End Class
