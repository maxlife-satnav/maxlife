<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewadminSurrenderRequisitions.aspx.vb"
    Inherits="FAM_FAM_Webfiles_frmViewadminSurrenderRequisitions" Title="View RM Approved Surrender Requistions" %>

<%@ Register Src="../../Controls/ViewSurrenderRequisitions_admin.ascx" TagName="ViewSurrenderRequisitions_admin" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>View RM Approved Asset Surrender Requisitions
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <uc1:ViewSurrenderRequisitions_admin ID="ViewSurrenderRequisitions_admin1" runat="server" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
