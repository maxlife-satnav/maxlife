Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_InterMovementRequisition1
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim AAS_SNO, AAS_AAT_CODE, AAS_BDG_ID, AAS_FLR_ID, LCM_CODE, LCM_NAME, AAT_EMP_ID As String
    Dim Twr_code, Flr_code As String
    Dim strLCM_CODE As String
    Dim strTWR_CODE As String
    Dim strfloor As String
    Dim StrEmp As String
    Dim Strast As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindLocation()
                getassetcategory()
                BindDestLoactions()
                'BindAssets()
            End If
        End If
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
        ddlAssetCategory.SelectedIndex = 1

        ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
        DisableDestinationLoc()
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ddlAstSubCat.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", categorycode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        ddlModel.Items.Clear()
        'ddlAstBrand.Items.Clear()
        getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
        DisableDestinationLoc()
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        ddlAstBrand.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        'ddlModel.Items.Clear()
        getmakebycatsubcat()
        DisableDestinationLoc()
    End Sub

    Private Sub getmakebycatsubcat()
        ddlModel.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        DisableDestinationLoc()
    End Sub

    Private Sub DisableDestinationLoc()
        Try
            If ddlLocation.SelectedIndex > 0 Then
                ddlDestLoc.Items.FindByValue(ddlLocation.SelectedValue).Attributes.Add("disabled", "disabled")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindDestLoactions()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_LOCATIONS")
            ddlDestLoc.DataSource = sp.GetDataSet()
            ddlDestLoc.DataTextField = "LCM_NAME"
            ddlDestLoc.DataValueField = "LCM_CODE"
            ddlDestLoc.DataBind()
            ddlDestLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CAPASSETS_FOR_INTER_MOVEMENT")
            sp.Command.AddParameter("@LOCTN_CODE", ddlLocation.SelectedValue, DbType.String)
            sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedValue, DbType.String)
            sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedValue, DbType.String)
            sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedValue, DbType.String)
            sp.Command.AddParameter("@AST_MODEL", ddlModel.SelectedValue, DbType.String)
            'sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            grdAssets.DataSource = ds
            grdAssets.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        BindGrid()
        btnAstReq.Visible = IIf(grdAssets.Rows.Count > 0, True, False)
    End Sub

    'Protected Sub grdAssets_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
    '    grdAssets.PageIndex = e.NewPageIndex
    '    BindGrid()
    'End Sub

    Public Sub send_mail(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_MOVEMENT_REQUEST")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Execute()
    End Sub

    'Private Function GenerateRequestId(ByVal LocId As String, ByVal TowerId As String, ByVal FloorId As String, ByVal EmpId As String) As String
    '    Dim ReqId As String = ""
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_MMT_INTRA_MVMT_REQ_GetMaxMMR_ID")
    '    Dim SNO As String = CStr(sp.ExecuteScalar())
    '    ReqId = "MMR/" + LocId + "/" + EmpId + "/" + SNO
    '    Return ReqId
    'End Function

    Private Sub getRequestDetails(ByVal ReqId As String, ByVal strAst As ArrayList, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        SendMail(ReqId, strAst, Session("uid"), txtRemarks.Text, MailStatus, App_Rej_status)
    End Sub

    Private Sub SendMail(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal strRemarks As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        'lblMsg.Text = st
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet
            Dim strRMAur_id As String
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = ObjSubsonic.GetSubSonicDataSet("GET_ASSET_INTER_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strRMAur_id = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_ID")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If


            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty

            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = ObjSubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If

            Dim p1
            p1 = AstList.Item(0).ToString.Split(",")
            strAstList = "<table width='100%' border='1'>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Asset Code</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlLocation.SelectedValue & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTWR_CODE & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strfloor & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Person To Receive Assets</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & StrEmp & "</td></tr>"


            Dim strApproveList As String = String.Empty
            Dim strRejectList As String = String.Empty

            Dim AppLink As String = String.Empty
            AppLink = ConfigurationManager.AppSettings("AppLink").ToString

            strApproveList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=ap&pg=interit> Click to Approve </a></td></tr></table>"
            strRejectList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=rj&pg=interit>  Click to Reject </a></td></tr></table>"
            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strRemarks)
            body = body.Replace("@@RM", strKnownas)
            body = body.Replace("@@Approve", strApproveList)
            body = body.Replace("@@Reject", strRejectList)
            '1. Request Approved copy goes to request raised person.
            '2. One copy for IT ADMIN to approve or reject.

            If App_Rej_status = False Then
                Insert_AmtMail(body, strRR, strSubject, strRM)
            Else
                Insert_AmtMail(body, strRM, strSubject, "")
            End If

        Catch ex As Exception

            Throw (ex)
        End Try
    End Sub

    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "InterMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = DateTime.Now
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub

    Protected Sub btnAstReq_Click(sender As Object, e As EventArgs) Handles btnAstReq.Click
        'USP_MMT_INTRA_MVMT_REQ_Insert
        Dim AstParid As String

        'PARENT
        Dim reqid As String = "MMR" + "/" + ddlLocation.SelectedValue + "/" + Session("uid") + "/"
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ASSET_MVMT_REQUISITION")
        sp.Command.AddParameter("@CREATEDBY", Session("uid"), DbType.String)
        sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@REQID", reqid, DbType.String)
        sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
        sp.Command.AddParameter("@FROMLOC", ddlLocation.SelectedValue, DbType.String)
        sp.Command.AddParameter("@DESTLOC", ddlDestLoc.SelectedValue, DbType.String)
        AstParid = sp.ExecuteScalar()

        'CHILD
        For Each row As GridViewRow In grdAssets.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked Then
                Dim assetcode As Label = DirectCast(row.FindControl("lblAstCode"), Label)
                Dim assetname As Label = DirectCast(row.FindControl("lblAssetname"), Label)
                Dim astComments As TextBox = DirectCast(row.FindControl("txtcomments"), TextBox)
                ' Dim splitAssetname As String() = assetname.Text.Split("/")
                ' splitAssetname(0) = ddlDestLoc.SelectedValue
                ' Dim modyAssetName As String = String.Join("/", splitAssetname.ToArray())

                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & ".USP_MMT_INTRA_MVMT_REQ_Insert" & "")
                sp1.Command.AddParameter("@MMR_REQ_ID", AstParid, DbType.String)
                'sp1.Command.AddParameter("@ASSETNAME", modyAssetName, DbType.String)
                sp1.Command.AddParameter("@ASSETCODE", assetcode.Text, DbType.String)
                sp1.Command.AddParameter("@RAISEDBY", Session("uid"), DbType.String)
                sp1.Command.AddParameter("@COMMENT", astComments.Text, DbType.String)
                sp1.ExecuteScalar()
            End If
        Next
        Response.Redirect("frmAssetThanks.aspx?RID=intermovement&reqid=" + AstParid + "")
    End Sub

End Class
