Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class FAM_FAM_Webfiles_frmAssetReqReportDtls1
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not IsPostBack Then
                BindUsers()
                BindCategories()
                pnlItems.Visible = True
                BindRequisition(Request.QueryString("REQID"))
                'btnPrint.Attributes.Add("OnClick", "JavaScript:printPartOfPage('Div1')")
            End If
        End If
    End Sub
    Private Sub BindCategories()
        ObjSubsonic.Binddropdown(ddlAstCat, "GET_TYPES", "VT_TYPE", "VT_CODE")
        ' GetChildRows("1")
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    'Private Sub GetChildRows(ByVal i As String)
    '    'Dim str As String = ""
    '    'Dim id = CType(i, Integer)
    '    ''If i = "0" Then
    '    ''    id = CType(i, Integer)
    '    ''Else
    '    ''    Dim id1 As Array = Split(i, "~")
    '    ''    str = id1(0).ToString & "  --"
    '    ''    id = CType(id1(1), Integer)
    '    ''End If
    '    'Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
    '    'Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM " & Session("TENANT") & "." & "types WHERE ParentId = " & id, objConn)
    '    'Dim ds As New DataSet
    '    'da.Fill(ds)
    '    'If ds.Tables(0).Rows.Count > 0 Then
    '    '    For Each dr As DataRow In ds.Tables(0).Rows
    '    '        Dim j As Integer = CType(dr("CategoryId"), Integer)
    '    '        If id = 0 Then
    '    '            str = ""
    '    '        End If
    '    '        Dim li As ListItem = New ListItem(dr("CategoryName").ToString, dr("CategoryId"))
    '    '        ddlAstCat.Items.Add(li)
    '    '        'GetChildRows(str & "~" & j)
    '    '    Next
    '    'End If

    'End Sub

    Private Sub BindRequisition(ByVal reqid As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetByReqId")
            sp.Command.AddParameter("@ReqId", reqid, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = reqid

                Dim RaisedBy As Integer = 0
                Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                Dim CatId As String = ""
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)
                CatId = dr("AIR_ITEM_TYPE").ToString()
                li = ddlAstCat.Items.FindByValue(CStr(CatId))
                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = False

                txtRemarks.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")

                If String.IsNullOrEmpty(Trim(txtRemarks.Text)) Then
                    t4.Visible = True
                Else
                    t4.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                    tr1.Visible = False
                Else
                    tr1.Visible = True

                End If
                If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                    tr2.Visible = False
                Else
                    tr2.Visible = True
                End If

                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                BindGrid3(li.Value)
                For Each row As GridViewRow In gvpritems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
                    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)

                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_GetDetailsByReqIdAndProductId")
                    sp1.Command.AddParameter("@ReqId", reqid, DbType.String)
                    sp1.Command.AddParameter("@ProductId", CInt(lblProductId.Text), DbType.Int32)
                    Dim dr1 As SqlDataReader = sp1.GetReader
                    If dr1.Read() Then
                        chkSelect.Checked = True
                        txtQty.Text = dr1("AID_QTY")
                        If txtQty.Text = "NULL" Or txtQty.Text = "" Or txtQty.Text = 0 Then
                            row.Visible = False
                        Else
                            row.Visible = True
                        End If
                    Else
                        row.Visible = False
                    End If
                Next

            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindUsers()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_GetUserDtls_SP")
        ddlEmp.DataSource = sp.GetReader
        ddlEmp.DataTextField = "Name"
        ddlEmp.DataValueField = "AUR_ID"
        ddlEmp.DataBind()
        ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindGrid3(ByVal li As String)
        'Dim CatId As Integer = 0
        'Integer.TryParse(li, CatId)
        'gvpritems.DataSource = ProductController.GetByCategoryID_DataSet(CatId)
        'gvpritems.DataBind()
        'pnlItems.Visible = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSETREQ_DETAILS")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("REQID"), DbType.String)
        gvpritems.DataSource = sp.GetDataSet()
        gvpritems.DataBind()
    End Sub

    'Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click

    'End Sub
End Class
