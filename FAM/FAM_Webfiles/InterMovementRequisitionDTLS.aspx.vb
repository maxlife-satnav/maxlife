Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_InterMovementRequisitionDTLS
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim MMR_AST_CODE, MMR_FROMBDG_ID, MMR_FROMFLR_ID, LCM_CODE, LCM_NAME As String
    Dim TOEMP_ID As String
    Dim strLCM_CODE As String
    Dim strTWR_CODE As String
    Dim strfloor As String
    Dim StrEmp As String
    Dim Strast As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDetails()
        End If
    End Sub

    Public Sub BindDetails()
        Dim staid As Integer = 0
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Trim(Request.QueryString("Req_id"))

        Dim dr As SqlDataReader
        dr = ObjSubsonic.GetSubSonicDataReader("GET_REQID_InterMOVEMENTS_byREQ_ID", param)
        If dr.Read Then
            MMR_AST_CODE = dr.Item("MMR_AST_CODE")
            MMR_FROMBDG_ID = dr.Item("MMR_FROMBDG_ID")
            MMR_FROMFLR_ID = dr.Item("MMR_FROMFLR_ID")
            LCM_CODE = dr.Item("FLOC_CODE")
            LCM_NAME = dr.Item("FLOC_NAME")
            TOEMP_ID = dr.Item("TOEMP_ID")

            BindLocation(LCM_CODE)
            '  BindTower(MMR_FROMBDG_ID)
            ' BindFloor(MMR_FROMFLR_ID)
            'ddlEmp.Items.Insert(0, New ListItem(dr("MMR_TOEMP_ID"), "0"))
            BindAssets()
            ddlAsset.ClearSelection()
            Emp_Loadddl(ddlEmp, dr("TOEMP_ID"))
            ddlAsset.Items.FindByValue(LTrim(RTrim(dr("MMR_AST_CODE")))).Selected = True
            staid = dr.Item("MMR_LRSTATUS_ID")
            If staid = 1 Or staid > 1019 Then
                btnSubmit.Visible = False
                ddlAsset.Enabled = False
                ddlEmp.Enabled = False
                ddlSLoc.Enabled = False
                txtRemarks.Enabled = False
            Else
                btnSubmit.Visible = True
                ddlAsset.Enabled = True
                ddlEmp.Enabled = True
                ddlSLoc.Enabled = True
                txtRemarks.Enabled = True
            End If
            'ddlSLoc.Enabled = False
            'ddlSTower.Enabled = False
            'ddlSFloor.Enabled = False
            'ddlEmp.Enabled = False
            'ddlAsset.Enabled = False
        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If


    End Sub
    Private Sub Emp_Loadddl(ByVal ddlEmp As DropDownList, ByVal stremp As String)
        Try
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@loc", DbType.String)
            param(0).Value = ddlSLoc.SelectedItem.Value
            param(1) = New SqlParameter("@tow", DbType.String)
            param(1).Value = ""
            param(2) = New SqlParameter("@flr", DbType.String)
            param(2).Value = ""
            ObjSubsonic.Binddropdown(ddlEmp, "GET_LOCATION_EMPLOYEES", "AUR_FIRST_NAME", "AUR_ID", param)
            ddlEmp.Items.FindByValue(stremp).Selected = True
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLocation(ByVal strlcm_code As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlSLoc.DataSource = sp.GetDataSet()
            ddlSLoc.DataTextField = "LCM_NAME"
            ddlSLoc.DataValueField = "LCM_CODE"
            ddlSLoc.DataBind()
            ddlSLoc.Items.Insert(0, New ListItem("--Select--", "0"))

            ddlSLoc.Items.FindByValue(strlcm_code).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Private Sub BindTower(ByVal strtwr_code As String)
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCTWR")
    '        sp.Command.AddParameter("@dummy", ddlSLoc.SelectedItem.Value, DbType.String)
    '        ddlSTower.DataSource = sp.GetDataSet()
    '        ddlSTower.DataTextField = "TWR_NAME"
    '        ddlSTower.DataValueField = "TWR_CODE"
    '        ddlSTower.DataBind()
    '        ddlSTower.Items.Insert(0, New ListItem("--Select--", "0"))
    '        ddlSTower.Items.FindByValue(strtwr_code).Selected = True
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Private Sub BindFloor()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TWRFLR")
    '        sp.Command.AddParameter("@dummy", ddlSLoc.SelectedItem.Value, DbType.String)
    '        sp.Command.AddParameter("@dummy1", ddlSTower.SelectedItem.Value, DbType.String)
    '        ddlSFloor.DataSource = sp.GetDataSet()
    '        ddlSFloor.DataTextField = "FLR_NAME"
    '        ddlSFloor.DataValueField = "FLR_CODE"
    '        ddlSFloor.DataBind()
    '        ddlSFloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub



    Private Sub BindAssets()
        Try
            
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@loc", DbType.String)
            param(0).Value = ddlSLoc.SelectedItem.Value
            param(1) = New SqlParameter("@tow", DbType.String)
            param(1).Value = ""
            param(2) = New SqlParameter("@flr", DbType.String)
            param(2).Value = ""
            ObjSubsonic.Binddropdown(ddlAsset, "GET_ALLLOCATION_ASSETS", "AAT_NAME", "AAT_CODE", param)
 
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub



    'Private Sub BindLocations(ByVal LCM_CODE As String)
    '    ObjSubsonic.Binddropdown(ddlSLoc, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")
    '    ddlSLoc.Items.FindByValue(LCM_CODE).Selected = True
    'End Sub

    'Private Sub BindTower(ByVal AAS_BDG_ID As String)
    '    '----------- Binding Tower -----------------------
    '    If ddlSLoc.SelectedIndex > 0 Then
    '        Dim LocCode As String = ddlSLoc.SelectedItem.Value
    '        BindTowersByLocation(LocCode, ddlSTower)
    '    End If
    '    ddlSTower.Items.FindByValue(AAS_BDG_ID).Selected = True
    '    '-------------------------------------------------
    'End Sub
    Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
        param(0).Value = LocCode
        ObjSubsonic.Binddropdown(ddl, "USP_TOWER_GETBYLOCATION", "TWR_NAME", "TWR_CODE", param)
    End Sub


    'Public Sub BindFloor(ByVal AAS_FLR_ID As String)
    '    If ddlSTower.SelectedIndex > 0 Then
    '        Dim TwrCode As String = ddlSTower.SelectedItem.Value
    '        BindFloorsByTower(TwrCode, ddlSFloor)
    '    End If
    '    ddlSFloor.Items.FindByValue(AAS_FLR_ID).Selected = True
    'End Sub

    Private Sub BindEmp()

        ObjSubsonic.Binddropdown(ddlEmp, "GET_USERS", "AUR_FIRST_NAME", "AUR_ID")
        ddlEmp.Items.FindByValue(TOEMP_ID).Selected = True
    End Sub

    Private Sub BindFloorsByTower(ByVal TwrCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@TwrCode", DbType.String)
        param(0).Value = TwrCode
        ObjSubsonic.Binddropdown(ddl, "USP_FLOOR_GETBYTOWER", "FLR_NAME", "FLR_CODE", param)
    End Sub
    'Private Function GenerateRequestId(ByVal TowerId As String, ByVal FloorId As String, ByVal EmpId As String) As String
    '    Dim ReqId As String = ""
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_INTRA_MVMT_REQ_GetMaxMMR_ID")
    '    Dim SNO As String = CStr(sp.ExecuteScalar())
    '    ReqId = "MMR/" + TowerId + "/" + FloorId + "/" + EmpId + "/" + SNO
    '    Return ReqId
    'End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim strASSET_LIST As New ArrayList

        Dim Location As String = ""
        Dim Tower As String = ""
        Dim Floor As String = ""
        Dim DLocation As String = ""
        Dim DTower As String = ""
        Dim DFloor As String = ""
        Dim PersonName As String = ddlEmp.SelectedItem.Value
        Dim Remarks As String = Trim(txtRemarks.Text)
        Dim ReqId As String = ""

        If ddlEmp.SelectedIndex > 0 Then

        Else
            lblMsg.Text = "Please select a Employee."
            Exit Sub
        End If





        strLCM_CODE = ddlSLoc.SelectedItem.Value
        strTWR_CODE = ddlSLoc.SelectedItem.Value
        strfloor = ""
        StrEmp = ddlEmp.SelectedItem.Value
        Strast = ddlAsset.SelectedItem.Value

        ReqId = Trim(Request.QueryString("Req_id"))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_INTRA_MVMT_REQ_UPDATE")
        sp.Command.AddParameter("@MMR_REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MMR_AST_CODE", Strast, DbType.String)
        sp.Command.AddParameter("@MMR_FROMBDG_ID", strTWR_CODE, DbType.String)
        sp.Command.AddParameter("@MMR_FROMFLR_ID", strfloor, DbType.String)
        sp.Command.AddParameter("@MMR_RAISEDBY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@MMR_RECVD_BY", StrEmp, DbType.String)
        sp.Command.AddParameter("@MMR_MVMT_TYPE", "", DbType.String)
        sp.Command.AddParameter("@MMR_MVMT_PURPOSE", "", DbType.String)
        sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
        sp.Command.AddParameter("@MMR_COMMENTS", Remarks, DbType.String)
        sp.Command.AddParameter("@MMR_FROMEMP_ID", 0, DbType.String)
        sp.Command.AddParameter("@MMR_ITEM_REQUISITION", Strast, DbType.String)

        sp.ExecuteScalar()



        Dim i As Integer = 0

        strASSET_LIST.Insert(i, Strast & "," & strTWR_CODE & "," & strfloor & "," & StrEmp & "," & txtRemarks.Text)

       ' Dim MailTemplateId As Integer
       ' MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetInterMovementRequisition_employee"))
       ' getRequestDetails(ReqId, strASSET_LIST, MailTemplateId, False)

       ' MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetInterMovementRequisition_it_appoval_reject"))
       ' getRequestDetails(ReqId, strASSET_LIST, MailTemplateId, True)

        Response.Redirect("frmAssetThanks.aspx?RID=intermovement")
    End Sub


    Private Sub getRequestDetails(ByVal ReqId As String, ByVal strAst As ArrayList, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
       
        SendMail(ReqId, strAst, Session("uid"), txtRemarks.Text, MailStatus, App_Rej_status)
    End Sub


    Private Sub SendMail(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal strRemarks As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        'lblMsg.Text = st
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet
            Dim strRMAur_id As String
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = objsubsonic.GetSubSonicDataSet("GET_ASSET_INTER_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strRMAur_id = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_ID")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If


            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty


            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = objsubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If
 


            Dim p1
            p1 = AstList.Item(0).ToString.Split(",")
            strAstList = "<table width='100%' border='1'>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Asset Code</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(0) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(3) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Person To Receive Assets</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlEmp.SelectedItem.Text & "</td></tr>"


            Dim strApproveList As String = String.Empty
            Dim strRejectList As String = String.Empty

            Dim AppLink As String = String.Empty
            AppLink = ConfigurationManager.AppSettings("AppLink").ToString


            strApproveList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=ap&rl=rm&ty=imr> Click to Approve </a></td></tr></table>"

            strRejectList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=rj&rl=rm&ty=imr>  Click to Reject </a></td></tr></table>"

            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strRemarks)
            body = body.Replace("@@RM", strKnownas)
            body = body.Replace("@@Approve", strApproveList)
            body = body.Replace("@@Reject", strRejectList)



            '1. IT Request Approved copy goes to request raised person.
            '2. One copy for IT ADMIN to approve.

            'If App_Rej_status = True Then
            Insert_AmtMail(body, strRR, strSubject, strRM)
            'Else
            'Insert_AmtMail(body, strRM, strSubject, "")
            'End If






        Catch ex As Exception

            Throw (ex)
        End Try


    End Sub

    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "InterMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = getoffsetdatetime(DateTime.Now)
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub


    'Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click

    '    Dim strASSET_LIST As New ArrayList
    '    Dim param_Details(0) As SqlParameter
    '    param_Details(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
    '    param_Details(0).Value = Trim(Request.QueryString("Req_id"))

    '    Dim ds As New DataSet
    '    ds = objsubsonic.GetSubSonicDataSet("GET_REQID_InterMOVEMENTS_byREQ_ID", param_Details)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        MMR_AST_CODE = ds.Tables(0).Rows(0).Item("MMR_AST_CODE")
    '        MMR_FROMBDG_ID = ds.Tables(0).Rows(0).Item("MMR_FROMBDG_ID")
    '        MMR_FROMFLR_ID = ds.Tables(0).Rows(0).Item("MMR_FROMFLR_ID")
    '        LCM_CODE = ds.Tables(0).Rows(0).Item("FLOC_CODE")
    '        LCM_NAME = ds.Tables(0).Rows(0).Item("FLOC_NAME")
    '        TOEMP_ID = ds.Tables(0).Rows(0).Item("TOEMP_ID")
    '    End If


    '    Dim param(3) As SqlParameter
    '    param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
    '    param(0).Value = Request.QueryString("Req_id")
    '    param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
    '    param(1).Value = Session("uid")
    '    param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
    '    param(2).Value = txtRemarks.Text
    '    param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
    '    param(3).Value = 1025
    '    ObjSubsonic.GetSubSonicExecute("UPDATE_INTER_MVMT_REQ_ITSTATUS", param)



    '    Dim i As Integer = 0

    '    strASSET_LIST.Insert(i, MMR_AST_CODE & "," & LCM_NAME & "," & MMR_FROMBDG_ID & "," & MMR_FROMFLR_ID & "," & txtRemarks.Text & "," & ddlEmp.SelectedItem.Text)

    '    Dim MailTemplateId As Integer
    '    MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetInterMovementRequisition_it_reject"))
    '    getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)




    '    Response.Redirect("frmAssetThanks.aspx?RID=InterMVMTITRej")
    'End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("ViewInterMovementRequisition.aspx")
    End Sub

    'Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
    '    Try
    '        If ddlSLoc.SelectedIndex > 0 Then
    '            BindTowersByLocation(ddlSLoc.SelectedItem.Value, ddlSTower)

    '        Else
    '            ddlSTower.Items.Clear()
    '            ddlSTower.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlSTower.SelectedValue = 0
    '            ddlSTower.Items.Clear()
    '            ddlSTower.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlSTower.SelectedValue = 0

    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'Protected Sub ddlSTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTower.SelectedIndexChanged
    '    Try
    '        If ddlSTower.SelectedIndex > 0 Then
    '            BindFloorsByTower(ddlSTower.SelectedItem.Value, ddlSFloor)
    '        Else
    '            ddlSFloor.Items.Clear()
    '            ddlSFloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlSFloor.SelectedValue = 0
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'Protected Sub ddlSFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSFloor.SelectedIndexChanged
    '    Try
    '        If ddlSFloor.SelectedIndex > 0 Then
    '            BindAssets()
    '            'EmpLoad()

    '        Else

    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
End Class

