﻿Imports System.Data

Partial Class FAM_FAM_Webfiles_AssetLabelFormat
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lblMsg.Visible = False
            EditData()
            bindlabel()

        End If
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If CheckLoc.Checked = False And CheckCat.Checked = False And CheckSubcat.Checked = False And CheckBrand.Checked = False And CheckModel.Checked = False Then
            lblMsg.Visible = True
            LabelFormat.Visible = False
            lblMsg.Text = "Please Select Atleast One Value"
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_BSM_INSERT")
            sp.Command.AddParameter("@LOC", CheckLoc.Checked, DbType.Boolean)
            sp.Command.AddParameter("@CAT", CheckCat.Checked, DbType.Boolean)
            sp.Command.AddParameter("@SUBCAT", CheckSubcat.Checked, DbType.Boolean)
            sp.Command.AddParameter("@BRAND", CheckBrand.Checked, DbType.Boolean)
            sp.Command.AddParameter("@MODEL", CheckModel.Checked, DbType.Boolean)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.ExecuteScalar()
            lblMsg.Visible = True
            bindlabel()
            lblMsg.Text = "Asset Label Inserted Successfully"
        End If
    End Sub

    Protected Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        lblMsg.Text = ""
        bindlabel()

    End Sub
    Private Sub bindlabel()
        Dim AstCode As String = "LocationName" + "/" + "Category" + "/" + "SubCategoryName" + "/" + "BrandName" + "/" + "Model" + "/" + "00001"
        'If CheckLoc.Checked = False And CheckCat.Checked = False And CheckSubcat.Checked = False And CheckBrand.Checked = False And CheckModel.Checked = False Then
        '    lblMsg.Visible = True
        '    LabelFormat.Visible = False
        '    lblMsg.Text = "Please Select Atleast One Value"
        'Else
        If CheckLoc.Checked = False Then
            AstCode = Replace(AstCode, "LocationName/", "")
        End If

        If CheckCat.Checked = False Then
            AstCode = Replace(AstCode, "Category/", "")
        End If
        If CheckSubcat.Checked = False Then
            AstCode = Replace(AstCode, "SubCategoryName/", "")
        End If
        If CheckBrand.Checked = False Then
            AstCode = Replace(AstCode, "BrandName/", "")
        End If
        If CheckModel.Checked = False Then
            AstCode = Replace(AstCode, "Model/", "")
        End If

        LabelFormat.Visible = True
        LabelFormat.Text = AstCode
        ' End If

    End Sub

    Private Sub EditData()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_BSM_EDIT")
        Dim dt As DataTable
        dt = sp.GetDataSet().Tables(0)
        If dt.Rows.Count = 0 Then
            CheckLoc.Checked = False
            CheckSubcat.Checked = False
            CheckCat.Checked = False
            CheckBrand.Checked = False
            CheckModel.Checked = False
            lblMsg.Visible = False

        Else
            If dt.Rows(0).Item("AST_BSM_LOCATION") = "1" Then
                CheckLoc.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_CAT") = "1" Then
                CheckCat.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_SUB_CAT") = "1" Then
                CheckSubcat.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_BRAND") = "1" Then
                CheckBrand.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_MODEL") = "1" Then
                CheckModel.Checked = True
            End If
        End If
    End Sub
End Class
