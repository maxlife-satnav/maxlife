<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetReqReportDtls.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAssetReqReportDtls1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="row">
        <div class="col-sm-12">
            <form id="form1" class="form-horizontal well" runat="server">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row">
                                <asp:Label ID="lblMsg" runat="server" CssClass="col-sm-12 control-label" ForeColor="Red">
                                </asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-5 control-label">Requisition Id:</label>
                                <div class="col-sm-7">
                                    <asp:Label ID="lblReqId" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-5 control-label">Raised By:</label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddlEmp" runat="server" CssClass="btn dropdown-toggle selectpicker btn-default" data-live-search="true" Enabled="false" Width="100%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--</div>--%>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-5 control-label">Asset Category:</label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddlAstCat" runat="server" CssClass="btn dropdown-toggle selectpicker btn-default" data-live-search="true" Enabled="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px" id="pnlItems" runat="server">
                    <div class="col-sm-12">
                        <asp:GridView ID="gvpritems" runat="server" AllowPaging="true" AutoGenerateColumns="true"
                            EmptyDataText="No records found.." CssClass="table table-condensed table-bordered table-hover table-striped">
                            <%-- <Columns>
                                                <asp:BoundField DataField="sku" HeaderText="sku" ItemStyle-HorizontalAlign="left" ReadOnly="true" />
                                                <asp:BoundField DataField="productname" HeaderText="Name" ItemStyle-HorizontalAlign="left" ReadOnly="true" />
                                                <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" ReadOnly="true"></asp:TextBox>
                                                        <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("ProductId") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" Enabled="false"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>--%>
                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-5 control-label">Status:</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row" id="t4" runat="server">

                                <label class="col-sm-5 control-label">Requestor Remarks:</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row" id="tr1" runat="server">

                                <label class="col-sm-5 control-label">RM Remarks:</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row" id="tr2" runat="server">
                                <label class="col-sm-5 control-label">Admin Remarks:</label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
