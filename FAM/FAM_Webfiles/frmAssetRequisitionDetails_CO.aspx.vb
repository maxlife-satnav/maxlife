Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_frmAssetRequisitionDetails_CO
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim UID As String
    Dim ds As DataSet
    Dim changedDt As DataTable
    Dim CachedInterGrid As DataTable
    Dim dtInterGrid As New DataTable
    Dim dtIntraGrid As New DataTable
    Dim CachedtIntraGrid As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then

                UID = Session("uid")
                BindUsers(UID)
                BindCategories()
                pnlItems.Visible = True
                BindRequisition()
                lblMsg.Visible = False
            Else
                Dim btnSubmitText = Request.Form("btnSubmit")
                'Dim eTarget = Request.Params("__EVENTTARGET").ToString()
                'Dim buttonClicked = eTarget.Substring(eTarget.LastIndexOf("$") + 1).Equals("btnSubmit", StringComparison.OrdinalIgnoreCase)

                If btnSubmitText = Nothing Then
                    Cache.Remove("CdtInterGrid")
                    Cache.Remove("CdtIntraGrid")
                End If

                'If (Not btnSubmitText.Equals("Update and Approve")) Then

                'End If
            End If
        End If
    End Sub
    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function
    Private Sub BindRequisition()
        Dim ReqId As String = Request("RID")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_REQUISITION_GetByReqId")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                Dim RaisedBy As Integer = 0
                Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim CatId As Integer = 0
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)

                'li = ddlAstCat.Items.FindByValue(CStr(CatId))
                'If Not li Is Nothing Then
                '    li.Selected = True
                'End If
                'ddlAstCat.Enabled = False
                Dim CatId As String

                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = False


                getassetsubcategory(CatId)
                ddlAstSubCat.Enabled = False
                Dim asstsubcat As String = dr("AIR_ITEM_SUBCAT")

                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)

                Dim asstbrand As String = dr("AIR_ITEM_BRD")
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                ddlAstBrand.Enabled = False
                txtRemarks.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")


                BindLocation()

                Dim Location As String = dr("AIR_REQ_LOC")
                ddlLocation.Items.FindByValue(Location).Selected = True
                ddlLocation.Enabled = False



                txtRemarks.ReadOnly = True
                txtRMRemarks.ReadOnly = True
                txtAdminRemarks.ReadOnly = True
                txtStatus.ReadOnly = True

                If String.IsNullOrEmpty(Trim(txtRemarks.Text)) Then
                    trRemarks.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                    trRMRemarks.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                    trAdminRemarks.Visible = False
                End If

                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                'If RaisedBy = GetCurrentUser() Then
                '    If StatusId = 1001 Or StatusId = 1002 Then
                '        btnSubmit.Enabled = True
                '        btnCancel.Enabled = True
                '    Else
                '        btnSubmit.Enabled = False
                '        btnCancel.Enabled = False
                '    End If
                'Else
                '    btnSubmit.Enabled = False
                '    btnCancel.Enabled = False
                'End If
                'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
                'sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
                'gvItems.DataSource = sp1.GetDataSet
                'gvItems.DataBind()

                bindgrid(ReqId)

                For i As Integer = 0 To gvItems.Rows.Count - 1
                    Dim lblStock As Label = DirectCast(gvItems.Rows(i).FindControl("lblStock"), Label)
                    Dim txtStockQty As TextBox = DirectCast(gvItems.Rows(i).FindControl("txtStockQty"), TextBox)
                    Dim txtPurchaseQty As TextBox = DirectCast(gvItems.Rows(i).FindControl("txtPurchaseQty"), TextBox)
                    Dim txtQty As TextBox = DirectCast(gvItems.Rows(i).FindControl("txtQty"), TextBox)
                    Dim lblApprQty As Label = DirectCast(gvItems.Rows(i).FindControl("lblApprQty"), Label)

                    If lblStock.Text = "0" Or lblStock.Text = "" Then
                        txtStockQty.Enabled = False
                    Else
                        txtStockQty.Enabled = True
                    End If
                    'If (CInt(lblApprQty.Text) <> 0 And CInt(lblStock.Text) = 0) Or (CInt(lblApprQty.Text) = 0 And CInt(lblStock.Text) = 0) Then
                    '    txtPurchaseQty.Enabled = True
                    'Else
                    '    txtPurchaseQty.Enabled = False
                    'End If
                Next
                pnlItems.Visible = True
            Else
                lblMsg.Text = "No such requisition found."
            End If
        End If
    End Sub
    Dim CoOrdinatorGrid As DataTable
    Public Sub bindgrid(ByVal ReqId As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
        sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
        CoOrdinatorGrid = sp1.GetDataSet.Tables(0)
        Cache("CoOrdinatorGrid") = CoOrdinatorGrid
        gvItems.DataSource = CoOrdinatorGrid
        gvItems.DataBind()
    End Sub

    Private Sub BindUsers(ByVal AUR_ID As String)
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMT_GetUserDtls_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        'ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
        Dim li As ListItem = ddlEmp.Items.FindByValue(AUR_ID)
        If Not li Is Nothing Then
            li.Selected = True
        End If
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = AUR_ID
        ObjSubsonic.Binddropdown(ddlEmp, "AMT_BINDUSERS_SP_Raisedby", "NAME", "AUR_ID", param)
        'Dim li As ListItem = ddlEmp.Items.FindByValue(AUR_ID)
        'If Not li Is Nothing Then
        '    li.Selected = True
        'End If





    End Sub
    Private Sub BindCategories()
        'GetChildRows("0")
        ''ddlAstCat.DataSource = CategoryController.CategoryList
        ''ddlAstCat.DataTextField = "CategoryName"
        ''ddlAstCat.DataValueField = "CategoryID"
        ''ddlAstCat.DataBind()
        ''For Each li As ListItem In ddlAstCat.Items

        ''Next
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")
    End Sub
    'Private Sub GetChildRows(ByVal i As String)
    '    Dim str As String = ""
    '    Dim id
    '    If i = "0" Then
    '        id = CType(i, Integer)
    '    Else
    '        Dim id1 As Array = Split(i, "~")
    '        str = id1(0).ToString & "  --"
    '        id = CType(id1(1), Integer)
    '    End If


    '    Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
    '    Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id, objConn)
    '    Dim ds As New DataSet
    '    da.Fill(ds)



    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For Each dr As DataRow In ds.Tables(0).Rows
    '            Dim j As Integer = CType(dr("CategoryId"), Integer)
    '            If id = 0 Then
    '                str = ""
    '            End If
    '            Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
    '            ddlAstCat.Items.Add(li)
    '            GetChildRows(str & "~" & j)
    '        Next
    '    End If
    'End Sub
    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    'Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
    '    If ddlAstCat.SelectedIndex > 0 Then
    '        Dim CatId As Integer = 0
    '        Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)
    '        gvItems.DataSource = ProductController.GetByCategoryID_DataSet(CatId)
    '        gvItems.DataBind()
    '        pnlItems.Visible = True
    '    End If
    'End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        CachedInterGrid = CType(Cache("CdtInterGrid"), DataTable)
        CachedtIntraGrid = CType(Cache("CdtIntraGrid"), DataTable)
        '********* old code *******************

        'Dim ReqId As String = GetRequestId()
        ''DeleteRequistionItems(ReqId)
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
        '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
        '    Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
        '    If txtStockQty.Text = "" Then
        '        txtStockQty.Text = 0
        '    End If
        '    Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
        '    If txtPurchaseQty.Text = "" Then
        '        txtPurchaseQty.Text = 0
        '    End If
        '    UpdateAssetRequisitionData(ReqId, CInt(Trim(lblProductId.Text)), CInt(Trim(txtStockQty.Text)), CInt(Trim(txtPurchaseQty.Text)))
        'Next
        'UpdateData(ReqId, 1010)
        'Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
        '*******************************************************

        '**************** modified on 29 aug 11 ******************
        '**************** modified on 12 may 2016 ****************

        Dim VT_Code As String
        Dim Ast_SubCat_Code As String
        Dim manufactuer_code As String
        Dim Ast_Md_code As String

        Dim ReqId As String = GetRequestId()
        Dim cnt As Integer = 0
        For Each row As GridViewRow In gvItems.Rows
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
            Dim lblStock As Label = DirectCast(row.FindControl("lblStock"), Label)
            Dim lblStockmsg As Label = DirectCast(row.FindControl("lblStockmsg"), Label)
            Dim lblApprQty As Label = DirectCast(row.FindControl("lblApprQty"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)


            Dim lbl_vt_code As Label = DirectCast(row.FindControl("lbl_vt_code"), Label)
            Dim lbl_ast_subcat_code As Label = DirectCast(row.FindControl("lbl_ast_subcat_code"), Label)
            Dim lbl_manufactuer_code As Label = DirectCast(row.FindControl("lbl_manufactuer_code"), Label)
            Dim lbl_ast_md_code As Label = DirectCast(row.FindControl("lbl_ast_md_code"), Label)


            VT_Code = lbl_vt_code.Text
            Ast_SubCat_Code = lbl_ast_subcat_code.Text
            manufactuer_code = lbl_manufactuer_code.Text
            Ast_Md_code = lbl_ast_md_code.Text

            If txtStockQty.Text = "" Then
                txtStockQty.Text = 0
            End If

            If CInt(Trim(txtStockQty.Text)) > CInt(Trim(lblStock.Text)) Then
                lblMsg.Visible = True
                lblMsg.Text = "Asset stock not available."

                Exit Sub
            End If
            If txtPurchaseQty.Text = "" Then
                txtPurchaseQty.Text = 0
            End If


            If Regex.IsMatch(txtPurchaseQty.Text, "^[0-9 ]+$") Then
                If CInt(Trim(txtPurchaseQty.Text)) > 0 Then
                    If CInt(Trim(txtPurchaseQty.Text)) <= CInt(txtQty.Text) Then
                        InsertAssetRequisitionData(ReqId, Trim(lblProductId.Text), CInt(Trim(txtQty.Text)), CInt(Trim(txtStockQty.Text)),
                                                   CInt(Trim(txtPurchaseQty.Text)), VT_Code, Ast_SubCat_Code, manufactuer_code, Ast_Md_code)
                        UpdateAssetRequisitionData(ReqId, Trim(lblProductId.Text), CInt(Trim(txtStockQty.Text)), CInt(Trim(txtPurchaseQty.Text)))
                    Else
                        lblMsg.Visible = True
                        lblMsg.Text = "Purchase qty. Should be lessthan than order qty."
                        Exit Sub
                    End If
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Qty to purchase Should be greater than zero"
                    Exit Sub
                End If
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please enter Qty to purchase in numbers..."
                Exit Sub
            End If


            If txtPurchaseQty.Text = "" Then
                txtPurchaseQty.Text = 0
            End If
            cnt = cnt + 1
        Next

        If CachedtIntraGrid Is Nothing Then
        Else
            For I As Integer = 0 To CachedtIntraGrid.Rows.Count - 1
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_AMT_ASSET_SPACE_STAT")
                sp1.Command.AddParameter("@LOC_ID", CachedtIntraGrid.Rows(I).Item("LOC_ID"), DbType.String)
                sp1.Command.AddParameter("@TWR_ID", "", DbType.String)
                sp1.Command.AddParameter("@FLR_ID", "", DbType.String)
                sp1.Command.AddParameter("@AAT_AST_CODE", CachedtIntraGrid.Rows(I).Item("AAT_AST_CODE"), DbType.String)
                sp1.ExecuteScalar()


                ' ALLOCATE_SPACEASSET
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ALLOCATE_SPACEASSET_INTRA")
                sp.Command.AddParameter("@AAT_AST_CODE", CachedtIntraGrid.Rows(I).Item("AAT_AST_CODE"), DbType.String)
                sp.Command.AddParameter("@AAT_SPC_ID", "", DbType.String)
                sp.Command.AddParameter("@AAT_EMP_ID", CachedtIntraGrid.Rows(I).Item("AAT_EMP_ID"), DbType.String)
                sp.Command.AddParameter("@AAT_ITEM_REQUISITION", CachedtIntraGrid.Rows(I).Item("AAT_ITEM_REQUISITION"), DbType.String)
                sp.ExecuteScalar()

                '--------------- updating the asset tagged table to 1035 (Assigned)-------------
                Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_ALLOCATE_ASSET")
                sp2.Command.AddParameter("@AAT_AST_CODE", CachedtIntraGrid.Rows(I).Item("AAT_AST_CODE"), DbType.String)
                sp2.Command.AddParameter("@AAT_EMP_ID", CachedtIntraGrid.Rows(I).Item("AAT_EMP_ID"), DbType.String)
                sp2.Command.AddParameter("@AAT_REQ_ID", CachedtIntraGrid.Rows(I).Item("AAT_REQ_ID"), DbType.String)
                sp2.ExecuteScalar()
            Next
        End If

        If CachedInterGrid Is Nothing Then
        ElseIf CachedInterGrid.Rows.Count > 0 Then
            'RAISE INTER MOVEMENT REQUISITION START
            Dim AstParid As String

            'PARENT
            Dim interreqid As String = "MMR" + "/" + ddlLocation.SelectedValue + "/" + Session("uid") + "/"
            Dim spParent As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ASSET_MVMT_REQUISITION")
            spParent.Command.AddParameter("@CREATEDBY", Session("uid"), DbType.String)
            spParent.Command.AddParameter("@REQID", ReqId, DbType.String)
            spParent.Command.AddParameter("@MMR_ITEM_REQUISITION", Request("RID"), DbType.String)
            spParent.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
            spParent.Command.AddParameter("@FROMLOC", ddlLocation.SelectedValue, DbType.String)
            AstParid = spParent.ExecuteScalar()



            If cnt = gvItems.Rows.Count Then
                CachedInterGrid = CType(Cache("CdtInterGrid"), DataTable)
                For I As Integer = 0 To CachedInterGrid.Rows.Count - 1
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & ".USP_MMT_INTRA_MVMT_REQ_Insert" & "")
                    sp.Command.AddParameter("@MMR_REQ_ID", AstParid, DbType.String)
                    'sp.Command.AddParameter("@MMR_AST_CODE", CachedInterGrid.Rows(I).Item("AST_MD_CODE"), DbType.String)
                    'sp.Command.AddParameter("@MMR_FROMBDG_ID", CachedInterGrid.Rows(I).Item("LCM_CODE"), DbType.String)
                    'sp.Command.AddParameter("@MMR_FROMFLR_ID", "", DbType.String)
                    'sp.Command.AddParameter("@MMR_RAISEDBY", Session("UID"), DbType.String)
                    'sp.Command.AddParameter("@MMR_RECVD_BY", Session("UID"), DbType.String)
                    'sp.Command.AddParameter("@MMR_MVMT_TYPE", "", DbType.String)
                    'sp.Command.AddParameter("@MMR_MVMT_PURPOSE", "", DbType.String)
                    'sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1017, DbType.Int32)
                    'sp.Command.AddParameter("@MMR_COMMENTS", "", DbType.String)
                    'sp.Command.AddParameter("@MMR_FROMEMP_ID", 0, DbType.String)
                    'sp.Command.AddParameter("@MMR_ITEM_REQUISITION", Request("RID"), DbType.String)
                    sp.Command.AddParameter("@ASSETCODE", CachedInterGrid.Rows(I).Item("MMR_AST_CODE"), DbType.String)
                    sp.Command.AddParameter("@RAISEDBY", Session("uid"), DbType.String)
                    sp.ExecuteScalar()
                Next

              


            Else
                Exit Sub
            End If

        End If
        Dim IntraCount As Integer
        Dim InterCount As Integer

        If CachedtIntraGrid Is Nothing Then
            IntraCount = 0
        Else
            IntraCount = CachedtIntraGrid.Rows.Count
        End If

        If CachedInterGrid Is Nothing Then
            InterCount = 0
        Else
            InterCount = CachedInterGrid.Rows.Count
        End If

        UpdateData(ReqId, 1010)
        Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId + "&IntraCount=" + Convert.ToString(IntraCount) + "&InterCount=" + Convert.ToString(InterCount))

    End Sub


    '--------- Function for Purchase item ---------------------------
    Private Sub InsertAssetRequisitionData(ByVal strReqid As String, ByVal ProductId As String, ByVal ProductReqQty As Integer,
                                           ByVal StockQty As Integer, ByVal PurchaseQty As Integer, ByVal VT_CODE As String,
        ByVal AST_SUBCAT_CODE As String, ByVal manufactuer_code As String, ByVal AST_MD_CODE As String)

        Dim strPurReqId As String = ""
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strReqid
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("INSERT_PURCHASE_ITEMREQ", param)
        If ds.Tables(0).Rows.Count > 0 Then
            strPurReqId = ds.Tables(0).Rows(0).Item("PUR_REQ_ID")
        End If
        Dim paramItemDetails(8) As SqlParameter
        paramItemDetails(0) = New SqlParameter("@ReqId", SqlDbType.NVarChar, 200)
        paramItemDetails(0).Value = strPurReqId
        paramItemDetails(1) = New SqlParameter("@ProductId", SqlDbType.NVarChar, 200)
        paramItemDetails(1).Value = ProductId
        paramItemDetails(2) = New SqlParameter("@SQty", SqlDbType.NVarChar, 200)
        paramItemDetails(2).Value = StockQty
        paramItemDetails(3) = New SqlParameter("@PQty", SqlDbType.NVarChar, 200)
        paramItemDetails(3).Value = PurchaseQty
        paramItemDetails(4) = New SqlParameter("@ProductREQQty", SqlDbType.Int)
        paramItemDetails(4).Value = ProductReqQty

        paramItemDetails(5) = New SqlParameter("@AID_ITEM_TYPE", SqlDbType.NVarChar, 200)
        paramItemDetails(5).Value = VT_CODE

        paramItemDetails(6) = New SqlParameter("@AID_ITEM_SUBCAT", SqlDbType.NVarChar, 200)
        paramItemDetails(6).Value = AST_SUBCAT_CODE

        paramItemDetails(7) = New SqlParameter("@AID_ITEM_BRD", SqlDbType.NVarChar, 200)
        paramItemDetails(7).Value = manufactuer_code

        paramItemDetails(8) = New SqlParameter("@AID_ITEM_MOD", SqlDbType.NVarChar, 200)
        paramItemDetails(8).Value = AST_MD_CODE

        ObjSubsonic.GetSubSonicExecute("INSERT_PURCHASE_REQ_DETAILS", paramItemDetails)



        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_UpdateStockPurchaseQtyByReqIdAndProductId")
        'sp.Command.AddParameter("@ReqId", strPurReqId, DbType.String)
        'sp.Command.AddParameter("@ProductId", ProductId, DbType.Int32)
        'sp.Command.AddParameter("@SQty", StockQty, DbType.Int32)
        'sp.Command.AddParameter("@PQty", PurchaseQty, DbType.Int32)
        'sp.ExecuteScalar()
    End Sub


    Private Sub UpdateData(ByVal ReqId As String, ByVal StatusId As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionStatus_UpdateByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    'Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As Integer, ByVal Qty As Integer)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_AddNew")
    '    sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
    '    sp.Command.AddParameter("@ProductId", ProductId, DbType.Int32)
    '    sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
    '    sp.ExecuteScalar()
    'End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim ReqId As String = GetRequestId()
        UpdateData(ReqId, 1011)
        Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId())
    End Sub
    'Private Sub CancelData(ByVal ReqId As String, ByVal Remarks As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisition_UpdateByReqId")
    '    sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
    '    sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
    '    sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
    '    sp.Command.AddParameter("@CatId", CInt(ddlAstCat.SelectedItem.Value), DbType.Int32)
    '    sp.Command.AddParameter("@StatusId", 1003, DbType.Int32)
    '    sp.ExecuteScalar()
    'End Sub
    'Private Sub DeleteRequistionItems(ByVal ReqId As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_DeleteByReqId")
    '    sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
    '    sp.ExecuteScalar()
    'End Sub
    Dim AssetCode As String
    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "UpdateRecord" Then
            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
            Dim ReqId As String = GetRequestId()
            Dim ProductId As Integer = 0
            Integer.TryParse(e.CommandArgument, ProductId)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            UpdateAssetRequisitionData(ReqId, ProductId, CInt(Trim(txtStockQty.Text)), CInt(Trim(txtPurchaseQty.Text)))

        End If
        If e.CommandName = "BindInterMovement" Then
            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
            AssetCode = e.CommandArgument
            ' BindLocationForInterMovement()
            BindGridInter(AssetCode, Request("RID"))
            BindGridIntra(AssetCode, Request("RID"))
            mpe.Show()
            'ClientScript.RegisterStartupScript(Me.GetType(), "Popup", "ShowPopup();", True)
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), Guid.NewGuid().ToString(), "$(document).ready(function(){$('#interNintraMovement').modal().fadeIn();});", True)
        End If

    End Sub

 

    'Protected Sub btnShowPopUP_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnShowPopUP.Click
    '    ScriptManager.RegisterStartupScript(Me, [GetType](), "Show Modal Popup", "showPopUpInterMvmtData();", True)

    'End Sub

    Private Sub UpdateAssetRequisitionData(ByVal ReqId As String, ByVal ProductId As String, ByVal StockQty As Integer, ByVal PurchaseQty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_UpdateStockPurchaseQtyByReqIdAndProductId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@SQty", StockQty, DbType.Int32)
        sp.Command.AddParameter("@PQty", PurchaseQty, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Public Function getaurlocation(ByVal AUR_ID As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AURID_LOCATION")
        sp.Command.AddParameter("@AUR_ID", AUR_ID, DbType.String)
        Dim ds As New DataSet
        ds = sp.ExecuteScalar
        If ds.Tables(0).Rows.Count > 0 Then

        End If
    End Function


    'Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
    '    'bindgrid(Request("RID"))
    '    BindRequisition()
    'End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmCoCheck.aspx")
    End Sub

    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
    End Sub

    Private Sub BindGridInter(ByVal AssetCode As String, ByVal ReqID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GETASSETS_INSTOCKINTER")
            sp.Command.AddParameter("@AssetCode", AssetCode, DbType.String)
            sp.Command.AddParameter("@REQ_ID", ReqID, DbType.String)
            'sp.Command.AddParameter("@SELECTED_LCM_CODE", ddlInterMovementLocation.SelectedItem.Value, DbType.String)
            gvItemsInter.DataSource = sp.GetDataSet()
            gvItemsInter.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(control As Control)
        ' Verifies that the control is rendered 

    End Sub



    'Private Sub BindLocationForInterMovement()
    '    Dim param(1) As SqlParameter
    '    param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
    '    param(0).Value = "1"
    '    param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
    '    param(1).Value = Session("UID")
    '    ObjSubsonic.Binddropdown(ddlInterMovementLocation, "GET_LOCTION_COORDINATOR_CHECK", "LCM_NAME", "LCM_CODE", param)
    '    ddlInterMovementLocation.Items.Remove("--Select--")
    '    ddlInterMovementLocation.SelectedIndex = If(ddlInterMovementLocation.Items.Count > 1, 0, 0)
    'End Sub

    'Protected Sub ddlInterMovementLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInterMovementLocation.SelectedIndexChanged
    '    BindGridInter(AssetCode, Request("RID"))
    '    'mpe.Show()
    'End Sub

    Private Sub BindGridIntra(ByVal astcode As String, ByVal req_id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AXIS_GETASSETS_INSTOCK")
            sp.Command.AddParameter("@AssetCode", astcode, DbType.String)
            sp.Command.AddParameter("@REQ_ID", req_id, DbType.String)
            gvItemsIntra.DataSource = sp.GetDataSet()
            gvItemsIntra.DataBind()
            If gvItemsIntra.Rows.Count = 0 Then
                pnlItemsInter.Visible = True
                btnSubmitIntra.Visible = False
                'BindGridInter()
            End If
            For Each drow As GridViewRow In gvItemsIntra.Rows
                Dim ddlAssets As DropDownList = DirectCast(drow.FindControl("ddlassets"), DropDownList)
                Dim lbllocation As Label = DirectCast(drow.FindControl("lbllocation"), Label)
                Dim ddlEmp As DropDownList = DirectCast(drow.FindControl("ddlEmp"), DropDownList)
                'Dim ddlTower As DropDownList = DirectCast(drow.FindControl("ddlTower"), DropDownList)

                'loadtower(lbllocation.Text, ddlTower)
                Emp_Loadddl(ddlEmp, lbllocation.Text)
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Emp_Loadddl(ByVal ddlEmp As DropDownList, ByVal locationid As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AEMP_LOC")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LOC_ID", locationid, DbType.String)
            ddlEmp.DataSource = sp.GetDataSet()
            ddlEmp.DataTextField = "AUR_FIRST_NAME"
            ddlEmp.DataValueField = "AUR_ID"
            ddlEmp.DataBind()
            ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmitInter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitInter.Click
        Try
            Dim count As Integer = 0
            Dim count1 As Integer = 0
            For Each row As GridViewRow In gvItemsInter.Rows
                Dim chkItem As CheckBox = DirectCast(row.FindControl("chkItem"), CheckBox)
                Dim ddlEmp As DropDownList = DirectCast(row.FindControl("ddlEmp"), DropDownList)
                If chkItem.Checked Then
                    count = count + 1
                End If
            Next
            Dim ReqId As String = ""
            If count = 0 Then
                lblPopUpMsg.Text = "Please select atleast any one of the assets."
                Exit Sub
            End If

            dtInterGrid.Columns.Add("MMR_AST_CODE")
            dtInterGrid.Columns.Add("MMR_FROMBDG_ID")
            dtInterGrid.Columns.Add("MMR_FROMFLR_ID")
            dtInterGrid.Columns.Add("MMR_RAISEDBY")
            dtInterGrid.Columns.Add("MMR_RECVD_BY")
            dtInterGrid.Columns.Add("MMR_MVMT_TYPE")
            dtInterGrid.Columns.Add("MMR_MVMT_PURPOSE")
            dtInterGrid.Columns.Add("MMR_COMMENTS")
            dtInterGrid.Columns.Add("MMR_FROMEMP_ID")
            dtInterGrid.Columns.Add("MMR_LRSTATUS_ID")
            dtInterGrid.Columns.Add("MMR_ITEM_REQUISITION")
            dtInterGrid.Columns.Add("AST_MD_CODE")

            For Each row As GridViewRow In gvItemsInter.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkItem"), CheckBox)
                Dim lblassetName As Label = DirectCast(row.FindControl("lblassetName"), Label)
                Dim lblLocation As Label = DirectCast(row.FindControl("lbllocation"), Label)
                Dim lblAstModelCode As Label = DirectCast(row.FindControl("lblAstModelCode"), Label)
                If chkSelect.Checked Then
                    Dim R As DataRow = dtInterGrid.NewRow
                    R("MMR_AST_CODE") = lblassetName.Text
                    R("MMR_FROMBDG_ID") = ""
                    R("MMR_FROMFLR_ID") = ""
                    R("MMR_RAISEDBY") = Session("UID")
                    R("MMR_RECVD_BY") = Session("UID")
                    R("MMR_MVMT_TYPE") = ""
                    R("MMR_MVMT_PURPOSE") = ""
                    R("MMR_COMMENTS") = ""
                    R("MMR_FROMEMP_ID") = 0
                    R("MMR_ITEM_REQUISITION") = Request("RID")
                    R("MMR_LRSTATUS_ID") = 1
                    R("AST_MD_CODE") = lblAstModelCode.Text
                    dtInterGrid.Rows.Add(R)
                End If
            Next
            Cache("CdtInterGrid") = dtInterGrid
            updateCoordinatorGridAfterPopupSubmit(Request("RID"))
            If count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Please select the Asset(s) to map with employee..."
                Exit Sub
            End If

        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub

    Protected Sub btnSubmitIntra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitIntra.Click
        Try
            Dim count As Integer = 0
            
            For Each row As GridViewRow In gvItemsIntra.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)

                If chkSelect.Checked Then
                    count = count + 1
                End If

            Next
            If count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Please select the Asset(s) to map with employee..."
                Exit Sub
            End If


            ' UPDATE AMT_ASSET_SPACE TABLE
            dtIntraGrid.Columns.Add("LOC_ID")
            dtIntraGrid.Columns.Add("TWR_ID")
            dtIntraGrid.Columns.Add("FLR_ID")
            dtIntraGrid.Columns.Add("AAT_AST_CODE")
            dtIntraGrid.Columns.Add("AST_MD_CODE")

            ' INSERT INTO AMG_ASSET_TAGGED WITH STA_ID 15
            ' dtIntraGrid.Columns.Add("AAT_AST_CODE")
            dtIntraGrid.Columns.Add("AAT_SPC_ID")
            dtIntraGrid.Columns.Add("AAT_EMP_ID")
            dtIntraGrid.Columns.Add("AAT_ITEM_REQUISITION")



            'dtIntraGrid.Columns.Add("AAT_AST_CODE")
            ' dtIntraGrid.Columns.Add("AAT_EMP_ID")
            dtIntraGrid.Columns.Add("AAT_REQ_ID")



            For Each row As GridViewRow In gvItemsIntra.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim ddlEmp As DropDownList = DirectCast(row.FindControl("ddlEmp"), DropDownList)
                Dim ddlTower As DropDownList = DirectCast(row.FindControl("ddlTower"), DropDownList)
                Dim ddlFloor As DropDownList = DirectCast(row.FindControl("ddlFloor"), DropDownList)
                Dim AssetCode As Label = DirectCast(row.FindControl("lblassetName"), Label)
                Dim lbllocation As Label = DirectCast(row.FindControl("lbllocation"), Label)

                Dim lblasset_md_code As Label = DirectCast(row.FindControl("lblasset_md_code"), Label)

                If chkSelect.Checked Then
                    If ddlEmp.SelectedIndex > 0 Then
                        Dim R As DataRow = dtIntraGrid.NewRow
                        R("LOC_ID") = lbllocation.Text
                        R("TWR_ID") = ""
                        R("FLR_ID") = ""
                        R("AAT_AST_CODE") = AssetCode.Text
                        R("AAT_SPC_ID") = ""
                        R("AAT_EMP_ID") = ddlEmp.SelectedItem.Value
                        R("AAT_ITEM_REQUISITION") = Request("RID")
                        R("AAT_REQ_ID") = Request("RID")

                        R("AST_MD_CODE") = lblasset_md_code.Text

                        dtIntraGrid.Rows.Add(R)

                        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_AMT_ASSET_SPACE_STAT")
                        'sp1.Command.AddParameter("@LOC_ID", lbllocation.Text, DbType.String)
                        'sp1.Command.AddParameter("@TWR_ID", "", DbType.String)
                        'sp1.Command.AddParameter("@FLR_ID", "", DbType.String)
                        'sp1.Command.AddParameter("@AAT_AST_CODE", AssetCode.Text, DbType.String)
                        'sp1.ExecuteScalar()


                        '' ALLOCATE_SPACEASSET
                        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ALLOCATE_SPACEASSET_INTRA")
                        'sp.Command.AddParameter("@AAT_AST_CODE", AssetCode.Text, DbType.String)
                        'sp.Command.AddParameter("@AAT_SPC_ID", "", DbType.String)
                        'sp.Command.AddParameter("@AAT_EMP_ID", Session("UID"), DbType.String)
                        'sp.Command.AddParameter("@AAT_ITEM_REQUISITION", Request.QueryString("req_id"), DbType.String)
                        'sp.ExecuteScalar()

                        ''--------------- updating the asset tagged table to 1035 (Assigned)-------------
                        'Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_ALLOCATE_ASSET")
                        'sp2.Command.AddParameter("@AAT_AST_CODE", AssetCode.Text, DbType.String)
                        'sp2.Command.AddParameter("@AAT_EMP_ID", ddlEmp.SelectedValue, DbType.String)
                        'sp2.Command.AddParameter("@AAT_REQ_ID", Request.QueryString("req_id"), DbType.String)
                        'sp2.ExecuteScalar()
                        ''   send_mail(Request.QueryString("req_id"), AssetCode.Text)
                        ''End inserting asset data
                        ''(Integer.Parse(lblProductId1.Text), ddlPO.SelectedItem.Value, Integer.Parse(txtQty.Text))
                        ''End If
                    Else
                        lblMsg.Text = "Please select Employee to map the asset."
                        Exit Sub
                    End If

                End If
            Next
            Cache("CdtIntraGrid") = dtIntraGrid
            updateCoordinatorGridAfterPopupSubmit(Request("RID"))
            'Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_UPDATE_MVMNTQUAN")
            'sp3.Command.AddParameter("@REQ_ID", Request.QueryString("req_id"), DbType.String)
            'sp3.Command.AddParameter("@AST_CODE", Request.QueryString("id"), DbType.String)
            'sp3.Command.AddParameter("@APPRVDQTY", count, DbType.Int32)
            'sp3.ExecuteScalar()
            'lblMsg.Visible = True
            'lblMsg.Text = "Asset mapped successfully..."
            'pnlItems.Visible = False
            'End after apporving inter movement assets in same location update the qunatity for the particular request
        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub

    Public Sub updateCoordinatorGridAfterPopupSubmit(ByVal ReqId As String)
        changedDt = CType(Cache("CoOrdinatorGrid"), DataTable)
        CachedInterGrid = CType(Cache("CdtInterGrid"), DataTable)

        CachedtIntraGrid = CType(Cache("CdtIntraGrid"), DataTable)

        If CachedInterGrid Is Nothing Then
        Else
            For I As Integer = 0 To changedDt.Rows.Count - 1
                If changedDt.Rows(I).Item("AID_ITM_CODE") = CachedInterGrid.Rows(0).Item("AST_MD_CODE") Then
                    changedDt.Rows(I).Item("INTERSTOCK") = CachedInterGrid.Rows.Count
                End If

            Next
        End If

        If CachedtIntraGrid Is Nothing Then
        Else
            For I As Integer = 0 To changedDt.Rows.Count - 1
                If changedDt.Rows(I).Item("AID_ITM_CODE") = CachedtIntraGrid.Rows(0).Item("AST_MD_CODE") Then
                    changedDt.Rows(I).Item("INTRASTOCK") = CachedtIntraGrid.Rows.Count
                End If

            Next
        End If


        gvItems.DataSource = changedDt
        gvItems.DataBind()

        For Each row As GridViewRow In gvItems.Rows
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            Dim lbInterMvnt As Label = DirectCast(row.FindControl("lbInterMvnt"), Label)
            Dim lbIntraMvnt As Label = DirectCast(row.FindControl("lbIntraMvnt"), Label)

            If txtPurchaseQty.Text = "" Then
                txtPurchaseQty.Text = 0
            End If

            txtPurchaseQty.Text = CInt(Trim(txtQty.Text)) - (CInt(Trim(lbInterMvnt.Text)) + CInt(Trim(lbIntraMvnt.Text)))

        Next


    End Sub
End Class
