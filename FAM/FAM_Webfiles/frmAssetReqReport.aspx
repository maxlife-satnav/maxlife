<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetREQreport.aspx.vb" MaintainScrollPositionOnPostback="true"
    Inherits="frmAssetREQreport" %>

<%@ Register Src="~/Controls/AssetReqReport.ascx" TagName="ucAssetREQ" TagPrefix="uc12" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">   
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Asset Requisition Report 
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" />
                            <uc12:ucAssetREQ ID="ucreq" runat="server" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>  

    <%-- Modal popup block --%>

    <div class="modal fade" id="myModal1" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Asset Requisition Details</h4>
                </div>
                <div id="AssetReqreport">
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {
            $("#AssetReqreport").load("frmAssetReqReportDtls.aspx?reqid=" + id, function (responseTxt, statusTxt, xhr) {
                $("#myModal1").modal().fadeIn();
            });
        }
    </script>

    <%-- Modal popup block--%>
</body>
</html>
