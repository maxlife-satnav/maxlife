﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmItemViewSMReport.aspx.vb" Inherits="FAM_FAM_Webfiles_frmItemViewSMReport" %>

<%@ Register Src="../../Controls/ucItemViewSMReport.ascx" TagName="ucItemViewSMReport" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Consumable Stock Item Requisition Report
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" />
                            <uc1:ucItemViewSMReport ID="ucItemViewSMReport1" runat="server" />

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <%-- Modal popup block --%>

       <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Consumable Stock Request Summary</h4>
                </div>
                <div id="modelcontainer">
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>


    
 

    
    <script>
        function showPopWin(reqid) {
            //$("#modelcontainer").load("frmaddroleviewusers.aspx?ROL_ID=" + id, function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal().fadeIn();
            //});
            
            $("#modalcontentframe").attr("src", "frmConsumableStockRequestSummary.aspx?Prd_id=" + reqid);
            $("#myModal").modal().fadeIn();

            return false;
        }
    </script>



</body>
</html>
<script src="../../Scripts/moment.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ddlRange").val('');
            rangeChanged();
        });

        function rangeChanged() {
            var selVal = $("#ddlRange").val();
            switch (selVal) {
                case 'TODAY':
                    $("#ucItemViewSMReport1_FromDate").val(moment().format('DD-MMM-YYYY'));
                    $("#ucItemViewSMReport1_txtToDate").val(moment().format('DD-MMM-YYYY'));
                    break;
                case 'YESTERDAY':
                    $("#ucItemViewSMReport1_FromDate").val(moment().subtract(1, 'days').format('DD-MMM-YYYY'));
                    $("#ucItemViewSMReport1_txtToDate").val(moment().subtract(1, 'days').format('DD-MMM-YYYY'));
                    break;
                case '7':
                    $("#ucItemViewSMReport1_FromDate").val(moment().subtract(6, 'days').format('DD-MMM-YYYY'));
                    $("#ucItemViewSMReport1_txtToDate").val(moment().format('DD-MMM-YYYY'));
                    break;
                case '30':
                    $("#ucItemViewSMReport1_FromDate").val(moment().subtract(29, 'days').format('DD-MMM-YYYY'));
                    $("#ucItemViewSMReport1_txtToDate").val(moment().format('DD-MMM-YYYY'));
                    break;
                case 'THISMONTH':
                    $("#ucItemViewSMReport1_FromDate").val(moment().startOf('month').format('DD-MMM-YYYY'));
                    $("#ucItemViewSMReport1_txtToDate").val(moment().endOf('month').format('DD-MMM-YYYY'));
                    break;
                case 'LASTMONTH':
                    $("#ucItemViewSMReport1_FromDate").val(moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY'));
                    $("#ucItemViewSMReport1_txtToDate").val(moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY'));
                    break;
                case '':
                    $("#ucItemViewSMReport1_FromDate").val('');
                    $("#ucItemViewSMReport1_txtToDate").val('');
                    break;
            }
        }

        $("#ddlRange").change(function () {
            rangeChanged();
        });
    </script>
