<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InterMovementRequisitionDTLS.aspx.vb"
    Inherits="FAM_FAM_Webfiles_InterMovementRequisitionDTLS" Title="Inter Movement Requisition Details" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Inter Movement details
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" ForeColor="red" CssClass="col-md-12 control-label"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">From Location<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlSLoc" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlAsset" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Person To Receive Assets<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="multiline" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Modify" CssClass="btn btn-primary custom-button-color"></asp:Button>
                                        <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
