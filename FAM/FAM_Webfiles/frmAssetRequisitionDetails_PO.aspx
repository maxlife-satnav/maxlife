<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetRequisitionDetails_PO.aspx.vb"
    Inherits="FAM_FAM_Webfiles_frmAssetRequisitionDetails_PO" Title="PO Generation" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>PO Generation
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Vendor<span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlVendor"
                                                Display="none" ErrorMessage="Please Select Vendor !" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pnlItems" runat="server">
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 300px">
                                        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false"
                                            EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:BoundField DataField="AST_MD_CODE" HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="AST_MD_NAME" HeaderText="Asset Name" ItemStyle-HorizontalAlign="left" />
                                                <asp:TemplateField HeaderText="Requested Qty" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" MaxLength="10" Text='<%#Eval("AID_QTY") %>'
                                                            ReadOnly="true"></asp:TextBox>
                                                        <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_CODE") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblReqId" runat="server" Text='<%#Eval("AID_REQ_ID") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Stock" HeaderText="Available Stock" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" DataFormatString="{0:c2}"
                                                    ItemStyle-HorizontalAlign="left" />
                                                <asp:TemplateField HeaderText="Unit Price" ItemStyle-HorizontalAlign="left" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="form-control" MaxLength="10" ReadOnly="true" Text='<%#formatnumber(Eval("UnitPrice"),2) %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty From Stock" ItemStyle-HorizontalAlign="left" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtStockQty" runat="server" CssClass="form-control" MaxLength="10" Text='<%#Eval("AID_MVM_QTY") %>'
                                                            ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty To Purchase" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPurchaseQty" runat="server" CssClass="form-control" MaxLength="10" Text='<%#Eval("AID_ORD_QTY") %>'
                                                            ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Price" ItemStyle-HorizontalAlign="left" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control" MaxLength="10" Text='<%#formatnumber(Eval("Price"),2) %>'
                                                            ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="Rs. {0}" ItemStyle-HorizontalAlign="left"
                                                    Visible="false" />
                                                <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                                            ToolTip="Click to check all" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-md-3">
                                            </div>
                                            <label class="col-md-3 control-label"><strong>TAX DETAILS</strong></label>
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-3">
                                                <strong>PAYMENT TERMS</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Entry Tax</label>
                                            <asp:RequiredFieldValidator ID="rfvtax" runat="server" Display="None" ErrorMessage="Enter Tax"
                                                ValidationGroup="Val1" ControlToValidate="txtFFIP"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtFFIP" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Advance</label>
                                            <asp:RequiredFieldValidator ID="rfvadvance" runat="server" Display="None" ErrorMessage="Advance"
                                                ValidationGroup="Val1" ControlToValidate="txtAdvance"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtAdvance" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">CST/ST</label>
                                            <asp:RequiredFieldValidator ID="rfvcst" runat="server" Display="None" ErrorMessage="CST/ST"
                                                ValidationGroup="Val1" ControlToValidate="txtCst"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtCst" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">On Delivery</label>
                                            <asp:RequiredFieldValidator ID="rfvdelivery" runat="server" Display="None" ErrorMessage="On Delivery"
                                                ValidationGroup="Val1" ControlToValidate="txtDelivery"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtDelivery" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">VAT</label>
                                            <asp:RequiredFieldValidator ID="rfvvat" runat="server" Display="None" ErrorMessage="VAT"
                                                ValidationGroup="Val1" ControlToValidate="txtWst"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtWst" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Installation</label>
                                            <asp:RequiredFieldValidator ID="rfvinstallation" runat="server" Display="None"
                                                ErrorMessage="Installation" ValidationGroup="Val1" ControlToValidate="txtInstallation"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtInstallation" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Octroi</label>
                                            <asp:RequiredFieldValidator ID="rfvoctroi" runat="server" Display="None" ErrorMessage="Octrai"
                                                ValidationGroup="Val1" ControlToValidate="txtOctrai"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtOctrai" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Commissioning</label>
                                            <asp:RequiredFieldValidator ID="rfvcomm" runat="server" Display="None" ErrorMessage="Commissioning"
                                                ValidationGroup="Val1" ControlToValidate="txtCommissioning"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtCommissioning" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Service Tax</label>
                                            <asp:RequiredFieldValidator ID="rfvservice" runat="server" Display="None" ErrorMessage="Service Tax"
                                                ValidationGroup="Val1" ControlToValidate="txtServiceTax"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtServiceTax" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Retention</label>
                                            <asp:RequiredFieldValidator ID="rfvretention" runat="server" Display="None" ErrorMessage="Retention"
                                                ValidationGroup="Val1" ControlToValidate="txtRetention"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRetention" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Others</label>
                                            <asp:RequiredFieldValidator ID="rfvothers" runat="server" Display="None" ErrorMessage="Others"
                                                ValidationGroup="Val1" ControlToValidate="txtOthers"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtOthers" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Payments</label>
                                            <asp:RequiredFieldValidator ID="rfvpayments" runat="server" Display="None" ErrorMessage="Payments"
                                                ValidationGroup="Val1" ControlToValidate="txtPayments"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtPayments" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnTotalCost" runat="server" Text="Get Total Cost" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <label class="col-md-5 control-label">Total Cost<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvcose" runat="server" Display="None" ErrorMessage="Please Enter Total Cost"
                                                ValidationGroup="Val2" ControlToValidate="txtTotalCost"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtTotalCost" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Expected Date Of Delivery<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvdate" runat="server" Display="none" ErrorMessage="Please Select Expected Date Of Delivery"
                                                ValidationGroup="Val2" ControlToValidate="txtDOD"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="txtDOD" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Exchange Charges<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvcharges" runat="server" Display="None" ErrorMessage="Please Enter Exchange Charges"
                                                ControlToValidate="txtExchange" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtExchange" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Remarks<span style="color: red;"></span></label>
                                            <%--<asp:RequiredFieldValidator ID="rfvremarks" runat="server" Display="None" ErrorMessage="Please Enter Remarks"
                                                ControlToValidate="txtRemarks" ValidationGroup="Val2"></asp:RequiredFieldValidator>--%>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="multiline"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" Text="Generate PO" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2" />
                                        <asp:Button ID="btnCancel" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" Visible="false" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>





