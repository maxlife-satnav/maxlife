Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_frmpaymentmemo
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindWorkOrder()
            End If
        End If
    End Sub

    Private Sub BindWorkOrder()
        ObjSubsonic.Binddropdown(ddlAsset, "GETASSETPO", "REQ_VALUE", "REQ_CODE")
    End Sub

    Protected Sub ddlAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAsset.SelectedIndexChanged
        getDetails()
        'GETASSETPO_REQ_ID
    End Sub

    Private Sub getDetails()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlAsset.SelectedItem.Value
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GETASSETPO_REQ_ID", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblPayment.Text = ds.Tables(0).Rows(0).Item("AIPD_TOTALCOST")
        End If
        ds = New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_TOTAL_AMT_PAID_PO", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblPaymentPaid.Text = ds.Tables(0).Rows(0).Item("PAYMENT")
            Fieldset1.Visible = True


        End If

        ObjSubsonic.BindGridView(gvList, "GET_TOTAL_AMT_PAID_PO_DETAILS", param)

        lblBalPay.Text = CInt(lblPayment.Text) - CInt(lblPaymentPaid.Text)


        If CInt(lblPayment.Text) = CInt(lblPaymentPaid.Text) Then
            txtPayAmt.Text = ""
            trpaid1.Visible = False
            trpaid2.Visible = False

        Else
            txtPayAmt.Text = ""
            trpaid1.Visible = True
            trpaid2.Visible = True
        End If




    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If txtPayAmt.Text = "" Or txtPayAmt.Text Is Nothing Then
            lblmsg.Text = "Please enter payment."
            Exit Sub
        End If

        If ddlAsset.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Please select PO."
            Exit Sub
        End If
        If dllPayMode.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Please select payment mode."
            Exit Sub
        End If

        If CInt(txtPayAmt.Text) > CInt(lblPayment.Text) Then
            lblmsg.Text = "Payment is more than PO."
            Exit Sub
        Else
            lblmsg.Text = ""
        End If



        Dim param(10) As SqlParameter
        param(0) = New SqlParameter("@AMG_ITEM_PO_REQ", SqlDbType.NVarChar, 200)
        param(0).Value = ddlAsset.SelectedItem.Value
        param(1) = New SqlParameter("@PAYMENT_MODE", SqlDbType.NVarChar, 200)
        param(1).Value = dllPayMode.SelectedItem.Value
        param(2) = New SqlParameter("@CHEQUE_DD_NUMBER", SqlDbType.NVarChar, 200)
        param(2).Value = txtDDChqNumber.Text
        param(3) = New SqlParameter("@PAYMENT", SqlDbType.NVarChar, 200)
        param(3).Value = txtPayAmt.Text
        param(4) = New SqlParameter("@PAID_DT", SqlDbType.NVarChar, 200)
        param(4).Value = getoffsetdatetime(DateTime.Now)
        param(5) = New SqlParameter("@CHQDT", SqlDbType.NVarChar, 200)
        param(5).Value = txtchqDt.Text
        param(6) = New SqlParameter("@CHQNUMBER", SqlDbType.NVarChar, 200)
        param(6).Value = txtDDChqNumber.Text
        param(7) = New SqlParameter("@BANKNAME", SqlDbType.NVarChar, 200)
        param(7).Value = txtBankName.Text
        param(8) = New SqlParameter("@BRANCHNAME", SqlDbType.NVarChar, 200)
        param(8).Value = txtBranchName.Text
        param(9) = New SqlParameter("@IFCSCODE", SqlDbType.NVarChar, 200)
        param(9).Value = txtDDChqNumber.Text
        param(10) = New SqlParameter("@AC_NUMBER", SqlDbType.NVarChar, 200)
        param(10).Value = txtAccountNumber.Text
        ObjSubsonic.GetSubSonicExecute("AMG_ITEM_PO_MEMO_INSERT", param)
        getDetails()

        txtchqDt.Text = ""
        txtBankName.Text = ""
        txtDDChqNumber.Text = ""
        txtAccountNumber.Text = ""

        trDDChkDetails.Visible = False

    End Sub

    Protected Sub dllPayMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dllPayMode.SelectedIndexChanged
        If dllPayMode.SelectedItem.Value = "Cheque" Then
            trDDChkDetails.Visible = True
            lbltitle.Text = "Cheque Details"
            lblDt.Text = "Cheque Date"
            lblNumber.Text = "Cheque Number"
            TRACNUMBER.Visible = False
        ElseIf dllPayMode.SelectedItem.Value = "DD" Then
            trDDChkDetails.Visible = True
            lbltitle.Text = "DD Details"
            lblDt.Text = "DD Date"
            lblNumber.Text = "DD Number"
            TRACNUMBER.Visible = False
        ElseIf dllPayMode.SelectedItem.Value = "NEFT_RTGS" Then
            trDDChkDetails.Visible = True
            lbltitle.Text = "NEFT / RTGS Details"
            lblDt.Text = "NEFT/RTGS Date"
            lblNumber.Text = " IFCS CODE "
            'txtDDChqNumber.Visible = False
            'lblNumber.Visible = False
            TRACNUMBER.Visible = True
        Else
            trDDChkDetails.Visible = False
        End If
    End Sub

    Protected Sub gvList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvList.RowDataBound
        Dim objListItem As DataControlRowState
        objListItem = e.Row.RowState
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblPAYMENT_MODE As Label
            lblPAYMENT_MODE = CType(e.Row.FindControl("lblPAYMENT_MODE"), Label)
            Dim lnkPAYMENT_MODE As LinkButton
            lnkPAYMENT_MODE = CType(e.Row.FindControl("lnkPAYMENT_MODE"), LinkButton)

            Dim lblID As Label = CType(e.Row.FindControl("lblID"), Label)


            If lblPAYMENT_MODE.Text <> "Cash" Then

                lnkPAYMENT_MODE.Attributes.Add("onclick", "showPopWin('frmpaymentmemodtls.aspx?id=" & lblID.Text & "',600,180,null)")
                lnkPAYMENT_MODE.Attributes.Add("href", "#")
                lblPAYMENT_MODE.Visible = False
                lnkPAYMENT_MODE.Visible = True
            Else
                lblPAYMENT_MODE.Visible = True
                lnkPAYMENT_MODE.Visible = False
            End If
        End If



    End Sub
End Class
