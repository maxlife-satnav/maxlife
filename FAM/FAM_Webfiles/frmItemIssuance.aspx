﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="frmItemIssuance.aspx.cs" Inherits="FAM_FAM_Webfiles_frmItemIssuance" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function CheckDataGrid() {
            document.getElementById("lblMsg").innerText = '';
            var k = 0;
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        k = k + 1;
                    }
                }
            }
            if (k == 0) {
                document.getElementById("lblMsg").innerText = 'Please check at least one space to Release';
                return false;
            }
            else {

                var con = confirm("Are you sure you want to release the space?");
                if (con == true) {
                    return true;
                }
                else {
                    return false;
                }

            }
        }
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Consumable Item Issuance
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ScriptManager ID="ScriptManager1" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" ValidationGroup="Val1" />

                            <%--<uc1:ItemIssuance runat="server" ID="ItemIssuance" />--%>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                <asp:Label ID="Label2" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Employee Id</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlEmp" runat="server" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Issuence Location<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstIssuence"
                                                Display="none" ErrorMessage="Please Select Asset Issuence Location" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlAstIssuence" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Category<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                                                Display="None" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                                                InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Sub Category<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstSubCat"
                                                Display="none" ErrorMessage="Please Select Asset Sub Category !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                                                Display="none" ErrorMessage="Please Select Asset Brand/Make !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Invoice If Needed</label>
                                            <div class="col-md-7">
                                                <asp:CheckBox ID="rdbpo" runat="server" Text="Invoice" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:LinkButton ID="Button1" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                                            OnClientClick="P" />
                                    </div>
                                </div>
                            </div>

                            <%-- <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                                                Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>


                            <div id="pnlItems" runat="server" class="row">
                                <div class="col-md-12">
                                    <h4>Item List</h4>
                                    <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="true"
                                        EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <%--<Columns>
                                            <asp:BoundField DataField="AST_MD_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left">
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="AST_SUBCAT_NAME" runat="server" Text='<%#Eval("AST_SUBCAT_NAME") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Price" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblITEMCODE" runat="server" Text='<%#Eval("AST_MD_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="AST_MD_CODE" runat="server" Text='<%#Eval("AST_MD_CODE") %>' Visible="false"> </asp:Label>
                                                    <asp:TextBox ID="txtPrice" Text='<%#Eval("AMG_VENCAN_RT") %>' runat="server" Width="50px" MaxLength="10" Enabled="false"></asp:TextBox>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Available" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotavblQty" runat="server" Text='<%#Eval("AAC_CON_TOTAVBL") %>'> </asp:Label>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Qty Requested" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please enter quantity less than available."
                                                        ValueToCompare='<%# Eval("AAC_CON_TOTAVBL") %>' ControlToValidate="txtQty" Display="Dynamic" Type="Integer"
                                                        Operator="LessThan" ValidationGroup="Val1"></asp:CompareValidator>

                                                    <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" OnTextChanged="txtQty_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="txtQty_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtQty">
                                                    </cc1:FilteredTextBoxExtender>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Discount(%)" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtDiscPer" runat="server" Width="50px" MaxLength="2" AutoPostBack="true" OnTextChanged="txtDiscPer_TextChanged"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="txtDiscPer_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtDiscPer">
                                                    </cc1:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Price">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotPrice" runat="server" Text="0"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>--%>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRem"
                                                Display="None" ErrorMessage="Please Enter the Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRem" runat="server" CssClass="form-control"
                                                    Rows="3" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                                            OnClientClick="return CheckDataGrid()" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Employee Details</h4>
                </div>
                <div id="modelcontainer">
                    
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin() {
            //$("#modelcontainer").load("frmaddroleviewusers.aspx?ROL_ID=" + id, function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal().fadeIn();
            //});
            //$("#modalcontentframe").attr("src", "frmaddroleviewusers.aspx?ROL_ID=" + id);
            $("#myModal").modal().fadeIn();
            loaddata();
            return false;
        }

        function loaddata() {
            $(document).ready(function () {
                $.ajax({
                    type: "POST",
                    url: "/SampleRestService",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{}",
                    success: function (res) {
                        $('#Results').append(CreateDetailView(res, "CoolTableTheme", true)).fadeIn();
                    }
                });
            });


        }
    </script>

</body>
</html>




