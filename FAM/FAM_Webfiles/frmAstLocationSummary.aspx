<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAstLocationSummary.aspx.vb"
    Inherits="frmAstLocationSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Controls/ucAstLocationSummary.ascx" TagName="ucAssetSummary"
    TagPrefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Asset Location</title>
   

 
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%" cellpadding="2">
                <tr>
                    <td align="left" valign="top">
                        <fieldset>
                            <legend>Mapped Assets </legend>
                            <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                EmptyDataText="No Asset(s) Found." Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Asset Count">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAstCount" runat="server" Text='<%#Eval("CNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AssetName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("PRODUCTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <fieldset>
                            <legend>Unmapped Assets</legend>
                            <asp:GridView ID="gvUnmappedAst" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                EmptyDataText="No Asset(s) Found." Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="AssetName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("PRODUCTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AssetCode">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcode" runat="server" Text='<%#Eval("AAS_AAT_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <%--<table id="table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="fornormaltext" Width="95%" Font-Underline="False"
                            ForeColor="Black">Asset Location  
             <hr align="center" width="60%" /></asp:Label></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%" class="fornormaltext">
                <tr class="bcolor">
                    <td align="left">
                        <span><strong>&nbsp;Asset Location </strong></span>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 100%" border="1">
                            <tr>
                                <td colspan="4" align="center">
                                    <table>
                                        <tr>
                                            <td align="center">
                                                <span><a href="#" title="Name" onclick="showPopWin('frmAstLocationSummary.aspx',850,338,'')"> </span>
                                                <table id="AssetLocationForInnerDetails1_list">
                                                </table>
                                                <div id="AssetLocationForInnerDetails1_pager" class="scroll" style="text-align: center;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table id="AssetLocationForInnerDetails2_list">
                                                </table>
                                                <div id="AssetLocationForInnerDetails2_pager" class="scroll" style="text-align: center;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>--%>
            <%-- <uc4:ucAssetSummary ID="ucAssetSummary" runat="server" />--%>
        </div>
        <!--#INCLUDE File=../../includes/modal_divs.aspx-->
    </form>
</body>
</html>
