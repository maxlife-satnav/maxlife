<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmChangePassword.aspx.vb" Inherits="frmChangePassword" Title="Change Password" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Change Password</legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">User Id</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtUsrId" runat="server" ReadOnly="True" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Employee Name</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtEmpName" runat="server" ReadOnly="True" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Email Id</label>
                                            <asp:RegularExpressionValidator ID="revemail" runat="server" ControlToValidate="txtmailid"
                                                Display="None" ErrorMessage="Enter Valid Email Id" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                            </asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtmailid" runat="server" ReadOnly="True" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Enter Old Password<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvOldPwd" runat="server" ControlToValidate="txtOldPwd"
                                                Display="None" ErrorMessage="Please Enter Old Password" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtOldPwd" runat="server" CssClass="form-control" MaxLength="20"
                                                    TextMode="Password">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Enter New Password<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvNewPwd" runat="server" ControlToValidate="txtpwd"
                                                Display="None" ErrorMessage="Please Enter New Password" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtpwd" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password">                                      
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Confirm New Password<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvCpwd" runat="server" ControlToValidate="txtcpwd"
                                                Display="None" ErrorMessage="Please Enter Confirm New Password" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvPwd" runat="server" ControlToCompare="txtpwd" ControlToValidate="txtcpwd"
                                                Display="None" ErrorMessage="Passwords do not match" SetFocusOnError="True">
                                            </asp:CompareValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtcpwd" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password">                                      
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" OnClick="btnSubmit_Click" />
                                        <asp:Label ID="lblPwd" runat="server" CssClass="clsnote" Visible="False"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <h5>
                                                <label class="col-md-12 control-label" style="text-align: center;">
                                                    <span style="color: red;">Note:</span> Password Should Contain Minimum of 8 characters, 2 Numerics, 1 Special
                        Character
                                                </label>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
