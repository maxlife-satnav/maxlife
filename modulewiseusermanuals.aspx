﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
</head>
<body>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>User Manuals</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">                                    
                                    <a href="#" onClick="SpacePopWin()">Space Management</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">                                    
                                    <a href="#" onClick="PropertyPopWin()">Property Management</a>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <a href="#" onclick="AssetPopWin()">Asset Management</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">                                    
                                    <a href="#" onclick="MaintenancePopWin()">Maintenance Management</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">                                    
                               <a href="#" onclick="HelpdiskPopWin()">Help Desk Management</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">                                    
                                <a href="#" onclick="ConferencePopWin()">Conference Management</a>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

     <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> User Manual </h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>

        function SpacePopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Space_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function PropertyPopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Property_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function AssetPopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Asset_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function MaintenancePopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Maintenance_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function HelpdiskPopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Helpdesk_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function ConferencePopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Conference_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }
    </script>

</body>
</html>
